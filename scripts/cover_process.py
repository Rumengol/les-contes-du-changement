from PIL import Image
from pathlib import Path
import sys

RELPATH_TO_ASSETS = Path(Path(__file__).parent.parent, "assets/images/")
def convert_to_webp(source:Path, name:str):
    """Convert image to WebP.

    Args:
        source (pathlib.Path): Path to source image

    Returns:
        pathlib.Path: path to new image
    """
    if name is None:
        name = source.name
    destination = RELPATH_TO_ASSETS.joinpath(name).with_suffix(".webp")

    image = Image.open(source)  # Open image
    image.save(destination, format="webp")  # Convert image to webp

    image.thumbnail((400,400), Image.LANCZOS)
    image.save(destination.parent.joinpath("miniatures",destination.name), format="webp")

    return destination


if __name__ == "__main__":
    source = Path(sys.argv[1]).resolve()
    name = sys.argv[2]
    print(source, name)

    webp = convert_to_webp(source, name)

    print("Successfully generated cover", webp)