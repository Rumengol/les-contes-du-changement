import os
import re

def getWordCount(text):
    return len(re.findall("\\w+",text))

def getAllContes():
    #return os.listdir("./_contes/")
    return [file for file in os.listdir("./_contes/")]


def removePoemWC(wc):
    return [count for count in wc if count > 300 ]

TEMPLATE = """Total : {0}
WC_total : {1}
WC_mean : {2}
WC_med : {7}
No_poem : {3}
WC_min : {4}
WC_max : {5}
Novel_equiv : {6}
Read Time : {8}"""

if __name__ == "__main__":
    posts = getAllContes()
    WC = [getWordCount("".join(open("./_contes/" + conte ,"r",encoding="utf-8").readlines()).split("---")[2].replace("\n","")) for conte in posts]


    sum_WC = sum(WC)
    mean_WC = round(sum_WC/len(WC))
    mean_WC_no_poem = round(sum_WC/len(removePoemWC(WC)))
    med = (sorted(WC)[int(len(WC)/2)])
    min_WC = min(removePoemWC(WC))
    max_WC = max(WC)
    read_time = sum_WC/249

    total = len(posts)

    novel_equiv = round(sum_WC/40000,2)

    print(TEMPLATE.format(total,sum_WC,mean_WC,mean_WC_no_poem, min_WC,max_WC, novel_equiv, med, read_time))
    # with open("stats.yml","w") as f:
    #     f.write(TEMPLATE.format(total,sum_WC,mean_WC,min_WC,max_WC))
