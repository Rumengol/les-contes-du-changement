%%~name: Deuil
%%~path: 282b92cffabbf/f8f6b76ab1e2c
%%~kind: NOVEL/DOCUMENT
### Deuil

Quelque chose s’était brisé et répandu sur le sol. Les cris d’une dispute qui lui avaient échappés dans sa concentration le frappaient soudainement par leur violence. Emilie s’était réveillée et était maintenant recroquevillée sur le canapé, à regarder les deux frères se hurler mutuellement dessus. Instinctivement, Axellan alla vers elle et la pris dans ses bras pour la rassurer. Elle paru se détendre grâce à sa présence, alors il la lâcha et s’intéressa au sujet de la dispute.

“C’est pas possible, c’est pas possible, c’est pas possible… Elle peut pas être morte, pas elle ! Pas maintenant !” Nolan semblait perdu dans un tourbillon de mauvaises pensées. De toute évidence, son frère lui avait parlé de la mort de Lydia, ce qu’Axellan voulait à tout prix éviter e faire lui-même. Mais à en voir le résultat, il n’avait pas fait preuve de suffisamment de tact.

“Ho, Hé, reprend toi ! Ca sert à rien de pleurer comme une fillette, il faut aller leur faire payer !” Ethan ne faisait rien pour consoler son grand frère ou apaiser la situation. Au contraire, sa dernière remarque le fit exploser.

“Faire payer qui ? Dis moi que t’as buté cette ordure de Mike au moins. Pourquoi tu l’as pas protégé ? Qu’est-ce que tu foutais quand elle est morte ?”

“C’est allé trop vite, elle l’a planté, et il s’est retourné en un éclair. Et c’était déjà trop tard. Je l’ai finis juste après”

“Putain, mais comment t’as pu être nul à ce point ? Si c’était à ce prix, je voulais pas être sauvé.”

“Et comment j’aurais pu savoir ce qui allait se passer, hein ? Le plan était bon, on a rien fait de mal. Et c’est toi qui a voulu rester avec la bande de malades mentaux de Mike, je te rappelle. Moi et Lydia on était contre, mais elle s’est rangé de ton avis, comme toujours. Et on voit où ça l’a mené !”

“T’insinues quoi par là ?”

“Que c’est tes décisions de merde qui l’ont tué. Tu l’as tué, mais comme t’étais trop stone pour assister à sa mort, tu rejettes la faute sur moi, comme toujours, comme la fois où…”

Le plus jeune des deux n’eut pas le temps de terminer sa phrase avant que le coup de point ne le cueillit au coin de la mâchoire. La violence couplée à la surprise l’envoya en arrière sur la table, renversant l’une des deux chaises. Crachant un mollard sans aucun respect pour le meuble, il se retourna et se jeta sur Nolan. Ils se battaient pour de vrai, envoyant de véritables coups à l’autre.

Axellan s’avança pour les séparer, mais pu à peine saisir leurs épaules que d’un commun accords les deux frères le repoussèrent et il termina sa route assis dans le canapé, sans comprendre ce qu’il venait de se passer. Il tenta de recommencer son opération, mais leurs regards conjoints l’en dissuada. Alors il resta assis là, sans savoir quoi faire, à regarder deux adolescents se battre.

Heureusement, aucun des deux n’avaient encore beaucoup de forces, et le combat tourna court. Ils se séparèrent d’eux-mêmes, s’allongeant sur le dos, à même le parquet, le silence entrecoupé de leurs seules respirations haletantes. Puis Nolan éclata en sanglots, incapables de les retenir. Ethan fit la moue, et se releva, sans un regard pour son aîné.

Il se dirigea droit vers Axellan, ou plutôt vers l’épée qui reposait à côté de lui. Par réflexe, l’étudiant s’en saisit.

“Donne la moi.”

“Je te l’ai juste prêté tout à l’heure. Tu veux faire quoi ?” Il avait la sensation que s’il accédait à sa demande, les choses allaient très mal tourner.

“J’y retourne. Je vais les massacrer jusqu’aux derniers pour ce qu’ils ont fait.”

“Fais pas ça.” Nolan parvint-il à articuler entre deux pleurs. “Ne vas pas te suicider pour ça.”

“Alors quoi, tu veux laisser faire, chialer par terre et allumer un cierge ? Pas mon genre, frérot. Désolé.”

“Je te laisserais pas faire.” Nolan se relevait en garde, prêt à se battre à nouveau s’il le fallait pour faire entendre raison à son frère.

“Moi non plus.” Intervint Axellan. “Je sais que tu as envie de te venger, mais ça ne servirait à rien, ils sont beaucoup trop nombreux pour toi tout seul.”

“Qu’est-ce que t’en sais, toi ? Tu t’es même pas battu. Si t’avais fait quelque chose là bas, peut-être que Lydia serait encore en vie, hein ?”

La gifle parti toute seule. Peut-être était-elle due à la fatigue, à l’agacement du jeune homme vis-à-vis de la discussion ou de sa propre frustration quant à son inutilité. Mais elle surprit tout le monde, et Axellan le premier. Ethan le regarda, stupéfait, tenant sa joue qui rougissait à vue d’oeil dans la main. Ses yeux rageurs se fixèrent sur lui, trouvant une nouvelle proie sur qui passer sa colère. Mais encouragé par cet élan impromptu de bravoure, son adversaire ne se laissa pas impressionner et soutint son regard. Après quelques angoissantes secondes, l’adolescent abandonna.

“Alors c’est ça, on fait rien ? On s’en va sans regarder derrière nous, on dit adieu à Lydia ?”

“On a toujours fait comme ça. Et tu sais que ça me fait plus mal que toi de le dire. Mais t’as merdé, ça arrive. Tant qu’on survit, on doit continuer. Pour ceux qui ne peuvent plus.”

Les mots de Nolan parvenaient difficilement à traverser ses lèvres balbutiantes, malgré le visage neutre qu’il s’efforçait de maintenir. Les joues ravagées par les larmes, il ne détachait pas son regard de son frère. Ce fut ce dernier qui finit par baisser les yeux.

Emilie s’approcha de lui timidement, le visage à moitié caché dans son t-shirt. Elle tira sur le pantalon du garçon pour attirer son attention.

“Dis, qu’est-ce qu’il te fait pleurer ?”

La question tomba comme un cheveux sur la soupe, interrompant brutalement l’émotion. Un ange passa, puis ils éclatèrent tous d’un fou rire nerveux, incontrôlé et sans joie, mais si libérateur. Emilie, plantée au milieu du salon, resta pantoise avant de comprendre qu’on se moquait d’elle et parti bouder dans l’une des chambres. Axellan tenta de la retenir, mais elle l’ignora.

La tension était soudainement retombée, et avec elle l’animation qui avait parcouru l’appartement. Les trois garçons se regardèrent, puis Ethan se décida à prendre la parole.

“Mais plus sérieusement, qu’est-ce qu’on fait maintenant ?”

“Ivan est blessé.” Intervint Axellan. “Plutôt gravement. Je sais même pas s’il va s’en sortir, mais même si c’est le cas, il va avoir besoin de repos et de ne pas bouger.”

Nolan acquiesça faiblement.

“De toute façon, ça va être plutôt agité dans la rue pendant quelques temps. Mieux vaut garder profil bas le temps que les choses se tassent. Heureusement, on est pas très loin du casino. S’ils nous cherchent, ils vont penser qu’on s’est éloigné le plus possible, et ça peut jouer en notre faveur. N’allumez aucune lumière et évitez de faire trop de bruit.”

“Comme une dispute, par exemple ?” Demanda Axellan avec un léger rictus.

“Par exemple, ouais. On pourra en profiter pour apprendre à mieux se connaître et décider de notre avenir, si on en a encore un.”

“J’aime pas ça.” Râla Ethan. “Mais si on peut rien faire d’autre, ainsi soit-il.”

Il ponctua sa phrase par un mouvement de bras exagéré, avant de se lever pour commencer à arpenter la pièce.

“Et puis, on pourra toujours t’apprendre à te battre,” fit-il en dirigeant un regard encore plein de reproche vers Axellan, “ça évitera un autre fiasco, ou une autre…”

“Et on instaure une règle,” l’interrompit son frère, “personne ne parle ou ne fait référence à ce qui s’est passé dans le casino. C’est compris ?”

Il regarda avec insistance son frère, qui soutint son regard un moment avant de s’avouer à nouveau vaincu.

“Cela dit, il a pas tort. C’est pas du luxe de savoir coller quelques droites dans le nouveau monde. Et toi, tu sais pas coller grand chose. Même la petite elle se débrouille mieux, alors qu’elle a, quoi, 8 ans ?”

“6.”

“Encore pire ! On va te reprendre en main, mon petit celte, tu vas voir !”

“Mon petit ? Vous avez quel âge, je suis presque certain d’être plus âgé que vous !”

“15 ans.” Fit Ethan, le nez dans les placards qu’il avait commencé à fouiller.

“16.” Corrigea Nolan. “Depuis 3 mois à peu près.”

“Et moi j’en ai 21, ce qui fait de moi l’aîné.”

“Mon petit, je vais t’apprendre quelque chose sur la loi de la rue. L’aîné respecté, c’est celui qui a le plus de chances de survivre. Autant te dire que pour le moment, tu vaux pas mieux qu’un nourrisson et tu peux t’estimer incroyablement chanceux d’avoir tenu jusque là.”

Ce n’était pas la première fois qu’il entendait ça, et ce qui l’énervait le plus était qu’il ne pouvait pas argumenter contre.

“Très bien, apprenez-moi.” Concéda-t-il.

“On commencera demain. Il y a rien qui presse de toute façon, avec l’autre dans le coma. Et de toute façon, s’ils nous trouvent d’ici-là, tout l’entraînement du monde te sauvera pas, sauf si ça te permet de voler.”