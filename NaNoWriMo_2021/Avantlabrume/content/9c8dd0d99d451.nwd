%%~name: Porte de devant
%%~path: 282b92cffabbf/9c8dd0d99d451
%%~kind: NOVEL/DOCUMENT
### Porte de devant

Donald avait toujours été quelqu’un de chanceux. Pas bien futé, il le savait, mais c’était très accessoire pour quelqu’un comme lui. A seulement 29 ans, il était très musclé et avait le physique d’une armoire à glace. Issu d’un milieu pauvre, ses capacités naturelles l’avaient dirigées tout droit dans la criminalité. Il avait d’abord travaillé pour Pablo, un italien qui se prenait pour le nouvel Al Capone. Par un coup du sort, il ne se trouvait pas au quartier général lorsque la bande de Rhose les avait attaqué. C’était son jour de congé.

Le gang de Pablo avait été absorbé organiquement par celui de la grosse dame, et il s’était retrouvé sous les ordres de Mike, un gars très ambitieux mais craint pour cela par les autres capitaines. Et puis cette brume était venue, et avait tout balayé, même Rhose. Le groupe de Mike était celui qui avait subi le moins de pertes, alors il avait entrepris de chasser les capitaines survivants pour être le chef incontesté de Jersey City. Là encore, Donald avait été chanceux. Il avait même eu l’opportunité de trancher lui-même la gorge de Chief D.

Et voilà qu’il se retrouvait à garder le casino en journée, ce qui signifiait qu’il pourrait se joindre aux festivités dans la soirée. Les gamins étaient les protégés de Rhose, mais ils n’avaient jamais plu à Mike. Mais il voulait pas simplement les tuer, il voulait humilier, les briser physiquement et mentalement. Donald non plus ne les aimait pas, surtout l’insolence de Nolan. D’un autre côté, son visage poupon était tellement attirant. Et ce soir, il pourrait laisser libre court à ses pulsions. Ethan était nettement moins joli, mais s’il pouvait se faire les deux frères d’un même coup, il n’allait pas refuser.

Comme il pensait à lui, le plus jeune de la fratrie apparu dans son champs de vision. Il portait dans son dos une grosse lame dont coulait un liquide rouge poisseux. Cela voulait sûrement dire qu’il avait réussi. Donald grimaça. Il espérait que Mike ne changerait pas d’avis. Derrière le gamin suivait Lydia, qui boitait fortement. Un épais bandage entourait sa cuisse gauche. La gaze virait avait viré au brun à force de se gorger de sang, et elle devait utiliser une béquille pour ne pas s’effondrer. Son visage était pâle et elle suait à grosses gouttes. Si c’était le prix qu’ils avaient dû payer pour buter trois clochards, aucune chance que Mike ne reconnaisse leur utilité. En plus, il avait déjà Laura, pas besoin d’une pisseuse en plus.

Il lui fallut quelques secondes pour remarquer celui qui titubait à la suite d’Ethan. Pourtant, le jeune homme n’était pas petit, et se démarquait des deux gamins. C’était sûrement parce qu’il avançait courbé. Son visage était tuméfié, et il haletait. Donald ne pouvait pas bien voir sa face, mais il se doutait que ce n’était pas l’un des siens.

“Alors gamin, la pêche à été bonne ? C’est qui lui ?” Lança-t-il, hilare, à Ethan.

“La ferme le canard. C’est le sacrifice de Mike.” Entendit-il le pisseux lui répondre. “Reggie est pas avec toi ?”

Voilà, c’était exactement ce qui horripilait le colosse. Ils n’avaient pas encore de poil au menton, mais ne se privaient pas pour se croire au dessus de leurs aînés. Il avait envie de lui enfoncer son poing dans la figure, là, maintenant. Mais ça ne serait pas marrant de le tuer maintenant, et Mike ne serait pas content. Ravalant sa rage, il tenta d’arborer un visage neutre.

“Il est à l’intérieur avec les autres. Probablement en train de s’amuser avec ton frère, si tu vois de quoi je parle. Va voir, ça doit être un sacré spectacle.”

Les yeux d’Ethan s’écarquillèrent un temps et Donald sourit, satisfait. Jusqu’à la remarque de Lydia qui poussa son ami à l’intérieur.

“Merci, Donald. Détend toi par contre, on dirait que tu vas te chier dessus.”

“Espèce de petite…”

Le claquement de la porte masqua son dernier juron. A bout de patience, il se retourna et frappa violemment le mur, qui trembla. La marque de son poing était inscrite dans la façade du casino sous la forme de quatre doigts ensanglantés. Sa main lui faisait un mal de chien, mais la douleur avait le don de le calmer.

Il pouvait tolérer une dernière insulte de ces gamins. Il prendrait tout son temps pour se venger dès que Reggie reviendra. Après tout, il avait la chance de toujours être dans le bon camp, contrairement aux gosses. Au final, c’était toujours lui qui s’en sortait.

Les sens encore étourdis par le coup qu’il venait d’abattre, il ne remarqua pas la silhouette qui se faufila derrière lui, pas plus qu’il ne sentit la lame effilée lui traverser la gorge avant qu’il ne soit trop tard. Il tenta de crier, mais aucun son ne sorti de sa gorge emplie de sang. Son épaisse main ne parvenait pas à empêcher le sang de couler abondamment. Recouverte du liquide carmin, elle glissait et ne faisait qu’en répandre plus. Ses forces déclinaient rapidement, il ne put qu’entendre le rire de contentement de son assassin avant que tout ne devienne noir.