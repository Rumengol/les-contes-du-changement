%%~name: Conciliabule
%%~path: 282b92cffabbf/1e201aff354c9
%%~kind: NOVEL/DOCUMENT
### Conciliabule

“On ne peut pas leur faire confiance, c’est tout.” Postula Ivan en rangeant son arme dans sa poche bien trop grande.

“Mais si elle dit vrai ?” Opposa Axellan. “Si tout son gang contrôle la ville, je ne vois pas comment on peut s’en sortir.”

“Du bluff, pour nous amener à prendre son parti. Et même si c’est vrai, on a plus de chance en se rendant à ce Mike et en les dénonçant.”

“Tu t’entends parler ? Et tu l’as entendu, lui ? Il est complètement fou ! Hors de question que je me rende à lui, et encore moins que j’y aille avec Emilie !”

“Alors tu proposes de te jeter dans la gueule du loup, et de combattre à un contre deux des gangsters qui ont l’habitude de ce genre de choses ? Bien joué, c’est beaucoup plus malin. Quoi que non, vu tes compétences, tu comptes même pas, et la gamine non plus.”

“Ils doivent avoir un plan, ils savent mieux que nous qu’on ne peut pas gagner en combat à la loyale.”

“Elle est amoureuse du gamin qui s’est fait prendre, et l’autre c’est son frère. Ils pensent pas rationnellement, alors qu’ils aillent se faire crever si c’est leur délire.  Avec un peu de chance, ils feront distraction et on pourra partir tranquillement.

“Mais…” Commença timidement Emilie. “C’est un peu de notre faute s’ils ont des problèmes. Et puis si on les aide pas, ils vont pas juste appeler les gros méchants ?”

Les deux garçons regardèrent avec de gros yeux la petite fille, qui se recroquevilla sur elle-même. C’était quelque chose que ni l’un ni l’autre n’avait considéré, mais qui pourtant était tellement évident. Ils n’avaient pas d’autre choix que de se battre, que ce soit contre Lydia et Ethan, ou contre un gang prêt à détruire des groupes entiers sur un simple désaccord.

“A quel moment on a basculé aussi vite ?” Se lamenta Axellan. “Ca fait même pas une semaine, comment tout a pu autant changer ?”

“Va falloir t’y faire, Axel. La petite a raison. On va pas avoir le choix, alors on va écouter leur plan. Mais s’ils en ont pas, ou s’il est débile, on les trucide et on cherche notre propre voie pour s’échapper, c’est compris ?”

Axellan hocha la tête, Emilie aussi, incertaine de si c’était une bonne chose ou non. Ils attendirent le retour de leurs futurs complices, qui ne se firent pas prier. Un peu moins d’une heure après leur départ, ils revinrent chargés de matériel divers. Ethan déposa devant eux ce qu’il avait dans ses bras.

[A voir quoi] quelques flashbangs et deux grenades. Ivan s’en saisit et les observa. C’était bien des armes militaires, il interrogea d’un regard l’adolescent sur leur provenance, ce à quoi il n’eut qu’un haussement d’épaule pour toute réponse. Il continua d’examiner le reste du matériel pendant un instant, avant de demander d’un coup.

“C’est quoi votre plan ?”

“Alors vous êtes avec nous ?” S’extasia Lydia.

“J’ai demandé votre plan. On décidera après si on le suit ou non.”

“Bon. On connaît bien les voies d’aération du coin, et on peut les emprunter pour sortir du Mall Center sans être repéré, et pour aller là où Mike retient Nolan. A partir de là, on les prend en embuscade. Si Nolan n’est pas avec eux, on utilise ça.” Lydia pointa les grenades. “Ca fera un carnage et on pourra le chercher. S’il est avec eux, on sera obligé de se battre. Il y a des chances que Mike ait un flingue, mais il a toujours refusé que ses sbires en aient en sa présence.”

“Donc on va là-bas, on tue tout le monde et on repart. Brillant. Et après, il se passe quoi ?” Ivan était toujours aussi sceptique et avait croisé les bras.

“Mike est pas très aimé dans le groupe, il a réussi à être le chef parce qu’il est un peu moins con que les autres, mais personne ne va rester loyal à un cadavre. Il va plutôt y avoir un chaos pendant que les survivants se battent pour savoir qui récupérera le trône.”

“Ca ne marchera jamais, c’est bien trop puéril comme plan.”

Ethan se redressa, lui crachant presque au visage.

“Alors vas-y, propose quelque chose si t’es si malin.”

“Vous avez un plan du réseau d’aération, au moins ?”

“Je le connais par coeur,” fit Lydia en attrapant un crayon et dessinant à même le sol. “Mike est dans la salle du fond du casino [NOM DE CASINO]. C’est sous terre, mais on peut y accéder via le toit, ou les bouches d’aération dans la rue de derrière.”

Au fur et à mesure qu’elle dessinait, Ivan paraissait en train de réfléchir intensément. Une idée sembla poindre dans son esprit, qu’il polit avant de l’exprimer. Son plan était ingénieux, et avait bien plus de chance de fonctionner que l’assaut désorganisé prévu par les deux adolescents.