%%~name: Rencontre
%%~path: f5e0b2b6cdf68/fb99f1dd8d29e
%%~kind: NOVEL/DOCUMENT
### Rencontre

Axellan se 	réveilla avec un poids lui enserrant le torse. Il ouvrit faiblement les yeux pour remarquer le petit corps qui l’avait rejoint pendant la nuit. Emilie dormait quasiment sur lui, malgré l’espace de 4 lit qu’il avait ménagé. Très gêné, il tenta de la réveiller délicatement, avant d’abandonner et simplement la faire glisser sur le lit plutôt que sur lui.

En regardant du côté de l’entrée du magasin, il vit que le soleil se levait paresseusement, illuminant les vitres verglacées. Il avait eu la bonne idée de bien fermer la porte hier, mais plusieurs fenêtres brisées laissaient libre court à la fraîcheur de la nuit. Il ajouta une couche de couverture sur son amie pour lui tenir chaud et décida de partir explorer les environs.

Il était encore tôt et les ombres s’allongeaient énormément. Il progressait en frissonnant dans une New-York morte qui semblait pourtant s’éveiller en dansant dans la lueur de l’aube. L’objectif de ce matin était de trouver quelque chose de consistant pour le petit-déjeuner. Il ne s’attendait pas à trouver des viennoiseries et un bon café, mais quelque chose dans cette ordre d’idée ne serait pas de refus.

Aucune des enseignes vendant de la nourriture ne paraissaient avoir survécu, malgré leur nombre. Il trouva néanmoins un plan de la ville devant une sortie de métro. Manhattan n’était plus très loin. S’il ne pouvait pas trouver de quoi manger, il pouvait au moins partir en éclaireur pour vérifier si le pont tenait toujours debout. Son estomac criait famine, alors il déballa une barre de céréale pour la manger sur le chemin.

Quelques rues plus tard, il arrivait en vue du Brooklyn Bridge. Il semblait avoir été durement éprouvé, d’immenses blocs de la structure étaient tombés dans la rivière, mais la myriade de carcasses de véhicules ornant sont plateau témoignait de sa robustesse. Ils pourraient même le traverser à vélo, c’était déjà ça. Il pensa à traverser l’île pour vérifier aussi l’autre côté, mais il ne pouvait pas trop tarder.

Il s’apprêtait à faire demi-tour quand il se retrouva face à face avec trois hommes. Ils étaient tous les trois solidement bâtis, vêtus de vestes de cuir et portant des barres métalliques à la main. Celui du centre paraissait tout droit sorti d’une bande de bikers des années 80, avec un bouc, une coupe mulet et des lunettes de soleil. A tout moment Axellan s’attendait à sourire et dévoiler une dent en or. Les deux molosses qui le flanquaient se ressemblaient furieusement, avec leur visage rond et chauve qui appuyait leur regarde patibulaire. Ils semblaient tous avoir dans la trentaine bien entamée, si ce n’était la cinquantaine.

“Eh bien, on a un petit rat qui se balade sur notre territoire ?”

C’était le has-been qui avait parlé, il s’avançait vers le jeune homme en caressant son arme. Bien sûr, dès que l’ordre s’effondre, les vermines sortent des souterrains pour faire leur loi à la surface. C’était tellement courant qu’il aurait dû s’y attendre. Maladroitement, il battit en retraite et laissa tomber son sac pour le fouiller sous l’oeil interrogateur de ses agresseurs.

Il mit d’angoissantes secondes à trouver ce qu’il cherchait, l’arme des parents d’Emilie. Il n’avait pas vérifié si les chargeurs étaient pleins, mais tant pis, il voulait surtout les effrayer. Il prit un air assuré et pointe son pistolet vers l’homme qui s’avançait vers lui. Aussitôt, celui-ci leva les mains en l’air et parti d’un rire tonitruant.

“Oh là, on se calme. On déconnait, tout va bien.”

“Je t’avais dit que t’avais un sens de l’humour pourri, Bryan”

“La ferme Rudy” décocha Lunettes de soleils. “On te faisait une petite blague, c’est tout. Faut se serrer les coudes dans la fin du monde, mon gars. Tu peux baisser ton arme.”

Voyant qu’Axellan n’avait pas l’intention de rengainer, il soupira.

“Tu fais pas peur à grand chose, à trembler comme un lapin effrayé. En plus, ça tire mieux si tu enlèves la sécurité, regarde.”

Il arriva jusqu’au jeune homme pétrifié dont le doigt était crispé sur la détente, incapable de tirer ou de se détendre. L’homme dénommé Bryan lui arracha le pistolet de force, avant d’appuyer sur un bouton qui déverrouilla un mécanisme dans l’arme. Il pointa ensuite le canon vers le sol devant lui, et pressa la détente. Sa démonstration s’acheva dans un clic pitoyable.

“Eh bien ?”

Il éjecta le chargeur et le regarda d’un air dubitatif, avant de repartir de son rire gras.

“Effectivement, t’allais pas chasser beaucoup d’autres lapins comme ça, mon gars. Comment t’as survécu à la Brume ?”

Il ponctua sa phrase par une lourde tape dans le dos d’Axellan, qui s’activa enfin. Il regardait l’homme d’un air confondu. La majuscule était audible dans ses mots.

“La Brume ?”

“Tu vois pas de quoi je parle ? Mais d’où tu sors, toi ?”

“Arrête un peu Bryan, tu vois bien que tu lui fais peur, au petit.” 

Axellan était incapable de dire si c’était Rudy ou sa copie conforme qui avait parlé. Il détestait cette sensation d’être traité comme un gamin qui n’a rien à faire dans une discussion d’adultes.

“Non, je comprends vraiment pas. Vous parlez du gros brouillard d’il y a trois jours ?”

“Ouais, appelle ça comme tu veux, nous on s’est fixé sur Brume. J’arrive pas à croire que quelqu’un comme toi ai passé la nuit.”

“C’est qui ‘on’, vous trois ?”

“Nan, on est une petite communauté dans Manhattan. On s’est tous retrouvés par hasard dans la Brume, et depuis on survit comme on peut, et on recrute tous les autres survivants qu’on croise.”

Cette fois, c’était l’autre chauve qui avait parlé, mais même leurs voix étaient identiques.

“Drôle de façon de recruter.”

“C’était qu’une blague, tu vas pas en faire tout un plat !” s’exclama Bryan, exaspéré.

“Ruby va encore te gronder.”

“Pas si tu lui dis rien. Bon, gamin. On va te raccompagner au camp et tu vas nous raconter comme ça s’est passé pour toi.”

“C’est gentil, mais il faut que j’aille dans le Bronx. J’ai une amie là-bas, et il faut que je la retrouve.”

Sa phrase laissa un blanc un peu trop long à son goût. Les trois hommes se regardèrent d’un air gêné, semblant se demander qui devrait parler. Finalement, l’homme aux lunettes prit les devants.

“Tu comptais bien passer par Manhattan, non ? Passe quand même au camp, au moins pour prendre de quoi faire le voyage.”

Cette alternative convenait à Axellan, mais il avait la sensation que Bryan ne lui disait pas tout. Impression renforcée par les regards de désapprobation de ses camarades et la moue de l’homme. Mais il hésitait encore. Il n’était pas certain qu’il pouvait faire confiance à ces hommes, et il rechignait à l’idée de laisser Emilie seule. D’un autre côté, si c’était un piège, mieux valait qu’il soit seul à tomber dedans.

Sans entrain, il ramassa son sac et emboîta le pas des trois hommes. Bryan lui lança son pistolet sans même le regarder.

“Si t’as des balles, tu devrais essayer de les mettre dedans, c’est fait pour ça.”