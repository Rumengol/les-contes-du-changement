%%~name: Sortie
%%~path: 4b4cb2fff1b57/8e5e256a77ae9
%%~kind: NOVEL/DOCUMENT
### Sortie

Il devait sortir pour s’en assurer, voir de ses yeux ce qu’il s’était passé. Trouver d’autres gens. Ses amis ! La destruction ne s’était sûrement pas limité à son quartier, et le réseau faisait toujours défaut. Marius n’habitait pas si loin, en une dizaine de minute il pouvait retrouver son appartement. Jess était à l’autre bout de la ville, mais ils pourraient y aller à deux.

Dehors était sûrement dangereux. Ca pouvait être une apocalypse zombie, d’après son visiteur. Cette éventualité lui fit prendre conscience de son état. S’il y avait véritablement une horde de zombies là dehors, il était dans le pire endroit possible, en plein milieu d’une grande ville. Il repensa soudain aux morts-vivants de Je suis une légende, qui s’abritaient dans les souterrains le jour pour déferler dans les rues la nuit.

Et puis il avait été mordu. Deux fois, et proche du cerveau en plus. Il avait l’impression de sentir le virus morbide se frayer un chemin vers sa tête, à travers ses vaisseaux sanguins, ses muscles… La panique lui montait à la tête, il se sentait fiévreux, nauséeux, et l’insupportable bourdonnement revint le hanter. Jusqu’à ce qu’une petite voix à l’arrière de sa tête ne le ramène à la raison. Il s’était fait mordre le matin, il y a presque huit heures. Les virus zombies des films sont bien plus rapide à agir. Et les virus ordinaires beaucoup plus lent. Donc soit il partait du principe qu’il n’allait pas subitement à se transformer en carcasse avide chair fraîche, soit il se faisait dévorer par le stress jusqu’à mourir de faim et de soif dans son petit studio.

La première option était plus alléchante, et le ramenait à sa première préoccupation; Le danger. Il avait déjà sa batte dont il pouvait se servir comme arme. Il avait également une dague reçue à une époque où il aimait les couteaux. La lame n’était pas du tout affûtée, mais ça restait une arme, à l’origine. Il remplit son sac à dos de pansements, d’antiseptiques et de barres énergétiques, y ajouta une lampe torche et [PENSER AU RESTE, armure, bouclier]. Ainsi équipé, il se sentait comme un véritable aventurier, prêt à faire face à la fin du monde.

Mais avant, il devait faire face à un autre obstacle, bien plus terrible : le cadavre dans le couloir. Un coup d’oeil infructueux dans le judas ne lui appris pas grand chose : il ne voyait pas le sol. Résigné, il respira un grand coup, puis ouvrit brusquement la porte, batte levée. 

Rien ne lui sauta à la gorge d’autre que l’odeur de putréfaction Le zombie - car c’en était bien un, il en était cette fois sûr - était toujours étalé dans la même position, le visage enfoncé. La brume s’était également enfuie du couloir, qui était désespérément vide et sombre. Il ne discernait que le carré de lumière dessiné par le soleil éclairant son appartement, les néons du couloirs avaient visiblement sauté. Il alluma sa lampe torche et balaya le couloir à droite et à gauche d’un geste angoissé. Mais il était bien seul.

Axellan donna des petits coups dans le corps étendu au sol, pour vérifier qu’il ne se relèverait pas. Une fois rassuré, il ferma la porte de son appartement d’un tour de clef, enjamba prudemment le mort et s’en éloigna à pas rapides. Il ne connaissait pas bien ses voisins, mais c’était l’instant ou jamais de faire connaissance. Il frappa à plusieurs portes et tenta de les ouvrir, mais toutes étaient fermées et silencieuses. Est-ce que tout le monde dormait ? Il se remémora la vision qu’il avait de sa fenêtre. Il était plus probable qu’ils aient tous évacué l’immeuble, si ce n’est la ville.

Il ne comprenait toujours pas comment il avait pu louper quelque chose d’aussi énorme. Il avait dû y avoir des cris, des grondements, la moitié d’un immeuble s’était effondré, et lui avait simplement dormi. Perdu dans ses pensées, il ne toquait même plus et essayait simplement toutes les portes du couloir.

Il failli trébucher quand l’une d’entre elle s’ouvrit, et se retint au dernier moment de ne pas tomber dans le gouffre qui s’étendait derrière. De l’autre côté de son immeuble, tout la façade semblait avoir été soufflée par une explosion retentissante. Plusieurs appartements s’étaient purement et simplement volatilisés, mais la plupart étaient encore discernables à travers le bâtiment éventré. 

La situation de la rue derrière l’immeuble était la même que celle de son appartement. Si ce n’est qu’il voyait à présent la route, et que le spectacle n’était pas réjouissant. A plusieurs endroits la terre s’était comme soulevée et les amoncellements de voitures formaient des piles de tôle froissée. La station essence qui côtoyait l’immeuble avait été vaporisée, seuls les trous carrés des pompes étaient encore visibles. C’était peut-être son explosion qui avait abattu la façade, pensa Axellan.

Frapper aux portes ne servait à l’évidence à rien. Ses voisins étaient probablement tous parti dans les premiers instants de la catastrophe, contrairement à lui. Ou alors… Il lança un regard derrière lui, dans la pénombre. Il n’avait pas bien regardé le visage du zombie. Mais il n’allait pas faire demi-tour. S’il s’avérait être quelqu’un qu’il connaissait, Axellan n’était pas certain de pouvoir continuer.

Il devait descendre. Emprunter la façade dévastée serait plus rapide, mais les fils d’aciers apparents et les rebords tranchants en faisaient une très mauvaise idée. Ce serait donc les escaliers. A condition qu’ils ne soient pas dans le même état.

De la première qu’il trouva montait une terrible odeur de charogne 
qui le prenait au nez. Pire encore que celle du zombie, il y avait quelque chose en bas d’insoutenable. En se bouchant le nez, il avança rapidement vers la suivante, qui avait l’air intacte. Le jeune homme descendit prudemment les étages, armé et paré à la moindre attaque. Mais rien ne vint des ténèbres insondables.

Le rez-de-chaussé était plongé dans la même obscurité que les étages. Axellan était frustré de devoir autant utiliser sa lampe torche en pleine journée, dans son propre immeuble.

Le hall d’entrée était silencieux comme la mort. Il ne savait pas à quoi il s’attendait, peut-être voir des gens se regrouper avant de partir ? Il avança entre les rayons de boîte aux lettres, à nouveau attaqué par la puanteur. Il plaça un bout d’écharpe sur son nez pour le protéger et se dirigea vers la source. S’il devait combattre un autre mort-vivant, ou pire, une horde, il préférait le faire dans un couloir plutôt quand dans une étroite cage d’escalier.

L’odeur comme les mouches émanaient de la salle commune de l’immeuble. Il se rappela enfin de la fête du voisinage de la veille. En réalité, les portes fermées et les appartements silencieux l’étaient depuis un moment. Avec méfiance, il s’approcha de la double porte et risqua un regard, prêt à déguerpir à tout moment.

Le spectacle qui s’imposa à lui dépassa ses attentes. Même s’ils ne bougeaient pas, des dizaines de morts étaient entassés au centre de la pièce. Les tables étaient renversées, et le sol jonché de confettis et de viscères. Ils avaient vraisemblablement été surpris en plein milieu de la fête, et n’avaient rien pu faire. D’immenses griffes avaient lacéré les murs et le plafond. Ce ne pouvait pas être l’oeuvre de simples zombies. Elles ne pouvaient appartenir qu’à une créature gigantesque, au moins aussi grande que la pièce. Que pouvait-il bien s’être passé pendant qu’il était coincé dans son appartement ?

Il remercia le ciel de ne pas avoir décidé de descendre rejoindre la fête et détourna le regard. Ce qui avait fait ça pouvait encore être dans les parages, et Axellan n’avait aucune envie d’ajouter son cadavre à la pile macabre. Ignorant les larmes qui perlaient au niveau de ses yeux, il poussa la porte dégondée et sorti dans la rue.