%%~name: Nouvel objectif
%%~path: f5e0b2b6cdf68/7c96191eff108
%%~kind: NOVEL/DOCUMENT
### Nouvel objectif

Pour rejoindre le quartier de Jess, le plus court était de couper par l’île de Manhattan. Mais si son quartier était dans cet état, il n’osait pas imaginer l’état de l’île centrale. Il espérait déjà que les ponts étaient encore debout. Au vu du temps qu’il lui avait fallu pour aller simplement jusque chez Marius, il n’imaginait pas devoir contourner Manhattan. 

Ses appréhensions se concrétisèrent au fur et à mesure qu’ils s’approchaient, croisant de plus en plus de rues dévastées et d’immeubles démolis. Manoeuvrer le vélo devenait presque mission impossible, Axellan se retrouvait la plupart du temps à le porter à bout de bras plutôt qu’à le pousser, ce qui l’épuisait. Au moins, il convenait facilement aux pauses que réclamait Emilie qui, privée de son véhicule, devait escalader parfois des montagnes de gravas avec ses petites jambes.

La nuit approchant, le jeune homme fut tenté de faire une partie de la route dans le noir, mais sa partenaire été épuisée et ne paraissait plus capable d’avancer. Elle ne disait rien par fierté peut-être, mais son visage la trahissait. Il décréta qu’ils allaient chercher un endroit sûr pour passer la nuit. Un immeuble pas trop en ruine de préférence et s’il pouvait être vidé entièrement de ses occupants serait l’idéal.

Ils ne trouvèrent pas de tel endroit, mais le visage d’Axellan s’éclaira quand il dépassèrent la devanture d’un magasin de literie. L’intérieur avait l’air d’être encore dans un état convenable malgré la pénombre du début de soirée. Ils traversèrent les rayons en s’éclairant à la lampe torche, Emilie sautant avec joie sur les immenses matelas qu’ils croisaient. Elle faisait du trampoline sur les plus mou, testant leur résistance jusqu’à entendre un crac sonore indiquant que le sommier avait cédé.

Le jeune homme ne pensa même pas à l’arrêter. Tout avait disparu après tout, elle pouvait bien s’amuser un peu. Et puis, qui allait leur reprocher quelques lits cassés ? Ils s’arrêtèrent finalement au fond du magasin. Il s’étendait sur une surface surprenamment grande pour l’étroite façade, et ils étaient maintenant à bonne distance de la sortie. Axellan commença à évaluer différents lits, mais Emilie était catégorique.

“On prend un grand lit et on dort ensemble dedans !”

“On dormira mieux dans des lits séparés, mais tu peux aller dans un grand si tu veux.”

“Mais j’ai froid, et j’ai peur toute seule.”

Il ne pouvait pas lutter contre ces arguments et cet air abattu, même si c’était de la comédie. Mais il déplaça un autre lit double à côté du premier pour avoir beaucoup de place, ignorant la moue d’Emilie. Ils mangèrent une fois de plus frugalement, avant que le Morphée ne les appelle de ses bras enjôleurs.