%%~name: Baptême du feu
%%~path: 282b92cffabbf/8f1432d780c68
%%~kind: NOVEL/DOCUMENT
### Baptême du feu

L’épée à la main, il tenta d’assurer sa poigne sans trembler. Les silhouettes encapuchonnées et menaçantes qui s’avançaient vers lui étaient plus petite, mais cela ne le rassurait pas. Le garçon paraissait mieux bâti, tandis que la fille faisait tournoyer avec assurance son cylindre de métal. Prenant une grande inspiration, il leva son épée pour les avertir.

“On vous veut pas d’ennui. Si vous nous rendez nos sacs je vous laisse partir.”

L’adolescent qui lui faisait face parti d’un rire insultant.

“Oh, il nous laisse partir, il est trop gentil. T’en pense quoi Lydia, on lâche les sacs et retourne dans notre trou ?”

“Ta gueule, dis pas mon nom !” la rabroua son amie

“Bah, qu’est-ce qu’il va en faire ? Le donner à la police ?” Il reporta son attention sur son adversaire. “Moi, c’est Nolan McGuire. C’est Ecossais, même si j’ai pas l’accent. Et toi ?”

La nonchalance avec laquelle ce garçon s’adressait à lui alors qu’ils s’apprêtaient à se battre décontenançait l’épéiste en herbe. Il lui répondit en bredouillant.

“Axellan. Reygwyn.”

“Oh, je connais pas, ça a quoi comme origine ?”

“C’est celte.”

Il eut à peine le temps de finir sa phrase que la fille se jeta sur lui. Elle avait profité qu’il soit occupé par Nolan pour se rapprocher, jusqu’à être assez proche pour l’attaquer par surprise. Il releva son épée juste à temps pour parer le coup qui lui arrivait dessus. La violence de l’impact le fit reculer, alors que le garçon prenait le relais pour l’attaquer. Axellan passa maladroitement la lame de l’autre côté, faisant reculer son agresseur.

Ils se regardèrent tous les trois, se jaugeant mutuellement après ce bref échange. Axellan sentait sa confiance fondre comme neige au soleil. De ce que lui avait expliqué Marius, il se souvenait surtout de la théorie. Remise, septime, feinte, il connaissait les définition, mais ses mains semblaient incapable de les appliquer. D’autant que la lame pesait terriblement lourd dans ses mains, il sentait déjà ses muscles supplier.

Les deux autres réalisaient également leur avantage, et raffermissaient leurs prises en anticipation de leur prochain assaut. Il prit une grande respiration, et adopta une pose, pâle imitation des postures des épéistes qu’il avait vu. Il en fallut plus pour décourager ses adversaires, qui attaquèrent des deux côtés. Il recula vivement pour ne pas se laisser encercler et commença à agiter sa lame tout autour de lui.

Il était évident qu’il ne savait pas ce qu’il faisait, mais Nolan comme Lydia savaient qu’ils n’avaient que des armes contondantes qui ne pouvaient infliger de réelles blessures qu’avec beaucoup de puissance ou à force d’insistance, tandis qu’ils affrontaient une épée aiguisée capable de trancher profondément même par accident. Ils attendaient que l’impotent qu’ils avaient en face de lui se blesse tout seul pour baisser sa garde, mais il ne semblait pas idiot à ce point. Alors ils jouèrent l’attente, il s’épuiserait bien avant eux à ce rythme, surtout s’ils continuaient de le harceler à grand renfort de feintes.

Puis Nolan en eut marre et se lança tête baissée, ignorant son amie qui lui criait de ne pas faire l’imbécile. Il percuta Axellan de plein fouet, récoltant par la même occasion une coupure superficielle à l’épaule. Ils tombèrent tous les deux au sol, l’épée glissant quelques mètres plus loin. Pendant quelques instants, les deux garçons se battirent comme des chiffonniers. L’étudiant n’en revenait pas de la force du garçon qui ne devait pourtant pas dépasser les 16 ans. Il était roué de coups, parvenant à peine à protéger son visage quand son ennemi visait son torse.

Il roula sur le côté pour passer au dessus de son adversaire, mais celui-ci fut plus vif et enclencha un autre tour, reprenant l’avantage. Axellan se savait absolument surclassé. Bien qu’il soit plus jeune, Nolan était plus vif, plus fort, et savait bien mieux se battre que lui. Il se doutait qu’il n’était encore conscient que parce qu’il jouait avec lui comme un lion avec sa pauvre victime. Mais il était déterminé à ne pas abandonner, tant qu’il serait encore capable de se battre.

Occupé à défendre sa vie, il n’entendit pas le cri de douleur, mais celui qui le dominait le saisit clairement. Il se déconcentra un moment, créant une ouverture qu’Axellan saisit immédiatement, parvenant à le déséquilibrer et à le repousser. Il écopa d’un ultime coup qui atteint son oeil, brouillant momentanément sa vision. Quand son autre oeil parvint à compenser, il vit ce qu’il avait manqué juste avant.

La fille était tombée à genoux, un couteau de cuisine enfoncé jusqu’au manche dans sa cuisse. Elle tentait tout en hurlant de compresser sa cuisse pour empêcher le sang de trop couler. A côté d’elle, Emilie se tenait depuis, un couteau identique dans la main gauche. Elle était figée dans une expression choquée qui déformait son visage d’un rictus stupéfait. Cela donna l’occasion à Nolan de se porter au secours de son amie. Il percuta la petite fille de plein fouet, l’envoyant voler à plusieurs mètres.

Emilie heurta le sol dans un faible gémissement. Cette vue fut comme un électrochoc pour Axellan, qui recommença à s’activer. Il n’était pas si loin de son épée, qu’il récupéra en quelques instants. Son adversaire était en train d’examiner la blessure de Lydia, voyant s’il pouvait retirer la lame sans risque de déclencher une hémorragie.

Il tournait le dos au jeune homme, qui, aveuglé par sa rage, leva sa lourde épée sans y penser. L’autre lui tournait le dos, et ne dû sa survie qu’à l’avertissement de Lydia. Il se jeta avec elle sur le côté, et la lame percuta le sol avec violence, projetant de petits éclats aux alentours. Dans la panique, il avait appuyé sur la jambe où était planté le couteau, qui s’était déplacé dans la plaie. Abandonnant sa camarade, il se retourna, une barre de métal dans chaque main.

Il était bien moins adroit qu’avec une seule arme, mais compensait son manque d’adresse par une avalanche de coups à laquelle Axellan était incapable de répondre. Il fut forcé de battre en retraite jusqu’à l’endroit où Emilie était en train de récupérer. Sa colère s’évanouissait et avec elle la force qui l’avait accompagné. Les cylindres de métal le frappaient de plus en plus ses bras, ses mains, faiblissant sa prise.

Il ne fut sauvé d’un coup direct qui aurait probablement fracturé sa main que par l’intervention d’Emilie, qui s’était relevée et avait attaqué l’adolescent à son tour, brandissant toujours son couteau de cuisine. Cela laissa à Axellan le temps de se ressaisir et de le menacer en gardant une distance de sécurité.

C’est à ce moment là que Lydia s’avança. Elle avait retiré le couteau et bandé sa plaie avec un bout de tissu et une agilité d’experte. Elle boitait et son bandage de fortune s’imbibait rapidement de sang, mais était prête à se battre. Nolan lui passa sa barre de fer et elle s’en servit pour désigner Emilie. Axellan lui jeta un regard méprisant. Quel genre de personne fallait-il être pour s’en prendre à une petite fille ? Mais il ne pouvait faire plus que ça, il n’était déjà pas certain de pouvoir tenir sa position face à son propre adversaire, Emilie devrait se battre seule. Ils se regardèrent dans la pénombre quelques instants qui parurent durer une éternité. Le jeune homme faisait durer l’attente volontairement, espérant regagner un peu d’énergie. Les deux jeunes virent clair dans son jeu et se préparèrent à un nouvel assaut quand une voix hors d’elle les faucha dans leur élan.

“Arrêtez tout, et lâchez vos armes ! Maintenant !”

Il y avait tellement d’autorité dans ces mots qu’Axellan commença à baisser sa garde, même en ayant reconnu celle d’Ivan. Quatre paires d’yeux se tournèrent vers lui, dubitatives. Il tenait entre ses mains la tête du troisième agresseur, qui se débattait furieusement pour se sortir de l’étau d’acier dans lequel il était enfermé. Son visage était tuméfié, il semblait avoir subi un passage à tabac en règle, malgré le peu de temps avec lequel il avait été laissé seul.

Les deux autres hésitèrent un instant, figés dans leurs poses.

“Sinon je lui brise le cou !”

Ce second ordre brisa leur détermination, et ils lâchèrent leurs barres à mine qui rebondirent bruyamment sur le sol, noyant le magasin dans un fracas métallique pendant quelques secondes. Ils voyaient mal à cette distance, mais de la bave semblait couler des lèvres de leur ami.

“Ethan, c’est pas cool ce que tu nous fait… T’avais quand même le job le plus facile.” 

La voix de Nolan était devenue blasée et déçue. Pendant un instant, Axellan se demanda s’ils allaient repartirent à l’assaut, pensant qu’Ivan ne faisait que bluffer. Mais ils restèrent immobiles, attendant la prochaine demande du preneur d’otage.

“Vous allez chercher nos sacs et nous les rapporter. Si vous jouez au plus malins, il meurt, et je ne bluff pas.”

Pour appuyer son propos, il secoua le corps de son otage comme un sac à patates. Nolan leva les mains.

“Ok, ok, on va les chercher. C’est juste un peu loin, on en a pour pas loin d’une heure à faire l’aller retour, ça pose pas de problème ? Sinon, on peut vous emmener jusqu’à…”

“On va attendre.”

Comme toujours, la voix d’Ivan ne souffrait d’aucune contradiction et son interlocuteur comprit qu’il ne servait à rien d’argumenter. La fille n’avait pas prêté attention à l’échange et braquait un regard incandescent sur Emilie, qui lui répondait de la même manière. Quand Nolan et elle repartirent, elle se tint à son épaule et commença à boiter de plus belle. Le clair de lune révéla que tout une jambe de son pantalon pâle avait pris une sombre couleur brunâtre.

Ivan relâcha légèrement sa prise sur leur prisonnier, qui pu enfin respirer. Il continua de le maintenir en place le temps que ses camarades s’approchent. Il chargea en quelques mots secs Axellan de lui trouver quelque chose pour l’attacher. Sans trouver de meilleur idée, celui-ci rapporta plusieurs t-shirts que le jeune russe déchira pour en faire des liens de fortune. Il attacha leur otage et le lança au sol avant de lui décocher un coup de pied. Axellan se demandait s’il devait l’empêcher de s’acharner sur le pauvre corps qui tentait de retrouver son souffle quand Ivan se tourna vers lui.

“Qu’est-ce que tu foutais bordel ? Pourquoi t’étais pas là quand ils nous ont attaqué ? Tu sais ce que ça veut dire, monter la garde ?”

Ce sermon hurlé dans ses oreilles lui avait vrillé les tympans, mais il ne savait pas quoi répondre. Il était clairement en tort, il avait abandonné son poste. Et s’il disait la vérité, il n’était pas certain des conséquences. Il décida d’opter pour un mensonge crédible, qui apaiserait peut-être la colère de son compagnon.

“J’ai entendu un bruit un peu plus loin, dans une allée. J’ai voulu vérifier ce que c’était, par curiosité.”

[Encore une engueulade ?]