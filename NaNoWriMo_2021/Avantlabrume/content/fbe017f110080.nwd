%%~name: Fouille
%%~path: 282b92cffabbf/fbe017f110080
%%~kind: NOVEL/DOCUMENT
### Fouille

Axellan fut réveillé par un chahut venant de l’extérieur des fenêtres, dans la rue. Plusieurs hommes parlaient d’une voix forte, riant ou parlant férocement. Immédiatement, le jeune homme porta la main à l’épée qui reposait au sol au pied du canapé, mais sa main se referma sur du vide.

Il ouvrit les yeux paniqués pour se calmer aussitôt. Il reconnu la forme caractéristique du fourreau entre les mains d’Ethan, plaqué contre le mur à côté de la vitre. Il ouvrit la bouche, mais Nolan l’interrompit en mettant un doigt sur la sienne pour lui imposer le silence. Alors sans parler, il s’approcha de la vitre pour voir ce qui causait une telle commotion, et vit la foule d’une dizaine de personne qui s’était arrêtée dans la rue en contrebas.

Sans même avoir à poser la question, Axellan savait d’où ils venaient. C’était presque écrit sur leurs visages malfaisants. Au centre du petit groupe, une jeune femme parlait. Il n’était pas possible depuis l’appartement d’entendre ce qu’elle disait, mais elle donnait visiblement des instructions, répartissant ses sous-fifres dans les différents bâtiments. Une fois seule avec un homme et une femme, elle se dirigea dans leur immeuble et le pénétra avec assurance.

Axellan remarqua que les jointures des mains d’Ethan étaient blanches, crispées sur la poignée de l’épée. Il interrogea silencieusement Nolan, qui lui répondit en secouant la tête. L’adolescent lui indiqua Emilie, qui dormait encore, et la chambre d’Ivan. Il avait compris le message, et pendant que les deux frères commençaient à dissimuler les traces de leur passage, il secoua doucement la petite fille. Alors qu’elle ouvrait des yeux fatigués, il passa un doigt devant sa bouche pour lui signifier de rester silencieuse et de le suivre.

La petite fille ensommeillée le suivit sagement et en silence. Il la conduisit dans la chambre du blessé, qui dormait à poings fermés, comme la plupart du temps. Il s’agenouilla et chuchota à Emilie.

“Je vais te demander de rester très discrète, d’accord ? On joue au roi du silence, si tu sors de la chambre ou que tu fais du bruit, t’as perdu. Si tu y arrives jusqu’à ce que je revienne, t’auras quelque chose, c’est ok ?”

Elle hocha la tête, les yeux brillant d’avidité. Il ne pensait pas qu’elle saisissait la gravité de la situation, leur refuge était sur le point d’être découvert. En tournant ça comme un jeu, elle avait plus de chances d’être sage. Il retourna dans le salon et les garçons le repoussèrent instantanément vers le recoin qui menait aux trois pièces. De là, plaqués contre le mur, il était impossible de les remarquer depuis la salle principale à moins de beaucoup s’avancer. Le jeune homme remarqua qu’Ethan avait tiré l’épée de son fourreau et qu’il la tenait comme s’il comptait s’en servir.

Quelqu’un venait d’entrer dans l’appartement. Ses pas étaient légers, difficilement remarquables, probablement impossible à entendre si ce n’était pour le silence pesant de la pièce. Bien moins discrets que les pas délicats, les placards de la cuisine s’ouvrirent violemment dans un claquement de bois. L’individu qui venait d’entrer fouillait avec méthode chaque placard, et semblait entreposer les boîtes sur la table.

Soudain, le remue-ménage s’arrêta net. Ce n’était que le cinquième placard d’ouvert, mais le maintenant familier fracas des boîtes de métal n’avait pas suivi. Les regards d’Axellan et de Nolan se tournèrent vers le plus jeune frère, qui pâlit. C’était lui qui avait fouillé les placards la première fois, et aussi lui qui les avait remis en place à l’instant.

Les pas s’étaient fais plus discrets, mais il était encore possible de les entendre. Ils parcoururent la pièce prudemment, l’un après l’autre. En restant proche des fenêtres d’abord, puis la personne s’avança directement vers le renfoncement où les garçons étaient dissimulés. Axellan remarqua que la prise d’Ethan se modifiait légèrement.

Au moment où la tête de l’infortunée passa le mur, elle se retrouva avec une pointe d’acier glacial dirigée vers sa gorge. Axellan s’aperçut que c’était la femme qui semblait diriger le groupe qu’il avait vu à la fenêtre. Elle se figea et parcouru le trio du regard, son visage figé dans une expression de neutralité. Elle ne dit rien, se contentant simplement de lever les mains.

Ethan avait des yeux fous. Il semblait soudainement devenu enragé, et aurait certainement égorgé l’intruse si la main de son frère ne l’avait pas arrêté. Pendant quelques secondes qui parurent durer des minutes entières, ils restèrent tous dans la même position, immobiles comme des statues représentant une scène tragique. Puis un appel survint du couloir, hors de l’appartement.

“Alors Laura, t’as quelque chose ?”

Déglutissant difficilement, la jeune femme recula légèrement son cou, immédiatement suivi par la lame qui appuyait sur sa gorge. Elle ne se laissa pas démonter et parla d’une voix forte et assurée, se tourna vers la porte.

“Non rien. J’ai l’impression qu’il a déjà été fouillé celui-là. Je vais regarder dans la salle de bain s’il y a des médicaments, Rena, mais mieux vaut ne pas trop compter sur cet immeuble.”

Puis elle leur refit face, un fin sourire suffisant sur son visage.

“Vous la connaissez ?” Osa enfin demander Axellan à voix basse.

“C’est la pute de Mike.” Siffla Ethan entre ses dents

“Hey, c’est pas cool ça. J’ai fait ce qu’il fallait pour survivre, comme vous. C’est pas par plaisir que j’étais avec ce porc, vous pouvez me croire.” Elle avait répondu sur le même ton, un brin d’impatience dans la voix.

“Et nous on l’a tué.” Fit remarquer Nolan. “Pourquoi t’as pas appelé ton molosse, on est pas dans ton camp ?”

“Déjà, l’épée sous ma gorge. Je tremble sous la pression actuellement. Et puis comme tu l’as si bien souligné, vous avez buté mon protecteur, et Reggie a envie de faire table rase du passé, et ça m’inclus dedans. Je suis en sursis actuellement. En dehors de Rena, plus personne ne m’est loyal, et je ne suis même pas sûre pour elle.”

“Oh, je vais verser une larme. Mais t’as pas répondu à la question. Donne moi une seule bonne raison de pas te saigner là, maintenant.” Tout en parlant, Ethan avait appuyé un peu plus sa lame, et une sphère de sang se forma sur la gorge de Laura, avant d’éclater et de couler le long de son cou.

“J’y viens, doucement ! Tu veux pas le calmer un peu, ton frère ? Comme je l’ai dit, je suis en sursis. Je devrais bientôt faire un choix que j’ai pas envie de faire, soit m’offrir à Reggie soit me faire saigner, comme tu l’as si poétiquement dit.”

“Et en quoi ça nous concerne ?”

“Parce que maintenant que je vous ai trouvé, j’ai une alternative. Je me casse d’ici avec vous, et je reviens plus jamais dans cette ville pourrie.”

“C’est mort.” Vociféra Ethan. “C’est la chienne de Mike !”

“Mike était un connard, on est tous d’accord. Mais vous aussi vous vous êtes rangés derrière lui et vous lui obéissiez, on est pas si différents.”

“Répète un peu ça, pour voir ?”

Le jeune garçon s’était brutalement avancé et avait empoigné par le col de sa veste Laura, tout en gardant la pointe de la lame sous sa gorge. Leurs visages se touchaient presque, et elle ferma les yeux pour éviter les postillons qui jaillissaient du jeune en furie.

“T’es pas innocente dans sa mort ! C’est de ta faute, tout ça !”

“Non mais tu t’entends parler ? J’étais même pas là ! Et c’est vous qui avez fait irruption dans le casino, j’ai rien à voir là dedans ! Pour une fois…”

“Ethan, elle a raison. Ca sert à rien de passer ta colère sur elle.”

“Ah, le grand frère, la voix de la raison, écoute le donc un peu.” Elle ne récolta qu’un regard noir de ses eux interlocuteurs.

“Ca ne veut pas dire qu’on accepte. Je suis pas contre te laisser dans les mains de Reggie et de sa nouvelle clique, pour la différence que ça fait avec l’ancienne.”

“Sauf que vous avez pas le choix, mes lapins. Si vous me tuez ici et maintenant, vous avez intérêt à courir vite, parce que le nouveau chef rêve de finir ce que le précédent avait commencé. Vous savez, il faut faire ses preuves, montrer qu’on peut faire ce que le boss d’avant a été incapable de réaliser. Et dans ce cas, ça implique votre scalp. Alors voilà ce que je vous propose. Dans quatre jours, soit on est tous les quatre loin de la ville, ou alors une foule de sauvages débarque dans l’immeuble et vous étripe.”

“Quoi ?” Ethan appuya encore un peu plus sa prise, arrachant un premier gémissement à la jeune femme.

“On pourrait aussi partir maintenant et te laisser te débrouiller.” Avança Nolan.

“Si vous êtes restés là plusieurs jours, c’est que vous avez une bonne raison de pas partir. Et j’espère vraiment que c’est pas parce que vous voulez détruire le groupe. Si c’est ça, vous êtes vraiment cons et je perds mon temps ici.”

“T’as vraiment une grande gueule pour quelqu’un dans ta situation. T’as envie que je te l’écarte encore plus, pour voir jusqu’où elle peut aller ?”

“Attend Ethan.” Intervint pour la première fois Axellan. “Elle a pas tort, on est coincés ici jusqu’à ce qu’Ivan se remette. Alors évitons d’attirer l’attention sur nous, ça ne peut qu’être bénéfique de l’avoir dans notre camp.”

Les deux garçons le regardèrent avec stupeur. Nolan se frappa le front du plat de la main, et Ethan alla même jusqu’à relâcher la menace qu’il pointait sur la jeune femme, qui en profita pour se soustraire à la lame.

“J’y crois pas… Comment tu peux être assez débile pour tout lui balancer, comme ça, sans même savoir si tu peux lui faire confiance ?”

Axellan réalisait tout juste qu’il venait de faire une erreur potentiellement très grave. En révélant leurs cartes, il avait donné à Laura l’avantage qu’elle désirait depuis le début de cette conversation. Il voulu se rattraper, mais le mal était fait. Leur interlocutrice afficha un regard suffisant.

“Au moins un d’honnête, ça fait plaisir. J’ai pas menti non plus, et je peux même vous aider. On est en train de récupérer toutes les ressources des rues avoisinantes. Je vais dire qu’il n’y a rien dans cet immeuble. Sortez pas, tous les autres vont être pillés, et faites vous petits. Je reviendrais dans quelques jours, et ça sera le moment de partir. On a un accord ?”

“Maintenant qu’on a plus le choix…” Commença Nolan.

“Tu penses pas vraiment accepter ça ?” S’exclama Ethan.

“On peut pas faire autrement. Si c’est un piège, on s’en sortira, et on saura à qui on le devra.”

“Moi, ça me va.” Leur sourit-elle.

Elle conclut l’échange ici et commença à s’éloigner promptement et de disparaître dans l’encadrement de la porte.

“Je la suis ?” Proposa Ethan. “Pour la surveiller.”

“Ca servirait à rien. Elle s’y attend, et elle peut à tout moment disparaître. C’est bien son meilleur atout.”

“Ecoutez, les gars, je suis désolé, j’ai pas réfléchi et…”

“Stop. Arrête toi là. T’as pas réfléchis non, et ça arrive, mais si tu ouvres ta gueule encore une fois je vais devenir violent.”

Coupé dans son élan, Axellan resta un moment bouche bée en plein milieu du salon, tandis que les frères s’asseyaient, rageurs, dans le canapé. Derrière la porte, il pouvait entendre des pas lourds descendre et une voix sonore se plaindre d’avoir fait chou blanc, ainsi que celle, plus délicate, de la pernicieuse Laura. Il attendit d’être certain qu’elles ne remontaient pas les marche pour retourner voir Emilie et lui annoncer avec un grand sourire qu’elle avait gagné le jeu. Elle sauta de joie, réclamant sa récompense. Il dû tempérer ses ardeurs en lui promettant qu’elle l’aurait au soir.