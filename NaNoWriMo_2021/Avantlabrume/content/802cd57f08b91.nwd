%%~name: Plus vrai que nature
%%~path: 282b92cffabbf/802cd57f08b91
%%~kind: NOVEL/DOCUMENT
### Plus vrai que nature

Tout était en place, les rôles bien définis. Emilie était restée à l’appartement avec la compagnie silencieuse d’Ivan. Les deux frères étaient parti activer leur dispositif, et Axellan se retrouvait seul avec Laura près du bâtiment qui servait de réserve de nourriture au groupe de malfrats. Ils attendaient de voir un brouillard, ou un signal quelconque avant de passer à l’action. Ils avaient oublié de deviser cette partie du plan.

Ils s’étaient dissimulés au troisième étage d’un bâtiment dans la rue opposée, d’où la vue était parfaite pour surveiller les allées et venues du casino. Elle l’avait guidé jusque là, déployant ses talents pour les emmener en toute discrétion aussi proche du repaire de l’ennemi.

Il en profita pour l’observer. Elle était belle, avec une sorte de grâce dans les traits qui lui donnaient un air plus âgé, malgré le fait qu’elle ne devait pas être beaucoup plus vieille que lui. Sa peau était incroyablement lisse pour quelqu’un qui avait dû avoir des difficultés à en prendre soin les derniers jours. Ses longs cheveux bruns étaient toujours attachés en un chignon à l’arrière de sa tête. Il se demandait à quoi ils ressemblaient lorsqu’ils étaient détachés. Une mèche rebelle passait de temps en temps devant ses yeux avant d’être soufflée sur le côté.

De fins bijoux paraient presque imperceptiblement ses oreilles et son nez. Ses profonds yeux verts étaient concentrés intensément sur sa tâche, dessinant un froncement léger qui creusait quelques rides sur son front. Elle se mordait la lèvre inférieure, témoignant de son stress quant à la situation, mais le reste de son corps ne tremblait pas;

Ses habits tranchaient avec son charme naturel assez brutalement. Elle était vêtu d’une chemise noire couverte par un sweat bleu marine de marque, ainsi que d’un pantalon sombre et serré. Ses chaussures étaient quelque chose que le jeune homme n’avait jamais vu, elles ne faisaient aucun bruit lorsqu’elle marchait malgré leur apparence de simples baskets, le genre qui couinait à chaque pas ou presque. Elle remarqua son regard sur elle et se tourna vers Axellan, qui détourna les yeux, embarrassé. Elle souffla, mais il ne put dire si c’était par amusement ou dépit. A la place, il reporta son attention vers la rue en contrebas.

Quelque chose attira son regard dans la rue qui se déroulait derrière le casino. Le fond de sa vision devenait flou, comme brouillé par quelque chose, et l’atmosphère pâlissait à vue d’oeil.

“Tu crois que c’est eux ?” Fit-il à son acolyte en lui donnant un petit coup de coude. “Je ne pensais pas qu’ils étaient capable d’en faire autant.”

“Non.” Lui répondit-elle, un brin de terreur dans la voix. “Ils sont pas supposés être dans cette rue-là.”

“Alors ça veut dire…”

Il n’acheva jamais sa phrase. Ils se relevèrent d’un seul tenant et se mirent à courir à l’étage supérieur, dans l’espoir de trouver une fenêtre avec une meilleure vue. Ils débarquèrent dans un appartement saccagé dont plusieurs vitres étaient brisées, et furent aussitôt transis de froid. Ils purent cependant voir clairement leur peur se réaliser.

La brume qui s’installait doucement n’était pas le fait de leurs compagnons. Déjà, des cris venaient d’en bas, et des silhouettes s’agitaient.

“Au moins, on aura pas à faire semblant.” Rit nerveusement Axellan.

“Tu crois que c’est le moment ? Il faut qu’on aille retrouver les autres !”

“Non, il faut qu’on monte ! Un point au dessus de la brume, pour être hors d’atteinte.”

“T’as vu jusqu’où elle monte ? Il n’y a pas de bâtiment assez haut pour y échapper à Jersey City, oublie ça.”

“Alors on fait quoi ?” Il commençait à paniquer, perdant le peu de confiance qu’il avait réussi à rassembler.

“Je t’ai dit, on retrouve Nolan et Ethan, on avisera après !”

Il n’avait rien à opposer, alors ils descendirent les marches quatre à quatre jusqu’à déboucher dans la rue. La plupart des gens effrayés ne leur jetaient pas un regard, occupés à courir vers le casino ou au contraire à le fuir. De fins filaments de brume commençaient à s’insinuer dans la rue, augmentant la tension ambiante. 

Quelqu’un, au milieu de la foule désordonnée, vint directement sur eux.

“Eh ben, Laura, on te cherchait, t’étais passée où ? Et c’est qui le gigolo ?”

“Lâche-nous, Bert. C’est pas le moment !”

“Et moi je crois que t’essaie de te barrer du clan. C’est dommage, Reggie voudrait beaucoup te dire quelques mots.”

Le dénommé Bert s’approcha et voulu se saisir du bras de Laura, mais avant qu’il ne puisse aller jusqu’au bout de son mouvement, celle-ci tourna rapidement sur elle-même avant de décocher un terrible coup de talon dans la mâchoire du fâcheux. Les os claquèrent bruyamment, du sang et un petit morceau de chair jaillissant d’entre les dents de l’infortuné. La bouche en sang, il tenta de dire quelque chose mais seul un gargouillis passa ses lèvres. Sans ménagement, la jeune femme l’étala d’un second coup de pied et il cessa de bouger. Axellan nota mentalement de ne pas la contrarier jusqu’à ce qu’il soit capable de se battre au moins aussi bien.

Personne n’avait remarqué leur petite altercation, et ils continuèrent sans plus d’interruption. A l’angle de la rue, ils manquèrent de percuter les deux frères, qui venaient également à leur rencontre.

“C’est pas nous !” S’écria Ethan. “C’est la brume, la vraie !”

“On avait compris.” Le coupa Laura. “Qu’est-ce qu’on fait ? Où on va ?”

“Il faut retourner à l’appartement, Ivan et Emilie sont en danger !”

“Trop tard pour ça Axellan, il est déjà plongé dans la brume. Aucun moyen d’y aller sans se faire tuer.”

“Et si on essayait les égouts ?” Proposa Laura, et tout le monde se tourna vers elle, interloqué.

“On a jamais essayé encore, et il y a des salles qui se ferment avec des portes de métal, ça devrait arrêter la plupart de ces choses.”

“T’es malade ? C’est l’idée la plus débile que j’ai jamais entendue ?”

“T’as mieux, gamin ? Si oui, dépêche toi de cracher le morceau !”

“On a pas mieux.” Trancha Nolan. “On y va, j’espère que tu sais ce que tu fais.”

“Moi aussi.”

Ils se précipitèrent vers la bouche d’égout la plus proche. La plaque de métal était solidement fixée, et demandait une clef pour pouvoir être déplacée.

“C’est une blague ?” S’exclama Axellan.

“Attendez, je connais un endroit par où on peut y accéder !”

Ils suivirent tous Ethan, sauf Axellan qui resta sur place. Après quelques mètres, les autres se retournèrent pour voir le jeune homme à genou se tenir la tête. La brume était désormais omniprésente, et ils ne voyaient plus le bout de la rue. Elle s’épaississait à chaque seconde, et déjà des sons d’horreurs leur parvenait, parmi lesquels des cris de souffrance.

Sans retenir quelques jurons, Nolan se précipita vers le retardataire.

“Aller, mec, dépêche toi !”

Il le secoua un peu pour l’activer, mais l’autre semblait catatonique. Il ne bougeait pas, le regard fixé sur le sol et la bouche figée dans une expression de douleur. A court de temps, l’adolescent lui fit passer un bras autour de son épaule et appela son frère. Il accouru, et ils commencèrent à supporter un Axellan à moitié conscient. En arrivant au niveau de Laura, Ethan lâcha sa charge et poussa la jeune femme à sa place pour ouvrir la route.

Il les conduisit dans une ruelle qu’aucun des deux autres n’avait remarqué, puis descendit une volée de marches de pierre jusqu’à la porte d’une cave qu’il défonça d’un coup de pied. Le vieux bois durement éprouvé vola en éclat, et il passa en baissant la tête. L’intérieur était bas de plafond, et ils étaient tous obligés d’avancer en étant courbés. Le pire était pour Axellan, dont le passage avait été difficile, mais qui était maintenant traîné sans ménagement.

Ils n’avaient pas encore rencontré de monstres, et la brume à l’intérieur de la cave n’était pas très dense, à leur grand soulagement. Ils n’avaient pas beaucoup de temps avant d’être coincés, et espéraient réellement qu’Ethan ne les emmenait pas dans une impasse.

Mais le jeune garçon était sûr de lui, et ils débouchèrent sur une grille de métal leur barrant le passage. Derrière, dans la faible luminosité du sous-sol, ils pouvaient apercevoir de nouveaux escaliers s’enfonçant encore plus profondément. Sans hésiter, Ethan s’approcha de la grille et la souleva à mains nues. Elle paraissait très lourde mais n’opposa aucune résistance à la pression de l’adolescent et le passage s’ouvrit.

“J’y suis jamais allé, mais je sais que ça descend dans les égouts.”

“Un jour, il faudra que tu me dises comment t’as fini par découvrir un truc comme ça.” Souffla Nolan.

“Désolé frérot, je fais pas tout avec toi dans ma vie.”

Sur ces mots, il alluma une lampe torche et s’enfonça sur les marches inégales. Axellan se sentait légèrement mieux, il se redressa et après s’être cogné au plafond, s’engagea à la suite de ses compagnons, la tête encore dans un brouillard figuratif.

Les marches étaient glissantes, et ils devaient se tenir à un mur recouvert d’une étrange substance collante. Malgré l’apparence de l’escalier, ils débouchèrent tout de même dans les égouts. L’odeur ne pouvait tromper personne. En l’absence d’entretien, de nombreux conduits s’étaient bouchés, et un parfum de pourriture flottait dans l’air. Mais c’est quelque chose d’autre qui retint leur attention.

Même si elle n’était pas aussi dense qu’à la surface, la brume s’était infiltrée jusque sous terre, et ses occupants avec. Partout, des claquements et râles inhumains s’élevaient. Ethan s’était pétrifié en bas des marches, et faisait signe de reculer. Nolan, qui fermait la marche, chuchota avec insistance.

“Ca descend déjà derrière nous, on a plus le temps de monter. Faut avancer et chercher une pièce close !”

Après un bref moment d’hésitation, Ethan s’engagea dans l’obscur tunnel. Sans lampe torche pour éclairer leur voie, ils avançaient à l’aveugle. Ils progressèrent très lentement sur quelques mètres, le vacarme de monstres hors de vue résonnant autour d’eux. Ce fut Laura qui éleva légèrement la voix pour se faire entendre.

“Mais en fait, s’ils évoluent dans le noir, c’est qu’ils n’ont pas besoin de lumière pour voir, non !”

Ethan se retourna pour lui dire de la fermer, mais elle le prit de vitesse et alluma sa lampe torche. Le faisceau illumina la tête déformée du zombie qui s’apprêtait à dévorer le visage de l’éclaireur du groupe. L’adolescent réagit au dernier moment, interposant le fourreau sur le trajet de la mâchoire décharnée. Le cadavre réanimé mordit violemment dans le cuir, y laissant des traces de dents indélébiles. Poussant de toutes ses forces, le garçon repoussa la créature, mais elle n’en démordait pas. A force de lutter, il parvint à l’envoyer dans le drain troublé.

A bout de souffle, et aveuglé par le rayon lumineux, il regarda Laura. Celle-ci se contenta de hausser les épaules.

“De rien. Mais on ne fait que s’handicaper en restant dans le noir, on devrait au moins voir quelque chose si on veut survivre.”

Ethan maugréa quelque chose, mais alluma tout de même sa propre lampe avant de continuer. L’argument de Laura eut encore plus de poids lorsqu’ils arrivèrent à un embranchement. Un autre tunnel rejoignait le leur depuis la gauche, et ils seraient tombés dedans s’ils ne l’avaient pas vu. Puis, en tournant sa lampe vers ce nouveau couloir, le faisceau fut très vite arrêté par un mur de chair.

Il n’était pas possible de même comprendre comment cette chose avait pu arriver là. Mais sur leur chemin était encastré dans toute la largeur du du tunnel une masse de chair grisâtre dégoulinante, qui se soulevait à intervalles réguliers. Une multitude d’yeux se fixa sur les nouveaux venus et quatre bouches informes s’ouvrirent pour cracher des jets d’un liquide vert foncé à l’odeur très forte.

Ethan fut tiré en arrière juste à temps par Laura, mais quelques gouttes tombèrent sur le bout de sa chaussure. Aussitôt le tissu se mit à fondre, sous les yeux médusés du quatuor. La victime de cette attaque se mit à secouer son pied pour faire sortir sa chaussure, mais heureusement pour lui, l’acide se consumait lui-même au moins aussi vite qu’il ne consumait les matériaux. Il avait eu beaucoup de chance, ne s’en sortant qu’avec les doigts de pieds exposés.

Laura pointa son faisceau vers le mur opposé à la créature. Les briques fumaient à l’endroit où le reste du jet avait atterrit, et beaucoup avaient disparu. A leurs pieds, le drain des égouts bouillonnait de façon menaçante.

“Si quelqu’un avait un doute, faut pas se faire toucher par ce truc. On fait demi-tour ?”

Nolan balaya le tunnel derrière lui de sa torche. Il discerna plusieurs silhouettes, dont seulement deux avaient une forme identifiables. Et toutes se dirigeaient vers eux.

“Nan, pas moyen. On peut que avancer. Pas moyen de contourner le lance-acide ?”

Fermant la marche, il n’avait pas pu voir la créature qui remplissait l’entièreté du tunnel. Laura secoua la tête.

“Il a l’air de réagir au mouvement, si on saute assez rapidement au dessus du canal ça devrait le faire. Ca te parait faisable, gamin ?”

Pour toute réponse, Ethan prit son élan, couru et se lança par dessus le drain. Il se réceptionna en grognant et dépêcha de se mettre à l’abri derrière le mur. Moins d’une seconde plus tard, un nouveau jet d’acide traversait l’air à l’endroit où il avait sauté.

“Ca dit quoi de ton côté ?” Lui cria son frère.

“On peut continuer, je vois rien.”

“Alors faut se magner, ça arrive derrière nous.”

Laura attendit quelques secondes, puis sauta à son tour. Elle se rattrapa plus élégamment sans perdre de temps. Une nouvelle fois, l’acide arrosa le vide. Nolan attendit qu’Axellan passe à son tour, mais le jeune homme s’était immobilisé, le regard dans le vide, mains sur les tempes.

“Mais c’est pas vrai ! On a plus le temps de jouer, bouge toi !”

Il donna sans ménagement un coup dans le dos d’Axellan, qui trébucha en avant, à deux doigts de se retrouver dans la ligne de mire de la chose immonde. Elle tira à nouveau, visant cette fois l’angle du mur. Le liquide mortel passa très proche des yeux du jeune homme, et agirent comme un électrochoc. S’agitant comme un automate, il sauta sans même prendre d’élan et traversa le drain avant de se réceptionner de l’autre côté avec une roulade.

Nolan eut à peine le temps d’arriver, en ayant échappé de peu aux créatures qui le suivaient et aux jets d’acide que le jeune homme s’éloignait dans le noir, insensible aux appels perturbés de Laura et d’Ethan.

“Mais qu’est-ce qu’il fout ?”

“On sait pas, il est arrivé et a baragouiné un truc, puis il est parti sans t’attendre.”

“Je crois que c’était ‘suivez moi’. Il nous fait quoi là, le coup de l’illumination ?”

“Au point où on en est, est-ce qu’on a beaucoup d’autre choix ?”

“T’es quand même pas sérieux ?”

“Aller, dépêchons-nous avant de le perdre !”

Nolan se mit à courir après leur compagnon qui se contentait de marcher, comme un somnambule. Il prenait chaque tournant avec une confiance déconcertante. Les trois autres se laissaient guider sans comprendre, guettant chaque recoin, craignant à tout moment qu’un terrible monstre surgisse des ténèbres. Mais en dehors de l’occasionnel zombie, leur chemin était libre.

Il ne leur fallu qu’une dizaine de minutes gorgées d’angoisse pour atteindre ce qu’ils étaient venu chercher : une porte de métal entrouverte, qui débouchait sur une pièce close. Axellan y entra sans hésiter, seulement retenu par Ethan qui le repoussa avant de pénétrer plus prudemment, l’épée à la main.

L’endroit était exigu, sa fonction obscure. Des sortes de bobines géantes étaient empilées dans un coin, et une sorte de table était montée à base d’une palette de bois vermoulu et d’un bidon vide. Mais ce qu’ils recherchaient avant tout était la sécurité, et l’épaisse porte répondait à leurs attentes. Ils y entrèrent sans plus attendre.

Il fallut les efforts combinés des deux frères pour fermer la lourde porte qui résistait. Elle se ferma finalement dans un grincement sinistre. La serrure était épaisse et vieille, mais pas encore trop rouillée. Par précaution, ils déplacèrent le bidon devant, au cas où cela ne s’avérait pas suffisant.

Laura s’était approchée avec méfiance d’Axellan. Il ne s’était pas arrêté de marcher et se rapprochait dangereusement du mur de la pièce. Elle tendit la main vers lui quand il percuta la façade de briques et s’écroula au sol. La jeune femme ne pu retenir un mouvement de recul, avant de se baisser pour l’aider. Elle tâta maladroitement son pouls. Il était encore en vie, à son grand soulagement. Seulement assommé par sa rencontre avec la pierre froide.

Elle dégagea le sac du jeune homme de son dos et le plaça sous sa tête pour en faire un oreiller de fortune, et revint vers les deux frères. Elle répondit à leur interrogation silencieuse en haussant les épaules., un geste qu’elle aimait définitivement. A son tour, elle pointa la porte.

“Ca devra bien tenir.” Lui répondit Nolan.

“Et quand est-ce qu’on saura qu’on peut sortir ?” Questionna Ethan.

Laura montra les volutes pâles qui noyaient la pièce dans une humidité latente. La sensation d’inconfort typique de la brume les habitait tous, cette impression de ne plus tout à fait être dans le monde réel.

“Quand il n’y aura plus de brume dans la pièce, on pourra jeter un oeil dehors.”

“Et si jamais elle vient en réalité des égouts, et qu’elle y et tout le temps présente ?”

“Attention, c’est beaucoup de spéculation pour un si petit bonhomme. Pour le moment, on peut rien faire d’autre qu’attendre. Peut-être qu’Axellan pourra nous éclairer sur son comportement, quand il se réveillera.”

Nolan acquiesça et commença à faire les cent pas dans la pièce. Au bout de deux cent, il regarda ses compagnons.

“Quelqu’un a un jeu de carte ?”