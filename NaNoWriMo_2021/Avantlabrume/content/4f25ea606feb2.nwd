%%~name: Périple 2
%%~path: f5e0b2b6cdf68/4f25ea606feb2
%%~kind: NOVEL/DOCUMENT
### Périple 2

Il avançait beaucoup moins rapidement que la veille en progressant à pied. La faible endurance d’Emilie les forçait à faire de nombreuses pauses, et les gourdes qu’il avait rempli se vidaient à vue d’oeil. Au bout d’un moment, il la fit monter sur son vélo quand elle fatiguait. Elle était bien trop petite pour atteindre les pédales, aussi Axellan s’occupait de le pousser. Ce qui ne réglait pas vraiment le problème de la vitesse, car à présent c’était lui qui fatiguait plus vite, les bras en compote.

Ils parlèrent un peu, de leur vie d’avant surtout. Axellan raconta ses études, tenta de vulgariser maladroitement le concept de génétique et de l’université. Emilie répliquait avec sa vie de tous les jours, ses jouets et les histoires de super-héros que ses parents lui racontaient pour l’endormir. Ils évitaient tout deux soigneusement d’évoquer l’avenir, ou même le présent. Il y avait quelque chose de rassurant à parler du passé comme s’il n’était pas révolu.

Malgré les très nombreux obstacles de la route, ils ne se retrouvèrent pas totalement coincé comme il l’avait été avec le bus. Un immeuble effondré en plein milieu d’un carrefour menaça de leur couper la route, mais Emilie repéra un passage un peu étroit qui permettait de le traverser sans encombre.

C’était très étrange de marcher dans la rue au milieu des fournitures de bureau, des meubles éventrés et des bibliothèques abattues. C’était comme se retrouver piégé dans une peinture surréaliste qui représenterait la fragilité de la société. Pendant qu’ils contournaient une cuisine arrachée à son étage, Axellan s’arrêta un temps dans l’espoir de trouver de quoi manger, mais les aliments qui n’étaient pas pourris avaient été écrabouillés dans la chute.

Ils s’arrêtèrent en début d’après-midi devant une petite supérette. Les fenêtres et la porte avaient été remplacées par des trous béants qui dévoilaient un intérieur obscur et humide. Le carrelage était mouillé, mais Axellan était incapable de dire si c’était dû à la brume de presque deux jours auparavant ou aux réfrigérateurs hors services dont la glace fondue suintait par les joints peu hermétiques.

Une quantité effroyable d’articles prenait l’eau et se ratatinait sur le sol, et plusieurs rayons avaient été vidés. Ceux contenant les produits de première nécessités avaient été particulièrement ciblé, ce qui réconforta Axellan dans l’idée que d’autres avaient survécu et erraient dans la ville. 
[Trouver de la bouffe ricaine]

Il vida également deux bouteilles d’eau dans ses gourdes et en emporta trois autres, se servant d’un torchon pour essuyer l’humidité qui les recouvrait. Il resta tout du long sur ses gardes, conscient qu’il se trouvait dans la situation typique où les survivants de l’apocalypse se font prendre en embuscade alors qu’ils se réapprovisionnaient. Il ne su s’il était déçu ou soulagé quand il retrouva Emilie à la sortie et qu’ils reprirent la route sans encombre.

Deux heures plus tard, ils atteignirent enfin une rue familière au jeune homme. Elle était aussi dévastée que le reste de la ville qu’ils avaient traversé jusque là, mais certains détails étaient reconnaissables entre mille. La bouche d’égout dont la plaque semblait trop grande pour l’ouverture, le panneau courbé par le passage d’un camion et qui était à présent couché, le kebab qui esquivait la fermeture depuis des années…

Le coeur d’Axellan battait à la chamade tandis qu’ils s’approchaient de l’immeuble où habitait Marius. Il craignait plus que tout de le voir réduit en poussière comme tant d’autres qu’il avait croisé. Quand il arriva enfin en vue du bâtiment, il sentit un immense soulagement de le voir tenir debout, apparemment en un seul morceau. Il commença à courir, prenant par surprise Emilie qui manqua de chuter de son vélo.

Il abandonna son véhicule sur le trottoir sans ménagement, attendant à peine que la petite fille descende. La porte était fermée, mais il connaissait la technique à force de faire irruption par surprise. Un petit coup dans le bas de la porte suffisait d’ordinaire à la déverrouiller, et cette fois ne fit pas exception. L’immeuble était notoirement connu dans le quartier pour son insécurité, uniquement compensé par les énormes verrous installés dans les entrées des appartements.

Sans même faire attention à Emilie, il se mit à monter les marches quatre à quatre jusqu’au deuxième étage. Il n’avait pas les clefs de l’appartement, mais si Marius était là il lui ouvrirait. Sinon… Il ne voulait pas y penser. Il ralentit en arrivant sur le pallier, l’appréhension lui serrant le coeur. La dernière marche fut la plus dure à prendre, au point que la petite fille eut le temps de le rattraper.

Les deux portes qui bordaient le couloir avaient été enfoncées, ou plus exactement déchirées, et quelque chose avait ravagé l’intérieur. Axellan commençait à craindre le pire pour son ami lorsqu’il arriva devant sa porte.