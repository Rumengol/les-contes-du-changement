%%~name: Cambriolage
%%~path: 282b92cffabbf/1d867bcea7565
%%~kind: NOVEL/DOCUMENT
### Cambriolage

Ils restèrent quelques heures par terre à monter le modèle. Les petites pièces avaient été éparpillées et le vaisseau n’était probablement pas complet, mais peu importait, il en avait la forme. Ils s’amusaient à le faire voler et cherchaient un autre modèle à monter quand un cri résonna dans le bâtiment.

D’un coup debout et alerte, ils firent tous deux silence et tendirent l’oreille. Des bruits de lutte légèrement étouffés leu parvenait de dehors. Axellan se précipita dans le hall du centre, inquiet pour Ivan. Là, le combat était plus audible et il put se guider au son tout en courant. Deux virages plus loin, ils débarquèrent sur leur compagnon aux prises avec trois autres personnes.

Dans la lumière pâlissante du soleil couchant leurs traits étaient difficile à saisir, mais ils n’avaient l’air que d’adolescents. A première vue, deux garçons et une fille, portaient des bonnets noirs et chacun un sac qui paraissait minuscule dans l’épaisseur et leurs gros manteaux. Ivan se débattait comme un beau diable, et l’un des garçons reculait maladroitement en se tenant le nez. Axellan ne perdit pas plus de temps à les observer et réagit à l’instinct.

Il dégaina son épée et commença à avancer vers les lutteurs, en criant de la voix la plus grave dont il était capable. Il fit de son mieux pour paraître convaincant, car si les autres voyaient au travers de son bluff, la situation pouvait rapidement dégénérer. Heureusement pour lui, les adversaires d’Ivan focalisés sur le combat ne virent rien d’autre qu’un fou furieux se précipitant sur eux avec lame effilée dans la main.

“Venez, on se casse !”

La fille avait joint le geste à la parole, tirant derrière elle le garçon blessé au nez. L’autre lança un dernier coup de pied à Ivan, qui le bloqua avec son avant bras, puis s’enfuit sans demander son reste. Axellan s’arrêta au niveau d’Ivan en soupirant de soulagement. Mais son compagnon n’était pas du même avis. Il se retourna vers lui, rouge de colère.

“Nos sacs ! Ils ont pris nos sacs !”

Le jeune homme mit quelques instants à procéder ce qu’il venait d’entendre, puis écarquilla les yeux.

“Merde ! Le flingue !”

“Tu avais un flingue ? Depuis quand ?”

“C’est pas vraiment à moi, c’était au père de…”

“On s’en fout ! Faut qu’on les rattrape.”

Les pas de courses s’étaient presque évanouis, il n’en restait que des échos mourant. Axellan secoua la tête et rengaina son épée.

“C’est trop tard, ils sont déjà loin et ils connaissent sûrement le coin mieux que nous.”

Une grosse veine avait gonflé sur le front d’Ivan, et il haletait. Pendant un instant, Axellan cru qu’il allait le frapper, les poings serrés et tremblant de rage du jeune russe encourageaient son raisonnement. Finalement, il respira un grand coup et se détendit, relâchant ses poings dont les jointures étaient devenues blanches. Lorsqu’il fut certain de ne plus courir de danger, Axellan osa parler à nouveau.

“Comment c’est arrivé ?”

“Je m’étais assis pour réfléchir un peu quand ils me sont tombés dessus par surprise.”

“Et tu les as pas vu ni entendu venir ? Je vois pas où ils auraient pu se cacher dans le couloir.”

“Ils sont peut-être sorti des aérations ou il y a des passages dans les boutiques.”

“Raison de plus pour ne pas les pourchasser alors, on aurait jamais pu les avoir.”

Axellan semblait satisfait de sa réponse. Ivan ne pouvait se résoudre à confesser que les trois adolescents l’avaient réveillé en pensant dépouiller un mort. Il était en train de dormir profondément quand ils ont malmené son corps jusqu’à le réveiller. Plutôt que de l’admettre, il retourna en direction de l’étalage de conserve du supermarché.

Leurs sacs avaient bel et bien disparu, avec une quantité importante de boîtes de biscuits et quelques conserves. Ivan renversa une table à moitié vide de rage, renversant les boîtes de métal dans un fracas infernal.

“Ils pourraient revenir, on ne sait pas à quel point ils sont dangereux. On devrait faire des tours de garde, ce soir.”

Axellan acquiesça et Emilie se cala sous la table renversée et commença à jouer avec son vaisseau fraîchement assemblé. La nuit s’installa rapidement, plongeant la grande surface dans une obscurité bercée par le clair de lune. Ils ne pouvaient pas la voir de l’intérieur du magasin, mais les allées du centre commercial avaient des fenêtres sur le toit. Les rais de lumière lunaire donnaient aux allées où des grains de poussières éclatant flottaient une atmosphère presque féerique.

Sans leur matériel pour réchauffer la nourriture, ils ne voulaient pas entamer les conserves et se contentèrent de [un truc froid et pas périssable]. Axellan décida de prendre le premier quart, et Ivan alla se coucher immédiatement après manger. Leurs sacs de couchages avaient aussi été volés, il se contenta donc du sol froid. Cela ne sembla pas le déranger puisque sa respiration devint régulière au bout de quelques minutes.

Emilie resta un petit peu avec Axellan, à regarder la lumière surnaturelle du hall. Ils conversèrent à voix basse.

“On va faire comment, si on a plus d’affaires ?” Demanda candidement la petite fille.

“Je sais pas. On va les chercher, et si on les trouve pas on aura qu’à se rééquiper avec ce qu’il y a ici. On trouvera quelque chose.” Il essaya de se montrer convaincu, mais il était complètement perdu.

“Pourquoi les gens ils sont méchants ?”

La question était sortie si naturellement qu’il ne su au début quoi répondre. Il oubliait parfois qu’Emilie était très jeune, et qu’il y avait certains concepts qui restaient hors de portée pour elle. Il réfléchit un moment à la réponse convenant le mieux.

“Quand les gens vivent des grands bouleversement, comme l’arrivée de la brume, ils commencent à ne penser qu’à survivre et ne se soucient pas de ce qu’il peut arriver aux autres. Alors ils deviennent méchants.”

“Mais pourquoi nous on est pas des méchants ?”

“Parce que… on résiste mieux au changement, et on arrive à garder notre humanité. C’est ce qui importe le plus, ne l’oublie pas.”

Axellan attendit de voir l’impact de son petit discours sur la petite, mais elle ne paru pas particulièrement mouvée par ses mots. Ils restèrent un moment assis sans rien dire, puis Emilie bailla. Elle se leva pour aller dormir dans un recoin du supermarché. Le jeune homme resta seul, à surveiller avec zèle l’allée déserte. Rien ne bougeait excepté les grains de poussières qui reflétaient abondamment la lumière de la lune. Le paysage monotone et l’heure tardive manquaient de l’endormir, alors il se leva et commença à arpenter les rayonnages pour se maintenir éveillé. Toujours attentif au moindre bruit, il prit lui-même soin de marcher le plus discrètement possible.

Il avait perdu de vue Emilie lorsqu’elle était partie dormir, mais la retrouva soudainement dans les bras d’un gigantesque ours en peluche. Elle s’en était fait un nid douillet, entourée des peluches survivantes. Un animal semblait être passé avant elle et avait fait un sort aux autres, qui étaient démembrées au sol. Laissant la petite fille à son somme, il continua sa ronde silencieuse.

Il n’avait jamais été dans un lieu abandonné avant. Certains de ces amis aimaient l’urbex, Marius avait parfois tenté de l’emmener dans ses expéditions qu’il trouvait sans intérêt. Il comprenait maintenant la majesté subtile de ces lieux mystérieux, que le temps a vidé de leurs occupants et privés de leurs fonction. Son ami lui avait un jour dit que dans les lieux qui étaient auparavant très fréquentés, ce n’est qu’après leur abandon qu’on pouvait déceler des décorations subtiles, des fissures dans les murs ou le sol qui s’effaçaient derrière un entretien quotidien pour d’ordinaire ne reparaître qu’au soir, une fois foulé de centaines de chaussures.

Il pouvait se faire à cette atmosphère hors du temps, où il était le seul être vivant, conscient à des dizaines de mètres, peut être des kilomètres à la ronde. Il passa près d’étals qui devaient abriter des pièces de viandes ou des poissons auparavant. La nourriture avait disparu, et la glace pilée avait fondu, dévoilant le fond d’un bac métallique tout à fait classique, bien loin des machines complexes qu’il s’était imaginé étant gamin.

Il fut tenté d’aller explorer la réserve, voir quel genre d’endroit ces zones hors-d’accès étaient, mais il s’était suffisamment éloigné, au point qu’il ne montait plus réellement la garde. Il remonta les allées jusqu’à l’endroit où Ivan était roulé en boule, et se reposa contre les tables. L’énergie qui commençait à s’accumuler diminua rapidement jusqu’à lui faire fermer les yeux. Encore une fois, il se remit en action et se dirigea cette fois vers l’entrée du magasin. Il y avait peu de chances que leurs agresseurs apparaissent d’autre part, alors il pouvait tout aussi bien veiller depuis l’extérieur de la grande surface.

Il déambula un moment, fit les cent pas devant les rangées de caisses, eut mal aux pieds et s’assit sur un banc. Il recommença plusieurs fois cette routine avant de s’en lasser. Il n’avait aucun moyen de savoir l’heure qu’il était mais il redoutait de réveiller Ivan trop tôt et de se faire incendier. Il pensait avoir aperçu une boutique d’horlogerie plus tôt dans la journée, à un autre endroit du centre commercial. Cela l’éloignerait de sa garde, mais s’il était rapide, il n’y avait pas de chance que quelque chose se passe mal.

Il commença à s’éloigner rapidement, essayant de retrouver son chemin dans le dédale obscur qu’était devenu le centre commercial. Il manqua plusieurs fois de se perdre, malgré la faible distance qui le séparait de la boutique dont il se souvenait. Seulement deux virages, et c’était la troisième échoppe de l’allée. Il ne s’était pas trompé, c’était bien un artisan qui vendait des montres comme des horloges. Le vieux type, des mécaniques. Tant mieux, la technologie était souvent moins fiable et demandait des réparations régulières, qui n’était plus vraiment accessibles dans ce nouveau monde.

La porte était fermée à clef, mais la vitre renforcée avait été à moitié enfoncée dans la vitrine, écrasant plusieurs modèles. Il se sentait un peu coupable vis-à-vis du travail de cet artisan qui devait être mort depuis, mais il commençait à se faire à l’idée de prendre des mains des morts. Il força sur la vitrine jusqu’à la déloger suffisamment pour se ménager un espace et entrer. L’intérieur de la boutique était particulièrement sombre, la lumière était arrêtée par l’épaisseur du verre composant les vitrines.

Il s’avança dans l’étroit magasin, cherchant une montre intacte. Il en trouva finalement une, l’étudia un moment et la passa au poignet, ravi de voir qu’elle lui allait. Elle semblait encore fonctionner, indiquant une heure treize du matin. Il avait du mal à savoir quelle heure serait acceptable pour réveiller Ivan, et se fixa sur deux heures. Avec un dernier regard pour la pièce obscure et une excuse à l’esprit de l’artisan, il repartit par là où il était venu.

Il retourna sans se presser en direction de la grande surface. Il sentait ses paupières devenir lourdes, et se demandait s’il n’allait pas avancer le moment de sa relève. Un cri l’extirpa de sa demi-rêverie. Il se précipita, soudain alerte, vers le supermarché. Il attendit de voir les trois silhouettes à nouveau autour du corps d’Ivan pour dégainer son épée. Cette fois, ils ne battirent pas en retraite, bien au contraire. Deux des adolescents se tournèrent vers lui, des barres de fer dans les mains, tandis que le dernier continuait de battre son compagnon.