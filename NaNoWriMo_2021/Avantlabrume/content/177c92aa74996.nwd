%%~name: Repos, repas et entraînement
%%~path: 282b92cffabbf/177c92aa74996
%%~kind: NOVEL/DOCUMENT
### Repos, repas et entraînement

Une routine s’était doucement installé. Le premier soir, Axellan avait retrouvé Emilie sous le lit d’Ivan, et l’en avait sorti en craignant pour sa santé, vu la proximité avec le sang. Il lui avait par la suite interdit de pénétrer dans la chambre du blessé, afin qu’il puisse convenablement se reposer.

L’appartement contenait un nombre impressionnant de boîtes de conserves et leur fournit des repas pour plusieurs jours avant qu’ils ne dussent aller en chercher ailleurs. Ethan et Axellan explorèrent l’immeuble les premiers jours, évaluant les différentes sorties possibles, ce qu’ils pouvaient récupérer dans les habitats abandonnés, et guettant d’éventuels fouineurs.

Nolan se reposait pendant ce temps, arguant qu’il était lui aussi blessé pour emprunter la deuxième chambre. Il passa la plupart des deux premiers jours reclus,  gardant les apparences lors des repas sans pouvoir tromper personne de ses yeux rougis.

Quelques jours plus tard, il semblait être redevenu lui-même. Ou du moins ce qu’Axellan imaginait de lui. Jovial, parfois trop tactile, et très souriant. Il se déclarait avoir recouvert et être prêt à la suite, qui n’impliquait toutefois pas de grand changements. Ivan était toujours souffrant, ne reprenant conscience que quelques heures dans la journée. Sa fièvre était fortement montée, malgré les médicaments [Y AURAIT QUOI ?] qui lui étaient administrés. Son état était plus qu’inquiétant, mais les trois garçons étaient totalement impuissants.

Ils mirent ces jours d’inaction à profit pour entraîner Axellan. Ethan avait commencé le premier jour, mais la rudesse de ses leçons et son absence de pitié avait rapidement découragé le néophyte. Nolan reprit l’entraînement avec une méthode plus douce mais toutefois efficace. En quelques jours, il n’avait pas physiquement changé, mais il comprenait mieux où placer ses coups, comment se positionner et de quelle manière il pouvait garder efficacement. Ethan avait accepté à force de suppliques de lui enseigner le maniement de l’épée, mais seulement quand son niveau à mains nues serait satisfaisant.

C’était curieux, de traiter des garçons aussi jeunes comme ses professeurs. Il n’avait pas l’habitude, et devait se faire violence pour prendre au sérieux les leçons et remontrances des adolescents. Son égo luttait violemment contre cette idée, mais il était incapable de rester en place face à la réalité : lorsqu’il ne se battait pas pour sa vie, avec la peur au ventre, il était tout simplement incompétent.

Ivan se remettait lentement, mais parvenait à rester éveillé de plus en plus longtemps chaque jour. Le soir du sixième jour, ils se réunirent tous dans sa chambre pour discuter. L’initiative était de Nolan, qui souhaitait que tous les cinq apprennent à mieux se connaître, et éventuellement déterminer une course d’action, s’ils tombaient sur un accord. 

Emilie était accoudée au bord du lit, à côté de la tête d’Ivan qu’elle s’amusait à tapoter de temps à autres. Ethan avait prit place sur le bureau qui ployait légèrement sous son poids et Nolan avait apporté les deux chaises du salon pour lui et Axellan, qui finissait de s’occuper de nettoyer la blessure qui suppurait lentement. Le jeune garçon tentait d’ignorer l’odeur avant de commencer à parler. Les draps avaient déjà été changés deux fois, et remisés dans un appartement pillé jusqu’aux fondations, mais la senteur de mort et de sang était à peine couverte par celle, tout aussi forte, des désinfectants. Ils ne pouvaient ouvrir la fenêtre, et espéraient donc que l’unique porte grande ouverte soit suffisante pour évacuer le plus gros de l’odeur. En prenant une grand inspiration qu’il regretta aussitôt, l’aîné des deux frères entama la discussion.

“Pour mieux nous connaître, je propose qu’on se raconte chacun notre histoire, ce qu’on faisait avant, tout ça. Bien sûr, vous racontez uniquement ce que vous voulez, si c’est trop dur pas la peine. Et puisqu’il en faut bien un, je me propose de commencer. Comme vous avez pu le remarquer à nos charmes indiscernables l’un de l’autre, moi et la brindille on est frères. Et jumeaux, en plus. Mais je reste quand même l’aîné. D’une minute treize, mais ça compte quand même, hein. On vient d’Ecosse, tous les deux. Enfin, nos parents, je crois qu’on est quand même nés ici, mais c’est même pas sûr. Moi et Ethan, on se souvient pas beaucoup de nos vieux. Ils ont cannés quand on devait avoir huit ans ou quelque chose comme ça. Et comme on avait pas de famille dans le pays, on a été dans un orphelinat. Puis un autre, et encore un. Je sais même plus combien on en a fait. T’as le compte ?” Ethan fit non de la tête. “Beaucoup. On s’intègre assez mal en société, comme vous pouvez le comprendre. On a essayé pourtant, au début. Mais au final on s’est cassé, parce qu’on avait pas vraiment besoin de la pitié des adultes pour vivre. On a vécu comme on pouvait, en indépendants au début. On s’en sortait pas trop mal, entre moi qui faisait la causette et Ethan les poches. Ouais, on était un plutôt bon duo. Après, il y a eu des… évènements, et on a fini par rejoindre le gang de Rhose. On peut dire qu’on s’est fédéré, même. C’est là qu’on a rencontré Lydia, et que je suis sorti avec elle. Ca a duré deux ans, puis la brume est arrivé et tout est parti à vau-l’eau, pas besoin de vous faire un dessin je crois. Et voilà qui nous sommes.”

Il enlaça son frère et se figea dans un sourire gênant pour ponctuer la fin de son récit. Il était définitivement satisfait de son effet, mais son auditoire le regardait avec scepticisme. Ivan était particulièrement circonspect. Parler lui était encore un peu douloureux, mais il posa tout de même sa question.

“De quels évènements tu parles ?”

“Ca, mon ami souffrant, c’est une excellente question, mais la réponse sera pour une autre fois. Peut-être dans cette soirée, qui sait ? Mais en attendant, j’aimerais en savoir plus sur vous.”

“C’est pas drôle, moi aussi je voulais savoir !” Rouspéta Emilie, mais Nolan était intraitable.

“Une histoire pour une autre, et je suis quasiment certain d’être celui qui en a le plus en stock, et je suis pas en soldes.”

Axellan se fendit d’un sourire puis se redressa. C’était à son tour, et il raconta sa vie bien moins mouvementée que celle des jumeaux. Comment il avait toujours vécu à New York, sa première année d’école de médecine ratée qui l’avait conduit à s’orienter vers une université de biologie, puis son expérience surréaliste de a première nuit, et enfin son périple qui le mena à rencontrer Emilie.

“Ca alors, j’étais persuadé que c’était ta soeur !” S’exclama Nolan, rejoint par son frère.

“Qu’est-ce qui vous fait dire ça ?” S’étonna Axellan.

“Elle est tout le temps collée à toi, t’es protecteur avec elle, et puis, il y a un petit air de famille, quand même.”

“Mais pas du tout !”

“Axellan c’est pas mon frère.” Coupa Emilie. “C’est plutôt comme mon papa, puisque j’ai plus mon vrai papa !”

Face au regard innocent de l’enfant, Nolan et Ethan ne purent retenir un fou rire, tandis qu’Axellan se prenait la tête dans les mains pour cacher son visage qui s’empourprait. Tout à son embarrassement, il ne sentit pas venir la tape dans le dos de Nolan.

“Eh bien mes félicitations, le celte ! C’est un beau bébé que t’as là !”

Puis il reparti dans un autre fou rire. Désireux de changer de sujet, Axellan chercha à détourner son attention.

“Tu m’as encore appelé le celte, et la première fois t’as voulu savoir d’où venait mon nom… T’as un truc avec les étymologies ?”

“Ouais, j’trouve ça fascinant. Rien qu’avec quelque chose d’aussi banal qu’un nom de famille, que tout le monde a, tu peux savoir d’où vient quelqu’un jusqu’à des dizaines de générations en arrière, c’est pas formidable ?”

“Oui, si tu le dis…” répondit Axellan, satisfait mais toutefois peu convaincu.

“Regarde, moi c’est facile, c’est mes parents qui étaient écossais. Mais toi, si t’as un prénom qui a des origines celtes, ça veut dire que le fondateur de ta lignée était celte, peut-être un noble ou même un roi !”

“Et moi ! Et moi !” Réclama Emilie. “Je viens d’où dans mes ancêtres ?”

Nolan marqua une pause pour démêler les mots et comprendre la phrase.

“Ca dépend, c’est quoi ton nom de famille ?”

“Je suis Emilie Ledusal !”

“Ledusal, j’ai jamais entendu ça, je pourrais pas dire. Je sais que les français aiment beaucoup mettre des ‘Le’ avant leurs noms de famille, mais c’est la seule piste que j’ai. Peut être que t’es française à l’origine, Emilie !”

“Waaah. C’est quoi la France ?”

Une fois la perplexité passée, Nolan entreprit d’expliquer les concepts de pays, de continents et de France à la petite, qui semblait peiner à comprendre malgré toute la bonne volonté du monde. Cela leur prit la plus grande partie de la soirée, Axellan et Ethan lui venant en secours quand ils le pouvaient, espérant assécher un jour l’intarissable flot de questions de la petite fille. Finalement, elle paru satisfaite et n’enchaîna pas une explication sur une nouvelle question. Ivan profita du léger moment de blanc pour s’exprimer d’une voix faible.

“C’était sympa, merci. Je suis sûr qu’on est tous devenu plus intelligents. Maintenant, si vous pouviez vous barrer, j’aimerais pouvoir dormir un peu.”

En s’excusant, le joyeux quatuor sorti de la pièce pour se diriger dans la salle principale pour terminer la conversation. Mais les yeux d’Emilie se fermaient, et ils décidèrent plutôt de la mettre au lit. Les trois garçons discutèrent encore un peu de tout et de rien, principalement de détails et de souvenirs qu’ils avaient de leur vie d’avant.

Les choses sans importances auparavant, qui relevaient désormais du rêve. Sortir paisiblement dans la rue, sortir son téléphone pour jouer à un jeu, écouter de la musique ou simplement parcourir les réseaux sociaux; manger trois véritables repas par jour; sortir dans la rue sans craindre pour leur vie. Ils parlèrent un peu d’amour, Axellan prodiguant quelques conseils bien avisés à la surprise des deux frères qui ne le soupçonnaient pas d’une telle maturité dans le domaine. Lorsque les baîllements devinrent trop communicatifs pour que la conversation s’entretienne, ils se souhaitèrent une bonne nuit et se couchèrent, chacun à sa place. Axellan avait eu le canapé, les deux frères dormaient ensemble dans la deuxième chambre, et Emilie, qui dormait déjà, aimait se rouler en boule dans le fauteuil.

Quelques jours encore passèrent ainsi, sans incidents. Puis les choses s’agitèrent à nouveau à l’extérieur.