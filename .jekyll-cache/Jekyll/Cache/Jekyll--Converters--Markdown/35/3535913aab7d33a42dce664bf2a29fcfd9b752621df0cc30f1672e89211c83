I"�<p>Quelque chose n’allait pas avec cette forêt. Nirléa et ses compagnons avaient été contraints de s’y poser à cause de la tempête qui s’annonçait, mais elle doutait à présent du bien-fondé de cette décision. Alors que la troupe d’elkens était malmenée au-dehors, ils s’étaient retrouvés dans une quiétude surnaturelle une fois abrités sous la cime. Les arbres étaient noueux et nombreux, si bien qu’une fois au sol, il leur était impossible d’apercevoir le ciel. Il régnait dans cette forêt une obscurité angoissante. Mal à l’aise, Nirléa sortit ses deux sabres, bientôt imitée par le reste de ses compagnons.</p>

<p>Il leur était impossible de savoir si la tempête s’était calmée, aussi décidèrent-ils de traverser le bois au sol, prudemment. Chaque bruissement d’aile les faisait tressaillir. Ils se sentaient tendus comme une corde d’arc, prêt à trancher la moindre menace. Le sol était inégal et compliquait leur progression, tandis que les branches au dessus de leur tête les emprisonnaient dans une cage ligneuse.</p>

<p>“C’est un miracle qu’on ait pu se poser sans y laisser des plumes, fit remarquer Algher.”</p>

<p>“La forêt n’était pas comme ça à notre arrivée. Il y a de la foutue magie là dessous, répliqua Kléon.”</p>

<p>Ils se replongèrent dans un silence maussade, aucun ne souhaitant renchérir sur cette lugubre évocation. Cette forêt ne semblait peuplée que d’oiseaux, tant la cime semblait vive et le parterre mort. En l’absence de sentier, ils progressaient erratiquement, effectuant de nombreux détours pour esquiver un tronc abattu, un cours d’eau à la profondeur trouble, un buisson épineux bien trop imposant… Les ailes des elkens, habituellement source d’orgueil et avantages incontestables, étaient devenues des handicaps gênant leur avancée.</p>

<p>Ils marchèrent pendant ce qui leur paru être des jours avant de tomber sur le premier cadavre. Un homme en armure lourde, aux couleurs délavées du royaume du Rénant. Algher l’examina, et estima qu’il ne devait pas être mort depuis plus de quelques semaines. Pourtant, si ce n’était l’extrême pâleur de son teint, il paraissait être mort depuis quelques heures à peine. Kléon grommela quelque chose sur les origines maudites de la magie, avant de s’éloigner avec Filsper à la recherche d’autres cadavres, les hommes du Rénant se déplaçant rarement seuls.</p>

<p>“Nous étions pourtant en Syrianie lorsque nous sommes entrés, fit remarquer Nirléa, et il n’existe pas de forêt couvrant suffisamment de territoire pour lier les deux royaumes.”</p>

<p>“Les lois naturelles telles qu’on les connaît n’ont visiblement pas cours ici, revint Kléon. Je ne suis même pas sûr que nous pouvons encore nous fier à nos sens. Enfin, venez tous, Filsper en a trouvé un qui respire encore.”</p>

<p>A ces mots, ils se précipitèrent dans une sorte de clairière, où une demi douzaine de corps étaient à moitié recouverts de racines. Agenouillé auprès de l’un d’eux, Filsper murmurait un charme destiné à raviver l’étincelle de vie qui subsistait encore en l’homme. Celui-ci entrouvrit à peine les yeux, ses lèvres frémirent mais aucun son n’en sortit. Nirléa l’arrêta d’un geste et, détachant sa gourde, lui versa de l’eau doucement dans la bouche. L’homme sembla savourer chaque goutte, puis, rassemblant ses dernières forces, regarda ses sauveurs.</p>

<p>“Elkens… achevez…moi.”</p>

<p>“Attend, avant ça, dis-nous quel est cet endroit ? Que vous est-il arrivé ?”</p>

<p>“La forêt… vivante. Méfiez-vous… elle…”</p>

<p>Il semblait sur le point d’ajouter quelque chose, mais fut pris d’une violente quinte de toux. Plusieurs fois il cracha du sang en tentant de parler. Kléon raffermit la prise sur son sabre, et le leva au dessus de sa tête.</p>

<p>“Ne t’en fais pas, ce ne sera pas douloureux.”</p>

<p>L’homme accueillit sa libération avec soulagement. Les elkens n’avaient pas obtenus les informations qu’ils désiraient, mais au moins leur priorité était fixée : ils devaient quitter cet endroit sans plus attendre. Gilan et Algher s’envolèrent, tentant de trouver une faille dans ce qui formait désormais un toit de branchages impénétrable. Plus inquiets que jamais, les jointures de leurs mains blanchies tant ils tenaient solidement leurs armes, ils reprirent leur marche.</p>

<p>Nirléa guidait le groupe, résignée à se soumettre à la volonté d’une entité dont la puissance les dépassait. Elle baissa ses lames et intima à ses compagnons de suivre son mouvement, espérant que ce geste d’apaisement plaiderait en leur faveur. Elle ne les rangea toutefois pas, mieux valait être prudent.</p>

<p>Ils croisaient encore, ça et là des cadavres, dont les plus anciens semblaient faire corps avec la forêt, leur peau devenue écorce, leur sang sève. La plupart étaient des soldats du Rénant, quelques autres ressemblaient à des chasseurs, ou de simples paysans. Ils avaient appris à les ignorer, ces hommes étaient morts et eux vivants, pour l’instant du moins. Nirléa fit le tour d’un épais tronc qui devait appartenir à un arbre majestueux, avant que celui-ci ne soit abattu par la foudre.</p>

<p>De l’autre côté, le silence la frappa avec la puissance d’un poing. Ses pas étaient les seuls à fouler le parterre d’automne, elle n’entendait plus s’entrechoquer les pièces d’équipement de Nielen, les psaumes que répétait sans cesse Kléon, même les horripilants bruissements d’ailes avaient cessés. Elle se retourna. Elle était seule. D’instinct, elle se mit en garde. Ça n’allait pas. La magie maudite de la forêt venait de l’enfermer, loin de ses compagnons. Elle n’avait probablement aucune chance de survie, mais elle était décidée à défendre chèrement sa vie. La voix fluette qui s’éleva derrière elle la déstabilisa.</p>

<p>“Bienvenue dans les profondeurs de cette épaisse forêt. Tu es un visiteur plutôt inhabituel.”</p>
:ET