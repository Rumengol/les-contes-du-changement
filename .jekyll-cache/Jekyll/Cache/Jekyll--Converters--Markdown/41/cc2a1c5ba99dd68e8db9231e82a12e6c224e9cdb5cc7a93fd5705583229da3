I"�<p>C’était la première fois que le village d’Obaki faisait appel à un traqueur de démons. Pourtant, cette région reculée était un terrain de jeu apprécié de cette engeance. Sans doute leur milice avait-elle été dépassée, songeait Akihiko alors qu’il s’approchait de l’entrée. Perdu dans l’épaisse forêt qui recouvrait tout le Sud de l’île, Obaki était presque impossible à trouver sans carte. Les villageois s’écartaient respectueusement sur son passage, et cela le mettait mal à l’aise. D’habitude, les gens se montraient méfiants envers les hommes vêtus comme des vagabonds. De toute évidence, ils ne voyaient pas suffisamment d’étrangers pour être hostiles.</p>

<p>Un vieil homme à l’air hagard s’approcha de lui. Son regard volait dans toutes les directions comme une mouche qui tenterait d’échapper à des mains rageuses. Soudain, il se fixa sur le mercenaire.</p>

<p>« 森の中で答えが出ました！ それの四〇から二、それに領主を保持します!»</p>

<p>Akihiko eut un brusque mouvement de recul et sa main se posa instinctivement sur son sabre. L’homme en face de lui n’avait plus toute sa tête, ses paroles n’avaient aucun sens. Lorsqu’il remarqua les regards amusés des villageois, il se raidit un peu plus.</p>

<p>« Oh, ne faites pas attention à Jin, fit une voix dans son dos. Personne ne comprend ce qu’il dit depuis qu’il a ramené son cube. Va-t-en, Jin, tu déranges notre visiteur.<br />
— Pamätajte! Pamätajte! vociférait l’illuminé en s’éloignant sans détourner le regard.»</p>

<p>Le guerrier se retourna pour faire face à un homme à la carrure impressionnante malgré l’âge qui se lisait sur son visage. Il s’inclina respectueusement avant de se présenter.</p>

<p>« Je suis Akihiko Torikami, je viens pour résoudre votre problème de démon. Votre message n’était pas des plus clairs, quelle est la menace exactement ?<br />
— Pardonnez-moi, seigneur Torikami…<br />
— Je vous en prie, je ne mérite pas ce titre.<br />
— Entendu seigneur. Nous mêmes ne savons pas vraiment, un jour quelqu’un disparaît, et le suivant il revient de la forêt en portant un cube de pierre plus grand que lui. Et il est changé à jamais, comme Jin. Aidez-nous seigneur, je vous en prie !<br />
— Pas de raison d’attendre, je vais me rendre dans cette forêt. »</p>

<p>« Seigneur ?<br />
— Oui ?<br />
— Vous vous dirigez vers le bois. La forêt est de l’autre côté.»</p>

<p>En quittant le village, le chasseur de démons fut heureux d’enfin retrouver un sol plat. La créature tourmentant les habitants de ce village avait l’air d’un esprit malin. Souvent farceurs, ils n’étaient pas très redoutables en combat et de la taille d’un enfant. Même sans repos, il était persuadé d’avoir l’avantage. Les troncs se massaient devant lui, rendant sa progression ardue. Enfin, après une longue marche, il déboucha dans une sorte de clairière, où d’énormes cubes couverts de mousses semblaient avoir été jetés comme des dés dans un sol meuble. L’endroit était parfait pour être un repaire de démon. Il avançait prudemment, main sur la garde de sa lame, toujours dans l’ombre. La première règle d’une chasse était de ne pas se laisser surprendre.</p>

<p>« Hey, psst, tu veux acheter des cubes ?»</p>

<p>Tant pis pour la première règle. Reprenant contenance, Akihiko relâcha sa lame et sorti de la pénombre. Il voulut dire quelque chose, mais ses mots restèrent bloqués au fond de sa gorge. Ce n’était pas un esprit malin ordinaire. Le géant qui se tenait derrière un cube couché était au moins trois fois plus grand que lui, recouvert de mousse et de feuilles. Sa coiffe doublait sa taille, et ses sortes d’antennes lui donnaient un air de papillon terrifiant. Deux lueurs vertes fixaient l’homme, la créature tendant un cube de la taille d’un homme.</p>

<p>« Qu’est-ce que tu es ?<br />
— Moi ? Un lutin.<br />
— Oui, évidemment.<br />
— Alors, tu achètes un cube ?<br />
— Oui. Non. Je ne suis pas venu pour ça.»</p>

<p>Les yeux de la créature virèrent au rouge et un grondement sourd s’éleva.</p>

<p>« Tu veux pas acheter mon cube ?<br />
— J-j’en serais ravi. Combien il coûte ? bafouilla l’homme en sortant sa bourse.<br />
— Pas grand chose. Une toute partie de toi, à l’intérieur. Tiens, voilà ton cube.»</p>

<p>Le cube de pierre effectua une parabole dans les airs, pour retomber pile sur le chasseur pétrifié par la peur. Il ferma les yeux en anticipation du choc, mais à sa grande surprise, rien ne se produisit. Le cube était aussi léger qu’un enfant, malgré sa taille. Au moment où il entra en contact avec la surface lisse, un flot de connaissances se déversa dans son esprit. Soudain, il savait. Tout ou presque, il avait la réponse à toutes les questions qu’il s’était un jour posé. Il comprenait des concepts qui n’auraient pas cours avant des siècles. Il compris les paroles de Jin. Il connaissait maintenant les secrets du maniement du sabre. Il voulut mettre en application ses nouvelles théories, il réalisa que ses doigts ne pouvaient s’empêcher de pianoter sur un clavier imaginaire. Le prix était beaucoup trop élevé pour quelqu’un comme lui. Il cria.</p>
:ET