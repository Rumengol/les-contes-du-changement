/*
 * Zoom Images, Get Date and Shadow
 * ========================================================================== */

(function() {
  /* variables */
  var shadow = document.getElementById("shadow");
  var images = document.querySelectorAll(".blog-content a img");
  var imageHeight = window.innerHeight - 20;
  var aside = document.querySelector(".sidebar");
  var asideImg = document.querySelector(".cover-hidden");
  var cover = document.querySelectorAll(".library-cover");

  /* events */
  shadow.addEventListener("click", resetShadow, false);
  window.addEventListener("keydown", resetStyles, false);
  window.addEventListener("resize", refreshImageSizes, false);
  aside.addEventListener("click", function(e) {
    ToggleImage(asideImg, e);
  });
  if (asideImg != null)
    asideImg.addEventListener("click", function(e) {
      ToggleImage(asideImg, e);
    });
  [].forEach.call(cover, function(img) {
    img.addEventListener("click", function(e) {
      newURL(img, e);
    });
  });
  /* functions */
  setDate();
  toggleImages();

  function setDate() {
    var currentYear = document.querySelector(".full-year");
    if (currentYear) {
      currentYear.innerHTML = new Date().getFullYear();
    }
  }

  function refreshImageSizes() {
    // select all images
    [].forEach.call(images, function(img) {
      // if image zoomed
      if (img.classList.contains("img-popup")) {
        img.style.maxHeight = imageHeight + "px";
        img.style.marginLeft = "-" + img.offsetWidth / 2 + "px";
        img.style.marginTop = "-" + img.offsetHeight / 2 + "px";
      }
    });
  }

  function resetShadow() {
    shadow.style.display = "none";
    resetAllImages();
  }

  function resetStyles(event) {
    if (event.keyCode == 27) {
      event.preventDefault();
      shadow.style.display = "none";
      resetAllImages();
    }
  }

  function resetAllImages() {
    if (asideImg.classList.contains("img-popup")) {
      asideImg.classList.remove("img-popup");
      asideImg.style.display = "none";
    }
    [].forEach.call(images, function(img) {
      img.classList.remove("img-popup");
      img.style.cursor = "zoom-in";
      img.style.marginLeft = "auto";
      img.style.marginTop = "auto";
    });
  }

  function toggleImages() {
    [].forEach.call(images, function(img) {
      img.addEventListener("click", function(e) {
        ToggleImage(img, e);
      });
    });
  }

  function ToggleImage(img, event) {
    event.preventDefault();
    if (event.ctrlKey) {
      window.open(obj.ref, "_blank");
    }
    img.classList.toggle("img-popup");
    if (img.classList.contains("img-popup")) {
      if (img.classList.contains("cover-hidden")) img.style.display = "block";
      img.style.cursor = "zoom-out";
      img.style.maxHeight = imageHeight + "px";
      img.style.marginLeft = "-" + img.offsetWidth / 2 + "px";
      img.style.marginTop = "-" + img.offsetHeight / 2 + "px";
      shadow.style.display = "block";
      img.title = obj.source + "\n Ctrl+clic pour voir l'image originale";
    } else {
      if (img.classList.contains("cover-hidden")) img.style.display = "none";
      img.style.cursor = "zoom-in";
      img.style.maxHeight = "100%";
      img.style.marginLeft = "auto";
      img.style.marginTop = "auto";
      shadow.style.display = "none";
    }
  }

  /*
   * Visite de l'image originale depuis la couverture
   * ========================================================================== */
  function newURL(image, event) {
    if (event.altKey) {
      var link = image.childNodes[1].childNodes[0];
      window.open(link.innerHTML, "_blank");
    }
  }
})();

/*
 * Aside Resize
 * ========================================================================== */

(function() {
  var aside = document.querySelector(".sidebar");
  var mainContainer = document.querySelectorAll(".content-wrapper");
  var switcher = document.getElementById("switcher");

  switcher.addEventListener("click", function(e) {
    e.preventDefault();
    e.stopPropagation();
    slide();
  });

  function slide() {
    aside.classList.add("transition-divs");
    aside.classList.toggle("aside-left");
    [].forEach.call(mainContainer, function(c) {
      c.classList.add("transition-divs");
      c.classList.toggle("centering");
    });
  }
})();

/*
 * Easter Egg
 * ========================================================================== */
(function() {
  var title = document.querySelector(".blog-logo");
  title.innerHTML = "Contes du Chargement";
  window.addEventListener(
    "load",
    function(e) {
      title.innerHTML = "Contes du Changement";
    },
    false
  );
})();
