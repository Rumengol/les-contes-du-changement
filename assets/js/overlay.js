let overlay = document.getElementById("overlay");
let cover = document.querySelector(".post-cover");
overlay.style.display = "flex";

overlay.addEventListener("click", function(event){
    event.preventDefault();
    cover.classList.add("transition");
    setTimeout(() => {
        cover.classList.add("cover-animated");
    }, 100)
    
    setTimeout(() => {
        cover.parentNode.removeChild(cover);
        overlay.classList.add("transition");
        setTimeout(() => {
            overlay.classList.add("overlay-animated");
        }, 100)

        setTimeout(() =>{
            overlay.parentNode.removeChild(overlay);
        }, 1100);
    }, 1000);
}, false);

