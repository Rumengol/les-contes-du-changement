---
bg: "owl"
layout: default
title: "Contact"
crawlertitle: "Contact"
permalink: /contact/
summary: "Formulaire pour me contacter en cas de question, proposition ou réclamation."
active: contact
lang: fr
---

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<h2>Contacter Rumengol</h2>

<p>Vous avez une remarque à faire sur le site ? Vous êtes un artiste souhaitant faire retirer votre oeuvre du site ? Ou bien mon style vous plaît et vous aimeriez que nous travaillions ensemble ? Envoyez moi un message :</p>

<script src='https://www.google.com/recaptcha/api.js' async defer></script>
<form action="https://formkeep.com/f/e92e30cf9cce"
   accept-charset="UTF-8"
   enctype="multipart/form-data"
   method="POST">
   <input type="hidden" name="utf8" value="✓">
<div class="form-box">
    <div class="textbox">
        <i class="fa fa-user"></i>
        <input type="text" placeholder="Entrez votre nom" name="NameBox" value="">
    </div>

    <div class="textbox">
        <i class="fa fa-clipboard"></i>
        <input type="text" placeholder="Quelle est la raison de votre prise de contact ?" name="ObjBox" value="">
    </div>

    <div class="textbox">
        <i class="fa fa-envelope-open"></i>
        <input type="email" placeholder="Entrez votre adresse e-mail (optionnel, pour être recontacté)" name="MailBox"
            value="">
    </div>

    <div class="msgblock">
        <textarea placeholder="Écrivez votre message..." name="BodyBox" rows="5" cols="50"></textarea>
    </div>

<div class="g-recaptcha" data-sitekey="6LepitYZAAAAAMTDeiFxPSjR6H-eRMS8bND_gWld"></div>
    <input type="submit" value="Envoyer">

</div>
</form>
<!-- form -->

<p class="getMail">Je n'aime pas les formulaires, montre moi ton adresse ! (cliquez ici)</p>

<script>
var mailTxt = document.querySelector(".getMail");

mailTxt.addEventListener("click", function(){
    mailTxt.innerHTML = "<a href='mailto:rumengol.pro@gmail.com'>rumengol.pro@gmail.com</a>";
    mailTxt.classList.toggle("getMail")
})
</script>
