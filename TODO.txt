- Afficher la couverture sur mobile
- Permettre de jouer un média
- Retirer l'alinéa des poèmes
- Améliorer le conteur de mot (regex)

[h3][url:https://www.artstation.com/eddie]Eddie Mendoza[/url][/h3]
[img:3403456]
[p]The best of the best, my favorite artist. I just love his style and his rich compositions more than anything else. My main regret is that I wasn't able to honour his work with my stories. He also made the background of the Netflix's animated show Tresse, which I recommend you to watch.[/p]
[/col3][col3]
[h3][url:https://www.artstation.com/stefankoidl]Stefan Koidl[/url][/h3]
[img:3403468]
[p]I don't know why, but his art is an incredible source of inspiration for me, so much that I could do a whole challenge only out of his works. Dark and creepy themes adept, he is a master of his trade. He recently launched his book, [url:https://www.kickstarter.com/projects/stefankoidl/the-art-of-stefan-koidl-artbook/]"the art of Stefan Koidl" that I'm sad not being able to afford.[/url][/p]
[/col3][col3]
[h3][url:https://www.artstation.com/wlop]WLOP[/url][/h3]
[img:3403474]
[p]Though most of his art is borderline NSFW, they have a wonderful style that leave me in awe. Not much to say except it was one of the few artists I was already familiar with before starting the very first challenge.[/p]
[/col3][/row]

[br]

[row][col3]
[h3][url:https://www.artstation.com/borisgroh]Boris Groh[/url][/h3]
[img:3403632]
[p]Rather dark, his style is quite unique and tantalizing. He inspired me multiple time and is a great artist.[/p]
[/col3][col3]
[h3][url:https://www.artstation.com/guweiz]Gu Zheng wei[/url][/h3]
[img:3403637]
[p]A style close to WLOP's, with very rich backgrounds and characters that seem almost alive. Every artwork tells a fantastic story of its own. [url:https://www.kickstarter.com/projects/1906838062/guweiz-the-art-of-gu-zheng-wei]"The art of Gu Zheng Wei"[/url] is his artbook of awesome quality.[/p]
[/col3][col3]
[h3][url:https://www.artstation.com/jocelincarmes]Jocelin Carmes[/url][/h3]
[img:3345170]
[p]Another incredible style, even a mere cloud study can make imagination going wild. I feel like all his work is an invitation to a new realm of dreams that I love to get lost in.[/p]
[/col3][/row]
[/center]The Gods aren't kind to those who seek to be their equal. A very long time ago, our ancestor reached immortality in his quest of godhood. Feeling threatened, the Gods punished his offense by destroying the archdukedom. They sentenced half of his people to an eternal travel through the endless sea, and the other half were sent to expiate the sins of their sovereign in the Divine Ordeal.

And so for centuries we have been trapped in this desert of gravel and black sand, damned people chasing the seals that will set us free under the grey sky. We are nothing like the frail folk that landed there however. Our skin took the monochrome hue of the place, and our minds are stronger than ever, determined to put an end to our torment. On the back of the titansects, the Ultimate Armada will yield to none and overcome any obstacle on its course.

Skim the books of the Ordeal, learn about our history and get your mount ready. We march towards the seals and they shall break us free!