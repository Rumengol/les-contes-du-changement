---
bg: "away-from-me"
layout: post
title: Away from me
summary: A collapsing metropolis sees the birth and shattering of a titanic friendship.
source: “Someday” par Alena Aenami
ref: https://aenamiart.artstation.com/projects/Ya4WAb
date: 2022-05-23
categories: posts
tags: []
form: poem
type: Historiette
author: Lucafé
---

Loitering alone while all the world is laying livid
Last sunday lights, we're larking above the electric lines
Long ago you let me know that all that is lit was lost

Creepy skyscrapers crawling over the horizon
Colossal crowded graves of ore and glass
How come we didn't kick the bucket climbing theses crumbling corpses of coal

Filthy strolls on the roofs, filled with fragments of flashbacks
Funny how the fumes from the funnels fucked up your face
From now on your features are frozen in this fixed frown

Following the trail along the rails like always
But this way isn't okay, dismay and pain waits ahead
Walking straight ahead we may only cross the trains

The flickering light at the end of the tunnel might be bright
But its trembling tune is filled with fire and fright
Falling for it we run forward and fearlessly defeat the stiff corpses of foil
Can you recall ?

These strange nights are naught but a tale of another time
When the constant stroll of us two was still a pact
You spitted on our time together and departed to a better city

I understand that this town is underwhelming
But I can't stand how you wounded me when you walked away without a warning
Where are you now when I want your wit and will and warming wisps of what we used to be

Still sinking as a cryptic critical cynical clinical condescending curmudgeon
Crying over spilled spoiled superfluous milk is never cathartic
Stuck in this city where the cold clicks of the ticking clocks commands every activity

Excuse me if I'm alarming but alas our hearts won't fuse back that easily
You broke in half a solidarity meant for ages and left me avid for another ally
Knowing that only you are that exact one who can satisfy my harm

Too late. Out of luck. Can't cure that colossal cut even if you come back
Still I'm wondering where, who and how are you now
Take me back and make me escape this apocalyptic landscape
I will not forgive you but I won't forget who we were back then