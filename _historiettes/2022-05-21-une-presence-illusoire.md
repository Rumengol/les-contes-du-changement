---
bg: "une-presence-illusoire"
layout: post
title: Une présence illusoire
summary: Paulin se sent en danger. Depuis quelques jours, il est suivi.
source: “Rainmaker” par Sandeep Karunakaran
ref: https://sanskarans.artstation.com/projects/RyE3v
date: 2022-05-20
categories: posts
tags: []
type: Historiette
author: Lucafé
lang: fr
---

*Click*

— Ça enregistre. On vous écoute. Prenez votre temps.

— Ok. Il a commencé à me suivre il y a une semaine et demie. C'était le mercredi soir, je m'en souviens bien parce que les poubelles étaient sorties sur le trottoir. Je rentrais chez moi après le boulot, je travaille dans le *Major Sport* pas loin du ciné, et comme toujours je rentrais chez moi à pied. Il pleuvait. Je me suis arrêté dans mon hall pour vérifier s'il y avait du courrier, et c'est là que je l'ai aperçu pour la première fois, à travers la porte vitrée. Il se tenait de l'autre côté du carrefour au bas de mon immeuble, tourné vers moi. Il bougeait pas. Je voyais mal son visage, caché à la fois par la pluie et son parapluie, mais son immobilité me donnait l'impression qu'il me fixait. Il était gênant mais je me suis pas posé plus de questions, juste un weirdo comme il y en a tant, avec son parapluie et son manteau long et noir au col trop haut. Alors je suis rentré chez moi. Je l'ai vite oublié. Je l'ai croisé de nouveau trois jours après. Je passais au café retrouver un ami en fin d'après-midi, et un moment, on discutait tranquillement, j'ai levé la tête et il était là, à travers la vitre, de l'autre côté de la rue, tourné vers moi de nouveau, je l'ai reconnu. J'ai eu un moment d'absence, mon ami m'a rappelé à la raison, je ne l'y ai plus vu. Ni du reste de la journée. Le lundi d'après, je faisais du rayonnage quand j'ai reconnu sa silhouette dans mon champ de vision. Il était collé à la vitre. Sans exprimer quoi que ce soit, je me suis simplement tourné dans l'autre sens et suis parti me réfugier dans la réserve. Une collègue qui me voyait plus dans les rayons est venue me chercher. Je lui ai expliqué qu'il y avait un stalker étrange avec un long manteau glauque de pervers qui me suivait depuis quelques jours, mais quand j'ai voulu lui montrer, il était parti. Je suis rentré en refusant de me retourner, à aucun moment. J'avais trop peur de confirmer sa présence derrière moi. Et quand je suis enfin parvenu jusqu'à chez moi, j'ai voulu ouvrir une fenêtre pour aérer. Il était là, en bas, dans la rue, avec son stupide manteau et son stupide parapluie alors qu'il ne pleuvait même pas, et surtout il était tourné vers ma fenêtre ce creep. J'ai immédiatement fermé les volets. Mardi je ne l'ai pas vu. J'étais en stress constant, je vérifiais toujours mes angles morts, l'autre côté des vitres, surtout les teintées, les cachettes, les ruelles, nulle part. Peut-être qu'il sentait que j'étais vigilant. Alors le soir j'ai baissé ma garde. J'ai laissé mes volets ouverts, d'habitude je les laisse ouverts comme ça le soleil m'aide à me lever. Le mercredi matin quand je me suis réveillé, il était là, collé à la fenêtre. Au troisième étage ! Comment c'était possible ? Il était plus proche que jamais cette fois. J'ai pu voir son teint blanc comme un fantôme et ses yeux vides. Le reste de son visage était caché par un col roulé remonté jusqu'aux joues. J'ai détourné immédiatement le regard, mais quand j'ai osé me retourner, il n'étais plus là. Je-

— Excusez-moi, je comprends qu'il s'agit d'une situation stressante, vous n'êtes pas le premier, mais si vous voulez que l'on retrouve cet individu, il va falloir nous donner des informations concrètes. Si vous commencez à mélanger le réel et votre imagination, cela ne facilitera le travail pour personne.

— Mais je l'ai vu ! Il était là ! Il a grimpé sur les fenêtres, à la gouttière, je ne sais pas, mais il était là ! Vous ne me croyez pas ?

— ...Continuez, je vous en prie.

— J'ai à nouveau fermé tous les stores. Je me suis dépêché d'aller au travail. Là-bas on m'a dit que j'étais pâle comme un linge. J'y suis resté pour la journée, évitant au maximum de penser à cet inconnu qui me suivait et me scrutait partout.

— ...C'est tout ?

— Non, mais ça ne sert à rien, vous ne me croirez pas.

— Essayez tout de même. Je ne suis pas là pour vous juger, seulement pour vous aider. Plus on aura d'informations mieux on pourra retrouver cette personne.

— J'ai pris deux jours de repos. J'ai pris ma voiture et je suis passé voir ma mère. Elle habite dans sa maison, en campagne, à trois heures de route. J'essaie de la voir régulièrement pour qu'elle ne se sente pas trop seule, donc non seulement ça lui ferait plaisir mais aussi ça me rassurerait de changer un peu d'ambiance. De fuir le, l'individu là. Il pleuvait sur la route, mais un moment j'ai vu une silhouette en noir sur le bord de la route, en pleine route de campagne. Vous allez me dire que c'était un rêve, que n'importe qui pourrait sortir avec un parapluie, que c'est la base du bon sens, mais c'était lui. Je sais que c'était lui. J'ai vérifié dans le rétro, il fixait ma voiture qui partait. J'ai failli terminer en aquaplaning. Quand je suis enfin arrivé chez ma mère, tout s'est bien passé. Elle était contente de me voir, elle m'a préparé ma chambre, à dîner, et elle m'a raconté les petites histoires inintéressantes du bourg comme elle n'a plus grand-chose d'autre pour s'occuper. Je lui ai proposé de prendre un chien, mais comme toujours elle a refusé, elle a pris ça comme une insulte comme si je lui disais en face qu'elle était incapable de se défendre. J'ai insisté, plus que d'habitude, pour pas qu'elle soit toute seule vous voyez, surtout-

— Excusez-moi de nouveau, pouvons-nous nous concentrer sur les faits qui nous intéressent ?

— ...*surtout* si le cinglé décide de s'en prendre à elle aussi. Vu qu'il m'avait suivi, qu'il était sur le bord de la route. Il pouvait être n'importe où. Et même si je ne l'avais pas croisé depuis la route, plus de deux heures avant d'arriver, il pouvait très bien être là aussi. Ma mère a remarqué que j'étais stressé et m'a vite envoyé me coucher, avec tout le surplus de bienveillance qu'une mère peut avoir. J'ai pas réussi à dormir. Là-bas les fenêtres de l'étage ont pas de stores, seulement des rideaux. Un moment j'ai cru voir sa silhouette à travers. Et en me précipitant dessus, il était pas là. Mais me regardez pas comme ça ! Je ne suis pas fou !

— Écoutez, vos histoires ridicules ne nous intéressent pas, vous nous faites perdre du temps. Il y a d'autres gens qui ont besoin de nos services et vos mauvaises blagues n'amusent personne.

— Mais attendez ! J'ai une preuve ! Regardez, là, sur mon portable ! J'ai une photo ! Sur la fenêtre, le lendemain matin, écrit à l'envers dans la buée, là ! "Paulin je dois te parler" Il connaît mon nom le gars, il m'espionne ! Non ne partez pas, je vous assure que c'est vrai ! Ces fenêtres on peut pas les ouvrir en grand à cause des barreaux, vous voyez bien, donc j'aurais pas pu l'écrire moi-même. J'ai rien dit à ma mère pour pas l'inquiéter, et je suis parti en début d'après-midi. C'était hier, donc. Il pleuvait, mais alors des trombes. Même avec les pleins phares et les essuie-glaces à fond on voyait rien, et j'avançais moins vite pour pas m'écraser au milieu des routes de campagne. Et au loin j'ai reconnu sa silhouette, cette fois il était pas au bord mais au milieu de la route, et me fixait toujours. J'ai voulu piler mais j'ai perdu le contrôle, la voiture a refusé de freiner et je lui ai foncé dedans. Dès que j'ai pu m'arrêter je suis sorti, paniqué, c'est pas parce que ce type me fichait la trouille que je tenais à avoir commis un homicide involontaire. J'ai cherché partout autour, aucun signe du gars. Il avait disparu. Mais je l'avais bien vu, c'était pas une hallucination. Je le sais. Et quand je suis revenu à ma voiture, j'avais laissé la portière ouverte, il y avait ça sur le siège conducteur. Ce bout de papier avec "Pardonne-moi Paulin" dessus. C'est pas hyper en état à cause de la pluie battante, mais ça reste entièrement lisible. Ce type connaît mon nom, visiblement me connaît personnellement, et je sais pas qui c'est ! Il me rend fou ! J'étais terrifié, j'ai vérifié qu'il était pas planqué dans la voiture, sous un siège, dans le coffre, n'importe où, puis je suis rentré. Je l'ai pas vu depuis hier. Je crois que c'est tout.

— Bon, récapitulons. Un homme habillé d'un long manteau noir, avec un col roulé lui cachant la moitié du visage et un parapluie. Plutôt petit ou grand ? Mince, large ?

— Euh, taille moyenne qui tend sur le petit. Pas très large non.

— Un teint pâle, des yeux clairs... et suffisamment en forme pour pouvoir grimper aux murs. Il n'y a personne que vous connaissez qui pourrait s'apparenter à cela ? Un ami, un collègue ? Ou tout simplement quelqu'un qui vous voudrait du mal ?

— Pas à ma connaissance, je connais personne d'aussi livide. Et puis je connais pas tant de monde tout court. J'ai pas d'ennemis, je suis pas du genre à m'embrouiller avec du monde. Je comprends pas qui ça peut-être.

— Et dans vos anciennes connaissances, quand vous étiez plus jeune ? Ou du côté de votre famille peut-être ?

— Non, je ne vois pas. Du côté de ma mère ils correspondent pas du tout. Et du côté de mon père, j'ai plus trop de nouvelles mais aucun ne correspond à ça, ni ne m'en veut. C'est pour ça que j'ai fait appel à la police, parce que je- **Là ! Là regardez !**

— Qu'est-ce que vous, la vitre ?

— **Oui, là ! À l'envers, on peut lire "Je suis désolé Paulin" dans la buée, voyez par vous-même ! Je ne suis pas fou !**

— Bon, c'est moi que vous voyez désolé, mais je n'en peux plus, vous nous avez fait perdre un temps considérable, vous pouvez dire à votre complice dehors que ce n'est pas drôle, et aussi de ne plus venir nous déranger pour quelque chose d'aussi ridicule. On peut pas se permettre de déranger l'ordre comme ça sans raison. Au pire, parlez-en à votre psychiatre je ne sais pas, mais pas à nous. Venez avec moi.

— Quoi ? Mais vous voyez bien ce que je vois, je vous promets que je ne suis **pas fou ! Lâchez-moi ! Je refuse de sortir d'ici tant que je n'aurais pas rempli un dossier de plaintes ou je ne sais quelle paperasse ! Lâchez-moi !**

&nbsp;

*bzzt*

&nbsp;

— ...'fectivement, il a fait tomber ses papiers quand je l'ai fait sortir. Qu'est-ce que c'est avec, encore une note déchirée ? Mais il avait tout prévu, ma parole ! "Paulin, j'aurais voulu rester plus longtemps. Excuse-moi. Prends soin d'elle." Ce que les gens s'inventent pour un peu d'attention... Eh, ça enregistre encore, ça ?

*Click*