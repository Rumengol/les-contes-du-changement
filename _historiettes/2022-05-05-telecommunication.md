---
bg: "telecommunication"
layout: post
title: Télécommunication
summary: Deux boîtes vocales se répondent. La tension, puis la peur, montent.
source: > 
    [Sans titre] par Lucafé
ref: "#"
date: 2022-05-05
categories: posts
tags: []
type: Historiette
author: Lucafé
lang: fr
---

Bienvenue sur la messagerie automatique du zéro*frrrrrrrrrrrrrrrrrrrr*ouze, nous vous invitons à laisser un message après le bip sonore. *Biiiiip*
Bonjour, je me permets de vous rappeler après les trois messages consécutifs que vous nous avez laissés, je pense qu'il s'agit d'une erreur de numéro, merci de corriger cela, au revoir !
*Bip.*

*Frrrrrrr*oir, euh, Mélanie, je*frrrrrrrrr* pas trompée de numéro, c'est bien ta voix, je la recon*frrrrrrr*, réponds à mes appels s'il te plaît, j'ai beso*frrrrrrrrr* t'entendre par*frrrrrrrrrrrrrrrrrrrrrrrrr*-
*Bip.*

Bonjour, écoutez madame... ou monsieur, ça fait deux fois maintenant, je ne suis pas Mélanie, vous avez dû vous tromper de numéro, ou alors on vous en a fourni un mauvais, je suis désolée, je ne peux pas plus pour vous, maintenant ne nous appelez plus, merci.
*Bip.*

Bons*frrrrr* Mélanie, merci pour ta réponse, *frrrrrrrrrrrrrr* bien dit "nous" ? C'est *frrrr*midable, je ne savais pas *frrrrrrrrrrrrr* quel*frrrrrrrrrrrrrrrrrrr* est gentil ? Comment s'appelle-t-il ? Ou elle ? Vo*frrrrrrrrrrrrrr* déjà des *frrrrrrrrr*fants ? Rappelle-moi s'il te plaît Mélanie. J'attends.
*Bip.*

Méla*frrrrrrrrrrrrrrr*quoi tu ne me laisses plus de messages ? J'ai*frrrrrrrrrrrrrrrrrrr* en ai besoin, d'entendre ta voix. Je t'ai*frrrrrrrrrrrrr*ie. Rappelle-moi.
*Bip.*

Bonsoir Mélanie, s'il te plaît, j'ai besoin d'entendre à nou*frrrrrrrrrrr* son de ta voix. S'il te plaît. Fais-le pour moi, Mélanie. Fais-le pour *frrrrrrrrrrrrrrrrrrrrrrrrrrrrrr*-
*Bip.*

Ce petit jeu a assez duré. Vous vous croyez drôle ? Je ne vois pas ce qu'il y a de drôle à harceler des gens au téléphone à deux heures du matin. Pour la dernière fois, n'appelez plus sur ce numéro.
*Bip.*

Je ne comprends pas, *frrrrrrrrrrrrrrrrrrrr*nie, pourquoi veux-tu m'évi*frrrrrr* à ce point ? Qu'est-ce que je *frrrrrrr*fait ? Ne me reje*frrrrrr* pas s'il te plaît, ne *frrrrrrrrrrr*as le cœur de*frrrrrrrrrrrrrrrrrrrr*-
*Bip.*

Mélanie, Mélanie, Mélanie, Mélanie, Mé*frrrrrrrrrrrrrrrrrrrrr*, Mélanie, Mélan*frrrrrrr*lanie, Mélanie s'il te plaît j'ai besoin de t'enten*frrrrrrrrrr*-
*Bip.*

Tu ne te souviens pa*frrrrrrrrrrrrrrrr*ment sur la glace, il avait n*frrrrrrrrrrrrrrrrr* la nuit. Et *frrrrrrrrrrrrr*vais couru dehors sans *frrrrrrrrrrrrrrrrrr* et surtout sans ton bonnet violet et jaune, avec le pompon, tu sais le *frrrrrrrrrrrr* déchiré qui tenait plus qu'à quelques bouts de ficelle, tu me *frrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr* d'accord ?
*Bip.*

Arrêtez tout de suite ce petit jeu. Je ne sais pas pourquoi vous faites ça, comment vous faites ça, en fait je m'en fous mais arrêtez ça tout de suite, vous faites peur à ma femme, votre Mélanie n'existe pas, si jamais vous rappelez une seule fois nous n'hésiterons pas à prendre des mesures, compris ? Il est trois heures quarante du matin, vous vous rendez compte que nous sommes en pleine nuit, la dernière chose dont quiconque ait envie c'est de se faire harceler au téléphone, d'accord ? *Non Charlotte retourne te coucher. Tout va bien, papa dev*-
*Bip.*

Méla*frrrr*, merci de la rép*frrrrrrrrrrrrrrrrrrrr*tait ton mari ? Il s'appelle comment ? Il est beau *frrrrrrrrrr*, grand ? Raconte-le moi, parle moi de *frrrrrrrrrrrrrrrrrrrrrrrrrrrrr* un enfant ? Je suis si heureuse pour vous, t*frrrrrrrrrrrr*senteras d'accord ? Je veux con*frrrrrrrrrrrrrr*etits-en*frrrrrrrrrrrrrrrrrrrrrrrrr*-
*Bip.*

Mélanie, laisse-moi entendre ta voix encore une f*frrrrrrrrrrrrrrrrr*, j'en ai besoin, tu sais à mon *frrrrrrrrrrrrrrrrrrrrrrrrrrr* facile, j'en ai besoin j'ai besoin de t'entendre Mélanie, Méla*frrrrrrr*-
*Bip.*

S'il te plaît raconte-moi ton mari s'il te plaît raconte*frrrrrrrrrrr*es enfants, s'il te plaît Mélanie s'il te plaît Mélanie je n'attends qu*frrrrrrrrrrrrrrr*uste ta voix ce n'est pas grand-chose s'i*frrrrrrrrrrrrr* plaît...
*Bip.*

*Biiiiipibiiiiipibiiiiipibiiii*-  
— ARRÊTEZ ! ARRÊTEZ ! Arrêtez, arrêtez, pitié, je n'en peux plus, je ne sais pas pourquoi vous faites ça, c'est quoi que vous voulez, c'est notre argent, c'est notre maison, c'est une information, une photo, vous voulez quoi, oui j'ai une famille, un mari, il est beau, il est pas si grand mais il est beau, c'est ça que vous vouliez entendre, c'est ça que vous vouliez, vous voulez me voler mon mari, ma famille ?

— Allô ? Mélanie ? En*frrrrrrrrrrrrr*lanie tu me réponds je suis si *frrrrrrrrrrrrrrrrrrr*pas imaginer à quel point *frrrrrrrrrrrrrrr*euse de t'entendre ! Il y a tant de *frrrrrrrrrrrrrrr* que tu dois me raconter enco*frrr*-

— Vous voulez m'entendre parler c'est ça ? Alors vous allez m'écouter maintenant. Si je vous parle, là, tout de suite, vous nous laisserez tranquilles après, d'accord ? D'accord ?

— ...Tu sais que je ne peux pas *frrrrrrrrrrrrrrrrrrrr*ettre ça, c'est absurde, tu ne m'as pas appelée pour coup*frrrrrrrrr*es ponts j'espère ?

— Ça fait des jours et des jours que j'essaie de couper les ponts ! On vous a bloqué quatre fois, on a changé de numéro, on sait pas comment vous faites mais vous revenez toujours, toujours, on peut plus dormir la nuit sans débrancher le téléphone, et si ce n'est pas le téléphone ce sont les portables qui sonnent, on peut plus dormir tout court, vous faites peur à nos enfants, vous me faites peur, arrêtez tout ! ...Par pitié, je vous en supplie.

— ...Mais pour*frrrrrrrr*vez peur ? Il ne faut pas, je ne veux pas vous inqué*frrrrrrrrrr*anie ne pleure pas s'il te plaît, et ne crie pas ! Tu vas réveiller tes en*frrrrr*-

— Donc c'est bien après mes enfants que vous en avez en fait. Je le savais, je le savais putain, des gens pervers et immondes comme vous, des kidnappeurs on croit que ça arrive toujours aux autres, que c'est dans les films mais en fait NON, vous êtes immonde madame vous êtes immonde ! Vous savez quoi ? Mes enfants à cause de VOUS ça fait une semaine qu'ils dorment plus ici, ça fait longtemps qu'ils sont plus dans cette maison, ils dorment chez ma sœur, parce que VOUS êtes dégueulasse et que VOUS n'aurez PAS mes ENFANTS !

— Non, je... non Mélanie tu ne*frrrrrrrrrrr*prends pas...

— Et arrêtez avec cette MÉLANIE vous voulez savoir quoi ? Je m'appelle LUCIE en fait, je m'en fous maintenant, je peux tout vous donner si je veux je m'en contrefous, vous savez pourquoi ? J'ai un message de mon mari, oui celui-là même, il s'appelle Frédéric et oui il est beau, plus beau que n'importe quel autre mec que vous pourriez jamais rencontrer, parce que c'est mon mari à moi et que je l'aime, eh bien sachez qu'avec Frédéric on a appelé la police et que vous êtes localisée depuis vingt minutes, il est trop tard pour vous barrer maintenant, c'est la TAULE qui vous attend vous entendez la TAUUUUUUULE.

— Mélanie... *frrrrrrrrrr* Mélanie pourquoi m'insulter comme ça ? Pour*frrrrrr* mentir sur ton nom ? Pourquoi *frrrrrrrrrr*tirais-tu à ta mère ainsi ? Qu'ai-je fait pour méri*frrrrrrrr* ta colère ? Je voulais juste de tes nouvelles...

— ...Excusez-moi vous plaisantez ? Ma mère va très bien merci. J'entends les sirènes dans votre combiné de toute façon, vous êtes foutue. Et ce sera tant mieux pour tout le monde. N'est-ce pas ?

— *Frrrrrrrrrrrrrrrrrr*-...Allô ? Madame Quarand ?

— Allô ? Oui ? Qui est-ce ? Vous êtes la police ?

— Exactement, madame. Je vous appelle depuis le combiné tracé, il est resté décroché.

— Merci, merci beaucoup, dites-moi que vous avez chopé la folle pitié dites-le moi...

— Les recherches continuent madame, mais nous avons fait le tour de l'endroit et pour l'instant aucune trace de votre harceleuse. Nous avions bouclé le périmètre bien avant que vous ne terminiez votre conversation, mais jusqu'à présent nous n'avons trouvé aucun individu dans les locaux.

— Comment ? Vous plaisantez ? Oui, vous devez plaisanter- Quels locaux ? De quels locaux parlez-vous ? Répondez-moi !

— Je ne me le permettrais pas madame. Je vous le dis les recherches continuent, votre agresseur n'a pas pu s'évaporer. Quant au lieu, si vraiment cela vous intéresse, il s'agit d'une maison désaffectée, en campagne. Les derniers résidents étaient un jeune couple, ils ont dû déménager depuis. Nous examinerons leur profil en détail sans tarder. De ce que nous savons pour le moment, ils proposaient un service de famille d'accueil pour personnes âgées. Je reste disponible, sur cet appareil, si vous avez besoin de moi, d'accord ? Je vous tiens au courant dès que nous la retrouverons. Elle n'a pas pu se volatiliser, soyez-en certaine.