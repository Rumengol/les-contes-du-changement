---
bg: "effroi-aux-yeux-luisants"
layout: post
title: Effroi aux yeux luisants
summary: Tu dors peut-être très profondément, cela ne m'empêchera pas de t'atteindre. On ne m'échappe pas.
source: “Your worst nightmare” par Stefan Koidl
ref: https://stefankoidl.artstation.com/projects/dX6qA
date: 2022-05-18
categories: posts
tags: []
type: Historiette
author: Lucafé
lang: fr
---

Tu es tout jeune. Tu joues aux billes, tu regardes des dessins animés, ton moment préféré de la journée c'est le goûter, tu aimes qu'on te raconte des histoires pour dormir. Tu n'aimes peut-être pas les légumes, les professeurs et aller à la piscine. Tu es un enfant et tu es heureux. Mais au final, tout cela n'a aucune importance. Car à partir de huit ou neuf heures du soir, dès que la lumière de ta chambre s'éteint, tout cela est terminé. Ton royaume est celui de la journée. La nuit tu n'es pas maître. Tu es victime.

Victime du sommeil. Qui t'attrape par le bas du visage, au moment où tu t'y attends le moins, te tire en arrière et te fait tomber un instant dans l'apesanteur la plus subite. Cet instant impossible à cerner temporellement. Un clignement d'yeux qui te paralyse des heures durant. Victime de l'obscurité. Devant la lumière de la veilleuse, les silhouettes dansent de manière immobile. Les monstruosités ont appris à manier l'art du camouflage. Une pile d'objets, un vêtement accroché, ou simplement quelques ombres, et soudain il y a quelqu'un. Dans le noir complet, tu ne peux même plus situer la source de danger. Mais tu sais qu'il est bien présent, et qu'il te scrute de ses multiples yeux nyctalopes. Victime du rêve. Dans ta chambre tu pourrais appeler papa, maman, à l'aide, mais ici, même ton propre corps ne t'appartient plus. Tu ne peux plus compter tes doigts. Tu ne vas plus où tu le souhaites, mais où ça a été décidé. Tu ne peux t'échapper nulle part. Et tu le sais.

Tu grandis. Tu oublies. Tu deviens raisonnable. La peur n'est pas rationnelle et n'a donc pas lieu d'être. Dormir devient presque optionnel. Tu te mets à traîner tard. Tu combats le sommeil. Tu éteins tout pour dormir. Tu braves l'obscurité. À ton insu, tu te permets d'entrer sur le territoire onirique. Tu affrontes les rêves. Mais même si tu ne peux pas t'en souvenir, ils sont bien différents de ceux d'autrefois. Certains parlent d'amour. Certains racontent une journée si banale. Certains n'ont juste absolument aucun sens. Tous disparaissent à la première sonnerie du réveil.

Tu croîs de nouveau. Mais le sommeil, lui, ne vieillit pas. Et chaque nuit tu règles tes comptes pour de faux, tu subis un tremblement de terre, ou tu perds quelqu'un. Tu ne peux toujours pas compter tes doigts, tu ne peux toujours rien décider. Tu ne peux toujours pas t'échapper. Tu as cru pouvoir fuir ce parasite du sommeil, qui transforme l'endormissement en angoisse et le repos en agonie. Tu croyais m'avoir filé entre les doigts, après toutes ces années. N'est-ce pas ?

Pauvre petit. On échappe pas aux cauchemars. Personne. Les peurs fluctuent mais ne disparaissent jamais. Et moi je reste caché dans chaque placard de ta chambre, dans chaque coin d'ombre, et j'attends ton inévitable assoupissement pour me repaître de tes craintes. Quand ton âme peureuse ne le supporte plus et que tu t'éveilles soudainement, oui, je reste. Je t'observe quelques secondes encore. Je presse sur ta peau mon regard omniscient qui feint de s'illuminer dans l'ébène nocturne, et je te rappelle que peu importe la lumière que l'on t'apportera, je serai là dès que tu t'assombriras.

Oh non, t'aurais-je encore réveillé ? Tu me sembles terrifié. Ton oreiller gît sur le plancher, ta sueur colle à tes habits et ton regard, même aveuglé par les ténèbres, parcourt la pièce, paniqué, de gauche à droite. Tu me vois maintenant, du moins tu crois me voir. Mes yeux brillent à peine, mais l'illusion est suffisante. Deux petites billes, plus hautes que le pas de la porte, qui engendrent la terreur quand elles croisent un regard vivant. Tu me fixes bien longtemps. Tu as peur qu'au moindre mouvement je vienne t'arracher quelque chose ? Ta volonté, ton esprit, ta tête peut-être ? Continue ce combat de regards, je t'en prie. Il est délectable.

Tu ne me quittes pas des yeux, et lentement, très lentement, tu commences à bouger, essayant de créer le moins de mouvement et de bruit possible. Si tu pouvais savoir à quel point ta tentative de discrétion est pathétique. Mais je salue ton effort. Dès que tu auras atteint ton interrupteur, je disparaîtrais. Tu trébuches sur un pied de chaise et te paralyses. Je continue à te fixer, sans broncher. Je cligne même des yeux, si rapidement que tu ne peux pas être sûr de l'avoir bien vu. Je ne l'ai fait que pour mon pur plaisir personnel.

Aurais-je à faire à un ambitieux ? Ton interrupteur se trouve sur ta gauche maintenant, tu l'as dépassé. Tu veux me plaquer peut-être, me choper ? Donner un coup dans ce qui s'avèrera pour toi être une ombre ? Vas-y, essaie. Au moment où tu croiras m'avoir frôlé, je serai dissolu. Tu inspires une dernière fois, et te jettes sur moi... si tendrement. Pas tendrement dans le sens où ton effort ridicule ne me semble pas être une attaque, mais réellement avec affection. Tu ne me frappes pas, tu veux m'enlacer. Et tu pleures.

Qu'espères-tu que je vais faire de toi ? Pourquoi m'embrasser ainsi, t'accrocher à mon tissu de peau fin et doux ? Est-ce car après avoir ressenti ma présence chaque nuit, tu crois me connaître ? Est-ce car je te surveille depuis toujours de mes billes de verre ? Est-ce car j'ai promis de ne jamais t'abandonner ? Est-ce car à force d'observer chacune de tes nuits, je te connais mieux que quiconque, je comprends parfaitement chacune de tes angoisses, de tes hontes, de tes colères ? Penses-tu réellement que tout cela je le fais pour ton plaisir ? Je suis un cauchemar, mon rôle n'est pas d'apporter le réconfort, mais la désolation.

Je t'ai fui, ne laissant dans tes bras qu'un manteau de coton accroché au mur. Tu le sens bien qu'il n'y a rien. Tu le savais bien, depuis ton réveil, que les mauvais rêves n'existent que dans la tête de leur porteur. Qu'il n'y avait rien ni personne pour donner explication à cela, pour briser le silence de l'inquiétude. Et bien évidemment, que si ta phobie est celle de l'abandon, elle te poursuivra même après un coma. Tu peux hiberner autant que tu voudras, te mentir jusqu'à en être persuadé, cette chose qui a accepté de te scruter n'était qu'un reflet sur des boutons. Pleure sur ton pardessus maintenant, hère délaissé, pleure. Tes larmes n'atteindront personne, pas même cette entité malfaisante en laquelle tu as espéré. Ton véritable cauchemar n'est pas tangible, mais reste bien réel. Tu es seul.
