---
bg: "quintessence-de-l-esperance"
layout: post
title: Quintessence de l'espérance
summary: La légende d'Hannah persiste à travers les siècles, celle d'une jeune fille pouvant voir l'avenir.
source: “The untold and the unseen.” par Maéna Paillet
ref: https://maena.artstation.com/projects/W2k4QG
date: 2022-05-13
categories: posts
tags: []
type: Historiette
author: Lucafé
lang: fr
---

Il y a très très longtemps, vivait une jeune et joyeuse famille : Hannah et ses deux parents. Ils avaient une maison dans la campagne, près de la forêt, et il leur arrivait de s'y promener. Mais un jour, une messagère encapuchonnée vint toquer à la porte. Elle disait recenser les enfants sur les ordres du roi, et devait emmener Hannah au village le plus proche pour qu'ils y comptent les petits garçons et les petites filles. Ses parents, bien sûr, étaient inquiets, mais la mystérieuse messagère leur souffla dessus et ils acceptèrent. Hannah était toute excitée à l'idée d'aller au village presque par elle-même, sans ses parents cela devenait une aventure ! Toute contente, elle a fait ses affaires et est partie avec la messagère sur son cheval. Elle était si heureuse qu'elle n'a pas remarqué qu'au lieu de se diriger vers le village, la cavalière s'enfonçait dans la forêt. Elle l'a amenée dans une petite clairière avec un rocher en son centre, et prononça ces mots : "Malice, montre-moi le passage !" Et une porte s'ouvrit dans le rocher. Hannah commençait à s'inquiéter, mais était paralysée par la peur et ne put pas réagir. Une fois le rocher refermé derrière elles, la cavalière enleva sa capuche, révélant un visage sans yeux. Elle sourit à Hannah de ses dents jaunes et l'attacha. Hannah hurlait, mais personne ne pouvait l'entendre depuis l'intérieur du rocher. Hannah est devenue l'esclave de cette sorcière, elle faisait la cuisine, le ménage, mais surtout elle lui permettait de voir ! En utilisant ses pouvoirs magiques, elle utilisait Hannah pour voir à travers ses yeux, et mieux encore ! Avec sa magie et les yeux d' Hannah elle pouvait voir... le futur. La sorcière en profita pour faire payer les gens pour leur révéler leur futur, et devint très riche et très connue. Un jour, c'est le roi lui-même, inquiet de l'état du royaume, qui lui demanda si son peuple allait prospérer encore longtemps. La sorcière utilisa à nouveau Hannah pour avoir une vision mais ce qu'elle vit était atroce, impensable ! Tellement horrible qu'elle s'en creva les yeux !

— Ah, beurk ! Non ! Dis pas ça ! Surtout pas devant Anna ! Tu vas lui faire peur !  
— Mais pas du tout, c'est toi qui as peur, pas ta petite sœur ! T'es qu'un froussard, t'es à deux doigts de te faire pipi dessus !  
— Taisez-vous, j'ai pas fini l'histoire ! ...La sorcière, tellement choquée de sa vision, chassa le roi sans rien lui dire et décida de s'enfermer dans la pierre et de ne plus jamais laisser personne rentrer. Elle avait peur du pouvoir qu'elle avait donné à Hannah, et depuis, elle la tient prisonnière, pour toujours, dans les bois...  
— Et le roi ? Et les parents ?  
— Je sais pas. Ils en parlent pas dans la légende. Par contre ce qu'ils disent, c'est que la forêt dans laquelle Hannah est enfermée est notre bois !  
— *Quoi ?*  
— Oui, les bois dans lesquels nous campons en ce moment même ! Alors ne faites pas trop de bruit, ou la sorcière viendra dans notre sommeil... et nous crèvera les yeux ! À tous les quatre !  
— Ah non ! Tais-toi ! Tu vas vraiment faire peur à Anna avec ça !  
— C'est ça, frou-ssard, frou-ssard !  
— Chhhhhhht, tu vas attirer la sorcière avec tes cris, eheheheh.  
— Attendez, c'était quoi ça ?  
— De quoi ?  
— J'ai entendu des branches craquer... Quelqu'un est là !  
— Éteins la lampe, vite ! Tout le monde dans son sac de couchage ! Cachez vos yeux ! Plus un bruit !

Anna n'arrivait pas à dormir. Depuis le bruit de la branche, plus personne ne parlait sous la tente, et les trois autres avaient fini par s'endormir. À sa droite, son frère avait le visage à moitié enfoncé dans son coussin, mais dormait quand même profondément. Anna n'avait pas dit un seul mot du conte. Non pas qu'elle avait peur, enfin si, elle avait eu un peu peur, mais elle se demandait surtout ce que Hannah avait pu voir de si atroce que ni elle ni la sorcière n'avaient pu le partager. Et surtout, elle avait de la peine pour la pauvre Hannah qui n'avait jamais revu ses parents.

Dehors, pas un bruit, à part un peu les bruissements du vent dans les feuilles. Anna s'ennuyait dans son sac de couchage. Elle s'en extirpa, mit ses bottes en caoutchouc, prit la grosse lampe torche, et sortit de la tente en refermant derrière elle. Elle ne connaissait pas très bien la forêt mais s'était déjà baladée et savait qu'il fallait suivre les traits jaunes, tourner avec eux, et ne pas aller là où il y avait des croix. Alors elle avança dans la forêt sous la lune claire, éclairant le chemin de terre parsemé de feuilles de sa lampe tremblante. Il y avait plus de vent que ce qu'elle avait prévu, aussi elle était contente d'avoir pris sa doudoune. Mais elle regrettait d'avoir laissé Tigrounet dans la tente. Un tigre, même en peluche, ça défend bien contre les chouettes, les loups, ou les dragons. D'ailleurs, elle s'arrêta soudainement quand elle entendit un hululement derrière elle. Était-ce réellement un oiseau ? On dirait un fantôme, les fantômes aussi font "Ouuuuh". Elle resta immobile de longues secondes, avant de se mettre à courir comme une dératée sur le sentier, terrifiée.

L'oiseau fantôme ne se faisait plus entendre quand elle s'arrêta. Devant elle se trouvait une petite zone herbue entourée par les arbres, que le chemin longeait avant de continuer. Au milieu de l'herbe, un gros caillou. Au moins une pierre. Anna retint son souffle. Elle était vraiment là. Hannah. Mais la sorcière aussi. Elle était très excitée, car c'est ce qu'elle cherchait, mais elle avait également très peur. Anna dut prendre une décision rapide, la sorcière ou le fantôme ? Le fantôme ou la sorcière ? La sorcière. Elle pouvait toujours l'assommer avec sa lampe, les sorcières c'est vieux et faible. Alors que les fantômes, ça passe au travers des coups. Elle murmura alors, pour ne pas alerter les monstres :  
— Malice, montre-moi le passage !  
Elle attendit un peu mais le rocher ne bougeait pas. Elle réessaya, un peu plus fort, cette fois :  
— Malice, montre-moi le passage !  
Anna retint son souffle à nouveau, ne s'attendant pas à le crier si fort. Aucune réaction du caillou. Mais au loin, elle entendit le spectre qui lui répondait par un long hululement, et courut se planquer dans un tas de feuilles derrière le caillou. C'est là qu'elle trouva quelque chose d'étrange : le caillou recouvrait en fait une ouverture, et derrière, sous la mousse et les herbes, il y avait une pente qui menait sous terre. Ayant déjà fait son choix entre le fantôme et la sorcière, elle n'hésita pas à entrer, lampe torche en main, prête à assommer de la vieille dame.

Anna fit quelques pas dans cette grotte noire comme le goudron, et alluma la lampe. Elle éclaira d'abord une paroi de roche. En fait, la grotte continuait en un couloir de pierre dans lequel elle avança. Parfois, les murs étaient peints : quelqu'un avait essayé d'écrire, mais ça ne ressemblait à rien. Et parfois quelqu'un avait dessiné des visages très moches et très impressionnants, des créatures mélangées à des animaux, et, enfin, un visage sans yeux. La sorcière. Le sang d'Anna se figea. Le dessin faisait vraiment peur. La peinture très grossière faisait pleurer des yeux n'existant pas. Et juste à côté de ce dessin... une ouverture dans la roche. La fin du couloir.

La petite fille était maintenant gelée de terreur. Mais elle ne pouvait plus renoncer. Pas maintenant. Alors elle fit lentement glisser la lumière contre les murs de cette nouvelle salle dont elle ne pouvait même pas distinguer la forme. Il y avait des pots par terre, très peu, et en très mauvais état, mais il y en avait. Sur le mur étaient accrochées des plantes un peu étranges, et toutes séchées. Et là, Anna s'arrêta. Elle venait d'éclairer la silhouette d'une femme aux cheveux longs, fins, mais rares. Elle était très élégante dans sa robe sombre, même si elle était très abîmée et déchirée, assise sur un maigre tabouret de bois. Elle avait des yeux. Ouf. Mais des yeux très tristes et très abîmés. Son teint était si pâle, elle semblait plus blanche que la neige. Anna souffla d'émerveillement :  
— Hannah...  
— Alors tu me vois. Je t'ai entendue arriver. J'espérais que tu ne me trouves pas.  
Sa voix était fatiguée, mais très lisse, très claire, et surtout, très douce. La petite fille s'approcha de la très vieille dame et lui chuchota :  
— Prends ma main, on va fuir la sorcière.  
— Sorcière ? Mais il n'y a pas de sorcière.  
— Mais alors qui vous a crevé les yeux ?  
Hannah eut un léger sourire mélancolique, mais ses yeux, qui ne fixaient rien, ne s'étaient pas allumés.  
— Je vois que tu connais la légende de Hannah, chère petite. En tous cas ne crains rien : il n'y a ici aucune sorcière qui puisse nous faire du mal.  
— Alors quel méchant vous a enfermée ici ? Est-ce que vous êtes une princesse ?  
La crainte, transformée en admiration, se chargeait maintenant de curiosité.  
— Personne d'autre que moi-même m'a enfermée ici.  
— ...Mais pourquoi ? Ça ne doit pas être drôle de rester toute seule si longtemps...  
L'aveugle, prise de tendresse pour l'intérêt innocent de cette fille, lui sourit tendrement.

Il y a très très longtemps, vivait une jeune fille, qui s'appelait Hannah. Elle vivait avec ses parents dans un royaume qui allait mal et qui avait besoin d'espoir en l'avenir. Les temps se faisaient durs, les récoltes comme le bétail diminuaient en nombre, et la guerre menaçait les habitants. Dans une volonté de se confier en un avenir radieux, le chef du village demanda aux jeunes filles de se former pour être oracles. Qu'est-ce qu'un oracle ? Un oracle est une personne communiquant avec des forces supérieures pour connaître le futur. Cela demande beaucoup d'entraînement, beaucoup de choix durs à faire, et beaucoup de solitude. Mais le royaume se désespérait. Alors, en échange de sa dévotion, l'univers donna l'éternité, et la capacité de voir à travers les âges à Hannah. C'est sous ce rocher, perdu au milieu de la forêt, qu'elle avait suffisamment de silence et de lien avec la forêt pour percevoir l'avenir. Des gens venaient de toute la région pour savoir s'ils survivraient à l'hiver, si leur enfant allait guérir, ou s'il allait réellement se dérouler une guerre. Sa renommée fut si grande que même le roi vint lui demander de lire l'avenir du peuple. Mais lorsque Hannah voulut voir les événements à venir, elle aperçut quelque chose d'odieux, de si terrible, qu'elle ne pouvait se résoudre à le révéler. Alors de peur elle se rendit aveugle, et renvoya le roi. De colère, ce roi répandit une rumeur sur une sorcière qui kidnapperait des enfants dans ces bois, pour que plus personne ne vienne trouver Hannah. S'il n'avait pas eu droit à sa réponse, alors personne n'en aurait. Depuis, Hannah resta dans sa grotte, sous son rocher, d'abord attendant une quelconque présence puis, s'étant résignée à la solitude, sentit les années passer à toute vitesse, enfermée sous la terre.

— Wouaaaaaah... Et elle est vraiment sous le rocher du coup ?  
— Non, Titouan, cela n'est qu'une légende. Mais une très belle, et qui sait ? Certaines légendes ont une part de vérité !  
— Mais cette Hannah... C'est toi ?  
— Mais non Youna, Hannah a vécu il y a très longtemps ! Je serais toute vieille et toute ridée si c'était moi ! La légende de Hannah est devenue une histoire si importante que maintenant cette forêt fait partie du patrimoine local protégé ! Donc remerciez cette belle histoire, sinon vous n'auriez pas pu venir vous promener ici, et essayer de trouver des écureuils ! Reprenons la marche, d'accord ? Je connais un endroit sympa pour le goûter, mais il n'est pas tout près !

Anna avait vingt-trois ans maintenant. Ce jour-là, elle dirigeait un groupe d'enfants en sortie scolaire à travers le bois. Attachée à sa région, et à sa forêt, elle n'avait pas pu quitter l'endroit avec les années. Attachée à son amie surtout, qu'elle passait voir quand elle pouvait. C'est pour cela qu'elle travaillait ici. Pour voir Hannah. Elle avait décidé de ne pas abandonner cette pauvre femme qui patientait seule, jusqu'à la fin du monde, enfermée dans la roche. L'ancienne n'avait en réponse à cela qu'une seule condition : sous peine de ne jamais la laisser la voir de nouveau, Anna ne devait jamais divulguer ses secrets, ni celui de sa présence, ni celui qu'elle lui avait révélé, cette nuit-là.

— Mais alors... qu'as-tu vu de si terrible ? Pourquoi ne pas le raconter ?  
Hannah s'était mise à rire.  
— Petite, sache quelque chose. Le roi n'était pas un bon roi. Il terrorisait la population, et c'était par sa faute que le peuple était si pauvre. Voilà pourquoi j'ai refusé de lui répondre. J'ai préféré m'empêcher de voir à nouveau pour justifier cela, ou il m'aurait tué sur-le-champ. Quant à ce que j'ai vu... il n'y avait rien. Rien d'autre que le noir de mes paupières. Comme dans le fond de l'univers, rien. Rien ne me répondait, pas une divinité, pas une étoile, rien. On ne m'a jamais donné le pouvoir de lire l'avenir. Mon rôle était difficile, et bien souvent j'ai souffert de ma position solitaire, avant même d'être rejetée, mais je ne regrettais pas d'avoir choisi cette vie. Car même lorsque je mentais, je donnais aux gens ce dont ils avaient besoin avant tout : de l'espoir. C'est l'espoir qui permettait à mon peuple de continuer malgré l'adversité.  
Anna avait réfléchi un instant.  
— Dans ce cas, pourquoi es-tu encore ici ? Comment as-tu reçu ton immortalité ?  
— C'est une réponse qui ne te plaira peut-être pas. Cela signifie que je suis bien la preuve que les fantômes existent.