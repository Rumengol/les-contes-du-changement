---
bg: "semblance-de-perfection"
layout: post
title: Semblance de perfection
summary: Fable de la Pie qui face au Paon, voulut devenir grandiose à ses propres dépends.
source: “Blind-Peacock” par A. Shipwright
ref: https://a_shipwright.artstation.com/projects/bKoNWa
date: 2022-05-24
categories: posts
tags: []
form: poem
type: Historiette
author: Lucafé
lang: fr
---

Il était une Pie s'ennuyant parmi les corvidés
Qui, lassée de sa condition, s'en alla observer
Dans le règne aviaire, quels oiseaux savaient
Par leur plumage et leur technique, comment intimider.
La Pie trouva un Paon, et tout de suite s'exclama
"Quel bel animal ! Après l'avoir découvert,
Je me sens maintenant si peu particulière,
Que je ne pourrais me présenter sans un travail sur moi."

"Quelles couleurs incroyables !" songea-t-elle,
"J'en ai assez d'être une noiraude,
Je veux ce bleu et cet émeraude !"
Et se couvrit de peinture pour ressembler à son modèle.
Le Hibou, inquiété, s'approcha : "Renonce !
Stupide et vaine est ton entreprise.
Sais-tu qu'elle est toxique, cette couleur dont tu t'enlises ?"
Mais la Pie, têtue, ignora sa semonce.

"Quelle taille imposante, je veux être pareille !"
La Pie s'en alla trouver chaque miette laissée
Chaque fruit, chaque plante, pour s'engraisser
Et atteindre la taille de la merveille.
"À quoi bon te grossir," dit le Hibou,
"Tu exploseras avant de l'atteindre
Une grenouille en est morte, tu souhaites la rejoindre ?"
Mais la Pie, obstinée, continua à se gaver de boue.

"Quelle humilité, de rester ainsi au sol !
Désormais je l'imiterai, je marcherai sur mes pattes."
"À te limiter ainsi, qui crois-tu que tu épates ?
Ces ailes vives t'ont été données pour que tu voles !"
"Mais ainsi posée je pourrais aussi copier son élégance,
À emprunter sa posture assurée je me sens grandie !"
"Modestie et pavane s'opposent, tu te contredis !"
Mais la Pie, têtue, se dandina avec extravagance.

"Quelle bravoure, ce Paon ! Un tel personnage
Doit pouvoir sans encombre en vaincre plus d'un !"
Le Hibou, désabusé, la sermonna avec dédain :
"Mais tu ne le connais pas, sous son vaste plumage,
C'est un froussard, tu lui inventes des qualités !
Tu t'es persuadée qu'un paon valait mieux qu'une pie.
N'étais-tu pas heureuse avant d'être une copie ?"
Mais la Pie, obnubilée, s'entraînait non sans pugnacité.

Le Paon, sans même prêter attention à son admiratrice
Dévoila sa queue fournie. La Pie jubila devant son idole
Elle subtilisa un éventail, des oculaires morts, un pot de colle
Et se confectionna elle-même cette nouvelle queue factice.
C'en fut trop pour le Hibou, qui désespéré s'en alla
Alors que La Pie se crevait les yeux pour la touche finale
Dans sa quête insensée pour ressembler à l'original
Et que sur son artifice, les deux billes elle installa.

Ainsi colorée, alourdie, humiliée, enhardie, aveuglée, mais parfaite
La Pie voulut côtoyer le Paon, magnifique plus que jamais
Par son imagination qui l'améliorait à force qu'elle l'acclamait.
Elle ne le trouva pas : Il avait déjà pris la poudre d'escampette
Dès qu'il avait entendu un chien sauvage s'approcher.
La Pie réinventée, cette cécité bariolée coincée à terre,
Ne put même pas réagir quand le canin mit fin à sa misère,
Et emporta dans sa gueule le ridicule oiseau amoché.

Le Hibou fut attristé mais ne put s'en vouloir
Il l'avait maintes fois prévenue de son imprudence
Il savait que le Paon, bien que magistral d'apparence
N'était qu'un oiseau, pas plus qu'une pie il ne saurait valoir.
Chacun est unique, certains sont juste plus voyants
Il ne tient qu'à l'envieux de plagier ses idoles
Quitte à se perdre soi, comme cette Pie devenue folle
Qui n'avait su voir en elle ce qu'elle possédait de chatoyant