---
bg: "deluge-de-lemures"
layout: post
title: Déluge de lémures
summary: Une invasion bien étrange.
source: “Ghosts in My Cellar” par Nirvana 
ref: https://nirvana_x.artstation.com/projects/5BOK8P
date: 2022-05-10
categories: posts
tags: []
type: Historiette
author: Lucafé
lang: fr
---

Jour 4  
Ce jour sera mon dernier. Les vivres que j'ai ramassés en vitesse pour survivre après ma retraite stratégique n'étaient suffisants que pour durer une semaine. Je les vois à travers la serrure de la salle de bains. Ils font semblant d'ignorer ma présence -ou peut-être l'ignorent-ils réellement ?- mais dès qu'ils me verront sortir, ils se jetteront sur moi et me dévoreront vivant dans d'atroces souffrances. Ni ma ruse ni ma force ne seront suffisantes face à une telle masse d'ennemis assoiffés de sang. Cette même masse de sagesse et de muscles m'empêche de me faufiler à travers le vasistas, la seule ouverture vers l'extérieur dans cette salle de bains, autre que la porte bien entendu. Mais cette dernière n'est qu'une solution de dernier secours. Je n'ai plus de nouvelles de mon allié depuis plusieurs heures maintenant. On m'avait promis un rapport radio, et l'horloge a dépassé l'heure désignée depuis de trop longues minutes. Il faut se rendre à l'évidence. Mes informateurs sont morts depuis longtemps. Ou alors ils n'ont juste plus de piles dans le talkie-walkie. C'est une possibilité.  
— *Crrrrrrr* Panthère Discrète à Volaille Détrempée, vous m'entendez ? À vous.  
— Je vous entends. Vous en avez mis du temps avant de me répondre, vous êtes en pénurie d'énergie ? Vous êtes dans votre chambre, il y a forcément des piles qui traînent. Aussi j'avais bien précisé Aigle Triomphant, c'est sérieux les pseudonymes. On ne sait jamais, s'ils sont sur écoute... À vous.  
— Ouais, c'est ça. Par contre ils sont à batterie tes talkies-walkies, t'es au courant j'espère. Et c'est moi qui ai le chargeur. Ça t'apprendra à avoir choisi le bleu, le jaune au moins il avait le chargeur.  
— L'heure n'est pas à la plaisanterie, camarade. Et nous avons déjà eu cette discussion, le bleu est réservé aux commandants. À vous.  
— Vérifie par toi-même dans le compartiment, Alpha trouillard. Ton forfait téléphone s'écoule... Déjà que je suis surpris que ces bidules en plastoc aient tenu aussi longtemps. Bon, je te dis bye-bye en avance, on sait jamais quand ça va couper. Sinon, toujours aussi bien la salle de bains ? T'as toujours à bouffer ? Hugo fait toujours ses rondes ?  
— Malheureusement je commence à manquer de substances nutritives. Et oui, Hugo passe de temps en temps. Il ne répond plus à mes avertissements. Ils ont dû lui laver le cerveau. Les lâches, ils ont attaqué le plus intellectuellement frêle en premier... Et à la chambre, tout va bien ? À vous.  
— ...  
— Allô ? Allô ? Panthère Discrète, répondez, à vous.  
— ...  
Ma correspondance avait malheureusement raison. Non seulement je manquais de vivres, mais je manquais maintenant aussi de moyens de communication. L'urgence se fait encore plus... urgente. Mes jours sont comptés. Je vois déjà leurs petits yeux ronds briller de malice à ma vue, et leurs bouches dévorantes s'ouvrir sur ma tendre chair musculeuse. Je dédie ce journal à-  
— Hugo ! La porte, vite ! ...Merci.  
Mise à jour sur la victime des envahisseurs : Il semble également entendre mes requêtes. Peut-être est-il juste simple d'esprit au point de suivre chaque ordre qu'on lui impose, peu importe qu'il vienne d'un allié ou d'un ennemi ? Je me demande lequel de ces deux camps lui a demandé de se brosser les dents.  
— En**c**ore là, le plan**qu**é ? 'udo, **k**'es relou chérieux, chors de chette challe de 'ains une 'onne fois pour **k**ou**k**es !  
— Je ne comprends pas les dialectes ennemis, traître.  
— Et donne-moi cha, j'en ai bechoin.  
— Hé ! Ma seule arme vaguement tranchante permettant de me défendre un peu avant de périr vaillamment ! Rends tout de suite !  
— Ch'est mon rajoir, et tu chais **k**rès 'ien que **k**u as in**k**erdi**k**chion d'y **k**oucher depuis **qu**e **k**u as voulu vérifier **qu**e j'é**k**ais pas un ly**c**an**k**hrope.  
— Est-ce que j'avais tort ? Et puis là c'est une situation d'urgence ! On est assiégés dans notre propre appart', tu peux pas dire que tu le vois pas !  
— Pas une raijon. Allez ouch**k**e main**k**enant, je **k**iens à me doucher cheul.  
— Lâche-moi ! Tu ne te rends pas compte de l'erreur que tu fais ! Ils vont m'assassi-aïe !

Ludovic s'était fait lâcher sur le parquet du couloir, sans ménagement, sans arme ou protection quelconque, à part son carnet aux piètres capacités combattives et son talkie-walkie vidé. Il se précipita sur la porte de sa chambre avant de se faire agresser, mais lorsqu'il l'ouvrit ses pires craintes se confirmèrent. Ils avaient envahi *sa* pièce. Il resta figé un instant, horrifié, leur laissant le temps de lentement se retourner vers lui, puis se jeta sur la porte de la chambre de Dustin. Sa chambre aussi en était infestée, mais lui s'en foutait royalement, allongé sur le ventre sur sa couette, une BD sous les yeux, et le talkie-walkie jaune à son côté. Sans relever les yeux il lui sortit :  
— Toi, tu t'es enfin fait virer.  
Ludo prit son stylo du bout de ses mains, et menaça les créatures qui, attirées par son entrée en trombe, s'approchaient lentement de lui.  
— Tu y es insensibilisé ! Ils ont fini par t'avoir aussi, sale Judas !  
— Pas loin de quatre-vingt quatre heures dans une salle d'eau et aucune douche, félicitations, tu pues mon pauvre.  
Le stylo fouettait l'air comme un coutelas, mais n'intimidait en rien les choses qui s'avançaient toujours vers lui.  
— Le bruit de l'eau aurait pu les alerter et les faire défoncer la porte ! Et vous aussi vous puez !  
— Nah, on a demandé à la voisine. On lui a dit que t'avais arraché la paume de douche pour te défendre avec. Ce qui, ne le cache pas, aurait été ton plan B. Mais ça reste une meilleure excuse que "Il s'est enfermé dans les toilettes avec des barres de céréales cet abruti."  
Les créatures étaient maintenant agglutinées autour de ses pieds, le scrutant de leurs yeux immobiles.  
— "Attention, ils sentent que tu as peur !" ...'sont inoffensifs, c'est pas faute de te l'avoir déjà dit. Leur pire exaction c'est de traîner dans nos pattes. Parfois.  
Ludo pointait toujours son stylo vers les envahisseurs. Il s'agissait de petits... trucs d'environ dix-quinze centimètres de hauteur recouverts comme des fantômes de minuscules draps blancs, sur lesquels se trouvaient deux petits yeux ronds accompagnés d'un sourire un peu niais. Aucun moyen de savoir s'il s'agissait là de véritables éléments faciaux ou seulement d'une façade dessinée par on ne sait quel instrument. Néanmoins cela semblait indiquer leur face avant. L'acculé fit un pas en arrière, toujours en maintenant son stylo et en n'ayant pas peur de s'en servir. Son deuxième pas se posa sur un élément un peu mou et doux, qui couina presque inaudiblement sous l'impact. Derrière lui se tenait un même masse d'ectoplasmes de poche curieux, le fixant depuis le parquet. Il se cogna à la porte en sursautant.

Récapitulatif des particularités des fantômes nains :  
— Certains suivent des gens, par exemple dans la rue, mais ils ne s'en rendent pas compte. Dans l'appartement ils aiment particulièrement talonner Hugo.  
— Depuis que j'en ai croisé un, je les vois tous. Et depuis que j'en ai parlé à Dustin et Hugo, ils les voient aussi.  
— Curieux assez naturellement, ils s'agglutinent sous chaque nouvelle personne qu'ils rencontrent. Sinon, ils se contentent d'explorer les lieux de manière un peu aléatoire. (ils ne sautent pas très haut, mais grimpent assez facilement les parois, en marchant aux murs notamment) Beaucoup se retrouvent autour des téléphones, du réfrigérateur, des livres, et de la porte d'entrée.  
— Plusieurs semblent s'être attachés à l'un de nous trois en particulier. Bien qu'impossibles à reconnaître, il suffit de suivre leurs déplacements pour remarquer que certains me lâchent juste pas.  
— Leur consistance n'est pas très rigide, mais ils sont tout de même parfaitement tangibles (une fois qu'on les voit du moins), et suffisamment durs pour tenir debout ~~sur leurs~~ . On ne sait pas s'ils ont des pieds ou non, tout est caché par le drap. Il fuient dès qu'on essaie de les attraper et sont très agiles à ce jeu-là.  
— Si l'on fait un cauchemar, ils s'approchent et nous observent dans notre sommeil. (Hugo a fait cette double expérience de panique pendant et après le sommeil. Et apparemment ils brillent de manière ténue dans le noir complet.)  
— Les fantômes nains adorent le poivre. Ils piétinent dedans et consument les grains cachés sous leur drap.  

Ludovic posa son stylo. Il avait dû dire adieu à son testament dramatique, alors il notait maintenant les quelques informations qu'il détenait. Il était dur de déterminer combien il y en avait, ne serait-ce que dans l'appartement. Parfois ils semblaient absents dans le silence de l'appartement, une dizaine au maximum, l'un dans un paquet de pâtes et l'autre sous un coussin, et à d'autres moments ils semblaient au moins cent, voire plusieurs centaines, alignés devant le canapé lorsque Dustin jouait sur la Xbox, ou assistant aux appels colériques d'Hugo avec la proprio ou son patron. Ils ne prenaient pas de place, ne semblaient jamais entraver les mouvements, au point où leur omniprésence devint une banalité. Les trois garçons s'y étaient habitués.  

À travers la fenêtre, il voyait du monde passer dans la rue. En ce mois de novembre, même si le froid montait, il y avait toujours du monde qui circulait à pied. Une personne âgée, sur un banc, avait assis auprès d'elle deux ou trois de ces étranges bonhommes. Cette mère au téléphone avec ses sacs de courses accumulait les suiveurs blancs, beaucoup étaient même empilés entre ses légumes et ses conserves. Quant à cet adolescent concentré sur son portable, le nombre de poursuivants fluctuait, d'un coup ils étaient plein, et quelques secondes après une bonne portion d'entre eux se désintéressait. Avant de revenir à la charge. Quelques-uns essayaient de suivre les voitures, mais se faisaient courser. Mais dans les véhicules, on pouvait en apercevoir aux vitres, et même sur la carrosserie. Au final, peu d'entre eux ne suivaient personne. Ils semblaient toujours à l'affut de quelqu'un. Peut-être tournaient-ils en rond dans l'appartement car il n'y avait que trois personnes pour les occuper ? Il se tourna et en vit quelques-uns qui le fixaient, toujours. Un peu inquiétants. Deux nouveaux s'ajoutèrent à cette opération "scruter Ludo". Impossibles à décrypter derrière ce sourire constant. Ils ne voulaient visiblement aucun mal, mais comment en être sûr ? Il s'agissait un peu de nouveaux gremlins. Il suffirait de mal s'en occuper et hop, la ville rasée à blanc ! Ludovic eut un sursaut sur sa chaise. Le temps qu'il sorte de ses pensées, une quinzaine s'étaient ajoutés au cortège. Et après un simple clignement d'yeux, une cinquantaine se tenaient maintenant sur son bureau, immobiles.
