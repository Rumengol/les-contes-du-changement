---
bg: "irresistible-pot-de-colle"
layout: post
title: Irrésistible pot de colle
summary: Carrie possède un animal de compagnie assez étrange. Ce n'est pas vraiment un animal pour être honnête, mais il semble très attaché à elle.
source: “Monster Concept” par Ryan Bittner
ref: https://ryanpaints.artstation.com/projects/R3ldrW
date: 2022-05-27
categories: posts
tags: []
type: Historiette
author: Lucafé
lang: fr
---

— Carrie, je suis désolée mais il va falloir prendre des mesures. C'est la huitième fois que votre... chose attaque d'autres animaux de compagnie, ça ne peut pas continuer ! Je ne sais pas moi, tenez-le en laisse, donnez-lui des calmants, faites-le dresser...

— C'est tout ? Vous plaisantez j'espère ! Son abomination a failli engloutir mon chien !

— Estimez-vous déjà heureux qu'il ait vite été recraché. Le chat de la dernière fois n'est pas passé loin du gobage simple et net.

— Et vous ne comptez même pas lui interdire la cour ? Lui interdire de sortir carrément ? La rendre à quelqu'un de plus compétent ? L'euthanasier ?

— Je comprends que vous soyez en colère, André, mais tout ça est un peu extrême. Calmez-vous je vous prie. Vous êtes là car vous êtes impliqué dans le litige causé par vos animaux de compagnie, mais le but n'est pas d'inciter à la maltraitance animale...

— Mais c'est pas un animal son machin ! C'est un monstre ! Un extraterrestre, un démon ! Allez pas me faire croire que c'est un chat, chien ou perroquet ou même un de ces N.A.C. !

— Vous auriez préféré que votre pitbull se fasse attaquer par une mygale peut-être, ou un boa ? Bon. J'ai eu une longue journée, je vous propose de continuer cette discussion une autre fois.

— Et vous allez la laisser partir comme ça, sans avis, sans avertissement, rien ? Son truc en liberté il va finir par bouffer des gens si ça continue !

— Vous pouvez y aller, je vous recontacterai tous les deux. Allez, ouste ! Je suis fatiguée.

André part dans le couloir en fumant. J'ai du mal à lui en vouloir, c'est pas la première fois que Scotch fait des siennes, et ses bêtises sont rarement de petite envergure. C'est un grand bonhomme mon Scoco. Déjà que j'évite de l'emmener au parc, autant pour les regards effrayés ou méprisants des passants que pour éviter les dégâts si j'en perds le contrôle, mais si je peux même plus le sortir dans la cour de l'immeuble, il ne va plus pouvoir respirer le pauvre. Il a besoin d'espace et ce n'est pas dans mon T3 qu'il peut s'étirer autant qu'il le voudrait. Je remercie la proprio avec un sourire coincé et en joignant les mains, comme pour implorer sa clémence divine, et qu'elle offre le salut à Scoco et Carrie, les impies du second étage. Sainte Sandra me rend un vague signe, mais elle est déjà partie. Un peu lasse comme personne, mais je ne pourrais jamais trouver de propriétaire aussi patiente donc j'ai pas intérêt à me la mettre à dos.

Je monte jusqu'à chez moi, je tourne la clef et j'entrouvre la porte tout juste suffisamment pour me glisser dans l'entrée avant de la refermer. Ça lui arrive d'essayer de sortir sans prévenir. Au lieu de ça, il m'attend dans son gros carton de déménagement lui servant de panier, tout liquéfié. Il sait qu'il a fait une bêtise et la joue tranquille pour éviter d'être puni. C'est dur de lui résister, mais il va falloir qu'il comprenne. Il va passer la nuit dans le placard, avec ses gamelles bien sûr, mais pas de friandise aujourd'hui, et pas de câlins. C'est réservé aux gentils animaux ça, pas aux vilains. Alors que je déplace le carton dans le petit cagibi, il tente de m'attendrir en collant ses deux langues sur ma main. Je fais de mon mieux pour ne lui montrer aucune réaction, même si je dois pour cela me contenir énormément. Cette nuit, je te mets de côté, mais je ne t'oublie pas, promis Monsieur Scotch.

Je m'en veux un peu. Pendant que je préparais le repas j'entendais les ventouses gluantes de ses tentacules racler contre la porte. Dans ces moments-là, même si ça ne fait qu'empirer les choses, j'ai envie de ressortir mon album photo. Il y a des photos de moi, de mes parents, de ma sœur, bien sûr, plein de sourires partout, mais il y a aussi ses premières photos à lui. Il a été déposé devant ma porte un matin, tout jeune, sans que je le demande, et je n'ai pas su dire non. Depuis je le garde. Au début il ressemblait à peine à un blob bleuté de la taille d'un hérisson ! Avec juste quelques tentacules et un orifice béant déjà bien gourmand. S'occuper d'un monstre pareil, ça en a demandé, de l'énergie, de la patience, et de l'argent. Mais maintenant que j'y suis attachée, c'est trop tard, non ?

J'ai craqué. L'affreux traîne maintenant sur mon lit, près de mes pieds, sous forme bien solide, car il sait à quel point je hais les traces visqueuses, surtout sur mes draps. Il garde ça pour dehors, ou pour son carton. Il sait se montrer adorable, et je suis incapable de ne pas céder à un moment ou un autre. Il faut avouer que Scotch a quelques avantages sur d'autres animaux, contrairement au stupide pitbull, là. Déjà, il ne produit pas d'excréments. Il absorbe tout ce qu'il avale, et ne le régurgite pas. (Sauf quand on lui demande de lâcher les objets qu'il mâchonne, mais ça c'est autre histoire.) Ensuite, il peut changer de taille et d'état. Pas mal non ? Liquide, solide, tout mou, tout dur, entre les deux. Ce qui le rend bien plus facile à transporter quand je l'emmène au vétérinaire par exemple ! Même si je ne l'y ai jamais emmené en fait. Je pense pas qu'ils soient qualifiés pour soigner ça, et même, il n'a jamais semblé souffrant, alors c'est qu'il n'en a jamais eu besoin. Et puis mon Scotch, surtout, il est pas très bruyant, il est bien attaché à moi, eeeet... il est original ! Moi au moins j'ai pas un hautain chat de salon ni un stupide cabot qui bouffe des enfants. J'ai mieux, un monstre qui bouffe les chats de salon et les stupides cabots.

&nbsp;

Je me réveille avec la moitié du visage enfoncée dans l'oreiller, et l'autre recouverte de gélatine brune violacée. Oh non, je sens que ça va être une de ces journées. Monsieur Scotch s'est fait appeler ainsi pour une bonne raison : Il a *beaucoup* de mal à me lâcher. Et certains matins, comme ce matin, il a l'air décidé à ne pas me laisser de la journée entière. Il s'agrippe à mon épaule, ou mon mollet, ou mon pied, et je le traîne de la chambre jusqu'à la cuisine, je le garde pendant mon petit déjeuner entier, même si je le laisse devant la salle de bain il essaie de passer dans les rainures et je l'entends engluer la poignée pendant ma douche, et sortir de l'appart sans qu'il s'accroche désespérément est un véritable casse-tête. Je n'ai pas l'énergie de me battre contre lui aujourd'hui, je vais faire ce que je déteste et l'amener discrètement au travail. Ce sera pas la première fois mais faut pas que ça se reproduise.

Il est dans un carton à dossiers vide, et je l'emmène avec d'autres rangements pleins dans la voiture, puis dans l'ascenseur, puis dans le bureau. Stupide mode de l'open space avec ses vitres omniprésentes qui empêchent au maximum l'espace privé et la discrétion. Je dépose la boîte sous ma table et je pousse l'entaille de carton destinée aux mains pour qu'il puisse me "voir". (Je ne lui ai jamais trouvé d'yeux, mais ça me semble pas incorrect de dire qu'il puisse me voir.) J'allume l'ordinateur, je sors mes dossiers, et je m'y mets. Enfin j'essaie. C'est compliqué quand on se fait constamment chatouiller la cheville par des mandi-tenta-je sais pas quoi-autour de sa bouche, ses moustaches de carpe là, tu vas me lâcher oui ? Oh non, pardon, je voulais pas, s'il te plaît, j'ai juste besoin de me concentrer, ne chouine pas, c'est pas le moment, oh non, trop tard.

Je sors parfaitement discrètement de mon bureau avec la mine la moins suspicieuse possible et mon petit carton dans mes bras, je me dirige tout naturellement vers les toilettes, comme n'importe qui le ferait avec une boîte de dossiers, et je m'y enferme dès la porte rabattue derrière moi. Enfin. Un endroit où je peux souffler. Je soulève le couvercle et laisse mon Monsieur Scotch jouer avec ma main. Je n'aurais pas dû l'emmener, mais ce n'est pas comme si j'avais eu le choix. Il m'a bien fait perdre une bonne heure de travail alors que tout était installé sur mon bureau, ce chenapan. On va dire qu'après cette pause toilettes je mettrai les bouchées doubles, reprendrai une demi-heure plus tôt cet après-midi et finirai les derniers rapports chez moi avant de manger. Oui, on va dire ça.

&nbsp;

La journée d'hier a été longue. Je n'ai pas fini tous mes dossiers avant une heure du matin, Scoco s'est montré trop demandeur d'attention, et moi la concentration me manque. Alors s'il veut désespérément des câlins, je vais finir par en vouloir aussi. Essayons d'oublier. J'ai devant moi une semaine de vacances, enfin, alors on va se changer les idées. Ça fait un moment que je n'ai pas pu retourner chez mes parents, dans la campagne. J'ai un peu de route devant moi, mais ça devrait le faire. Je n'ai qu'à manger un morceau, et préparer mes affaires. Ou alors couper le petit déjeuner, finalement. Scotch m'a retiré tout appétit. J'ai dû laisser la fenêtre ouverte et il m'a ramené un immonde pigeon citadin gras qu'il mâchonne goulûment. On va dire que je ne l'ai pas vu, j'ai pas la force pour ça à l'aube de mes congés.

Dans la voiture je me force à me remettre de bon poil pour revoir ma famille, il y aura même ma sœur, il ne faut pas gâcher ces moments ! Je mets ma playlist préférée, constituée de vieille pop niaise mais entraînante que j'écoutais quand j'avais encore quatorze ans. Je chante à tue-tête et à côté de moi, Monsieur Scotch gargarise en rythme. Il les connaît aussi à force, comme s'il les avait toujours entendues. Impossible de les fredonner sans avoir ses raclements et son babil baveux qui se superposent à la musique. Je crois que mon moment préféré c'est quand il finit enfin par s'endormir, si soudainement et silencieusement que moi-même j'oublie qu'il est là. Pour une fois je peux penser à moi. Mais quand il se met à ronfler doucement, je me rappelle que dès qu'il se réveillera je redeviendrais sa baby-sitter. Cette pensée me fait pousser un soupir, mais laissons-le rêver et efforçons-nous de nous concentrer sur la route, d'accord ?

Enfin arrivée ! Je suis accueillie par mes parents à bras ouverts, ma sœur attend son tour dans l'encadrement de la porte. Je sors mes affaires, les dépose dans ma chambre, et descends au salon, où mon après-midi commence enfin. On discute d'absolument rien d'intéressant mais c'est parfait, mes géniteurs me demandent comment mes amis portent, si je n'ai pas oublié de renvoyer le coupon d'abonnement, qu'attention les rendez-vous c'est important, regarde-moi cette nouvelle recette si elle n'a pas l'air excellente tu devrais absolument l'essayer, et sinon ça va ? Et là je l'entends. Il a fait basculer son carton et rampe mollement pour venir se coller à mes pieds. On peut pas avoir la paix tout le temps je suppose. Au moins ici il aura de l'espace pour bouger.

Le soleil disparaît à l'horizon, derrière les champs infinis des campagnes paumées du milieu du territoire. Une vision calmante et reposante, comme j'en avais besoin. Au loin je vois Monsieur Scotch qui se traîne sur le terrain d'herbes folles, "appréhendant" ses alentours, parce qu'on peut pas vraiment dire qu'il renifle avec ses appendices buccaux ressemblant à des vers de terre. Il a au moins décuplé de volume depuis qu'il a tout cet espace, mais pour l'instant ce n'est pas un problème, il suffit de le surveiller de loin. À quatre contre lui, il ne pourra que se montrer obéissant. Je profite de l'air frais, du chant des cigales, de me vider l'esprit et de juste laisser la douceur de la soirée me bercer, dans la chaise de jardin que j'ai déplacée pour faire face au couchant. Ma sœur me rejoint, ramenant sa chaise pour la déposer à côté de la mienne.

— Eh, *T- T- !* Scotch ! Pas touche ! C'est dangereux !

Mon abomination de compagnie l'entend et s'éloigne du poteau électrique auquel il donnait des petits coups de doigts. C'est toujours étrange de le voir avec des mains, en grandissant il lui apparaît plus de membres. C'est comme si l'espace le rendait plus capable.

— Dangereux pour lui ou pour le poteau ?

— Aucune idée mais s'il nous coupe l'internet je lui fais revivre un lavage à 30°.

On ricane toutes les deux au souvenir de cette anecdote. Il s'était endormi dans une poche et on l'avait retrouvé lessivé, au sens propre. Au moins ça prouvait à quel point il est résistant.

— Tu comptes le garder encore longtemps ?

— De quoi ? Tu parles de Monsieur Scotch ?

— Tu penses que je peux parler de quoi d'autre ? Bien sûr que je parle de lui.

— Eh bien, oui. Il s'est attaché à moi, et tu le connais, il est tenace. Je vais pas juste le faire partir comme ça, au revoir Scoco, on a passé des années ensemble mais en fait on va se séparer maintenant ! Non, ça fonctionne pas comme ça un animal de compagnie.

— Je m'attendais à cette réponse. Tu ne vois jamais à quel point il est encombrant ? À quel point ça t'épuise de t'en occuper ? Tu ne sais même pas d'où il sort, tu ne peux pas juste le garder et faire comme si de rien n'était. On ne sait même pas jusqu'à quand ça vit ces choses-là. Si ça se trouve, ça dure des centaines d'années et on sera mortes avant lui ! Tu veux quand même pas laisser cette pile de gélatine en héritage à tes enfants, non ?

Ça la fait rire, et je me force à rire avec elle. J'évite de me confronter à ce genre de questions. Monsieur Scotch fait partie de mon quotidien, fait partie de mon chez-moi, je ne m'imagine plus sans. Mais je dois avouer qu'elle a raison. Un appel venant de l'intérieur m'arrache à mes pensées.

— Bon, on en reparlera, hein ? Mais le gratin maison de maman est prioritaire. Vite, avant que ça refroidisse !

Elle rentre sans demander son reste. J'appelle Scotch, qui se retourne vers moi et s'approche en glissant sur ses tentacules. Je lui fais signe qu'il faut rentrer et lui laisse le temps de se dégonfler suffisamment. Je préfère l'avoir à l'œil. Il vaut mieux le surveiller que de le laisser faire des ravages tout seul dehors.
