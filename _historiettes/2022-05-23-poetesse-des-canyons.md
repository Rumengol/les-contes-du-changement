---
bg: "poetesse-des-canyons"
layout: post
title: Poétesse des canyons
summary: Si un jour vous voyagez dans ce désert, vous entendrez le chant de Pénélope qui erre.
source: “Orange” par nago00
ref: https://nago00.artstation.com/projects/bKNagG
date: 2022-05-22
categories: posts
tags: []
form: poem
type: Historiette
author: Lucafé
lang: fr
---

Si un jour vous voyagez, si un jour vous décidez de traverser
Les terres vidées et arides pour rejoindre un autre part
Vous vivrez la sèche, la chaude, malédiction du soleil violent
Vous verrez les tours de terre et de pierre des feuilles qui jamais ne se fanent
Et qui sait peut-être rencontrerez-vous lors d'un jour mourant
Une âme perdue qui vous demandera de rejoindre votre caravane

Un mirage apparaissant sur le plat de l'horizon
Fredonnant toutes les mélodies perdues de sa voix d'opéra
Sa gorge est sèche mais ses notes sont claires et puissantes
Et si sa voix ne vous enchante pas ce sera sa tenue qui vous convaincra
Maîtresse de la grâce faisant tournoyer sa robe avec passion
Elle vous fera l'emporter avec vous dans votre épopée grisante.

Aucune peau sur ses os et cependant fleur de lumière
Un cactus : Piquante, raide, puis rafraîchissante, colorée
Elle dira s'appeler Pénélope, errant pour retrouver son mari
Vous demandera de l'emporter pour sonder le désert
Elle connaît le chemin elle y a passé des siècles entiers
Et peut vous aider en échange d'un peu de votre compagnie

Pénélope vous demandera ce qui deviennent les vivants
Sont-ils encore frais, bienheureux, aimants
Puis vous fera chanter même si vous ne le souhaitez pas
Elle sait manipuler les timbres et vous arracher les notes
Vos harmonies les plus pures sortiront alors sans faute
La terre rougie, seule témoin, vibrera sous vos pas

Et la nuit elle veillera sur le feu, sur vous
Sa présence seule vous protègera des charognards et du froid
Elle scrutera l'horizon et murmurera tout bas
Les sons de toutes les époques les histoires de toutes les vies
De tous ceux qu'elles a connus vivants, qu'elle a croisés depuis
Et dans votre sommeil prendra vos rêves pour les raconter à son tour

Quand vous parviendrez à la frontière elle disparaîtra dans la nuit
Son domaine est celui du désert jamais elle ne s'en éloigne
Si vous repassez elle vous accueillera dans ses ravins
Vous dira qu'elle attend avec patience que ses enfants la rejoignent
Suivra votre marche, course, caravane, votre chemin
Pour que vous échangiez de nouveau vos notes inouïes

À moi elle m'a raconté qu'elle cherchait sa tendre amante
Peut-être pour me dissuader de rester l'accompagner dans ses traversées
J'étais tombé amoureux de son timbre mais qui y résisterait ?
Cependant je crois surtout qu'elle invente ses propres légendes
Ses propres histoires que sous la lune elle vous raconte
Et qu'elle n'attend personne si ce n'est vous et vos mondes

Je ne sais si Pénélope connaît encore l'ennui
Son étendue est vaste, les passants sont rares
Mais son temps n'a plus de murs ni de herse
Son seul espoir reste qu'à jamais les gens traversent
Ses terres vidées et arides et rejoignent leur autre part	
Qu'ainsi la source de son chant jamais ne soit tarie

Si un jour vous voyagez, si un jour vous décidez de traverser
Cette étendue bénie que je vous ai contée
Peut-être rencontrerez-vous lors d'une nuit naissante
La dame du désert se découpant sur le couchant
Et à ses côtés une silhouette l'ayant rejointe, comme promis
Je lui amènerai ma guitare une fois mes jours accomplis
