---
bg: "owl"
layout: page
title: "A propos"
crawlertitle: "A propos"
permalink: /about/
summary: "About this blog"
active: about
lang: fr
---

{% for posts in site.posts %}
{% if posts.title == "La Dame aux hiboux" %}
{% assign post = posts %}
{% endif %}
{% endfor %}

## Le Changement

Il s'est amorcé en janvier 2019, avec la découverte des [Bonfire Stories](https://www.instagram.com/bonfire_stories/) sur Instagram, puis la fascination provoquée par [un hibou](https://www.youtube.com/watch?v=ZOa-VO2_SLg). Un [premier conte]({{ site.baseurl }}/{{ post.url }}), et l'idée d'un défi à m'imposer : écrire un nouveau conte chaque jour, en relation avec une oeuvre, en tentant de me limiter à une oeuvre par artiste (autant le dire dès maintenant, je n'ai pas réussi). Un défi qui, malgré un jour d'échec, dura 30 jours pour 30 contes du Changement.

Un exercice passionant, qui demande de créer chaque jour un nouvel univers et de nouveaux personnages. Grâce à ça, j'ai découvert des artistes allant du merveilleux à l'incroyable, et construit les bases d'univers que je tiens à développer dans le futur.

Dorénavant, les contes paraissent sporadiquement, sans rythme imposé, au profit de la qualité d'écriture, j'espère. 
  

## Le Conteur

Je suis Rumengol, votre hôte et conteur d'histoires. Écrivain aléatoire et joueur de mots occasionnel, je suis bien plus doué à écrire des histoires qu'à raconter la mienne. J'espère sincèrement que le Changement que je vous propose résonnera en vous, ou au moins que mes contes seront plaisants à lire.

En bref, j'écris des contes d'après des oeuvres qui appartiennent à leur auteur.
Toute histoire peut être retirée à la simple demande du créateur de l'oeuvre.

favicon créé par [Good Ware](https://www.flaticon.com/authors/good-ware).

Tout contenu dont je suis le créateur (comprenez, ce qui est écrit principalement, le reste peu de chance) est libre de partage dans les mêmes conditions (gratuitement). Me créditer ou ajouter un lien vers ce site ferait de vous une belle personne, cela dit.
