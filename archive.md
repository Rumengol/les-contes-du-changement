---
bg: "tag"
layout: page
permalink: /posts/
title: "Archive"
crawlertitle: "All articles"
summary: "Archives du Changement"
active: archive
lang: fr
---

{%- assign collecs = site.collections | where: "output", true -%}
{% for collec in collecs %}

  <h2 class="category-key" id="{{ collec.label | downcase }}">{{ collec.label | capitalize }}</h2>

  <ul class="year">
    {% for post in collec.docs %}
        <li>
          {% if post.lastmod %}
            <a href="{{ post.url | relative_url}}">{{ post.title }}</a>
            <span class="date">{{ post.lastmod | date: "%d-%m-%Y"  }}</span>
          {% else %}
            <a href="{{ post.url | relative_url}}">{{ post.title }}</a>
            <span class="date">{{ post.date | date: "%d-%m-%Y"  }}</span>
          {% endif %}
        </li>
    {% endfor %}
  </ul>

{% endfor %}
