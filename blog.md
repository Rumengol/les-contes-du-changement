---
bg: "owl"
layout: default
title: "Blog"
crawlertitle: "Blog du changement"
summary: "Blog du changement, son évolution, son histoire."
lang: fr
---

<h2>Pas encore d'article ici, mais ça va venir !</h2>
<h3>Il y en a un en écriture, en fait.</h3>

{% for post in site.posts limit: 15 %}
{% if post.tags contains "blog" %}

<article class="index-page">
  <h2><a href="{{ post.url | relative_url }}">{{ post.title }}</a></h2>
  {{ post.description }}
</article>
{% endif %}
{% endfor %}
