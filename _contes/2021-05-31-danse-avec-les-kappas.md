---
bg: "danse-avec-les-kappas"
layout: post
title: Danse avec les kappas
summary: Un chasseur bougon arrive dans un village lacustre sous la pluie
source: “Village in the rain” par Isla Wsen
ref: https://www.artstation.com/artwork/282WNe
date: 2021-05-31
categories: posts
tags: ["Les chasseurs de démons"]
type: Conte
author: Rumengol
lang: fr
---

Le village était comme abandonné. On aurait pu croire que des pêcheurs vivants sur le lac ne seraient pas indisposés par quelques trombes d’eau, mais tous sans exceptions avaient trouvé refuge dans leurs masures de bois dès les premières gouttes. Un bien triste spectacle qu’offraient les couards recroquevillés sur eux-mêmes, à l’image d’un pays au déclin inéluctable.

Même le batelier, à l’approche des nuages d’orages, avait préféré lui donner sa barque plutôt que d’être payé pour l’emmener. Un entrelacs de filets à moitié remontés &mdash; ou plongés &mdash; lui barrait la voie vers le centre du village. Le guerrier se vit contraint de débarquer en bordure et de rejoindre l’imposant bâtiment en empruntant le labyrinthe de pontons de bois et de cordes.

Le sol était glissant, poisseux et vermoulu. La pluie pénétrait ses vêtements pour le glacer jusqu’aux os, et son chapeau de paille le protégeait plus du soleil que des averses. L’humidité risquait de faire rouiller sa lame et de moisir son fourreau. Sa vue était brouillée, les quelques lampes qui brûlaient encore n’apportaient qu’une lumière ténue, à peine suffisante pour se repérer. Il aurait donné beaucoup pour ne pas avoir à arpenter ce village perdu peuplé de lâches.

Les émissaires de l’empereur l’attendaient comme convenue devant la bâtisse qui devait être une sorte de salle commune. Ou de hangar à poisson. L’homme et la femme ne semblaient pas plus ravis que lui d’être là. Le soldat en rouge avait dans sa main un manuscrit gorgé d’eau, dont l’encre devait avoir coulé depuis longtemps.

Le chasseur de démon haussa la voix à son approche pour couvrir le tumulte des éléments.

“Je suis Akisumi Torikami, missionné pour résoudre le problème de ce village. Je vois que les pêcheurs n’ont pas très envie d’être aidés.

&mdash; Ils ont peur. Lorsque la pluie tombe, des gens disparaissent, alors ils n’osent plus sortir. Nous avions envoyé quelqu’un enquêter il y a quelques semaines, mais il semble avoir été victime de l’esprit, lui aussi.

&mdash; Allons à l’intérieur, et dites moi tout ce que vous savez.

&mdash; Avec plaisir. J’ai fait chou blanc, mais Yumei a discuté avec une femme qui prétend avoir aperçu l’esprit. Yumei ?”

Les deux hommes se retournèrent face à un rideau de pluie. L’averse se muait doucement en tempête, et la soldate était hors de vue. Ce n’était pas là œuvre naturelle. Akisumi tira son épée, imité par le soldat rendu anxieux par la disparition de sa compagne. Le chasseur fouilla dans sa besace et libéra une volée de luciole particulièrement brillante, qui s’envolèrent en illuminant le ponton où ils se trouvaient.

À travers les planches, des dizaines de paires d’yeux leur rendirent leur regards incrédules. Les kappas étaient d’ordinaires des créatures dociles et farceuses, nouant des liens avec les pêcheurs lacustres. Mais ceux-ci avaient quelque chose de différent. Leur faciès était hideux, déformé comme par une maladie dévastatrice.

Repérés, les petits démons se ruèrent vers les deux combattants, griffant et grimpant au ponton. Akisumi fut le premier à réagir, décapitant les monstres intrépides qui passaient à sa portée. Le soldat, plus lent à la détente, s’était lui aussi mis à se battre. Il était aux prises avec deux kappas ayant pris pied sur le plancher instable.

À peine était-il parvenu à les repousser que les planches qui le supportait se brisèrent. Hurlant de peur et de surprise, il tenta de se rattraper à un poteau, mais il fut tiré vers le fond avant même que le chasseur de démons ne puisse l’attrapé. Akisumi regarda, impuissant, le corps de son employeur se faire submerger par une marée de kappas.

Il ne pouvait prendre le temps de s’apitoyer sur le sort du pauvre homme dont il ignorait jusqu’au nom, car il n’était pas en meilleure posture. Quelque chose n’allait pas, les démons étaient beaucoup trop nombreux. Sa lame avait terrassé une vingtaine d’ennemis, et le tranchant était totalement émoussé.

Acculé, il sauta sur le toit de chaume, qui s’effondra sous son poids. L’intérieur de la grande bâtisse était plongé dans le noir. C’était en effet une salle commune, mais étrangement personne ne s’y était réfugié. Les kappas qui, alourdis par leur carapaces, ne pouvaient prétendre emprunter le trou du toit, entreprenaient de forcer la porte verrouillée. Comme le reste du village, elle ne ferait pas long feu, et rien ici ne pouvait servir d’arme à Akisumi.

Il grimpa les escaliers, fouillant rapidement du regard chaque étage, enchaînant les déconvenues. Excepté des restes de repas ou des relents de friture, tout était désespérément vide. Au dernier étage, il débarqua sur une terrasse trop fine à son goût, et contempla l’étendue de la désolation.

Les kappas étaient partout dans le village. Bien plus d’une centaine. S’ils le voulaient, ils pouvaient faire disparaître cet endroit de la carte. Et lui avec. Des échos lui parvenaient d’en bas, lui signalant qu’ils avaient finalement réussi à défoncer la porte. À court d’idée, Akisumi se retourna pour se retrouver face à une femme immobile.

Son kimono pâle était immaculé malgré les torrents d’eaux qui tombaient, et elle-même semblait enveloppée d’un halo lumineux. Son visage était dissimulé derrière un voile, mais il semblait au guerrier qu’elle riait. Son instinct de chasseur le fit aussitôt reculer, son sabre épuisé en garde. Cette jeune femme, aussi délicate soit-elle, ne pouvait être qu’un démon.

Alors qu’il se jetait sur elle pour la poignarder, elle releva son voile et révéla son hideux visage en déclamant quelques mots. Frappé par une douleur insoutenable, Akisumi lâcha son arme et tomba à genoux. Il sentait son crâne se dégarnir, et tout son corps changer. Sa bouche se mua en un bec grotesque, des écailles parurent sur sa peau devenue grise, et les os de son dos se disloquèrent, puis déchirèrent sa peau pour former une carapace qui le cloua au sol. Le visage déformé par un ignoble rictus, il cria.
