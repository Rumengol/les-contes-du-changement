---
bg: "antihectique"
layout: post
title: Antihectique
summary: Expérimentation extrasensorielle
source: “The End of the Dream” par Oleg Proshin
ref: https://www.artstation.com/artwork/KaJy5o
date: 2021-05-12
categories: posts
tags: []
form: poem
type: Conte
author: Rumengol
lang: fr
---

Soudain, le vent se lève comme je m’élève  
Douze chevaucha. L’heure me fond dans le tout  
Tonnerre galopant vers nulle terre ou mer  
Par là où s’éveillent des dragons de papier.  
Je suis l’arpenteur de ces sanctuaires de son  
Brillant d’absence, scintillant par ma présence  
D’un pas fugace il peine à retrouver son sens  
Lorsque l’hésitation le secoue d’un frisson.  
J’y vois tous les péchés que je ne peux expier  
Cent plaies béantes où poussent des fruits amers  
Des êtres de regrets, diables courant partout  
Abattu toujours, à chaque fois je me relève.

Je suis le voyageur éphémère et seul  
Dans l’étendue enflammée de mille vies  
S’éteignant dans un ridicule linceul  
Souffle éclair, prêchant l’avenir à l’envi  
Sans dieux ni cieux, seuls subsistent les Enfers  
Buvant goulûment le si précieux nectar  
Que j’alimente du sang de mes pensées.  
L’avide démon ne comprend ça que tard  
Et se tari avec le flot insensé  
Retour au rien, inutile de s’en faire  
Car déjà paraissent de nouvelles sphères.

Ma vieille amie-ennemie, la voilà  
Ses teintes glacées gèlent mes espoirs  
Alors que de ces grossiers appendices  
L’anathème émerge de mon sommeil  
De l’abysse jaillit l’Apocalypse  
Ma lumière vacille, ultime éclipse  
L’horizon perle d’un éclat vermeil  
Et dit adieu au doré de jadis  
Je suis vaincu, c’est la fin des espoirs  
Mes démons s’envolent au Walhalla.

La pensée se délite à l’instant,  
Redevient poussière cosmique  
Plus aucun souvenir  
Emprise perdue  
Glas de l’éveil  
Fin d’une ère  
Enfin,  
L’aube.
