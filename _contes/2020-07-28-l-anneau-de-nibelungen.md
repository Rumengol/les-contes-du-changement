---
bg: "l-anneau-de-nibelungen-1"
layout: post
title: L'anneau de Nibelungen
summary: Une merveille technologique prend son envol vers un horizon inconnu.
source: “Empire”, par TheBakaArts (aka Darius)
ref: https://www.deviantart.com/thebakaarts/art/Empire-792311992
date: 2020-01-25
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---

Il était le fleuron de la technologie terraïenne. Le seul foreur d'espace, un vaisseau pouvant exploiter la métrique d'Alcubierre. A l'avant, les deux foreuses généraient le champ de constriction spatial et l'énorme réacteur arrière permettait la dilatation de l'espace. Le double anneau, quant à lui, servait au maintien de l'intégrité de la bulle de subespace, en plus de former un anneau-monde accueillant des dizaines de milliers d'individus. Son assemblage avait duré plusieurs décennies, maintenu dans la haute atmosphère de Terra. Il était le dernier espoir d'une civilisation qui avait épuisé les ressources de sa planète jusqu'à la décadence et l'urbanisation totale.

Une fois les océans disparus, la terre bétonnée et le ciel colonisé, leur seule solution de survie était les étoiles. Hélas, aucune planète n'était suffisamment proche pour permettre un exode massif des terraïens. T-84-EC, ou New Terra, comme certains l'appelaient déjà, était une exoplanète située dans le bras galactique voisin, mais un seul voyage là-bas prendrait plus de temps que la planète n'avait. Des sondes ainsi que des drones y avaient étés envoyés, à une vitesse équivalente à 99.578% de la vitesse de la lumière, et ils n'avaient toujours pas achevé le quart du voyage.

La vitesse-lumière des vieux films de science fiction elle-même était trop lente. La seule option restante était de mettre en application à tout prix la métrique d'Alcubierre, permettant de voyager plus vite que la lumière. Des années durant, les plus grands ingénieurs du monde planchèrent sur cette question. De cette coopération internationale inédite jaillit l'Anneau de Nibelungen, symbolisant le pouvoir et l'influence dans une ancienne mythologie, et l'étendue de la technologie terraïenne. La construction fut bien plus longue que l'élaboration du projet, malgré les nombreux milliardaires mettant leurs fortunes à profit en échange d'une place de choix à bord.

L'équipage subit une sélection plus que rigoureuse, formant une véritable armée de près d'un million d'ingénieurs, informaticiens, techniciens et scientifiques de tous bord, dont chacun fit l'objet d'une analyse approfondies autant historique, psychologique que génétique. En plus des milliers de fortunes, quatre millions d'êtres considérés comme aptes génétiquement à constituer le noyau dur de la colonisation spatiale terraïenne furent sélectionnés. De violentes émeutes éclatèrent à la suite de ce choix, réprimées dans une violence devenue monnaie courante au sol.

De façon étonnante, l'organisation mondiale de la colonisation spatiale formée à l'occasion parvint à s'entendre et l'Anneau de Nibelungen pris son envol sans encombre, avec la promesse de revenir deux années terraïennes plus tard, chercher à nouveau quatre millions de colons. Les deux autres anneaux en construction devaient l'accompagner à partir de son troisième voyage.

"Et ensuite ?"

"Dur à dire. Les enregistrements postérieurs sont flous. Une erreur de calcul, une mauvaise gestion de leur technologie, un sabotage même, qui sait ? Les terraïens étaient des êtres perfides de nature.Toujours est-il que la destruction du vaisseau les a probablement fait arrêter la construction des deux autres, et que leur espèce s'est éteinte, prisonnière de son monde étouffant, sans avoir réussi à coloniser l'espace."

"Quel gâchis. Ils avaient achevé une prouesse qui des millions d'années plus tard nous est encore hors de portée. Ils auraient aisément pu dominer cette partie de la galaxie, s'ils en avaient eu l'occasion."

"Que veux-tu, les lois de l'univers sont cruelles et impartiales. Et puis, leur exploit ne sera pas vain. Le décodage des données de fonctionnement de l'engin seront bientôt terminées. Tâchons de réussir là où eux ont échoué."

{% include image.html url="l-anneau-de-nibelungen-2.jpg" description="“Disaster FULL”, par TheBakaArts (aka Darius)" ref="https://www.deviantart.com/thebakaarts/art/Disaster-FULL-792987827" %}
