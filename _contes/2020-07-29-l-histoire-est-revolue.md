---
bg: "l-histoire-est-revolue"
layout: post
title: L'Histoire est révolue
summary: Lorsque le passé ne suscite plus d'intérêt, quel avenir peut se dessiner ?
source: “Ancient & Future”, par Patryk Olkiewicz
ref: https://www.artstation.com/artwork/oO8reL
date: 2020-02-01
categories: posts
tags: ["Les 5 ciels","Les 26"]
type: Conte
author: Rumengol
lang: fr
---

Je me souviens de la première fois que j'ai vu les pyramides de Gizeh. J'étais enfant alors, et elles m'avaient paru si grandes, inatteignables. J'étais fasciné par la perspective de ces hommes, six mille ans plus tôt, élevant d'immenses blocs de pierre dans ces cimes calcaires. Des millénaires plus tard, la magie technologique nous avait donné accès aux étoiles, à des mondes infinis, et de nouvelles formes de vie primaires. Pourtant je ne pouvais qu'admirer les prouesses du passé. À une époque où rien n'était simple, où aucun ordinateur quantique ne calculait avec une précision nanométrique chaque paramètre, des merveilles avaient été bâties. Tout était si facile aujourd'hui, que ça en devenait fade. L'énigme de la construction de ces pyramides, qui avait tenu en haleine des experts sur des siècles de débats, avait été résolue en quelques minutes par la révolution quantique. Nous n'étions plus des bâtisseurs, tout juste des inventeurs et des explorateurs.

Lorsque je suis revenu, bien des années plus tard sur Terre, je compris mon erreur. L'expansion démographique terrestre n'avait pas diminué avec l'accès aux astres, bien au contraire. Une ville avait fleuri au pied des pyramides, et s'était élevée au-delà. Les Égyptiens avaient bâti des monuments à la hauteur de leur gloire, les Terriens avaient réalisé le même exploit, érigeant une pyramide-monde autosuffisante plus grande que tout ce qui existait. Y entrer était un privilège, à peine quelques millions vivaient à l'intérieur contre des dizaines, des centaines d'autres à ses pieds.

Elle était l'une des sept merveilles artificielles de l'univers, la seule créée par l'homme, en quelques dizaines d'années à peine. Située à quelques kilomètres à peine du site de Gizeh, elle toisait les anciennes pyramides de sa majesté, comme pour signifier que les exploits de jadis inspiraient toujours les chef-d’œuvres de demain. J'avais vu de mes propres yeux l'anneau-monde et la forge stellaire, derniers vestiges de civilisations depuis bien longtemps éteintes, et aucune de ces constructions n'inspiraient ce tel sentiment d'accomplissement. Je n'y étais pour rien, j'ignorais même son existence, et pourtant c'était toute l'humanité en moi qui applaudissait et s'embrassait.

Aujourd'hui, quand je contemple les pyramides de Gizeh, je suis pris d'une étrange mélancolie. Je vis ici, depuis cinquante-trois ans maintenant. La cité basse s'est encore étendue, elle lèche le site que je m'évertue à protéger. Je vis dans les hauts-jardins de la Pyramide, qui n'a pas été baptisée car la majuscule seule est évocatrice. J'ai été tour à tour gouverneur de districts, directeur du département d'histoire stellaire à l'Université - encore une fois, nul n'a jugé utile d'ajouter à la majuscule - et gestionnaire de l'expansion solaire. Mais j'ai toujours tenu à rester ici, à vivre sur une merveille d'aujourd'hui, à pouvoir admirer chaque jour les merveilles d'autrefois.

Ce matin, c'était la vingt-septième fois qu'il venait me voir, toujours plus embarrassé. J'aimerais conserver intact le patrimoine qui m'est cher, que je considère essentiel au futur. Mais la vie n'a que le présent, et je suis son passé. Je suis vieux maintenant, je n'ai plus la force de me battre. J'ai donné mon accord, le dernier nécessaire. Gizeh sera rasé au mois prochain. Ce dernier bout d'histoire, de Terre qui n'avait pas encore été absorbé par le métal et le verre, il va s'éteindre avec moi. Tout comme l'Histoire d'ailleurs, qui n'intéresse plus grand monde ici, et dont je suis le dernier professeur à l'Université.

Un mois est si vite passé. Je convaincs péniblement mon assistant de m'aider à me rendre jusqu'à la grande pyramide, pour un dernier adieu. Il me dit de faire vite, la démolition approche. Le chemin à l'intérieur est éclairé, balisé, dénaturé. Je lui dis que j'ai oublié mon carnet dans mon bureau. Soupirant, il est reparti. Cette pyramide m'est si familière, je me sens presque comme un pharaon approchant sa fin. Je verrouille l'accès, scellant à nouveau ce tombeau. L'énergie est coupée, il y fait terriblement noir. Je me hisse sans aisance sur le socle d'un sarcophage reconstitué. La matière ressemble à du plastique. Cela manque de panache, pour un pharaon, mais je suppose que pour un vestige d'historien, c'est adéquat.
