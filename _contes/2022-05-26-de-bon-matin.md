---
bg: "de-bon-matin"
layout: post
title: De bon matin
summary: C'est vraiment le meilleur moment de la journée.
source: “珈琲少女” (coffee girl) par Danny John
ref: https://www.artstation.com/artwork/2qQlmJ
date: 2022-05-25
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---
Ce matin, je me lève tôt. À peine réveillée que je saute du lit pour m'habiller. Mon uniforme est accroché contre la porte de ma chambre. Aujourd'hui j'ai école, comme tous les mercredis. Je descends dans la cuisine, où maman est en train de préparer le petit déjeuner. Une délicieuse odeur de tartine grillée sort du grille-pain. Je débarque alors qu'elle sort la casserole de lait chaud du four. Je n'attends pas pour sortir mon bol et le placer juste au-dessous de l'objet fumant. Elle secoue la tête en souriant, puis incline son récipient pour remplir le mien. J'attrape les tartines, et dépose le tout sur la table où m'attendent déjà une motte de beurre et un pot de confiture à l'abricot. 

Maman ne me rejoint pas tout de suite. Elle prépare son café qu'elle torréfie elle-même avec amour. Avec la bouilloire remplie, elle effectue des cercles concentriques spiralés pour ressortir le meilleur de l'arôme des grains. En tout cas, c'est ce qu'elle dit toujours. J'aimerais bien goûter son café, mais elle dit que je suis trop petite. Moi je pense qu'elle veut juste se le garder pour elle, même si elle en fait toujours trop. Elle met le reste dans un thermos qu'elle emporte à son travail. Ça lui permet de tenir la journée, apparemment. 

Moi, je n'ai pas besoin de ça. Mes deux tartines avalées et mon lait bu, je suis prête à partir. Je remonte les marches en courant pour récupérer mon cartable fait la veille. Je vérifie que je n'ai rien oublié, réajuste mon ras-de-cou, me coiffe avec soin et je suis partie. Je passe en coup de vent dans la cuisine où maman est encore en train de savourer son café et l'embrasse sur le front. Un instant plus tard, je suis sur le chemin qui m'emmène à la route. De temps en temps je prends le bus, mais l'école n'est qu'un arrêt plus loin, alors les matins où il fait beau comme aujourd'hui, j'y vais à pied. 

Je croise Sonoko au portail. Elle aussi vient en marchant, mais elle habite de l'autre côté de l'école par rapport à ma maison. Mais ça ne nous empêche pas d'être amies, et je lui saute dessus pour lui faire un câlin. Elle me raconte le drama qu'elle a regardé hier, et je veux tout savoir. On n'a pas la télé à la maison, maman n'aime pas les programmes et elle dit que c'est trop cher de toute façon. Et puis on a les journaux pour se tenir informées, elle n'a pas besoin qu'on les lui lise. J'aimerais bien regarder des dessins-animés ou des dramas, mais à la place c'est Sonoko qui me raconte les histoires. Elle raconte bien, Sonoko, c'est presque comme si on y était. 

La journée se passe bien, sous un soleil radieux. Pas un nuage à l'horizon, mais un vent frais nous empêche d'avoir trop chaud. On discute dans le fond de la classe avec Sonoko jusqu'à ce que le professeur ne nous sépare. Tant pis pour lui, on continue de s'échanger des regards. J'hésite à lui faire passer un bout de papier, mais la dernière fois qu'il en a intercepté un, le professeur l'a lu à voix haute devant toute la classe. Le pauvre Takuma était rouge de honte, et je n'ai pas envie d'être à sa place. Alors, quand on n'échange pas des messages silencieux avec Sonoko, je dessine. Ce n'est pas la faute de Mr. Tadami, je n'ai jamais aimé les maths. Même s'il faut dire qu'il n'aide pas beaucoup. 

À l'heure du repas, on prend nos bentos et on monte sur le toit retrouver Marie. Marie, elle vient du Royaume-Uni. Toute sa famille a suivi son père quand il a décroché un super travail ici. À son arrivée, elle parlait très mal japonais, alors on lui a appris avec Sonoko. Et maintenant, on est inséparables même si on est dans des classes différentes. On se voit à midi, et souvent le soir avant de rentrer chez nous. Ce midi, elle nous attend avec un air grave. Elle a l'air plongée dans ses pensées et ne nous voit pas arriver. Sans se concerter, on bouge simultanément, Sonoko et moi. Une de chaque côté, on contourne le champ de vision de Marie jusqu'à arriver sur ses côtés, et c'est à ce moment qu'on l'attaque à coup de guilis ! Elle crie de surprise et manque de jeter son repas par terre. Qu'est-ce qu'on rit, toutes les deux ! Marie n'a pas l'air d'apprécier et elle nous crie dessus, maintenant. 

On reprend notre calme avec difficulté et on ouvre nos propres plateaux qu'on attaque sans attendre. Maman m'a préparé de la salade, des petites saucisses, et un peu de riz au curry. Elle prépare toujours les meilleurs repas, je suis impatiente de savoir ce qu'elle a mis en dessous pour le dessert ! En plein milieu du repas, Marie nous coupe d'un ton inquiet. 

— Vous avez entendu parler des disparitions ? 

— Les disparitions ? je demande. 

— C'est des garçons de ma classe qui en parlaient. Apparemment, il y a de plus en plus de disparitions ces derniers temps. Des gens se font enlever, puis ils reviennent et ils ont changé. Ils deviennent méchants, ou alors leur comportement n'a plus rien à voir ! Et quelques jours plus tard, leur famille se fait kidnapper pareil avant d'être remplacée par des clones imparfaits ! 

— Haha, t'as dû mal comprendre, ils parlaient sûrement de leur dessin animé avec des aliens qui envahissent la Terre. 

— Tu le connais, Sonoko ? 

— J'avais commencé à le regarder, mais il faisait un peu peur, alors j'ai arrêté. 

— Je suis sérieuse, les filles ! J'ai pas mal compris, ils parlaient de quelque chose qui arrive en ce moment ! 

— J'en ai pas entendu parler à la télé, alors ça n'existe pas. 

— Pareil, les journaux de la semaine dernière en ont pas parlé, sinon maman me l'aurait dit.  

— Par contre, c'est vrai que le prof de sport a l'air d'avoir changé, on dirait presque qu'il est humain maintenant. 

— Ah oui, tu trouves aussi ? Je me demande s'il s'est fait remplacer ou s'il s'est juste rasé la barbe. 

— Vous êtes pas drôles. Puisque c'est comme ça je dis plus rien. 

On a continué de charrier Marie pendant le repas, mais la cloche a trop vite sonné. On est retournées en classe. L'après-midi passe en un éclair, et avant même de le réaliser, la cloche sonne à nouveau la fin de la journée. Je ne sais pas trop ce que j'ai appris aujourd'hui, mais je me suis bien amusée. Au portail, on attend Marie pendant dix minutes, mais elle ne vient pas. On demande au gardien s'il sait quelque chose, et il nous répond qu'elle se sentait mal et qu'elle est rentrée chez elle dans l'après-midi. On est inquiètes, mais Marie habite loin, hors du réseau de bus. C'est son père qui l'amène le matin, parfois sa mère. On ne peut pas aller la voir ce soir, mais on convient de passer se débrouiller pour passer chez elle demain. Avec un peu de préparation, on peut tout faire. 

Je rentre en sautillant, mais moins enjouée que ce matin. Marie m'inquiète un peu. J'essaie de ne pas trop y penser. Après tout, je ne peux rien y faire, et elle ne voudrait pas que je me fasse du mal pour elle. En rentrant, j'embrasse maman et file faire mes devoirs. Je ne comprends pas la formule écrite sur mon cahier, même en y passant une partie de la soirée, jusqu'à ce que maman m'appelle pour manger. Tant pis, j'apprendrai demain. On mange une super omelette. Maman me parle de sa journée, je lui parle de la mienne. À la fin du repas, je débarrasse la table et prends un roman que je vais lire sur la terrasse jusqu'à ce qu'il fasse trop sombre. Alors je vais me coucher. 

&nbsp; 

Le lendemain matin, je me réveille plus difficilement. J'ai mal dormi la nuit, et se lever est une torture. J'enfile quand même mon uniforme en sortant d'une bonne douche qui me réveille un peu. Maman a déjà versé le lait dans mon bol, et me regarde d'un air amusé. Je m'installe en traînant des pieds et regarde le mouvement hypnotique de ses cercles concentriques spiralés qui ressortent le meilleur de l'arôme des grains. J'avale mon petit-déjeuner sans trop y penser et m'apprête à partir quand maman me rappelle de prendre mon cartable. 

Je remonte en vitesse dans ma chambre et range à la hâte mes cahiers dans mon sac avant de dévaler les marches à moitié coiffée. Je suis en retard ! Pas le temps de marcher jusqu'à l'école, j'attends le bus qui, heureusement, ne met pas trop de temps à arriver. Le vieux chauffeur regarde ma carte d'un air absent et hoche la tête. Il n'y a pas grand monde, aujourd'hui, alors je peux me mettre au fond contre la fenêtre et regarder la forêt qui défile à toute vitesse. Je n'ai même pas le temps de m'assoupir que je suis déjà arrivée à mon arrêt. Je descends et passe le portail juste à temps avant que le surveillant ne se mette à souffler dans son sifflet du diable. Sonoko me dit qu'elle a failli ne pas m'attendre, et je m'excuse en rigolant. 

Ce matin, le sujet des disparitions est sur toutes les bouches. Ce n'était pas qu'une rumeur, apparemment, ça arriverait dans toute la région. Sonoko a entendu la nouvelle à la télévision, et pense qu'on devrait aller s'excuser auprès de Marie. Je suis d'accord, c'est vrai qu'on n'a pas été très sympa avec elle hier. Surtout si elle ne se sentait pas bien. Même le professeur en parle à un moment, et nous dit d'être prudent en rentrant, d'éviter d'être seul. 

En allant sur le toit pour manger, on ne peut pas s'arrêter de parler avec Sonoko. On échafaude des théories qu'on démonte aussitôt, des plans pour identifier les gens remplacés, et des stratégies pour les éliminer. En arrivant en haut des escaliers, on manque de percuter Marie qui vient du couloir d'en face. Je lui saute au cou. 

— Marie ! Tu vas mieux ? 

— Minute, papillon ! m'interrompt Sonoko. Marie, est-ce que tu peux nous dire le premier mot qu'on t'a appris en japonais ? 

— Hein ? 

— Réponds, c'est très important. 

— C'était rāmen, et vous aviez été surprises que je connaisse déjà le mot vu que vous pensiez que c'était un plat unique du Japon. Mais pourquoi tu me demandes ça ? 

— Ouf, c'est bon tu n'as pas été remplacée. Contente de te revoir ! 

— Ah, vous me croyez maintenant ! 

— Forcément, maintenant que tout le monde en parle, c'est difficile de l'ignorer. Même notre prof nous a dit de faire attention. 

— Au fait, pourquoi tu es partie hier après-midi ? 

— Ils ont appelé ça une crise de panique. Je me sentais mal, cette histoire de disparitions m'inquiétait et mes deux meilleures amies venaient de se moquer de moi. J'avais juste envie de rentrer, alors le principal a appelé mon père pour qu'il vienne me chercher. 

— Oh, on est désolées Marie. Promis, on ne se moquera plus de toi et on te croira. Tu nous pardonnes ? 

— Si vous me donnez vos œufs. 

C'était de bonne guerre, et Marie aimait les œufs durs bien plus que Sonoko ou moi. Je n'en ai pas aujourd'hui, mais maman me prépare toujours des œufs mimosas le vendredi, alors je lui promets de lui donner les miens demain. J'ai eu peur pendant un moment, mais on s'est remises à parler comme avant. Même avec cette histoire inquiétante en tête, c'était bon de rigoler entre amies. Grâce à ça, l'après-midi fut moins pénible que le matin, même si je n'attendais que la fin de l'école pour retrouver Marie. À la fin de la journée, les filles m'accompagnent jusqu'à l'arrêt de bus. La maison de Sonoko est sur le trajet de Marie, alors son père a accepté de la raccompagner, mais elles ne veulent pas que je sois seule, et je les remercie pour ça. Sonoko nous parle du programme télé de ce soir, et on est tout excitées, on lui demande de tout nous dire demain sans faute ! 

Le bus arrive, et je grimpe dedans. Il y a plus de monde ce soir, que des écoliers et un vieux monsieur. Le trajet est aussi rapide que ce matin. Je suis la seule à descendre à mon arrêt, et pour cause ma maison est l'une des seules des environs. J'embrasse maman sur le front en rentrant et me love dans ses bras. Je n'ai pas de devoir pour demain, alors j'en profite. On passe un moment comme ça avant qu'elle ne se lève pour préparer le repas. Je reprends mon livre et lis jusqu'au dîner. Après ça, je monte préparer mon cartable et me glisse rapidement dans mon lit, pour ne pas être à nouveau fatiguée demain. 

&nbsp; 

Ce matin je me sens d'humeur sautillante. C'est bientôt le week-end, et on a prévu d'aller se promener en forêt avec maman. Je suis impatiente, les longues promenades du samedi après-midi sont les meilleurs moments de la semaine. Mon uniforme ajusté, je débarque dans la cuisine alors que maman est en train de préparer son café. La casserole de lait n'est pas sur le feu, et mon bol n'est même pas sorti. Innocemment, je sors une tasse et demande si je peux avoir du café. 

— Pourquoi pas ? Tu es grande. 

Ça me surprend. Maman a enfin changé d'avis ? Elle me considère assez mature pour goûter à son nectar, ça me rend heureuse. Je l'observe verser l'eau bouillante en effectuant un cercle parfait. Les gouttes de liquide noir tombent une à une dans la cafetière. Elle me sert généreusement et je penche ma tête au-dessus de ma tasse. L'odeur amère me saisit aussitôt les narines et je grimace. Mais je ne m'avoue pas vaincue pour autant, et souffle abondamment pour chasser la fumée et refroidir ma tasse. En face de moi, maman prépare des sandwichs et coupe des oignons sans pleurer. Elle est très forte, moi je n'y arrive pas, et elle non plus d'habitude. Elle a peut-être trouvé une technique. Je voudrais lui demander mais elle a l'air très absorbée dans sa tâche. 

Je bois une gorgée et ma grimace s'intensifie. Le goût est très amer, c'en est à la limite du supportable. C'est ça qu'elle boit tous les matins ? J'ai envie de tout recracher, mais je suis une grande fille, alors je ne dis rien et bois tout. Je pense que c'est la dernière fois que je lui demanderais son café. Entre deux gorgées, je regarde maman préparer mon bento. Elle met dedans des sandwichs qui prennent presque tout l'espace, avant de glisser un peu de chocolat dans le double fond en me faisant un clin d'œil, et je rigole. Dans le petit espace restant, elle cale quelques tranches de concombre et un petit avocat coupé en deux. 

La tasse vide, je remonte me préparer. J'attrape mon cartable, me coiffe avec attention et vérifie que mon ras-de-cou tient bien. En bas, maman est en train de m'attendre. Elle va travailler plus tôt aujourd'hui, alors elle a décidé de m'emmener. Je ne dis jamais non à plus de temps passé avec maman, alors je me dépêche de la rejoindre. J'attrape mon bento à côté de la cafetière encore presque remplie de café refroidissant, et je saute dans la voiture à sa suite. Je crois que ce que je préfère dans ma vie, c'est les bons matins. 

 