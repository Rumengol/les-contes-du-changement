---
bg: "guichet-des-ames"
layout: post
title: Guichet des âmes
summary: Contacter le bureaucratique guichet des âmes n'est jamais de tout repos.
source: “Subscriber unit is switched off or is outside the coverage area” par Veron_1411
ref: https://twitter.com/Veron_1411/status/1722319650501845466/photo/1
date: 2023-11-12
categories: posts
tags: []
type: Conte
author: Rumengol
---
« Tous nos correspondants sont occupés pour le moment. Nous faisons notre possible pour écourter votre attente.»

Bien sûr. Le même message en boucle depuis des heures. Je ne sais même plus pourquoi j'essaie. La voix nasillarde à l'autre bout du fil se moque de moi, je peux entendre l'opératrice réciter son texte avec un sourire sadique. Les fils téléphoniques se multiplient autour de moi, ils s'emmêlent et me ligotent, m'étouffent. Les combinés s'alignent sur la table, au sol, pendent comme des nattes avec lesquelles on aurait trop joué.

Mes nattes à moi sont des langues de serpent, cheveux que je n'ai jamais eu qui prennent soudainement vie pour s'attaquer aux liens qui me retiennent dans cette boucle sans fin. Je suis devenue plus téléphone d'humaine, dans l'indifférence des rideaux délavés qui coulent comme de la lave sur le mur. Les motifs s'altèrent à intervalles réguliers, décrivant l'attente qu'il me reste dans un script que je ne comprends pas. Quelque part, ça doit correspondre à «longtemps» ou «éternité moins cinquante ans».

Je m'accroche désespérément au combiné rouge, comme s'il s'agissait de ma dernière chance. Il ne l'est pas. Je le laisse s'enrouler autour de mon bras, s'enroulant langoureusement tout en enserrant mes muscles. Mon bras s'ankylose, ma peau tourne au violet. J'ai peur qu'il tombe une fois que tout le sang l'aura quitté. Je le sens qui s'accumule dans mon épaule, peut-être que cela va se terminer comme ça ?

J'imagine la scène macabre de mon épaule explosant sous la pression de l'afflux sanguin. Il tombe mollement au sol, tout desséché, sans même un spasme. Avec un temps de retard, je réalise que je devrais avoir mal mais la douleur ne vient pas. Un câble téléphonique m'a amputé et je me retrouve manchote d'un bras, au bord de l'évanouissement. 

Pendant un moment, c'est arrivé. Mais la pression se relâche au moment où je tends le bras pour me saisir du combiné suivant. Un autre rouge. Il n'y a pas de boîtier sur la table. Pas besoin, le numéro que je cherche à joindre est déjà enclenché. La tonalité de retour d'appel m'accueille, une amie familière qui ne m'a jamais failli. C'est bien la seule, d'ailleurs. Ça décroche. La musique d'attente se veut exotique avec des airs tropicaux, mais les fougères qui surgissent derrière moi ne sont pas dupes.

Le caméléon qui en sort, en revanche, s'est fait avoir. Un peu comme moi, l'idiote qui appelle le même numéro pendant des heures. Ce n'est pas comme s'il y avait autre chose à faire, de toute façon. Je suis espionnée. *Ils* me regardent, guettent la moindre faille, le moindre échec. Quoique, depuis le temps qu'ils sont là, ils doivent préférer le goût du succès. Ou bien ils ne font que m'observer de leur regard inquisiteur. Comme ce caméléon albinos dont les horribles yeux coniques me jugent. Il pourrait me sonder profondément, creuser sous la surface des choses. Mais non, son regard reste superficiel, il m'examine sous toutes les coutures mais ne voit que mon corps, ma carcasse. Mon paraître.

Alors je fais de même. Je plante mes yeux dans les siens, je veux lui transmettre qui je suis réellement. Mes traumatismes, mes émotions, mon identité, qu'il parvienne à voir à travers moi et puisse enfin me répondre. Mais le reptile détache ses yeux face à l'intensité de mon œillade et se désintéresse de moi. Saloperie. Au moins, j'ai remporté ce duel.

L'ennuyante musique se coupe brutalement, et je recentre toute mon attention sur le téléphone tandis que le caméléon vaque à ses occupations arboricoles.

« Vous n'êtes plus vous-mêmes ? Le temps ne s'écoule pas comme il devrait ? Laissez-nous vous aider. »

Et la musique reprend du début. Je demande que ça, de vous laissez faire, bande d'escrocs ! Il faut y mettre du vôtre aussi. Les fougères frémissent. Je m'attends presque à voir un deuxième caméléon en sortir, mais il est seul, et me tourne désormais le dos. Il prend visiblement plus de plaisir à chasser des insectes immatériel qu'à me tenir compagnie. J'ai du mal à l'en blâmer, je ne sais plus en ce moment-là si je suis moi même présente. Pas dans cette pièce, mais dans la réalité. Est-ce que je suis quelqu'un, au-delà de ce que je montre ? J'aimerais répondre à cette question qui me rend l'existence impossible, mais mon compagnon d'infortune ne se sent pas d'une âme de psy. Tant mieux, il me demanderait sans doute un paiement que je ne suis pas capable d'honorer.

« Nous vous informons que cette conversation peut être enregistrée à des fins d'analyse dans l'unique but d'améliorer notre service et de vous proposer une prestation toujours plus qualitative. Vous pouvez vous opposer...

— Dans mes rêves, je sais, je sais. Skip, passe à la suite, je m'en fous ! »

Je ne sais pas ce qui m'a pris. Je perds patience, mais je n'aurais pas dû crier. Ce pauvre combiné n'a rien fait, et la voix mécanique qui m'a répondu non plus. Le reptile darde un œil réprobateur dans ma direction. L'autre se consacre à l'étude de la structure d'une fougère. Je m'excuse d'un haussement d'épaules. Mauvaise idée, mes liens se resserrent un peu plus. Un combiné tombe. Depuis quand avais-je ce gris sur moi ? Paradoxalement, je ne me sens pas plus légère. C'était un poids réellement mort, un de ceux qui ne pèsent rien.

La musique se relance. Du début. Ce n'est pas que j'aimerais connaître la fin de cette mélodie médiocre, mais les choses inachevées me frustrent. Même les récitals qui me donnent envie de monter sur scène et d'enfoncer le micro dans la gorge des interprètes jusqu'à ce qu'il ressorte de l'autre côté, je reste jusqu'à la fin. En espérant être agréablement surprise, ou parce que j'aime me faire du mal. Mais ce que je déteste par dessus tout, c'est la répétition sans comique. Même avec. Alors quand la musique s'arrête pour la troisième fois, je grince des dents.

« Toutes les lignes de votre correspondant sont occupées. Nous vous prions de rappeler plus tard, et nous nous efforcerons et répondre à votre demande dans les meilleurs délais. »

Et retour de la tonalité morte. J'envoie d'un geste brusque le combiné là où se trouve le caméléon, qui a déjà disparu. Le fil enroulé autour de mon poignet l'empêche de s'écraser au sol et il revient droit dans ma hanche. Les conséquences de mes actions sont douloureuses, une fois de plus. Je soupire bruyamment. La colère s'en est allée, et il ne me reste plus que le dépit. Un nouvel échec. Combien ça fait ? J'ai arrêté de compter après le vingt septième.

Je me saisis du combiné suivant. Il est vert, cette fois. Les fougères fanent et se rétractent à l'approche de l'appareil, pourtant de leur couleur. Le temps que je le porte à mon oreille, toute la végétation de la pièce est morte. La tonalité est visuelle maintenant. Un cygne à tête de serpent apparaît sous mes yeux. La créature fuit vers l'avant, repoussant une tête hargneuse qui cherche à se dévorer elle-même.

Aveugle, le corps suit des mouvements erratiques tandis que ses crocs remontent peu à peu sa nuque. Ils cherchent à se planter dans les plumes tendres et délicates. Qu'est-ce qui peut pousser un être libre de ses mouvements à se haïr ainsi ? Ceux qui sont capable d'atteindre le ciel d'un battement d'aile devraient être heureux. Est-il stupide ? Le cygne-serpent qui cherche sans cesse à mettre un terme à sa propre vie sans jamais s'y résoudre, ne peut-il pas regarder devant lui plutôt qu'en arrière ?

Il devrait suivre l'exemple du serpent-cygne qui vient de surgir des roseaux un peu plus loin. La créature majestueuse s'élève au-dessus des tourments mondains d'un simple mouvement de ses magnifiques ailes. Son long bec guide son être vers sa destination, droit et fier. Il m'a l'air si proche, à évoluer devant les rideaux, mais pourtant si loin. Lorsque j'essaie de faire un pas vers lui je suis retenue par un enchevêtrement de câbles. Évidemment, il est trop tard pour moi. J'ai choisi mon poison, Charybde plutôt que Scylla, et je baigne dans les conséquences de mes actions.

Je ne peux pas revenir en arrière, et même si je le pouvais, est-ce que ça vaudrait même le coup ? Le serpent-cygne n'en finit pas de son ascension, il est toujours plus haut jusqu'à disparaître dans les nuages, mais sans s'éloigner. Lui aussi est enfermé, sa prison est simplement trop grande pour en voir les barreaux. Son destin est enviable, à défaut de mieux. C'est tout ce que je peux faire après tout, rêver et jalouser.

Le cygne-serpent ne prend pas de la hauteur, trop occupé à se chasser lui-même. Et moi je le regarde. Je le touche, le caresse, effleure ses écailles et ses plumes. Je sépare sa tête alors qu'elle s'apprête à enfin parvenir à ses fins, sans trop savoir pourquoi. Un réflexe. Elle m'en veut clairement et plante deux croches dégoulinants de venin dans mon cou. Mes cheveux tremblent en sifflant, dispersant toute la haine dans la créature dans l'air. Elle emplit l'air, poisseuse, étouffante. Une vapeur violacée chargée de reproche et de mépris.

Le combiné est vide. Il est lourd, mais aucun câble n'en sort de l'autre bout du fil. La tonalité aiguë n'est rien d'autre que la réverbération de mon sang dans la cavité caverneuse, mon souffle qui remonte le tuyau pour rejaillir dans mes oreilles en sifflant. Je m'y attendais, je l'ai vu. Je laisse le cygne-serpent et le serpent-cygne s'envoler dans une danse nuptiale qui ressemble à s'y méprendre à une chasse sans merci, et je repose le combiné.

Le suivant, bleu, recolore immédiatement la pièce. La température baisse alors que je suis projetée dans une chambre froide. Pour un peu, je verrais des bouts de jambons pendre du plafond. Bien sûr, il y en aurait plein, ils masqueraient la vue et se battraient pour la moindre once de mon attention. Je ne mange plus de viande depuis un an, mais ça ne l'empêche pas de me narguer. Elle est vicieuse, et infatigable. Elle pense qu'elle m'aura un jour, mais je l'ignore comme j'ai toujours fait jusque là.

J'exulte presque quand la voix que j'entends n'est pas l'habituel enregistrement impersonnel qui habite chaque combiné dispersé sur moi et dans la pièce.

« Guichard Suzanne, qui est à l'appareil ? »

C'est donc ça que je suis à ses yeux ? Une simple blague, un canular téléphonique de bas étage. Je ne prends même pas la peine de jeter le combiné. Il tombe mollement, seulement retenu par son intersection avec un autre câble. Si je passe l'éternité à essayer, rien ne changera. Je peux tenter la même chose encore et encore, en espérant que le résultat soit différent. Et en même temps, j'en ai plein, du temps. Presque autant que de combinés devant moi, attendant d'être saisi.

« Tous nos correspondants ...

«  				...sont occupés. Le temps d'attente...

« 									... est estimé à cent trente huit ans.»

