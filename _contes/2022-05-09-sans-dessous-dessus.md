---
bg: "sans-dessous-dessus"
layout: post
title: Sans dessous, dessus
summary: Introduction d'un prochain roman, de toute évidence. Dans un monde pollué où les gens s'enfuient dans l'espace, qu'est-ce qui peut amener des résidentes de la ville haute à se rendre En-dessous ?
source: >
    [Titre Perdu] par @inwardsound
ref: https://www.yankodesign.com/2020/04/07/architectural-design-renders-that-give-us-a-glimpse-into-the-future-of-humanity/
date: 2022-05-09
categories: posts
tags: ["Sans dessous dessus"]
type: Conte
author: Rumengol
lang: fr
---
— Encore un accident. 

— Un accident ? Tu déconnes, t'as vu l'état du rafiot ? 

L'embarcation partait en flammes au-dessus de la ville. C'était un vieux bateau de pêche, le genre qui se faisait plus depuis des décennies. Le feu d'artifice venait de l'arrière, là où se trouvait la cale à carburant. Forcément, ces poubelles flottantes n'étaient pas faites pour l'espace. On pouvait voir le gars à la proue, fixant désespérément l'horizon. La ville devait défiler à toute vitesse sous ses yeux de condamné. 

— C'est un putain d'assassinat de le faire partir là-dedans. 

Xlicz vitupérait à n'en plus finir, comme à chaque fois depuis qu'on l'a accueilli chez nous. Il en avait pas encore assez vu, c'est pour ça. Lysa et moi, ça nous fait plus rien. Ce sont des choses qui arrivent, un bête accident. Un sabotage quelquefois. Mais on fait avec. Faut le comprendre, quand il était En-dessous il savait pas ce que ça signifiait, l'exode. Lysa lui répond d'une voix traînante alors que le bateau en perdition percute le sommet d'une tour. 

— Plutôt partir dans un coup d'éclat que crever à petit feu dans le smog. 

Elle a les yeux complètements éclatés. J'arrive pas à savoir si c'est à cause de son troisième joint ou parce qu'elle est sortie tout à l'heure. Elle pouvait pas savoir que ça monterait si vite, surtout à cette période de l'année, et s'était faite piéger. Heureusement qu'elle avait son masque, ou j'aurais dû partager mon lit avec Xlicz. J'adore ce gars, mais il y a des trucs qu'il sait pas faire. Manger proprement. Pisser assis. Savoir fermer sa gueule. 

Ah, je retire. Il vient de se barrer, il doit pas avoir envie de débattre avec Lysa. En même temps, avec ce qu'il s'est pris hier, je le comprends. J'espère juste qu'il va pas partir trop loin, j'ai pas envie d'aller à sa recherche. Oh, et puis qu'il se démerde. Je l'aime bien, mais il y a des limites à tout. Et puis, si ça me laisse du temps avec ma chérie, je vais pas dire non. L'intimité, ça se cultive de nos jours. Je me glisse dans le canapé à côté d'elle et elle me fourre directement son joint de la bouche. Je tousse par réflexe, mais je sens déjà les effets de la fumée. C'est de la forte, elle a dû la récupérer En-dessous. J'ai à peine le temps de lui faire comprendre mes intentions avant que mon esprit disparaisse complètement dans les vapeurs de l'herbe.  

J'arrive pas à savoir combien de temps il s'est écoulé quand je me réveille, encore à moitié dans les vapes. Littéralement et métaphoriquement. Il fait gris. Il fait tout le temps gris, même la nuit, ça aide pas. Mais j'ai la dalle, donc j'ai dû dormir un bon moment. Xlicz est rentré, sa silhouette tassée dans un fauteuil à l'autre côté de la pièce nous regarde dormir. 

— Pervers. 

Ma voix est sacrément rauque. J'ai dû gueuler comme c'est pas permis, heureusement qu'on a plus de voisin. À part la vieille harpie, mais elle je m'en fous. À travers mes yeux à demi-ouverts je vois Xlicz s'agiter, puis... tomber du fauteuil ? 

Merde.  

C'est pas Xlicz. 

Ça suffit à me réveiller complètement. Comment j'ai pu confondre le gabarit de Xlicz avec un gamin ? Il serait furax. Mais surtout, qu'est-ce que fout un gamin ici, dans notre appart ? Il y en a bien assez des vides autour. Je me resape en vitesse et jette un drap sur le corps de Lysa encore inconsciente. Il en a assez vu. Le môme s'est approché de moi, mais n'a encore rien dit. J'hésite entre l'envoyer balader, le jeter par la fenêtre ou lui demander ce qu'il fout ici, merde. J'opte pour la troisième option, avec un peu plus de tact. 

— Qu'est-ce que tu fous ici, merde ? 

Tant pis pour le tact. 

— Tu veux bien m'adopter ? 

Honnêtement, je m'attendais à rien. Mon cerveau embrumé n'a pas une imagination assez développée pour donner une raison d'être à cet enfant. Je bredouille le premier truc qui me vient à l'esprit. 

— Et ils sont où tes parents ? 

Il pointe le ciel derrière moi, où un paquebot de croisière tombe lentement, à la verticale. Trois départs d'incendies différents sont visibles, et plusieurs étincelles humaines sautent vers une mort certaine. Je reste médusée devant ce spectacle pendant plusieurs minutes, alors que la chute inexorable de la gigantesque embarcation est amortie par le smog. Il va continuer de brûler là plusieurs jours, échoué sur la chape de pollution. Et je viens de faire assister un enfant à ce spectacle macabre. Bien joué, NyX, tu sais y faire avec les enfants. Mais une minute, s'il est là et eux là-bas, il a été abandonné ? 

— C'était des salauds tes parents, de te laisser derrière eux comme ça. Bien fait, tiens. 

Mais pourquoi il continue de pointer le ciel, le navire est pas du tout là ? S'il montre le ciel, ça voudrait quand même pas dire que... 

— Ils sont partis au ciel. Ils étaient très malades, et maman a dit qu'ils allaient prendre un peu d'avance, pour me préparer mon lit. Et papa m'a dit de retrouver quelqu'un En-dessous, qui pourrait s'occuper de moi. Mais j'ai pas envie d'y aller. C'est là où il y a la fumée qui les a rendus malades. 

Oh. Merde. Bon, ça va pas, j'ai pas les idées claires. Je lui fais signe d'attendre ici et je vais dans la salle de bain. On devait pas prendre les pilules détox avant le coup de demain soir, mais j'ai besoin d'avoir les idées claires maintenant et d'arrêter d'enchaîner les boulettes. Je fais couler un peu d'eau dans un verre puis m'arrête, la petite gélule bleue dans la main. Un mauvais moment à passer. Et glou, et glou. Dégueulasse. 

Mais ça donne le coup de fouet qu'il me fallait. Brrr, ça réveille. J'y vois clair, j'y pense clair, et j'ai clairement mal à la tête. J'ai déjà avalé la pilule pour ça, et je suis pleinement fonctionnelle en un instant. Et il me fallait au moins ça pour constater que mon t-shirt avait été complètement déchiré et qu'il cachait plus grand chose. Ça a dû être sauvage, j'aurais bien aimé m'en rappeler. Je pique une veste militaire de Xlicz, histoire d'être moins indécente devant le petit, à défaut d'être décente. 

Le salon me pique les yeux et la gorge. Les effets de la pilule détox sont violents, j'ai l'impression qu'une armée de petits soldats sanitaires veut sortir de mon corps pour faire sa fête à l'atmosphère toxique de la pièce. Ah ouais, c'est pour ça qu'on en prend pas d'habitude. L'air vicié de la ville a rendu tout ce qui était sain douloureux. Je douille, mais le garçon a l'air d'aller. Dans le doute, je vais ouvrir la fenêtre. Faudrait pas qu'il soit défoncé avant qu'on ai mis les choses au clair. Pas de signe du smog sur au moins dix mètres, ça me va. Et au pire on a des alarmes. Si elles marchent encore. 

Pendant que les vapeurs d'herbe se dispersent, je dévisage le petit homme. Il est vraiment pas grand, je lui donne dix ans grand max. Brun, une calvitie beaucoup trop précoce ou des cheveux arrachés. Yeux verts, petit nez. Maigre et dépenaillé. Il tient fermement un bout de chiffon, sans doute un doudou. Ça fait combien de temps qu'il a rien bouffé ? Je disparais dans la cuisine pour ressortir deux barres. Les Énergix, ça nourrit pas, mais ça fait passer la faim. Et ça a le goût de chocolat. Je lui en tends une et gobe l'autre. Moi aussi faut que je me mette un truc sous la dent. 

Il prend son temps pour manger. Je sais pas s'il trouve la barre dégueu ou s'il savoure. Mais ça m'arrange, je peux le cuisiner un peu. Je lui pose des questions pour savoir d'où il vient, ce que faisaient ses parents, et qui il devait retrouver En-dessous. Il parle plus beaucoup, il devait préférer quand j'étais qu'à moitié consciente. J'ai au moins réussi à lui arracher son prénom. Thomas Torsley. C'est pas fameux, on va pas se le cacher. Je vais l'appeler T-T, avec sa tronche d'enterrement. 

Lysa émerge un peu après. Comme moi un peu plus tôt, elle observe la pièce complètement confuse avant de réaliser ce qu'il se passe. Elle remonte le drap et se saisit de ses lunettes de soleil allongées qui lui donnait des airs de diva des années 90. Elle a toujours été très photosensible après avoir fumé. Elle pointe son index vers le petit Thomas et ouvre plusieurs fois la bouche sans qu'aucun son n'en sorte. Quand enfin elle arrive à s'exprimer, sa voix est étonnamment calme. 

— C'est qui ? 

— Ma chérie, je te présente Thomas. On l'a heu... adopté. 

— Pardon ? 

— Je peux tout t'expliquer ! 

— Mais je suis pas du tout émotionnellement prête à avoir un gosse ! 

Sa voix est montée si haut dans les aigus que j'ai dû me couvrir les oreilles. Le choc a été double pour elle, je peux même voir les étoiles danser devant ses yeux. Je la prends dans mes bras pour la consoler en lui murmurant que tout va bien. Elle se calme à peine que Xlicz débarque en trombe, enfonçant la porte d'entrée ouverte. 

— Vous allez bien les filles ? J'ai entendu un... pourquoi il y a un mini-type dans le salon ? 

Fantastique. Au moins ça me permet d'économiser les explications. J'enfile une robe à une Lysa toujours sonnée et la réveille juste assez pour qu'elle puisse m'écouter. Je leur raconte le peu que je sais sur ce garçon, ses parents qui sont morts d'un empoisonnement au smog, et du type qu'il doit retrouver En-dessous sans savoir qui. J'espérais que le nom de Torsley rappelle quelque chose à Xlicz, mais il a beau être d'En-dessous, il n'est pas « de ceux qui traînent avec des gens de la ville haute ». J'ai failli le foutre dehors, surtout quand il a rajouté que nous on était pas pareil, qu'on était des vraies dessoudeuses. 

J'ai couché le petit dans la chambre, puis on a tenu conseil. On est beaux à voir, tous les trois autour d'une petite table, Lysa dormant à moitié sur mon épaule. Notre gros coup est pour le lendemain soir, et on peut pas se permettre de louper le coche. C'est pour ça que j'ai pas donné de pilule détox à Lysa. Je sais qu'elle serait contre mon idée. J'ai juste à convaincre Xlicz, et ça va déjà pas être facile. 

— On en fait quoi du gosse, on le laisse ici ? 

— Tu veux laisser un gamin de 10 ans livré à lui-même dans la haute ? Et dans notre tour, en plus ? J'te pensais pas si cruel, Xly. 

— Il y a Mera. 

— La vieille mégère du dessus ? Elle a fait crever treize chats. Treize, tu te rends compte ? Je sais même pas comment elle en a eu autant. 

— Ok, alors tu veux faire quoi ? 

— On l'emmène avec nous. 

— Dans l'assaut ? Mais t'es malade ! 

— Pas si fort. Mais non, faut qu'on aille En-dessous de toute façon. Avant de retrouver les gars, on a un petit moment avant pour chercher son tuteur. 

— C'est pas le plan, NyX. Et j'aime pas ça. 

— Depuis quand tu suis les plans, c'est pas toi le plus grand anarchiste de la ville souterraine ? 

— J'aime pas ça, c'est tout. Et on fait quoi, si on trouve pas ton gars avant demain soir, hein ? 

— On avisera. Luzile a dit qu'elle hésitait à venir avec nous, il pourrait rester avec elle. Remplacer Zimon. Et puis, on peut pas le laisser en plan. À ce compte-là, tu ferais mieux de le balancer toi-même dans le smog. 

— J'arrive pas à croire que j'approuve ton idée... Lysa va être furieuse quand elle va dessoûler.  

— Je la gérerai, t'inquiète pas pour ça. Va préparer tes affaires, départ dans une heure.  

Je l'ai pas gérée. Elle avait beau ne pas être active, elle a suivi notre discussion à propos de Thomas. Ne pas l'intégrer dans la décision n'était pas sympa. Elle n'avait rien dit devant lui, mais je savais que mon tour viendrait. Lui donner la pilule détox juste avant de pénétrer dans le smog non plus. Mais je crois que ce pour quoi elle m'en veut le plus, c'est la robe. Et je l'ai pas volé. Juste après m'avoir collé une baffe mémorable, elle s'est éclipsée pour se changer et mettre son habituelle combinaison de combat, moulante à souhait. J'ai hésité à lui faire la remarque, mais ma joue encore brûlante m'a fait comprendre que ce n'était pas le moment.  

De mon côté, j'ai gardé la veste de ranger de Xlicz, mais j'ai mis par dessous des vêtements plus convenables. J'ai pas de combinaison onéreuse comme celle qu'a volé Lysa, simplement plusieurs épaisseurs fines et collées au corps pour laisser un minimum de prise aux volutes toxiques. On a enveloppé Thomas dans une bulle de plastique imperméable de fortune, en espérant qu'elle soit assez étanche pour le trajet. Notre ami l'anarchiste est en simple débardeur et bermuda. J'envie la résistance naturelle des gens d'En-dessous au smog. Tant qu'ils ne le respirent pas, ils pourraient y vivre toute leur vie. 

Chacun met sa cagoule, un masque à gaz rudimentaire et nous sommes partis. Les capsules nous donnent une demi-heure d'air respirable. D'ordinaire c'est largement suffisant, mais avec tout notre équipement et un gosse dans les pattes, on est vachement plus lents. L'entrée de l'En-dessous la plus proche est de l'autre côté de la voie rapide. Qu'il faut traverser en vie. 

Les voitures et les automates filent à toute vitesse dans les lignes droites qui séparent les blocs d'habitations. Les gens de la ville basse me foutent les jetons. On dirait des fantômes dans leurs boîtes de métal, les yeux fixés sur la route, dans une recherche de vitesse infernale. Et c'est sans parler des robots de maintenance qui sont encore plus rapides et qui n'hésitent pas à dégager les bolides du chemin. C'est à peine si j'arrive à suivre du regard les engins aux lignes déformées. 

— On aurait dû la faire, cette passerelle. 

Personne ne répond à Lysa, mais on pense tous la même chose. On ne passera jamais. Avec la barque du premier, en revanche... Le gars à qui elle appartenait voulait s'en servir pour partir, mais il a finalement abandonné son projet pour un radeau communautaire. Je sais pas comment il a fini, mais j'ai vu aucune nouvelle d'un radeau du style qui était arrivé à destination. Donc mal, probablement. Il a eu la gentillesse de nous laisser sa barque. 

C'est une barque en bois, avec deux rames. Elle n'a clairement pas été pensée pour la navigation. Peut-être un vieux modèle, de ceux qui allaient sur l'eau ? Elle devra faire l'affaire, on n'a pas le temps de faire la fine bouche. Au moins, on tient tous dedans, même si on doit se tasser. Lysa prend les rames, Xlicz le gouvernail et moi Thomas. Pourquoi il y a un gouvernail dans une barque avec des rames ? 

J'ai pas le temps de poser la question que Lysa lance la barque dans le vide d'un grand coup de rames. Thomas souffle d'émerveillement. Notre embarcation est suspendue dans les airs au-dessus de la route déchaînée, supportée par la densité de la pollution qui nous empêche de nous écraser. Malgré tout, notre poids est trop grand et on tombe inexorablement. Lysa bataille férocement, ignorant les carcasses massives des robots qui frôlent notre coque, l'esquintant à plusieurs endroits. 

J'aimerais dire qu'on s'est posés en douceur, mais on s'est violemment crashés. La proue est en morceaux, un peu comme mon dos quand j'ai été balancée par-dessus bord. J'ai fait écran pour ne pas que Thomas ne se blesse, mais les graviers m'ont sévèrement égratigné. Malgré tout, on est arrivés de l'autre côté, et en vie. Grand succès ! 

C'est Xlicz qui repère en premier la trappe vers l'En-dessous. Il a l'œil pour ces choses-là, il faut dire. Il descend en premier, puis je le suis, Thomas sur mes talons. L'échelle de métal est assez longue, et je me tiens prête à le récupérer s'il glisse ou tombe, mais le bonhomme tient bon. Lysa nous rejoint après avoir méticuleusement fermé la trappe. Un air frais circule dans le tunnel, mais rien à voir avec l'atmosphère glaciale du smog. La brise charrie un air pur, ou du moins suffisamment respirable pour qu'on enlève les masques. On prend ce qu'on a. 

Je n'ai jamais pris cette entrée. On dirait qu'on a pas mal dérivé, dans la barque. Lysa n'en mène pas large non plus, mais Xlicz n'a aucun doute, encore une fois. Il a l'air plus confiant, maintenant qu'il est dans son élément. On traverse plusieurs galeries désertes avant de déboucher sur une avenue fréquentée. Il se tourne vers nous, et en particulier vers le petit. 

— Bienvenue dans la Souterraine ! 

La rue est gorgée de monde. Des dessoudeurs pour la plupart, avec le même teint pâle et la même carrure ramassée que Xlicz. Mais il y a aussi des gens de la ville basse avec leurs traits déformés par le manque de sommeil et la vitesse, ainsi que, beaucoup plus rares, des résidents de la ville haute, comme nous. On est ce qui se rapproche le plus des humains qu'on voit dans les anciens bouquins, mais il nous manque quelque chose par rapport aux autres. Un éclat dans les yeux, celui de la vie et de l'espoir, qui a quitté les résidents trop pauvres ou trop lâches pour aller dans l'espace. Ça se vérifie encore ici, les quelques congénères qu'on croise ont les yeux ensanglantés à force de consommer les drogues les plus dures pour oublier leur condition. J'ai peur que Lysa et moi on devienne comme ça, un jour. J'ai peur de perdre la flamme qui m'anime et me donne une raison de vivre. 

On se consulte rapidement du regard. On va d'abord déposer nos affaires à la planque, puis on se renseigne sur le Torsley. Notre repaire est vide. Cela dit, on a plus d'un jour d'avance, c'est pas vraiment une surprise. On lâche nos sacs ici et on devise de la marche à suivre.  

— On va jamais s'en sortir si on doit sillonner l'En-dessous ensemble. On se sépare, rendez-vous ici dans 5 heures. 

Ça c'est ma Lysa ! Toujours directe et charismatique. 

— Je vais garder T-T avec moi, peut-être que quelqu'un le reconnaîtra. 

Et même si je voulais m'en débarrasser, le gamin s'agrippe à moi comme un mollusque à son rocher fétiche. Impossible à dégager. Xlicz était déjà parti faire le tour de ses sources et amis. Quelque chose dans cette entreprise me paraissait vain. Si lui n'arrivait pas à trouver quelqu'un qui réponde à Torsley, je ne vois pas comment on aurait la moindre chance. Mais c'est pas ça qui va m'arrêter. Si j'étais du genre à abandonner lorsque je n'ai aucune chance, je n'aurais jamais demandé à Lysa de sortir avec moi. 

J'ai écumé tous les bars et hôtels qu'on a croisé, harcelé tous les passants. Aucun ne connaît de Torsley d'Eve ou d'Adam, mais beaucoup reconnaissent un nom de la haute. L'animosité dans certains regards me fait froid dans le dos. J'ai de la chance de ne pas avoir un look conventionnel, tout en piercing et en cheveux améthyste, sinon j'aurais pu mal finir. Je m'inquiète un peu pour Lysa, mais pas longtemps. Elle sait se défendre bien mieux que moi. C'est elle qui pourrait venir à ma rescousse. 

Au bout de quatre heures à faire chou blanc je décide de rebrousser chemin. Il n'est pas tout à fait l'heure de rendez-vous, mais j'ai faim et Thomas a l'air épuisé par notre marche. J'essaie de le prendre sur mon dos, mais je suis vraiment pas douée avec les enfants. En essayant de le caler au mieux je manque de rentrer dans une armoire à glace. J'aurais préféré que c'en soit vraiment une, parce que celle-ci est visiblement là pour moi. Et malgré le teint caractéristique, le dessoudeur est beaucoup plus grand que les habitant d'En-dessous. 

— C'est pas trop compliqué, pour franchir les portes ? 

Ah ouais. C'est tout ce que t'as en stock, NyX ? Une mauvaise vanne sur sa taille ? S'il ne me voulait pas du mal avant, là c'est cuit, et je le comprends. J'aurais fait la même chose à sa place. Mais il ne bouge pas. Il se contente de me fixer, jusqu'à ce qu'une voix s'élève derrière lui. 

— C'est vous qui cherchez partout l'ami de Torsley ? 

Eh merde. C'est jamais bon, quand quelqu'un a le besoin et l'argent pour embaucher un garde du corps. Surtout un de cet acabit. Je risque le bluff, mais j'ai du mal à retenir le tremblement dans ma voix. 

— Tout dépend de qui le demande. 

— Écoutez jeune fille, je n'ai pas le temps de jouer à ce petit jeu. Qu'est-ce que Martin me veut ? 

Je ravale une réplique cinglante. J'ai beau être impressionnée par son gorille, j'aime pas du tout son ton condescendant. Mon petit fardeau a réagi au nom de son père et je le fais descendre aussi délicatement que possible. 

— Il est mort. J'ai son fils avec moi, et il lui a dit de vous retrouver. 

— Martin, mort ? Comment est-ce arrivé ? 

— Empoisonnement au smog, apparemment. Avec sa femme. J'ai pas vu les corps moi-même, mais ce que le petit m'a rapporté laisse peu de place au doute. 

— Ce n'est pas possible, il était trop prudent pour ça... Je vous remercie, jeune fille. Ylkr, donne-lui donc une récompense pour ses efforts. 

Tout ça est un peu trop suspicieux à mon goût. Mais après tout, je ne lui dois rien à ce gosse, et le gros homme avait l'air sincèrement choqué d'apprendre la mort de Torsley senior. Par contre, pas question d'accepter une quelconque récompense. J'aurais l'impression de vendre Thomas, et le trafic d'humains n'est pas dans mes habitudes. 

— Gardez-la plutôt pour vous occuper de lui, moi ça ira. 

— Et généreuse avec ça ! Il vous en remerciera, soyez-en sûre. 

Le trio disparaît rapidement dans la foule. Il ne faudra pas que j'évoque la récompense que j'ai refusée devant Lysa. On en avait besoin, de cet argent. Mais tant pis, au moins je reste fidèle à mes valeurs. J'ai du mal à me persuader que j'ai fait une bonne action, cependant. J'ai un goût amer en bouche. Était-ce la bonne décision ? Je ne le saurais peut-être jamais. 

J'erre dans les rues pendant une demi-heure avant de rejoindre les autres. Je leur explique que la situation est résolue, que Thomas a été confié à son tuteur, un homme dont je n'ai pas saisi le nom, visiblement très riche. La question est enterrée lorsque Sylvic arrive, suivie du reste du groupe. Ce petit interlude a pu nous distraire, mais la journée n'est pas terminée. On a un navire spatial à voler. 

 