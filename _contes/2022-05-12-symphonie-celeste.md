---
bg: "symphonie-celeste"
layout: post
title: Symphonie céleste
summary: Un artiste de génie paye de sa raison d'être son orgueil.
source: “Nuage” par Quentin Mabille
ref: https://www.artstation.com/artwork/RY6bkO
date: 2022-05-12
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---
Tout jeune déjà, il préférait vivre à l'extérieur, loin de l'enfermement des murs qui l'enchaînaient. C'était un être de liberté, si aérien dans ses mouvements qu'il semblait s'envoler à chaque saut. Il vivait parmi les éléments, là où était sa place. Des grands artistes il fut le plus grand, jusqu'à ce qu'il perde l'essence de son existence. 

Issu d'une famille sans éclat, rien ne le prédisposait à son destin. Dénués d'art, ses parents ne pouvaient espérer quitter la citadelle, aussi ils furent effrayés lorsque leur jeune poupon se retrouva emporté hors des murs. Peu furent volontaires pour chasser les bandits, tant l'extérieur était effrayant et dangereux. Seuls les exilés et les brigands osaient y vivre, souvent pour leur malheur. Aussi, lorsque l'équipée vengeresse retrouva le groupe de kidnappeurs éparpillé par une tempête, ils ne s'attendaient pas à ce que la victime ait survécu. Même le plus résistant des gaillards peinait à avancer face aux turbulences. 

Dans la tourmente des éléments il en était le chaos pur, se mêlant souvent aux vents qui battaient la plaine sans relâche. Ses gestes bruts, sans élégance, pliait les courants comme d'autres le papier. Les lames qui avaient découpé les cruels hommes caressaient l'enfant comme une brise de printemps. De retour à Ahn-Terhêt, il fut porté en un triomphe à la hauteur de son génie. Un tisseur de vent, de ceux qu'on ne voyait plus depuis les fondateurs de la citadelle. 

Il grandit dans le faste, bien loin de ce que ses parents auraient pu lui offrir. Perfectionnant son talent, il s'éloigna des siens et s'enfonça dans la nature sauvage. Il foulait la plaine qui avait coûté tant de vies sans crainte, protégé par un vent bienfaiteur. Même quand des nuages jaillissait le cavalier des tempêtes, il revenait toujours immaculé. 

Son art grandit plus subtil et raffiné qu'il n'était supposé possible. Il faisait jouer les rafales par la pensée et soufflait plus fort que les zéphyrs. Pour la première fois en ses siècles d'existences, la ville fut épargnée par le vent une semaine entière, à sa seule décision. Mais l'air n'obéissait qu'à lui, et il ne faisait qu'un avec. Cette semaine le rendit profondément malade, d'un mal qui ne guérit que lorsqu'il put arpenter la plaine à nouveau. 

Jeune homme, il dépassait déjà ses maîtres, rien ne l'empêchait plus d'exprimer sans limite son art. C'était désormais aux nuages eux-mêmes qu'il commandait, les faisant danser de sa baguette. On pouvait admirer les volutes qu'il dessinait dans le ciel à des centaines de kilomètres à la ronde. D'autres villes assistèrent à un phénomène qu'ils craignirent avant de réaliser que ce n'était là que l'étendue d'un art poussé à la perfection. 

Sa plus grande pièce fut le Cumulonimbus, qu'il créa en assemblant des dizaines de cumulus jusqu'à obtenir une immense masse nuageuse mimant la forme d'un marteau, surplombant le ciel lui-même. Il y mêla le pouvoir destructeur du nimbostratus, et réduisit ses adversaires d'alors en cendres au premier déluge d'éclairs. Impressionné par la puissance du Cumulonimbus, il décida de lui-même de le dissoudre et de ne jamais reformer pareille monstruosité. 

Alors à l'apogée de son art, le mestre de la citadelle le convoqua. Il désirait que son plus grand artiste trouve un moyen de protéger son territoire contre les éléments hostiles même après sa mort. L'homme hésita longuement. Non qu'il en fût incapable, rien n'était hors de sa portée. Mais ériger une barrière signifiait s'exiler volontairement d'Ahn-Terhêt. Sa réflexion l'amena à accepter, et épouser pleinement sa liberté. Il avait oublié le visage de ses parents, ignorant même s'ils étaient toujours vivants. Sans y penser à deux reprises, il donna son ultime concert à la citadelle qui l'avait vu naître. 

Une prestation magistrale, où il déploya les plus majestueux nuages avant de les dissoudre, révélant peu à peu le soleil qui se faisait timide depuis plusieurs jours. Chaque nouveau rayon était un nuage mourant, pour le plus grand bonheur de la foule qui l'acclamait. Il reproduisit même une version miniature du Cumulonimbus, réunissant pour cela les ultimes nuages qui couvraient la citadelle. Et alors que le spectacle en était à son point culminant, le nuage se dissipa et tous les vents tombèrent. Le silence tomba sur la foule comme une chape de béton. D'un coup, les orages incessants et les bourrasques interminables n'étaient plus. Plus aucune force ne battrait leurs visages, rien ne grifferait leur peau ni mélangerait leurs cheveux. Le ciel de l'horizon était d'un bleu si pur et à jamais. 

Seul un homme ne se joint pas à la liesse. Un homme qui réalisait soudain ce qu'il avait perdu. Il avait rassemblé tous les nuages du monde en un point pour le tuer, un par un. Il avait contraint l'incarnation de la liberté qui lui était si chère pour la faire disparaître. En réponse les vents s'étaient détournés de lui. Ou plutôt ils s'étaient estompés avec. Mais son pouvoir venait du souffle, des alizés qui battaient la plaine sans discontinuer. À présent que sa vanité les avait congédiés, il se retrouvait faible et fragile. Esseulé. Sans but. Il réalisait l'ampleur de sa bêtise, mais aussi celle de son péché. 

Pour la première fois de sa vie, il ressentit la peur. Celle de l'inconnu. Celle de ne pas avoir le contrôle. Il avait tout perdu de sa grâce et de son art. Naturellement, les gens se désintéressèrent de lui. Quand bien même il exercerait toujours un contrôle absolu sur le vent, il était inutile au sein d'une citadelle sans vent. Cette absence était pour lui toujours plus douloureuse, jour après jour. L'extérieur n'était plus chez lui, et il se morfondait terriblement dans son manoir vidé. 

Et puis un jour, malgré son art perdu, malgré son corps frêle, il décida de passer une nouvelle fois les portes de l'inconnu. Il était persuadé que quelque part dans le vaste monde, vents et nuages s'étaient réfugiés. Il se refusait à croire que tous aient répondu à son appel. Il n'emporta dans son voyage qu'une carte, des provisions, et sa fidèle baguette. Puis il marcha, sans s'arrêter. 

Lui qui était tant épris de liberté, il n'y goûtait réellement qu'en ces instants. Sans foyer où retourner, la couche à l'air libre, le cosmos était son campement. Si libérateur en réalité, ce sentiment. Son voyage lui apprit bien des choses. Il admira des splendeurs sans pareil, escalada une montagne de verre, remonta une cascade inversée et traversa une forêt luxuriante dont tous les arbres étaient identiques. Il fit bien des rencontres aussi, sur son chemin comme au bout du monde. Les vagabonds n'étaient pas nombreux, mais il n'était pas seul. Des compagnons de voyages temporaires, d'autres avec qui il décida de former un groupe. Et puis, perdu dans la haute steppe, il retrouva son vent. 

Ce n'était pas ce à quoi il s'était attendu. Nul nuage n'avait couvert le ciel depuis son départ, bien des années plus tôt. Aucune brise ne lui avait effleuré le visage. Le soleil, seul, brûlait un sol assoiffé. À la honte s'était ajouté un sentiment de culpabilité, alors qu'il s'était convaincu que toute la misère du monde lui imputait. Il n'avait ressorti sa baguette de son sac, de peur d'être reconnu. Mais son vent l'avait reconnu, et lui avait pardonné. Il l'avait caressé comme il avait l'habitude de le faire auparavant. Il était jeune et faible, mais reconnaissable entre mille. 

Alors l'homme sortit une baguette humide, battue et malmenée par le temps, et commença à l'agiter. De son vent délicat jaillirent des nuages, puis les bourrasques reparurent. Il créa un nimbostratus et connut la sensation de l'eau se déversant sur son visage. Les nuages apparaissant, il ne commandait plus aux vents, mais les laissait vaquer à leur rythme. Il les laissa se disperser dans le monde, libres comme il l'était. Son art ravivé résonnait avec les rafales, donnant lieu à sa nouvelle symphonie. 

 