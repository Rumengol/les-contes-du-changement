---
bg: "la-prison-des-dieux"
layout: post
title: La prison des dieux
summary: L'archiduc ayant transgressé l'interdit divin se retrouve piégé dans le désert infini.
source: “Storm is coming” par Javier Franco Santacreu
ref: https://www.artstation.com/artwork/GanYlN
date: 2021-05-16
categories: posts
tags: ["La prison des dieux"]
type: Conte
author: Rumengol
lang: fr
---

Le désert infini n’avait pas de fin. Ainsi formulée, l’évidence était risible, mais elle portait en elle l’inconcevable vertige de ce concept. A perte de vue, il n’y avait que du sable, de la poussière et les ossements d’êtres titanesques. Les traces de la silhouette drapée de rouge étaient balayées par le vent aussitôt que la sandale quittait le sol. C’était comme si le voyageur était figé dans le temps, chaque pas oublié dès le suivant.

Son visage était lacéré par les grains de sable vindicatifs, le tissu qui le protégeait réduit en lambeau depuis bien longtemps. Autrefois royal, ce faciès n’était plus celui d’un seigneur, labouré de griffure, le regard seulement las. En réalité, si quiconque le voyait aujourd’hui, il le prendrait probablement pour un mendiant, si ce n’était pour son épée rutilante dont le pommeau doré était encore orné de cinq pierres précieuses. Mais il ne rencontrerait personne.

Durant ses sept cents ans de marche, l’archiduc déchu n’avait jamais rien vu d’autre que l’ocre du ciel, l’orangé du sable, et l’ivoire vieilli des squelettes. Telle était sa punition pour avoir bravé l’interdit et tenté de s’élever au rang de dieu. Il avait percé le secret de l’immortalité parfaite, permettant à son corps de ne jamais vieillir, à outrepasser les besoins si primitifs de se nourrir, boire ou dormir. Les dieux égoïstes, effrayés peut-être, n’avaient pas supporté qu’un mortel puisse obtenir par son astuce, ce qui leur avait été offert. Pour cette transgression, ils avaient condamné l’archiduc à errer sans fin dans leur geôle, et son peuple à l’oubli ou à la repentance.

Les premiers temps, il avait conjuré tous les pouvoirs qu’il se connaissait pour forcer la barrière qui le retenait dans ce piège de sable. Il s’était résigné au bout de treize ans. Il s’était alors équipé des habits les plus solides qu’il avait pu confectionner, et avait débuté sa longue marche. Il n’avait jamais dévié, progressant toujours selon une ligne droite. Si la curiosité ou un détail l’attirait hors de son chemin, il veillait à construire un repère, parfois à partir de ses vêtements. Ainsi il savait qu’il ne se désorientait pas.

Quatre fois, il avait abandonné, se laissant à une mort qui ne l’a jamais gracié de sa visite. Allongé au creux d’une dune, il s’était laissé ensevelir pendant des années, parfois des dizaines d’années. Chaque fois, ennuyé de ne pouvoir convoquer la mort de ses vœux, il s’était relevé. D’autres fois, il avait sombré dans la folie. Dans de tels épisodes, il avait avalé plusieurs des pierres précieuses qui décoraient son épée. Il ne se souvenait plus de la raison. Peut-être pour tenter de se suicider, comme la fois où il avait bu une dune entière ? Inutile de rappeler que ça n’avait pas marché. Peut-être la raison était-elle toute autre, une illumination qui lui était venue dans son délire ?

Un autre siècle, il avait pris la résolution de s’évader de la prison des dieux en volant, puis en creusant. Ensablé, il fallut seulement quelques mois avant que la douleur que provoquaient les griffures de quartz et de mica n’eut raison de sa volonté. Lorsqu’il était remonté, il avait découvert être à quelques mètres à peine de la surface. Du moins c’était ainsi qu’il se rappelait la chose. La mémoire d’un homme fou de vieillesse et de douleur n’était pas ce qu’il y a de plus fiable.

Maintenant qu’il s’était lassé de la folie, il avait trouvé comment s’y soustraire. Si d’aventure il sentait son esprit dériver vers les limbes, il portait le pommeau à ses yeux, tirait la lame si c’était nécessaire. Il se rappelait alors la grandeur de son domaine d’autrefois, les grands jardins qui l’enorgueillissaient de leur splendeur. Il revoyait le blason de sa famille, les deux lions dévorant le soleil symbolisaient l’ambition sans limite des Timéus. Il ne savait dire si ses ancêtres étaient fiers qu’il soit allé si proche du soleil, ou bien outrés que cela ait coûté l’existence de l’archiduché.

Il revoyait bien des choses, chaque souvenir était gravé au fer rouge dans son esprit. Il était désormais le seul dépositaire de la mémoire de son peuple. Les dieux souhaitaient le réduire à l’oubli, mais tant que son seigneur était vivant, la mémoire de l’archiduché ne disparaîtrait pas. Et il ne pouvait mourir.

Il s’était découvert une détermination qui le poussa vers l’avant pour quatre siècles sans faiblir. Sans folie ni abandon, rien qu’un pas lent contre le vent qui ne laissa aucune trace. Sortant de sa torpeur séculaire, le marcheur se remit à penser. Son pied s’était coincé dans une vertèbre aussi grosse que lui, et avait manqué de le faire chuter. Le fossile s’effrita facilement et le libéra. L’homme en profita pour s’arrêter. Pour la première fois depuis des centaines d’années, il porta son attention sur la carcasse.

Elle n’était pas si différente des autres, à moitié ensevelie dans le sable. Les ossements avaient appartenu à un peuple de géants, pour qu’une simple côté fasse le double de sa taille. Ils avaient de longs bras, des jambes plus courtes, et un crâne ne ressemblant à rien de ce qu’il connaissait. Ni homme ni animal, ce peuple condamné au même châtiment que lui était intrigant. Il s’était longuement interrogé sur la nature de leur crime, pour être ainsi sévèrement puni, avant de s’en désintéresser. Ils étaient morts depuis un temps si long que leurs os étaient devenus pierre sous la simple action du sable, leur sort était résolu et tout souvenir les concernant avait disparu avec le dernier d’entre eux.

Cependant, il remarqua qu’ils étaient plus épars, dernièrement. Si son point d’arrivée était un véritable cimetière aux proportions ridiculement grandes, il ne discernait un nouveau squelette qu’une fois par année désormais &mdash; ou ce qui lui semblait en être une. Les plus robustes, ou les plus jeunes, avaient tenté eux aussi de fuir, des éons auparavant. Grâce à leur taille, et sans doute un corps bien plus résistant que celui d’un humain, ils avaient parcouru des distances pharamineuses. Mais ils étaient dépourvu de son atout, celui qui permettra à l’ancien archiduc de triompher là où les anciens ont échoué.

Il marcha encore deux cents ans avant de croiser le dernier squelette. Il ne s’en douta pas alors, mais trois siècle plus tard, il fit cette conclusion. Sur son chemin, il n’y avait plus que l’ocre du ciel et l’orangé du sable. Pour l’éternité. Il se retourna pour adresser un mot d’adieu à ces infortunés qui eurent le bon goût d’agrémenter son voyage dans leur mort. C’est alors qu’il réalisa qu’il avait oublié comment parler. Les mots ne venaient pas, et ses cordes vocales étaient pétrifiées après un millénaire d’inaction. La douleur lui coupa les jambes, et il se résolut de ne plus parler. Personne n’était là pour l’entendre, inutile de se faire du mal.

Il marcha encore longtemps, perdant peu à peu la notion du temps. Il l’abandonna au profit de sa mémoire intacte. Il ne remarquait pas non plus ses habits qui se délitaient peu à peu. Nu, portant seulement une ceinture menaçant de se briser, les yeux tournés vers lui-même, il n’était plus qu’un automate de chair.

Il ne vit ni le ciel s’éclaircir peu à peu, ni la poussière laisser place à un azur dont il avait oublié jusqu’au mot, ni le vent meurtrier devenir une simple brise. Ses paupières fermement closes se fossilisaient depuis quelques années, comme son visage impassible qui, telle une statue, ne montrait qu’une expression d’effort acharné. Ses oreilles bouchées par le sable ne perçurent pas les sons si singuliers qui l’entouraient.

Mais lorsqu’il posa le pied sur un sol dur, qu’il ressentit pour la première fois en plusieurs millénaires l’impact de son poids être répercuté dans sa jambe, son visage minéral se fissura. Le cœur ranimé, il ouvrit les yeux.
