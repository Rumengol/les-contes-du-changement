---
bg: "le-chasseur-le-tengu-et-la-pluie"
layout: post
title: Le Chasseur, le Tengu et la Pluie
summary: Un chasseur de démon confronte un tengu dans une forêt de bambou
source: “Battle in the bamboo forest” par Z.T. Jing
ref: https://www.artstation.com/artwork/lxvn0k
date: 2021-05-29
categories: posts
tags: ["Les chasseurs de démons"]
type: Conte
author: Rumengol
lang: fr
---
Plus il s’enfonçait dans la forêt de bambous, plus les convictions d’Akira se confortaient. Il n’était pas à la poursuite d’un simple fantôme. Une cruauté pareille ne pouvait être déployée que par un authentique démon. À en juger par l’odeur du bambou fraîchement coupé, sa cible avait peu d’avance. 

Le démon ne se souciait pas d’être discret. Il ravageait la forêt sur son passage, tranchant indistinctement tout ce qui se trouvait sur son chemin. Le guerrier n’avait qu’à suivre le sillage des bambous décimés, comme une invitation. C’en était probablement une, mais il en fallait plus pour décourager Akira. Il se battrait sur le terrain choisi par le démon, et il vaincrait.

La pénombre de la forêt était peu à peu remplacée par la clarté d’une clairière. L’homme crut reconnaître des escaliers grossièrement taillés dans la pierre, signe d’un chemin humain. En s’approchant, il discerna des vieilles lanternes de pierres ainsi qu’un portail en ruines. Était-ce ça que le démon voulait lui montrer ? Ce chemin menait-il vers un sanctuaire abandonné ?

La pâle lueur du matin baignait la clairière d’une aura paisible. Le silence y régnait tant que les oiseaux eux-mêmes n’osaient le perturber. Le pas pourtant léger d’Akira résonnaient comme s’il était un ours. Perché sur la cime d’un bambou qui ployait sous son poids, un homme au visage de tengu pointait le guerrier de son sabre. Vêtu noblement, de longs cheveux blancs flottant dans la brise délicate, il aurait pu être confondu avec une divinité bienfaitrice, si ce n’était son long nez crochu et ses traits inhumains.

Tirant son sabre de son fourreau, le chasseur de démon respira profondément avant de s’avancer vers son ennemi immobile.

“Je suis Akira Torikami, je viens t’ôter la vie en réponse de tes crimes, démons !

&mdash; On me nomme Yama. Et tu n’es pas de taille.”

Les lèvres du tengu n’avaient pas bougé, mais sa voix si douce, contrastant avec son apparence, résonnait dans la forêt, portée par le vent. Les deux adversaires restèrent un temps d’observer, se jaugeant mutuellement. Puis Yama sauta de son bambou, qui claqua violemment contre ses congénères.

Akira eut à peine le temps de lever son sabre pour parer l’attaque. Le démon se mouvait à une vitesse surhumaine, chacun de ses coups étant précis et mortel. Le guerrier était repoussé sous une pluie de coup qu’il peinait à parer. À sa place, un guerrier ordinaire, même un samouraï, serait mort dix fois. Mais le chasseur était tout sauf ordinaire. Sur la défensive, il guettait la moindre ouverture lui permettant de récupérer l’initiative.

Elle fut brève, tellement qu’il la loupa presque. Replaçant son pied en avant, il déstabilisa légèrement le tengu et commença à frapper. Des étincelles surgissaient à chaque fois que les sabres s’entrechoquaient. Les deux combattants n’étaient qu’à quelques centimètres l’un de l’autre, la moindre erreur pouvait leur être fatale.

Akira ne réfléchissait plus. Tout son être était concentré sur le combat, il frappait avec la force de son expérience et de son cœur. Les mouvements qu’il avait répété des milliers de fois coulaient aussi naturellement que le sang de ses veines, chaque muscle lui obéissant parfaitement. C’était là le spectacle grandiose du guerrier accompli, qui a suivi la voie du sabre et l’a portée à son apogée. Contre un adversaire humain, il était invincible.

Mais Yama n’avait rien d’humain. Outre sa vitesse, sa résistance et sa force étaient hors du commun. Plusieurs fois il fut atteint, mais les blessures ne saignaient jamais plus de quelques secondes. Loin d’être ralenti, il prenait tous les risques, au mépris de sa sécurité, pour porter le coup à l’homme insolent qui le défiait.

Il changea alors de tactique, faisant appel à ses pouvoirs occultes. Une deuxième paire de bras fantomatiques vinrent harceler son adversaire, tandis que ses illusions troublaient les silhouettes de la forêt. Akira entama des prières destinées aux divinités protectrices des hommes, les conjurant de repousser la sombre magie et de purifier son corps.

Sans faiblir ni s’essouffler, le chasseur de démon parvenait à incanter continuellement, repoussant les multiples maléfices que Yama lui opposait. Les deux combattants étaient de force égale, aucun n’était à même de prendre le dessus sur l’autre. Ignorant les bambous qui tombaient, conséquence toujours plus importante de leur duel, ils progressaient dans la forêt.

Le tengu menait la danse, mais le guerrier s’y joignait sans se laisser distancer, dans un corps-à-corps passionné. Le métal, si vindicatif au départ, grinçait dangereusement à présent. Leurs sabres n’étaient pas forgés pour de si longs combats, et les lames menaçaient de se briser. L’arme d’Akira était en moins bon état, car le sang qu’il avait fait couler fatiguait considérablement le tranchant, aussi Yama attaqua de plus belle.

D’un coup rapide, il fit voler le chapeau de paille de son ennemi, révélant son visage dur et déterminé. Le démon s’inquiétait de voir ses forces diminuer. C’était assurément aussi le cas du guerrier, mais les tengus disposaient supposément d’une bien meilleure endurance que les humains. Plus la lutte perdurait, et plus ses certitudes de victoires s’amenuisaient. Il aurait cent fois préféré gagner équitablement, mais il lui fallait en finir.

Akira avait constaté la perte de vitesse de son adversaire. Elle était infime, mais indicatrice de sa victoire prochaine. Il faiblissait également, mais il était habitué à puiser sa force au plus profond de lui-même dans des situations périlleuses. Le tengu n’avait connu que des victoires faciles, et risquait de céder à la panique dès que son léger avantage disparaîtrait.

Une vive douleur perla dans la nuque d’Akira. Comme la piqûre d’une abeille enragée. Puis des centaines de pointes de douleurs descendirent sa colonne vertébrale jusqu’au bas de son dos. Momentanément paralysé par la douleur, il avait baissé sa garde. Trop tard pour la reprendre. Le sabre filait vers son cou beaucoup trop vite. Il lui fallait esquiver. Vite. Les muscles de son dos refusaient d’obtempérer. Trop tard. Il cria.