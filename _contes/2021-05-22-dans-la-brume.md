---
bg: "dans-la-brume"
layout: post
title: Dans la brume
summary: Une petite communauté se retrouve une fois de plus plongée dans la brume.
source: “Safehouse” par Eddie Mendoza
ref: https://www.artstation.com/artwork/8X1K6
date: 2021-05-22
categories: posts
tags: ["Après la Brume"]
type: Conte
author: Rumengol
lang: fr
---
D’après la lumière de leur chambre, Ivan et Laura sont déjà rentrés. Ils avaient été vachement rapides, pour remplir leurs deux sacs avant moi. J’espère juste qu’ils n’ont pas encore dépouillé un client. C’est mauvais pour les affaires. Remarque, c’est vrai que j’ai été dérangé. J’ai littéralement plus un clou, et ça devient de plus en plus dur de trouver des munitions pour mon fusil à air comprimé. Je devrais peut-être passer aux fusils classiques, il paraît qu’on en trouve partout, de leurs munitions. J’y penserai.

Ce que je craignais en arrivant se confirme : un mince filet de brume commence à recouvrir la région. Je siffle pour avertir Ethan et Sylvie, faudrait pas qu’ils se fassent surprendre. Surtout elle, elle a tendance à être tête-en-l’air. On a croisé Ethan en arrivant, donc en toute logique, elle monte la garde au niveau de l’autre chemin. Je siffle encore deux fois, pour être certain qu’elle a entendu.

“Home Sweet Home !”, avec un joli cœur à côté. C’est ce genre d’attention qui donne l’impression d’être à la maison. J’avais voulu l’appeler comme ça à l’origine, Douce Maison. Mais Daniel avait dit que c’était copyrighté. Comme si qui que ce soit en avait quelque chose à faire, du copyright d’un truc obscur du fin fond d’internet ? Comme il en démordait pas, on a appelé ça le Nouveau Paradis d’Eden. Et nous, on est le NPE crew. T’aurais pu t’en douter, c’est marqué juste en dessous. 

Ça ? Ah, tu parles du bordel qu’il y a autour de la maison. Au début, on s’était mis en tête de construire des barricades, pour les tenir à distance. Ça a pas été une riche idée, en une nuit elles ont été démolies, et violemment. Je sais pas non plus comment la maison tient debout, mais c’est comme qui dirait notre forteresse. Du coup on la retape et on l’agrandit comme on peut, mais c’est pas mal comme ça, je trouve. Et puis, c’est confortable.

Ah, Dolo’ a un client. Quoi, je t’avais dit qu’on faisait pas que survivre. On est aussi un point relai pour réapprovisionner ceux qui fuient. Apparemment, les sédentaires comme nous se font de plus en plus rares, surtout depuis que tout le monde essaie de rejoindre cette ville qui aurait survécu &mdash; Seattle, je crois. Nous on est bien, là, on a pas envie de partir. Du coup, on fait du commerce avec ceux qui traversent le pays. On leur échange la nourriture qu’on fait pousser ou qu’on chasse avec des matériaux ou des pièces difficiles à trouver. 

En ce moment, on essaie de réparer le quad pour faire des virées en ville plus facilement. On a de l’essence, mais Daniel dit qu’il manque une pièce du moteur ou je sais pas quoi. C’est notre priorité actuellement, mais il semblerait que les gens ne se promènent pas en forêt avec des moteurs de quad. Tu y crois, toi ? Tout se perd, dans ce monde. 

Le vieux est en train de marchander des vivres pour une semaine contre des comics détrempés ? D’accord, on est des gamins, mais faut pas pousser. Et il a le culot de s’énerver quand elle lui refuse le marché. Décidément, on voit de tout. Je lui saisis l’épaule en glissant ma machette sous sa gorge.

“Eh, papy, on en veut pas de tes merdes. La brume commence à se pointer, alors je te conseille de dégager vite fait d’ici si tu veux pas qu’on attende qu’elle s’installe pour te jeter dehors. Mais avant, tu vas nous donner tout c’que t’as.”

Le bougre est réticent, mais le temps joue contre lui, et le métal froid contre la gorge empêche de cogiter correctement. Je le sais d’expérience. Il finit par s’exécuter, abandonne son sac et fuit sans demander son reste. Une belle prise, finalement. Outre les bouquins, on a une carte de la région, une boussole, des allumettes, et même un pistolet. On en a déjà trop, mais ces petites merveilles, c’est trop tard pour se refuser. Non, tout à l’heure j’ai dit que les munitions étaient communes, pas les armes, suis un peu. Quand je me redresse, Dolores fait la moue.

“Tu aurais pu lui laisser ses affaires, quand même. Il était pas méchant au fond.

&mdash; Bah, c’est un homme mort de toute façon. Vu ce qu’il y avait quand je suis rentré, il n’a aucune chance de sortir de la forêt avant que la brume ne le rattrape. Ç’aurait été dommage que ça se perde, regarde !”

Elle n’a pas l’air convaincue par le *Spiderman* a moitié bouffé par l’humidité. Je la comprends un peu.  Je lui dis de barricader la maison et de prévenir les autres, une nouvelle nuit blanche nous attend. Dire que je n’aurais même pas eu le temps de me reposer... Je monte à l’étage, et trouve Daniel et Jericho en train de glander. Je les envoie faire le tour pour tout fermer, et rejoins le deuxième, pour interrompre Laura et Ivan pendant leur... détente. Moi non plus j’avais pas envie de voir ça. Je les préviens et leur donne dix minutes, avant d’enfin m’écrouler dans mon lit.

Je peux faire confiance aux autres pour calfeutrer la maison, en attendant je vais dormir un peu. Je suis crevé, et mieux vaut que je prenne dix minutes maintenant plutôt qu’en pleine nuit, vu ce qui nous attend. Oh, arrête de brailler, j’essaie de me reposer. C’est pas la première fois qu’on a de la brume, tout se passera bien. Si tu me laisses tranquille.

***

J’ai pas dormi autant que je l’aurais voulu. On se demande à cause de qui, hein ? C’est ça, excuse toi. Tous les autres sont réunis en bas. Huit, neuf... et dix. On est tous là. Les fenêtres sont entièrement barrées, la pièce est plongée dans le noir à l’exception de l’ampoule vacillante au plafond. Ils m’attendaient avant de se mettre en place, c’est trop mignon. 

L’organisation ne va pas tant changer que ça. Ethan, Emily et moi en bas, Jericho, Nolan et Rosy au premier, et le reste en haut. Ivan aurait dû aller à la place de Nolan, mais je n’ai pas le cœur à le séparer de sa chérie. Je sais, j’ai un cœur d’or. Je récupère ma machette, on s’équipe d’un épieu et d’un flingue, et c’est parti. 

On se dit toujours que l’attente c’est la pire partie. Alors qu’en fait, c’est la meilleure. Ou la moins pire. Il y a juste une ambiance pesante, où tout le monde se tait, mais pas encore de danger. Je vois bien qu’Ethan a envie de faire une vanne, pour détendre l’atmosphère. Moi aussi en vérité. Mais on reste silencieux, attentifs à ce qui se passe dehors. 

Ça commence. Cette fois, c’est avec un bourdonnement sourd, un peu comme si des milliers d’insectes recouvraient la maison. C’était peut être le cas, on ne pouvait être sûrs de rien. On se regarde, avec les deux autres. Je crois qu’ils pensent la même chose que moi. Le premier bruit, c’est toujours celui qui donne le ton. Là, c’est des insectes, ça devrait pas être trop pénible, tant qu’ils ne parvenaient pas à rentrer. Je frissonne à l’idée d’une brèche dans les murs.

Ce qu’on entend après, ça donnerait des cauchemars au gars le plus endurci du monde d’avant. C’est même pas descriptible, et tu peux t’estimer chanceux de ne pas avoir à subir ça. Rien qu’imaginer qu’il existe des créatures, là, dehors, capables de produire ces sons me glace jusqu’aux os. Le pire, c’est les appels au secours. Il y a un truc, là dehors, qui imite parfaitement un humain en détresse. C’est souvent une voix d’enfant en pleurs, garçon ou fille, qui appelle à l’aide. C’est la quatrième fois qu’on l’entend, et on a beau savoir que c’est un monstre qui fait ce son, ça ne nous aide pas à dormir. J’ai peur d’un jour condamner quelqu’un en me méfiant trop de lui.

Juste après, un grognement familier nous parvient, très proche. À chaque fois il y en a. Des zombies. Apparus avec les premières brumes, on les avait appelé comme ça, faute d’un meilleur terme pour les décrire. Ils sont grossièrement humanoïdes, stupides et lents, avec un appétit pour la chair humaine et un instinct surnaturel pour nous débusquer. C’est pour eux, les pieux. Ils vont se balader autour de la maison toute la nuit, et s’ils sont un peu trop entreprenant, on les empale à travers le mur. Après, on est obligé de laisser la lance improvisée en place, sinon la brume rentrerait quand on la retire.

J’ai l’impression qu’un voile blanc passe devant mes yeux. C’est toi, t’es devenu un fantôme ? Nan, tu me ferais jamais ça. Tu es vachement silencieux depuis quelques temps, d’ailleurs. Mais attend, j’ai pas rêvé. La maison était pas assez étanche ? On avait pourtant bouché tous les trous ! Peut-être un zombie qui a fait une percée ? Tant pis, on remonte, et demain on fera une vérification complète des murs.Ethan et Emily sont déjà dans l’escalier, mais qu’est-ce qu’ils foutent ? Pourquoi ils bougent pas, on a pas le temps de se faire envelopper par la brume ! 

Quand j’arrive à leur niveau je comprends. Il n’y a pas de brèche en bas. Mais la façade du premier étage a été défoncée par une patte monstrueuse. Les autres n’ont même pas eu le temps de crier.

Nous si.