---
bg: "le-sentier-des-lanternes"
layout: post
title: Le Sentier des Lanternes
summary: « Suis les lanternes, Tetsuo. Elles te guideront toujours à ta destination, tu peux leur faire confiance.» 
source: “Lake Of Fire” par Ira Helena Floros
ref: https://www.artstation.com/artwork/qQEn0L
date: 2023-11-17
categories: posts
tags: []
type: Conte
author: Rumengol
---
Lorsque j'étais petit, je me perdais souvent dans le bois derrière notre maison. Il n'était pas grand, constellé de petites marres qu'on pouvait traverser à l'aide de rochers astucieusement plantés dans l'eau. Pourtant, je m'égarais toujours dans la myriade de chemins et d'embranchements qui se séparaient, s'entremêlaient mais ne se rejoignaient jamais. Ce n'était pas l'œuvre de la nature, mais de forces qui s'en affranchissaient.

Ma mère, elle, ne s'égarait jamais. C'était toujours elle qui retrouvait ma petite personne effrayée et me ramenait à la maison. Elle se mouvait avec grâce au milieu des arbres torturés qui s'écartaient pour libérer le passage. Là où j'avais tourné et marché des heures sans apercevoir de lumière percer entre les arbres, elle me guidait vers l'orée en quelques minutes. 

Bien sûr, elle me grondait dans la foulée. Je ne devais pas jouer dans le bois, c'était son jardin à elle. Et je voulais lui obéir, je souhaitais réellement être un bon garçon. Mais quelque chose m'attirait entre ces arbres, autre que la curiosité sans borne des enfants. Elle en avait conscience. Pour les hommes, elle était la propriétaire de cette terre, mais elles appartenaient en réalité à une chose qui ne se souciait pas des lois des hommes.

Ma mère fut elle aussi attirée par ce lieu, des années avant ma naissance. Elle s'y installa et passa le plus clair de son temps dans les profondeurs de la petite forêt, aidant les villages avoisinants en échange de nourriture et de vêtements. Lorsque je fus en âge, elle m'autorisa l'accès au bois. Elle me guida jusqu'à son cœur, empruntant avec confiance les fourches qui m'avaient plus d'une fois égaré. J'étais un étranger dans cet endroit isolé du soleil, l'hostilité des arbres se ressentait tout autour de moi. Les buissons grondaient sur mon passage, les rochers sifflaient un air sinistre.

Mais pour elle, ils s'inclinaient respectueusement, comme les servants face à la maîtresse de maison. C'était peut-être entre quatre murs de bois qu'elle dormait, mais sa véritable vie était dans cette forêt. Je m'étais senti jaloux alors, envieux de ces plantes qui recevaient plus d'attention que son propre fils, et qui étaient capable d'en retourner plus que je ne le faisais. Elle dût presque me traîner tant tout mon être m'indiquait que je n'étais pas celui qui devait être avec elle.

Elle finit par me pousser jusqu'au cœur de la forêt, à un endroit qui ne pouvait pas exister. Le lac était  bien plus grand que le bois, et pourtant nous n'étions pas sorti du couvert des arbres, qui le bordaient de toutes parts. Elle m'avait simplement souri en m'entraînant sur les pierres glissantes qui serpentaient à la surface pour rejoindre l'îlot central, sur lequel trônait un arbre gigantesque qui surplombait tous les autres.

Là, nous avions attendu la nuit en contemplant les poissons qui évoluaient avec majesté dans l'eau peu profonde. Elle me racontait des histoires qui n'ont pas perduré dans ma mémoire, mais qui sont parvenues à me faire oublier mon anxiété. Le ciel était d'un bleu clair toute la journée. Peut-être bien qu'il a plu à un moment. Si les détails sont flous, cette journée m'a laissée un profond sentiment de bien-être, le dernier avant longtemps.

Le soir venu, je pus observer avec elle un festival de lanternes. Elle avaient surgi de nulle part, des centaines de lanternes rougeoyantes flottant à quelques mètres au-dessus de la surface. Elles ne ressemblaient pas aux lanternes de papier dans lesquelles brûlait une bougie et qui s'élevaient dans les airs à l'aide de la chaleur dégagée par la flamme. Non, on les aurait dit en verre, avec une armature de métal ou de pierre. Et la source lumineuse n'avait rien d'une bougie, plutôt elle s'agitait à la façon d'une luciole prise au piège dans un bocal et tentant désespérément de s'en échapper. Seulement, elles émettaient beaucoup trop de lumière pour être le fruit d'une simple luciole, même de centaines.

Les lanternes se déplaçaient paisiblement au gré du vent qui balayait paresseusement la surface du lac. Lorsque l'une d'entre elle flotta dans ma direction, je voulu l'attraper mais elle échappa à mon bras en l'esquivant comme si elle était douée d'une volonté propre. Ma mère avait ri, du rire compatissant de celle qui voit sa progéniture refaire les mêmes expériences qu'elle la première fois. Elle m'observa moi, son fils tentant d'attraper des sphères de lumières trop élusives pour mon corps pataud.

« Suis les lanternes, Tetsuo. Elles te guideront toujours à ta destination, tu peux leur faire confiance.»

Sur le chemin du retour, elles étaient encore là, mais contrairement à celles du lac, celles-ci ne se répétaient pas à perte de vue. Elles éclairaient un seul chemin parmi les dizaines qui s'offraient à moi, de sorte que je savais sans même y réfléchir quelle direction prendre. Ma mère disait vrai, les lanternes m'indiquaient la voie à suivre.

Depuis ce jour, j'ai toujours pu voir les lanternes. Elles étaient là, autour de moi, même quand je n'en avait pas conscience. Je ne me suis plus jamais égaré dans la forêt qui m'accueillait à bras ouverts. Et même lorsqu'il me fallait retrouver mon chemin dans la campagne lors d'une nuit sans lune, je ne déviait jamais du sentier que les lanternes traçaient pour moi. Ma mère les voyait aussi, c'était de cette manière qu'elle était capable de me sauver chaque fois que les chemins labyrinthiques du bois m'enfermaient.

Quelques semaines après ce fameux soir, des soldats frappèrent à notre porte. Ils accusèrent ma mère d'être une sorcière usant de ses maléfices pour invoquer la maladie et la famine sur les villages environnant. Elle protesta vigoureusement, mais elle n'était nulle sorcière et ne put se défaire de leur emprise. Ils nous enfermèrent tous deux dans les geôles du château, en attendant notre jugement. Pour la seule fois de ma vie, les lanternes traçaient un chemin qui nous était inaccessible, éloigné de quelques centimètres d'acier qui auraient tout aussi bien pu être un continent entier.

Le seigneur de cette forteresse, appris-je plus tard, était mon père. Pour lui, je n'étais sans doute que le fruit d'une aventure de passage. Néanmoins, il me prit en pitié et m'épargna le sort réservé aux pratiquants des arts obscurs. Il n'intercéda qu'en ma faveur, laissant ma pauvre mère à son sort. Elle fut exécutée à l'abri des regards, pour préserver la dignité du châtelain. Je reste persuadé qu'elle est parvenue à s'enfuir cette nuit-là. Elle était bien plus familière des lanternes que moi, cela ne pouvait être son seul secret. Elle ne retourna jamais pour me chercher, estimant sans doute que j'étais plus en sécurité sous l'égide de mon géniteur qu'en compagnie d'une fugitive.

Je devins donc un guerrier, m'endurcissant au fil de nombreuses années de pratique. En raison de ma basse extraction, il m'était impossible d'espérer acquérir un titre plus prestigieux, mais j'étais décidé à faire mes preuves en exploitant l'avantage que m'offraient les lanternes. Je quittais les baraquements officier, dirigeant l'escouade d'éclaireurs. Mes instructeurs ne furent jamais en mesure d'expliquer comment j'étais capable de ne jamais m'égarer, peu importe à quel point l'environnement ne m'était pas familier.

Je continuais de suivre les lanternes, qui ne m'avaient jamais laissé tomber. Et si j'avais abandonné l'idée d'éclaircir le mystère de leur existence, je les considérais comme des anges bienveillants, un vestige de ma mère qui continuais d'éclairer mon chemin sans faillir. D'une certaine manière, elle était toujours avec moi.

Jusqu'à aujourd'hui. Mon escouade était chargée de tracer un chemin au travers d'une forêt jusqu'au repaire d'une conspiration de sorcières, de véritables jeteuses de sorts. Leurs plans de corruption de la terre avaient été mis en déroute, et elles s'étaient repliqées dans les profondeurs d'une grande forêt, espérant y être en sécurité. Cela aurait pu marcher si ce n'était pour moi. En règle générale, les armées régulières évitaient de s'engager dans des bois de sorcière sans une solide assurance. Ils étaient des endroits terrifiants, où plus d'un bataillon avait perdu la vie après leur chemin.

Cependant, il n'y avait pas de meilleur éclaireur que moi dans tout le pays. Si mes principaux rivaux étaient plus habiles de leur lame ou de plus fins limiers, ils avaient tous connu l'échec un jour ou l'autre. Tant que je suivais le sentier des lanternes, jusqu'à l'existence de ce terme m'était inconnue. Nous progressions dans un sous-bois constellé de mares sur lesquelles étaient disposés des pierres pour permettre de les traverser. Une aura mauvaise s'échappait des arbres qui faisaient appel à une variété d'astuces plus retorses les unes que les autres pour nous éloigner de la piste.

Mais je prenais invariablement la bonne décision, accrochant une lanterne rouge, de papier cette fois, aux branches les plus solides pour signaler au gros des troupes quelle voie emprunter. J'en avais fait le symbole de mon escouade, sans expliquer à personne pourquoi. Les lanternes étaient mon secret, un que je comptais emporter dans la tombe, car je soupçonnais que c'était ça qui avait causé l'accusation et la chute de ma mère.

Avec le temps, je développais un certain instincts, ce que d'autres appelaient l'intuition du chasseur. Je savais lorsque ma proie était proche et que la traque arrivait à son terme. Si la cabale n'était pas juste de l'autre côté de cette mare, alors elle se cachait à peine quelques mètres plus loin. Je m'engageais avec confiance sur les pierres glissantes éclairées par la lumière pourpre des lanternes.

C'est à ce moment là que l'une des lanternes tomba au sol. Elle se brisa dans un fracas de terre cuite qui s'éclate. Une vapeur rougeâtre s'échappait de la coquille brisée, masquant l'intérieur à mon regard un instant. Puis je le distinguais clairement. C'était un crâne humain, quoique sinistrement petit, qui pulsait d'une lumière faiblissante avant de s'éteindre pour de bon.

Devant moi, d'autres lanternes perdaient de l'altitude pour s'écraser au sol ou plonger sous la surface, illuminant un temps l'eau calme avant qu'elle ne reprenne sa couleur d'encre. La lumière qui guidait mes pas depuis tant d'années se mourrait, s'effritait sous mes yeux sans que je ne puisse faire quoi que ce soit. Désemparé, je tentais de récupérer les lanternes mourantes qui ne faisaient plus le moindre effort pour m'éviter. Aux yeux de mes hommes je devais ressembler à un fou, mais ce n'était pas ce qui m'importait à ce moment.

Avant que la dernière des lanternes ne s'éteignit, j'aperçus sur l'autre rive la silhouette d'une femme encapuchonnée qui me faisait face. L'ultime lanterne éclaira subrepticement le bas de son visage, qui souriait à pleines dents.