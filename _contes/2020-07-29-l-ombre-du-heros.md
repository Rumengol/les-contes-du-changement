---
bg: "l-ombre-du-heros"
layout: post
title: L'ombre du héros
summary: Le traumatisme d'un groupe d'enfants dans un village reculé dans les montagnes.
source: “20170505”, par Kou Takano
ref: https://www.artstation.com/artwork/YQO56
date: 2020-02-05
categories: posts
tags: ["Le monde d'Alkénor"]
type: Conte
author: Rumengol
lang: fr
---

Je jouais souvent autour de cette statue lorsque j'étais enfant. Nous étions cinq casse-cous qui aimaient escalader ce géant de pierre et de mousse, érigé à la gloire d'un héros anonyme de jadis. Enkei était la plus téméraire de tous. Lorsque j'arrivais au sommet du crâne blanchi par le soleil, bien avant les autres, elle était déjà perchée là où elle seule osait grimper : la corne encore intacte du casque. Et elle rigolait. Ses taches de rousseur semblaient sautiller de joie sur son visage à chaque sourire, et sa chevelure de flamme étincelait, baignée dans les rayons du soleil.

Elle aimait se moquer de nous, faisant mine de perdre l'équilibre pour le plaisir de voir nos mines effarées. Lorsque le vent se levait en hauteur, elle se laissait glisser avec grâce pour nous rejoindre et enfin partager ensemble le pain que sa mère lui cuisait. Il était mœlleux, jamais trop cuit et toujours savoureux. Parfois, l'un ou l'autre dégottait quelques légumes ou même un peu de viande, et nous faisions alors un véritable festin.

J'étais amoureux d'elle, je crois. Yllan et Pierre aussi, mais jamais nous ne l'aurions avoué. L'orgueil juvénile, quelle idiotie. Oui, nous étions idiots alors. Insouciants aussi. Les affaires du monde ne nous concernaient pas du haut de notre forteresse de pierre perdue dans les montagnes.

Notre village était petit et isolé, nous en étions les seuls enfants, et en dehors d'Astrîd, plus jeune de quelques années, nous avions sensiblement le même âge. Nous grandissions ensemble, à la poursuite de buts différents. Mais Enkei était toujours là, avec sa bonne humeur et son charme, pour nous rassembler. Lorsque Yllan et Astrîd se disputaient âprement, c'était elle qui les réconciliait. Elle était la clef de voûte de notre église d'amitié, admirée autant qu'aimée.

Jusqu'à ce sombre jour. Des pillards en maraude s'étaient aventurés dans les montagnes, à la recherche de villages perdus comme le nôtre. Ils étaient des déserteurs, bien équipés et hors-la-loi. Le vieux Benham leur avait offert des vivres ainsi que tout l'or qu'il avait pu obtenir dans l'espoir que cela leur suffise et qu'ils partent sans causer de tort. Mais ils n'étaient venus que pour s'amuser à leur cruelle manière. En retirant son épée du corps de Benham, leur chef avait lâché ses hommes.

Les maraudeurs massacraient sans peine ni merci les quelques hommes qui tentaient de résister. Au milieu du brasier allumé par les gredins et des cris des femmes, c'est encore une fois Enkei qui nous avait sorti de notre torpeur. Sa voix couvrait à peine la cacophonie du carnage quand elle me criait de fuir, de la suivre. La suite s'était déroulée comme dans un rêve. Nous avions couru pour sortir du village. Personne ne se souciait de nous, et nous ne faisions attention à personne.

Je compris assez tardivement où Enkei souhaitait nous guider : un ancien terrier de gredille, que nous avions découvert quelques années auparavant. L'animal était parti depuis longtemps, et la cavité était suffisamment grande pour nous servir de refuge dissimulé derrière des fourrés. Yllan s'était réveillé le premier, et il portait Astrîd qui s'était tordue la cheville dans la panique. Pierre recouvra ses esprits un peu avant moi. Nous avions aidé Astrîd puis Yllan à pénétrer l'abri avant de les y rejoindre.

Je ne sais pas trop ce qui nous avait trahi. Sans doute est-ce le hasard qui a fait que l'un des pillards la remarque. Trois hommes ont commencé à courir vers elle, le regard mauvais. Ils ne pouvaient pas nous voir, alors elle avait remis le buisson en place et s'était enfuie. Mais c'étaient des adultes, et en meilleure forme physique. Ils l'avaient rattrapée en quelques foulées et plaquée au sol.

Nous étions là, à quelques mètres à peine, témoins impuissants de la scène cruelle qui se jouait devant nous. Pétrifiés par la peur. Elle pleurait et elle criait et elle les suppliait d'arrêter. Ils l'ont rouée de coups, maculant la terre de son sang. Nous étions tous muets, incapables de détourner le regard. Yllan masque les yeux d'Astrîd. Lorsque j'ai tendu la main vers elle, j'ignore encore si j'allais me lever pour la défendre. Quelqu'un a attrapé mon bras. C'était peut-être moi. Et je n'ai rien fait. Et alors qu'elle hurlait de plus belle, je maudissais mon inaction. Deux autres salauds s'étaient joints à la séance de torture, et ils prenaient tous un plaisir sadique à faire souffrir notre Enkei. Les larmes qui ravageaient son magnifique visage les faisaient rire. Et ils la frappaient, sans s'arrêter, même lorsqu'elle sombrait dans l'inconscience. Cela dura peut-être des heures. Lorsque l'un d'eux se lassait, il lui donnait un ultime coup avant de partir et d'être remplacé par un autre. Cela paraissait n'en plus finir. Finalement, après une après-midi de torture et de souffrance, ils l'abandonnèrent à moitié morte.

Enkei n'a plus jamais été la même après ce jour. Nous l'avions soignée physiquement, mais ses plaies psychologiques refusaient de guérir. Nous étions les seuls survivants de notre village. Toutes nos familles avaient été massacrées. Nous aurions pu nous effondrer, nous laisser mourir de peine, mais c'est toujours Enkei, à travers son épreuve, qui nous maintint unis. Elle avait besoin de nous, elle s'était sacrifiée pour nous - ou à cause de nous. Nous devions nous montrer forts pour elle, l'aider comme nous aurions dû le faire.

Nous avions quitté les ruines carbonisées de notre village pour nous réfugier dans l'ombre du héros. Enkei ne parlait plus. Elle s'était mordue la langue si profondément que sa blessure ne semblait pas se résorber. De temps à autre, sa bouche se remplissait de sang au point de l'étouffer si elle ne vomissait pas. Elle avait plusieurs dents cassées, certaines arrachées. Ses cheveux, partiellement arrachés, ne brillaient plus, ses yeux avaient perdu leur éclat. Elle n'était plus que l'ombre d'elle-même, prostrée et immobile contre la pierre. Elle n'acceptait plus que la compagnie d'Astrîd et nous regardait avec peur et méfiance. C'était blessant, mais nous comprenions, et aucun d'entre nous n'avait cœur à la bousculer.

Nous étions restés là longtemps, formant un semblant de famille. Enkei se remettait lentement. Elle tolérait de plus en plus notre présence. Je ne sais pas si elle nous avait pardonné de ne pas l'avoir secouru. Peut-être savait-elle que nous n'étions pas de taille face aux maraudeurs, et ne nous en voulait pas. Mais aucun de nous n'a pu se pardonner. Nous ne pouvions excuser notre faiblesse, notre incapacité à agir. Des disputes éclataient fréquemment. Nous étions en colère contre nous-même autant que contre les autres. Enkei n'avait pas besoin de nous, elle n'avait besoin que d'Astrîd. Et si elle n'était pas là pour nous et que nous ne pouvions rien faire pour elle...

L'un des oncles d'Astrîd était parti vivre à la ville, il y a quelques années. Nous avions pris la décision d'y emmener les deux filles. Puis Pierre, Yllan et moi nous sommes séparés. Nous nous étions fait le serment de se retrouver tous les cinq dans l'ombre du héros, une fois que nous serions devenus assez forts pour protéger notre famille. Il m'a fallu trente ans pour revenir sur ces terres. J'ignore ce qu'il est advenu des autres. Yllan voulait devenir chevalier, je crois. Sont-ils tous revenus ici avant moi ? Ai-je été le plus lent à accomplir mon serment ? Alors que je descends prudemment la pente, le cœur battant, seule l'image d'une Enkei rayonnante emplit mon esprit. J'espère la revoir, et cette fois, je saurais la protéger.
