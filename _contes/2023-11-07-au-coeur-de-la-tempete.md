---
bg: "coeur-de-tempete"
layout: post
title: Au cœur de la tempete
summary: Une mission désespérée mène une troupe de soldats à traverser un blizzard sans merci pour atteindre un vaisseau planté dans la neige.
source: “Sketch” par Young Qin
ref: https://www.artstation.com/artwork/rJdm3E
date: 2023-11-07
categories: posts
tags: ["Les 5 ciels"]
type: Conte
author: Rumengol
---
«Tout le monde est prêt ? Je vous rappelle que c'est pas une mission d'exploration. On rentre, on place l'émetteur, on sort. On est pas là pour enfiler des perles, compris McKlay ?

— Aye aye, captain. In'n out, cinq minutes max sur site. C'est rentré.

— Bien. On fait pas dans la finesse, Si on voit un hostile, on le refroidit, et on passe à la suite.»

Le lieutenant Cédric Roy se pencha vers sa voisine, la pilote Hecker Vort.

«M'est avis que si on croise qui que ce soit là dehors, on pourra pas plus le refroidir qu'il ne l'est déjà.

— Une remarque, Roy ?

— Aucune, capitaine. Tout est parfaitement clair, capitaine.

— J'aime mieux ça. Allez, carrez vos culs dans le pingouin et attachez-vous bien. Si j'en vois un qui retire son harnais avant qu'on soit arrivé je le balance moi-même pas dessus bord, c'est clair ?

— Clair !»

Le colonel Théodore Andrius fit un signe à sa pilote qui hocha la tête et alla s'installer aux commandes de l'appareil volant. Le refuge avait perdu la quasi-intégralité de sa flotte de défense en combattant les éléments, et ne possédait désormais plus que deux pingouins, des transporteurs civils réaffectés. Paradoxalement, ils ne manquaient pas de pilotes. Dépossédés de leurs montures, les cavaliers des cieux avaient dû se trouver une nouvelle occupation pour se rendre utiles à la survie du camp. Tous, sauf les trois meilleurs, dont Hecker Vort. La femme aux cheveux noirs de jai avait beau dépasser les 35 ans, elle ne s'était pas laissé faire par les plus jeunes et avait épaté tout l'état major avec ses réflexes et son instinct qui dépassaient de loin ceux des jeunes coqs qui se vantaient d'être l'élite de la flotte Tarballienne.

Il s'éloigna de la carlingue et se saisit d'un datapad pour revoir une dernière fois le plan de vol. C'était inutile, il avait tant étudié cette trajectoire qu'elle était imprimée sur sa rétine. Néanmoins, il ne voulait rien laisser au hasard. Cette mission, comme toutes les missions qu'il avait mené à bien depuis quelques années, était cruciale pour la survie du refuge. C'est pour cela qu'il s'efforçait de maintenir une discipline stricte dans sa troupe malgré les airs de fin du monde, et qu'il examinait les plans dix fois jusqu'au dernier moment pour être certain de n'oublier aucun détail. Au bout de quelques minutes, il reposa la plaque alors que les deux moteurs du pingouin se mettaient à crachoter, puis ronronner.

«Vous prenez le *Happy Feet*, finalement ? Je croyais que tu détestais les changements de dernière minute.»

La voix traînante qui venait de derrière les caisses était celle de Jenna Stillwell, la responsable logistique du refuge, et sa femme. Elle n'avait pas pu dire au revoir à son mari ce matin, occupée toute la nuit par une fuite dans un réacteur de propanol qui menaçait d'attaquer les murs extérieurs. Malgré la nuit blanche qu'elle venait d'encaisser, elle s'était tout de même traînée jusqu'au hangar pour l'embrasser, peut-être pour la dernière fois. Chaque mission pouvait être sa dernière, ils le savaient tous les deux.

«C'est toujours le cas, mais l'*Empereur* est définitivement manchot. Une aile est tombée dans la nuit pendant la maintenance, et ils ne peuvent pas la réparer rapidement. Foutus techos...

— Allons, c'est toujours mieux qu'elle tombe ici qu'en plein vol, n'est-ce pas ?»

Le colonel grommela, mais finit par acquiescer. Jenna était la seule personne capable de le faire plier, il ne parvenait jamais à lui donner tort. Il la serra fort dans ses bras et l'embrassa, avant de se mettre en marche vers son transporteur. Il sauta dans le cockpit sur le siège du copilote. Avant d'être officier de commandement, il avait lui aussi été pilote. Et s'il n'était pas aussi talentueux que Vort, il pouvait l'assister décemment. Lorsqu'il se retourna pour jeter un oeil dans la baie de lancement, la grande salle était vide. Il inspira un grand coup, et interrogea du regard sa pilote, qui hocha la tête.

«*Happy Feet* à tour de contrôle pour l'opération Canne à pêche. Tout est prêt de notre côté, vous pouvez ouvrir la porte.

— Bien reçu *Happy Feet*. Ouverture de la porte. Ça va secouer.»

Aussitôt que la porte s'entrouvrit, la températeur dans le hangar chuta de plusieurs dizaines de degré. Il n'était pas chauffé en temps normal et était surnommé "la chambre froide", et pourtant le vent glacé qui s'engouffra par l'ouverture gela instantanément la moindre goutte d'eau qui était suspendue en l'air, faisant pleuvoir une pluie de micro goutelettes de glace. L'appel d'air qui s'ensuivit provoqua une bourrasque qui fit méchamment grincer les attaches qui retenaient l'appareil et envoya voler les chariots et caisses qui n'étaient pas solidement fixés au sol. Un concert de craquements résonna à l'arrière du hangar, bien vite muselé par les rugissements du vent qui investissait chaque recoin exposé.

Les courants d'air balayèrent l'intérieur avec brutalité, malmenant sans merci l'engin volant décolla brièvement du sol à plusieurs occasions. Il fallut attendre une minute entière pour que les courants s'équilibrent et que le vent se calme suffisamment pour ne plus menacer de renverser le pingouin s'il cessait d'être maintenu au sol. C'était toujours le passage le plus pénible, qui pouvait faire échouer une mission avant même qu'elle ne débute. Il suffisait d'une avarie, même mineure, sur l'un des propulseurs ou un trou minuscule dans la coque pour faire abandonner des semaines de préparatifs.

Le colonel observa les voyants sur son tableau de bord. Ils étaient tous au vert, et les jauges suffisamment pleines. Pas d'avarie visibles sur les voyants les plus basiques. Vort confirma d'un pouce vers le haut que tout était bon concernant l'intégrité de l'appareil. Il n'avait subi aucun dégât. Il frappa trois coups secs à l'arrière du cockpit, vers la cabine où ses soldats étaient supposéments attachés. Deux coups rapides lui répondirent. Bien, il n'y avait pas eu de blessé derrière non plus. La mission débutait bien, voilà qui était bon signe.

«*Happy Feet* à tour de contrôle. Pas de dégâts, on peut y aller.

— Bien reçu *Happy Feet*. Largage des amarres. Bon courage là-dehors.»

Les attaches qui avaient empếché l'engin volant de s'écraser contre les murs et les plafonds se détachèrent avec un **clac** sonore, et une quinzaine de câbles se rétracta dans les petites ouvertures qui parsemaient le sol. Vort enclencha la propulsion, mais deux voyants rouges s'allumèrent à l'arrière de l'image qui représentait le pingouin. Deux des ancres ne s'étaient pas détachées, et retenaient à présent la carlingue au sol. Le joint qui les connectaient à la queue de l'appareil devait avoir gelé, les empêchant de se rétracter correctement. Un bras mécanique sortit du mur le plus proche muni d'un chalumeau et approcha la flamme des points d'attache. Elle chauffa doucement le câble sans toucher à la coque, et après quelques minutes les câbles se détachèrent dans un son de glace brisée. Le pingouin était libre.

Hecker Vort lança les moteurs à pleine puissance. Ils n'avaient que la longueur du hanger pour s'arracher à l'attraction gravitationnelle. S'ils ne volaient pas au bout des quelques secondes qui les séparait de la sortie, ce serait un a-pic de plusieurs dizaines de mètres dans une tempête de neige. A l'issue d'angoissante secondes qui parurent être des heures, le colonel se sentit plus léger et le pingouin se souleva légèrement du sol, avant de plonger droit sur le mur blanc dans lequel la tempête faisait rage. C'était mieux ainsi. Les missions qui débutaient sans à-coups étaient de mauvaise augure. Ce n'était pas que superstition de sa part : quelque chose foirait toujours. Alors mieux valait que ce soit au décollage plutôt qu'en plein territoire hostile.

Le voyage fut mouvementé. Le colonel avait les yeux fixés sur la dizaine d'instruments de navigations qui lui permettait d'estimer leur cap, la navigation à l'oeil étant impossible. Le blizzard secouait l'appareil et réduisait leur visibilité à quelques mètres devant eux. Même les radars les plus avancés étaient perturbés et incapables de distinguer le relief devant eux. Si Vort se trompait et qu'ils fonçaient dans une montagne, ils disparaîtraient dans une gigantesque boule de feu avant même de réaliser ce qui leur arrivait. Heureusement, elle n'était pas du genre à se laisser impressionner par un peu de neige et des vents charriant des lames de glaces de plusieurs mètres.

Ils ne parlaient pas. En temps normal, c'était le bruit des moteurs poussés à leur maximum qui limitait les capacités de communication. Là, c'était le souffle du vent et la grêle qui martelait la carlingue, les rendant sourds même au travers de leurs casques anti-bruits. Alors, ils communiquaient via la langue des signes militaires, étudiée pour être efficace et transmettre les informations les plus essentielles en quelques mouvements à peine. Le temps se dilatait à l'intérieur de la cabine de pilotage. Si ce n'était pour la poussée qui les enfonçait dans leurs sièges, les deux pilotes pourraient douter de leur progrès. Sans le moindre repère, leur destination paraissait toujours aussi éloignée, même au bout d'une heure et demi de trajet.

Enfin, le signal du radar longue portée se répercuta contre une surface métallique verticale. Aussitôt, Vort inversa lentement la poussée jusqu'à être en vol quasi-stationnaire, se reposant sur les propulseurs ventraux pour maintenir son assiette. Cette méthode coûtait beaucoup d'énergie et risquait de les faire tomber à court de carburant si elle était prolongée, mais c'était aussi le seul moyen de pénétrer dans leur objectif sans s'encastrer dedans.

Le pingouin déboucha dans une cuvette relativement épargnée par la furie des éléments. Le blizzard masquait toujours leur vision, mais les vent se faisaient moins violents et le thermomètre avait même regagné quelques degrés. Ce fut la dernière confirmation dont avait besoin le colonel. Il fit quelques moulinets avec ses épaules ankylosées, puis frappa trois grand coups à l'arrière de la cabine, pour réveiller sa troupe. Il attendit quelques secondes, avant de cogner à nouveau la plaque de métal. Il sentit sa peau craquelée rechigner à travers son épais gant, mais serra le point pour forcer la peau gelée à se détacher de sa main. Il ne tolérait pas les éléments qui sortaient du rang parmi ses hommes, il n'allait pas faire une exception pour son corps. La deuxième fois, un coup mou lui répondit, puis deux autres plus rassurés quelques secondes plus tard. Ils étaient tous réveillés, tout allait bien. Le plus grand risque sur ces longs trajets était qu'un soldat ne s'endorme pour ne plus jamais se réveiller, mort de froid en plein vol.

La gigantesque tour de métal noir se découvrit soudainement au travers du brouillard. Un amoncellement de fer et de câbles encastrés les uns dans les autres qui s'élevait jusqu'au ciel. Le colonel s'avança sur son siège pour tenter de distinguer le sommet de la structure, en vain. Plus ils s'approchaient, plus les détails devenaient visible. Des brèches ouvraient dans la façade des plaies béantes dans lesquelles s'engouffraient un air glacé qui ressortait par les nombreux trous dans un concert de sifflements sinistres. Le vaisseau de guerre qu'ils recherchaient était fiché dans la neige épaisse de plusieurs centaines de mètres, droit comme un piquet ou un gratte-ciel démesuré.

Le *Maréchal Stellaire* était le vaisseau amiral de la flotte d'expédition qui devait prendre possession de cette planète gelée. A peine entré en orbite de Magella III, de titanesques lances de roches et de glaces avaient jailli de la surface, projetées dans l'espace par une force qui défiait toute compréhension. Le Maréchal, de loin le plus massif de toute la flotte, avait été touché par deux d'entre elles, la première transperçant son ventre tandis que la deuxième sectionnait le quart arrière. La flotte n'avait pu que regarder avec effroi le fleuron de leur technologie sombrer au ralenti dans l'épaisse atmosphère.

Les vaisseaux cargos ainsi que treize des seize croiseurs qui les escortaient connurent le même sort, la plupart se désintégrant avant même d'atteindre la planète, leur noyau nucléaire dérégulé provoquant une réaction en chaîne et vaporisant tous leurs occupants. Le reste des vaisseaux avait été forcés de se poser sur cette planète inhospitalière, privés de leur plateforme de réapprovisionnement.

Cela faisait des mois que l'état major reconstitué avait créé le refuge à partir des vestiges de trois transporteurs et s'efforçait de survivre sur leurs réserves, tout en combattant la nature hostile qui cherchait à les enterrer, comme si la roche elle-même était vivante. C'était le temps qu'il avait fallu aux simulations pour déterminer l'emplacement approximatif de leur vaisseau-mère, une attente pour exécuter le plan qui les sauverait tous.

La pilote orienta lentement la descente du pingouin vers ce qui ressemblait à une baie de lancement. Malgré les contours déformés et les rayons d'énergie qui traversaient le bouclier thermique et signalaient son dysfonctionnement, c'était le point d'entrée qu'avait choisi Hecker Vort, et le colonel se fiait à son jugement. En traversant la barrière énergétique, des arcs se dessinèrent dans la paroi intangible. C'était comme si ils pénétraient dans une vitre qu'ils brisaient au ralenti. Les arcs énergétiques faisaient rayonner puis exploser les fragments de glaces qui s'étaient formés dans les interstices du transporteur. C'était déjà ça, songea l'officier.

« Ça fait du bien d'enfin pouvoir s'entendre, déclara la pilote.»

En effet, même en piteux état et malgré les nombreuses brèches dans la coque, le bouclier thermique du vaisseau était encore actif après plusieurs mois. La tempête qui rageait au dehors n'était plus qu'un murmure et la température était remontée de plusieurs degrés, leur permettant même de sortir sans leur combinaison la plus chaude, qui était également la plus encombrante. Vort amena son appareil jusqu'au bout de la baie, en prenant soin d'éviter les câbles qui pendaient du plafond d'où sortaient encore des arcs électriques intermittents. Une fois face au mur, elle fit prudemment demi-tour pour faire face à la sortie. S'ils devaient quitter l'endroit rapidement, autant se faciliter la tâche.

Alors qu'elle entamait la procédure d’atterrissage et de fixation à la rampe de lancement magnétique, le colonel Andrius se contorsionna pour pénétrer dans l'habitacle où l'attendaient ses hommes.

«Nous y sommes. Nous avons pénétré dans la coque du *Maréchal Stellaire* par une baie de lancement, quelque part entre le septième et le quinzième segment. Impossible d'être plus précis, la peinture qui n'a pas été enlevée a été recouverte par la neige. La température extérieure est supportable, vous pouvez laissez là l'équipement de type 4.

McKlay, vous trouvez un coin pour installer votre machine infernale et vous vous mettez au boulot. Sénart et Tils, allez voir ce bouclier thermique et dites moi s'il est encore capable de tenir. S'il donne la moindre indication qu'il va lâcher, arrêtez tout et prévenez le reste, qu'on s'équipe en conséquence. Sinon, regardez si vous pouvez le réparer, il doit être maintenu en vie par le système de secours mais les sources sont salement amochées. Sann, Meloson, vous restez avec la pilote Vort et vous défendez le vaisseau. Les autres, établissez un périmètre de sécurité. D'abord la proximité immédiate, puis la zone définie par McKlay.

Restez sur vos gardes, le Maréchal était très peuplé et on a dû faire un sacré bordel en arrivant. Tirez pour tuer.»

Ses troupes s'étaient débarrassé de leur équipement en surplus pendant son discours, et sortaient du vaisseau au pas de course, fusil à l'épaule. Il intima à la pilote de rester dans le cockpit et de garder le moteur chaud, puis sortit à son tour en empoignant son propre pistolet. Il emboîta le pas de Francis McKlay, qui transportait l'épaisse mallette qui était la clef de cette mission. Celui-ci examinait la baie avec une air contrarié qui paraissait même au travers de sa visière opaque.

«Un problème, McKlay ?

— Ouais, j'ai peur qu'on manque de portée. J'ai étudié les plans de cet engin, colonel, et on peut pas se contenter de le mettre n'importe où. Il faut que ce soit proche d'un moteur, pour la réaction en chaîne. Même un auxiliaire. Si vous m'aviez dit qu'on était pour sûr dans le segment 8, 11 ou 14 j'aurais pu installer ça sous le pingouin, no problemo. Mais là, sans certitudes, je veux pas risquer, faut aller au cœur.

— Pas si on peut l'éviter. Melsh, Dutrovsky ! Essayez de trouver où on est. Un datapad encore en état, un panneau, je m'en fous. En attendant McKlay, pensez au meilleur itinéraire qu'on pourrait prendre. Le plus court, dans des zones dégagées si possible.»

***

Plus loin dans le périmètre de sécurité, Cédric Roy utilisa le système de communication de proximité pour éviter de se faire entendre de tout le monde. Il ne s'adressait à personne en particulier, mais se doutait que ses voisins percevraient sa râlerie.

«Quelle pitié de pas pouvoir explorer un peu. C'était le point central de la flotte, vous imaginez un peu les ressources qu'il y a là dedans ? Peut-être même des vaisseaux en état de marche, c'est trop con. Tout ça, pouf, ça va disparaître, et on peut même pas se servir.

— T'es pas au courant ? lui répondit la voix grave de Serge Cullan. On est la mission du dernier espoir.  Le refuge aura plus assez de jus pour décoller si on attend trop longtemps. Si ça capote, on est bon à devenir des glaçons sur cette planète à la con.

— Vous pensez que l'amiral l'a reçu ? Le signal de détresse, je veux dire, interrompit la voix aiguë de l'adjudant Qweil.

— J'en sais foutrement rien, mais j'espère que oui, sinon tout ce beau spectacle n'aura servi à rien.»

Ils se turent, l'air plus sombre et plus déterminés qu'avant malgré leurs perspectives délétères. Ils surveillaient en particulier les portes des sas et le poste de contrôle dont les vitres avaient été soufflées.

***

Les soldats Melsh et Dutrovsky revinrent avec des mauvaises nouvelles. L'estimation du colonel était bonne, puisqu'ils se trouvaient dans baie 7-11. Septième segment donc, mais plus proche du sixième que du huitième. McKlay secoua la tête. Définitivement trop loin. Et le colonel Andrius avait vu les jauges de carburant du pingouin. Il n'aurait jamais assez d'énergie pour chercher une autre entrée plus haute et rentrer au refuge. S'il leur demandait, il se doutait que ses hommes accepteraient de sacrifier leur vie pour la réussite de la mission. Mais même dans cette situation, il refusait de mener le moindre soldat à une mort certaine si une alternative existait.

McKlay se livrait à l'un de ses exercices de reconstitution mentale des chemins. L'esprit de l'éclaireur était plus efficace qu'un datapad dans ce domaine, alors il le laissa faire. Il ouvrit le canal commun pour s'adresser à toute sa troupe.

«On ne peut pas activer l'émetteur ici, on va devoir remonter jusqu'au huitième segment. McKlay, Melsh, Dutrovsky, Culann, Qweil, Porter, Velenn et Stern, vous venez avec moi. Les autres, vous nous préparez une voie de repli jusqu'au pingouin. Roy, vous prenez le commandement en mon absence.

— Oui mon colonel !»

***

Ils pénétrèrent avec prudence dans le couloir plongé dans l'obscurité. Les systèmes de secours ne s'étaient pas activés partout, et celui qui gérait la lumière du segment 7 brillait par son dysfonctionnement. Melsh, l'autre éclaireur du détachement, ouvrait la marche. Le colonel suivait derrière, suivit de près par McKlay. C'était une sensation étrange de marcher sur les murs en devant esquiver ou sauter par dessus les portes automatiques qui étaient coincées dans l'une ou l'autre position.

Il leur avait intimé de se faire les plus discrets possibles, mais il ne pouvait pas rendre silencieux une colonne de soldats lourdement armés. De plus, les faisceaux lumineux de leurs armes balayaient le couloir, fouillant le moindre recoin à la recherche d'un mouvement qui trahirait une présence autre que la leur.

Ils finirent par déboucher sur un à-pic. Melsh les arrêta avant qu'ils ne se jettent dans le précipice. La double porte s'ouvrait sur le hangar d’entrepôt des exosquelettes et chasseurs. C'était un très long hangar, qui s'étendait sur l'intégralité de chaque segment et servait à distribuer efficace les appareils dans les baies de lancement adéquates. Ce qui facilitait autrefois la logistique du vaisseau de guerre devenait subitement un obstacle de taille. Ce n'était pas un grand couloir qu'ils devaient emprunter cette fois-ci, mais un mur qui s'étendait jusqu'à perte de vue dans les deux directions et qu'il leur fallait escalader.

Melsh balaya les ténèbres sous ses pieds avec sa lampe, essayant de repérer le reflet sur les carcasses métalliques des chasseurs qui avaient invariablement glissé jusqu'à s'écraser contre la lourde paroi de métal qui séparait les segments. Il se demanda si les parois avaient cédé, projetant des milliers de tonnes de métal à l'avant du vaisseau, ne laissant aucune chance à l'équipage qui se trouvait dans le poste de commandement, dans la tête de la bête. Si c'était le cas, leur mort atroce leur donnerait au moins une porte d'entrée facile à emprunter pour atteindre le segment 8.

Le colonel sortit une corde de la ceinture de sa combinaison.

« On s'accroche, décréta-t-il. Chaîne de confiance.»

Culann et McKlay déglutirent bruyamment. Leurs combinaisons grand froid possédaient un matériel d'escalade inné, et deux façons de s'encorder : soit de façon conventionnelle, un long câble reliant toute la cordée d'un seul tenant, soit en utilisant la ligne de confiance. Des câbles individuels qui se connectaient les uns aux autres au milieu. La connexion magnétique était presque impossible à briser, supposément plus solide que les câbles eux-mêmes. En revanche, un simple tour de mousquet suffisait à inverser la polarité de l'aimant et repousser brutalement l'autre. S'attacher en chaîne de confiance, c'était remettre sa vie entre les mains de celui qui vous retenait.

Et pourtant, le colonel avait ses raisons qu'ils comprenaient tous. L'escalade serait difficile, et la moindre erreur pouvait provoquer une réaction en chaîne fatale. Il y avait sous eux des kilomètres de vide et une fosse de pieux d'acier au fond. En cas de chute, mieux valait un que tous. Néanmoins, il n'était pas le type d'homme à réclamer la confiance de ses hommes sans donner la sienne en retour. Il recula pour être dernier de cordée, faisant taire les contestations d'un regard noir. Ils activèrent le magnétisme de leur équipement et entamèrent la délicate ascension.

Le vaisseau était planté dans le sol à une verticale quasi parfaite. Le sol si lustré qu'il faisait déraper les nouvelles recrues était devenu une paroi glacée qui n'offrait pas la moindre prise. La progression était horriblement lente, chaque mètre dépassé était une victoire. La concentration de toute la troupe se portait vers la gestion subtile du magnétisme. Chaque étape consistait à détacher un membre de la paroi en réduisant la force de l'aimant. Trop brutal, et le grimpeur serait propulsé dans le vide. Trop faible, et il restait collé, perdant une précieuse énergie.

***

Le constat des deux ingénieurs était équivoque, et pas très réjouissant. Prenant son rôle à cœur, Roy s'était rapproché d'eux et leur avait réclamé un rapport sur la situation.

« On va pas se mentir, c'est déjà un miracle qu'il ait tenu aussi longtemps, commença Sénart. Le système auxiliaire doit pas utiliser du nuc', et il a fallu bien entretenir la centrale pour qu'elle ne cède pas. 

— Le truc, reprit Tils, c'est qu'il a été salement fragilisé par le crash, et il est en mode étendu, ce qui veut dire qu'il forme une couche protectrice sur tout le vaisseau. C'est pas fait pour, et ça peut péter à la moindre interférence. Et une interférence, on en a causé une belle.»

Il hocha la tête en direction du pingouin, où Hecker Vort manchonnait un peu de tabac pour faire passer le temps.

« J'ai fait spé ingé des boucliers, à l'école militaire. On s'amusait à baisser la tension de petits générateurs avant de balancer des trucs dedans. Les interférences se propagent un peu partout à la surface, et grossissent à chaque collision, jusqu'à ce que la barrière perde son intégrité et éclate. La décharge cramait tout à l'intérieur, ça nous faisait marrer de balancer des insectes dedans qui se faisaient carboniser. Aujourd'hui, un peu moins.

— T'es en train de me dire qu'on va finir comme tes foutus insectes ?

— On a du temps, tempéra Sénart. Ce bouclier là est vachement grand, les interférences mettent du temps à voyager. On a encore quelques heures devant nous. Cinq grand max.

— Et pour ce qui est de le réparer ?

— C'est mort. Sauf si t'as de quoi remettre en marche le moteur principal du vaisseau, qui a assez de puissant pour override les interférences. On peut tenter des trucs pour stabiliser, mais je promets rien.

— Alors faites ça. Au point où on en est, je prends tout. Je vais dire aux autres de se grouiller.»

***

Ils faisaient une pause en se reposant sur des barrières métalliques. Avec leurs repères chamboulés, aucun d'entre eux n'était capable de dire quel était leur utilité, mais ces plateformes de fortunes s'étaient révélées providentielles. Ils pouvaient s'asseoir pour reposer leurs membres au bord de la tétanie, chacun étant capable de supporter le poids de trois d'entre eux. 

Melsh s'était détachée de la cordée pour reconnaître le terrain et voir si elle pouvait repérer d'autres points de repos comme celui là. Elle impressionnait Théodore Andrius. Il se faisait vieux, il en avait conscience, mais même le reste de sa troupe était à bout de souffle, en train de masser leurs muscles douloureux. Pourtant, elle n'avait soufflé que quelques instants avant de reprendre son escalade. En ce moment, il aurait aimé avoir la même fougue.

Porter porta la main à son casque dans un réflexe d'opérateur radio. Avec leur équipement intégré, appuyer près de son oreille n'avait aucun effet, mais les habitudes avaient la vie dure. Elle avait coupé tous les autres canaux pour se concentrer sur celui qu'elle recevait, probablement le longue portée. Elle semblait crier quelque chose, mais l'isolation de son propre casque empêchait les autres de l'entendre. Finalement, elle tapa du pied d'un air rageur et rétabli les communications locales.

«Le groupe Beta a tenté de me joindre. Je crois que c'était Beta-1, mais c'est difficile à dire. Il y avait énormément de friture, j'ai pas réussi à entendre ce qu'il me disait, puis la ligne a carrément sauté.»

Le colonel aurait souri si la situation était différente. Porter utilisait encore les noms de codes datés qu'ils avaient tous abandonnés en lançant la flotte d'exploration. Heureusement, il pouvait encore les comprendre. Beta-1 faisait référence à Roy. Il n'aimait pas cette coupure. L'émetteur longue portée était supposé fonctionner à l'intérieur du vaisseau, sa portée était de plusieurs dizaines de kilomètres. C'était de mauvais augure, mais il ne pouvait pas faire marche arrière.

«Qweil, intima-t-il. Tu te sens de redescendre jusqu'au vaisseau ?

— Sans problème colonel, la descente c'est ce que je préfère.

— Alors retournes-y et dis à Roy qu'on a perdu la communication et qu'on ira jusqu'au bout quoi qu'il arrive. Et s'il se passe quelque chose de grave là-bas, je te laisse aviser.

— Compris ! Bon courage, colonel.»

Et sans attendre la réponse, elle sauta sur le côté et se mit à glisser le long de la paroi, ses bottes magnétiques réglées au minimum pour l'attirer à peine vers la façade de métal sans ralentir sa chute.  En quelques secondes, elle avait disparu dans l'obscurité.

«Et nous, on y retourne, c'est parti !»

***

La température chutait déjà et plusieurs soldats commençaient à trembler. Roy alla dans le ventre du pingouin et en ressortit un réchaud électrique. Il appela un garde sur deux à le rejoindre pour aller se réchauffer, prévoyant d'établir une relève toutes les dix minutes. Des soldats qui tremblaient et avaient le bout des doigts gelés n'étaient pas d'une grande aide. Ils se réunirent à l'intérieur du transporteur, frissonnant mais sentant la chaleur revenir dans leur corps.

De l'autre côté de la baie, Sénart et Tils cherchaient toujours les commandes du système auxiliaire. Ce dernier suggéra l'espèce de container autour duquel ils tournaient autour. Aucun des deux ne savaient à quoi il servait, mais Tils pensait qu'il pouvait s'agir d'une cabine de contrôle d'urgence, au cas où la principale devenait inopérante. C'était souvent le cas sur les porte-chasseurs, où les tours de contrôle des baies étaient des cibles prioritaires pour neutraliser les porteurs souvent trop blindés pour être abattus. L'existence d'un tel dispositif dans un vaisseau amiral avait de quoi interroger, mais cela pouvait être un vestige d'une vieille norme suivie à la lettre sans poser de question. Si c'était le cas, il y avait de grandes chances qu'un paquet de commandes de secours y soient dissimulées.

Restait à pénétrer dans l'habitacle. Les murs étaient blindés, et les vitres trop étroites pour espérer que l'un d'eux ne puisse passer. La porte restait leur meilleur option, mais sans McKlay, ils n'avaient personne qui soit capable de procéder avec délicatesse. Alors Tils décida de ne pas faire dans la dentelle, s'empara de son fusil et tira trois décharges dans les gonds magnétiques qui cédèrent sans opposer de résistance et la porte s'effondra dans un fracas d'acier.

«Qu'est-ce que c'est que ça ? hurla le lieutenant en charge dans son oreillette.

— T'inquiète, mon lieutenant, c'est de l'ingénierie chirurgicale. On a des pistes pour résoudre notre problème.

— Putain, préviens la prochaine fois.»

La réplique de Tils mourut dans sa gorge. A l'intérieur du container hermétique, il venait d'éclairer un cadavre avec sa lampe torche. Recroquevillé sous un tableau de commandes, on aurait dit que son corps avait été vidé de tout liquide jusqu'à ce que la peau adhère à ses os. Puis, il s'était solidifié. Son corps avait la teinte bleu marine d'un mort de froid, mais la texture de sa peau était rugueuse, comme si elle s'était cristallisée. Ses articulations étaient déformées, allongées même, donnant à cette carcasse en position fœtale des proportions absurdes et tranchantes. Jusqu'à son crâne avait été dépossédé de ses cheveux et allongé jusqu'à ce que son menton trop long se termine en pointe et le sommet du crâne soit aplati. Cela lui donnait un air presque comique, s'il n'était pas cauchemardesque, de pic à glace aux traits figés dans une expression de colère.

Il balaya rapidement la pièce de son faisceau, pour découvrir une dizaine d'autres corps, disposés exactement dans la même position. Les occupants de cet endroit s'étaient visiblement assis les uns à côtés des autres pour attendre leur mort, tenante peut-être de mettre en commun leur chaleur pour survivre un peu plus longtemps. Il fallu deux passages à un Tils hébété pour réaliser qu'il y avait un trou, juste assez pour y placer un corps, dans la rangée qui lui faisait face.

«Nom de...»

L'ingénieur n'eut jamais l'occasion de terminer sa phrase, ni de réaliser ce qui lui arrivait. La créature qui lui avait bondi dessus depuis le mur intérieur lui avait écrasé le crâne, compressant son casque jusqu'à ce qu'il explose dans une gerbe de sang. Son corps s'écroula sur la porte, tandis que le monstre portait son attention sur Sénart. Il ressemblait en tout point aux cadavres recroquevillés dans l'abri, à l'exception qu'il était bien plus vif et ses yeux brillaient d'une profonde malice.

Le soldat n'eut que le temps de reculer d'un pas avant que l'autre ne soit sur lui. Il ne le tua pas tout de suite, savourant sa proie. Les renforts étaient trop loin, et la créature s'était habilement positionnée de façon à ce que le corps de l'homme soit entre elle et les éventuels tirs. Elle sortit une langue trop longue pour être humaine qui dégoulinait de cristaux de glace. Ayant fini de jouer, elle s'apprêta à enfoncer ses longues griffes dans le corps de l'ingénieur hurlant à la mort...

Lorsque sa tête explosa. Qweil avait aligné un tir parfait qui avait cueilli la créature entre les deux yeux, la charge explosive se chargeant de faire disparaître tout ce qui pouvait ressembler à un visage sur ce pic à glace. En entendant le boucan généré par Tils, elle avait abandonné toute prudence et s'était ruée jusqu'à la baie, où elle était arrivée juste à temps pour sauver son camarade. Roy les rejoint en premier, en nage.

«Qu'est-ce que c'est que ce bordel ?

— Planétomorphes, parvint à articuler Sénart, encore sous le choc. Tils est mort.

— Les autres se réveillent là-dedans, signala Qweil. Faut retourner au pingouin!

— Ils foutent quoi Skiet et Lean ? C'est eux qui devaient surveiller le périmètre !»

Pour toute réponse, un grésillement strident envahit la fréquence de communication locale, leur vrillant les tympans.

***

L'initiative de Melsh leur avait fait économiser peut-être une heure d'escalade. Partie en avant, elle s'était décalée pour voir si les bords de leur itinéraire ne disposait pas d'un trajet plus simple. Elle avait finalement trouvé mieux que ça, un escalier qui descendait à l'origine jusqu'à la salle des machines du segment 8. La nouvelle réjouis McKlay, qui promit à l'éclaireuse de l'embrasser lorsque la mission serait terminée, ce qu'elle accueillit avec un dégoût non feint.

S'il était certain que les moteurs étaient globalement en un seul morceau, l'autre éclaireur avait vu et revu la série d'explosions qui avait bardé les ponts inférieur du *Maréchal Stellaire*. Il en avait conclu que la plupart des passages jusqu'aux entrailles du vaisseau seraient condamnés par des palissades de débris. C'est pour cela qu'il s'était résolu à placer l'émetteur le plus près possible sans même tenter de descendre.

Avancer dans escalier à l'horizontale se révéla être une tâche plus ardue qu'ils ne l'avaient tous imaginés. La coupe droite des marches les avait transformé en une série de piques peu affûtés, qui auraient presque pu masser leurs pieds s'ils n'étaient pas empêtré dans plusieurs épaisseurs de chaussettes et d'épaisses bottes. Chacun veillait à sa démarche, pour ne pas se causer une entorse malvenue.

La salle des machines étaient plongée dans le noir. Il y régnait un silence pesant. C'était un endroit de bruit, où les engins faisaient un boucan terrible même lorsque l'activité était minimale, et cela se ressentait jusque dans les murs qui semblaient peinés de ne faire résonner le grondement d'aucun moteur.

Le compteur geiger intégré à leurs combinaison s'affola subitement. Il y avait une fuite radioactive quelque part, mais elle devait être suffisamment faible pour ne pas s'être propagée au delà de la salle tout ce temps. Néanmoins, cela leur donnait une raison supplémentaire de ne pas traîner. 

McKlay s'avança au centre de la pièce et posa sa mallette au sol. Il estimait être à équidistance des trois réacteurs mineurs qui alimentaient cette section du vaisseau, à quelques mètres près. Il ne faisait pas de la science exacte. Tandis que ses compagnons se déployaient pour le protéger de toute menace, il déverrouilla la boîte de métal qui s'ouvrit en grinçant. Un clavier se présenta à lui tandis qu'un long tube coiffé d'une diode s'élevait automatiquement.

Il entra ses instructions, puis les nombreuses validations et forçages de protocoles de sécurité. Ce qu'il cherchait à faire était très dangereux, l'avertissait l'écran en lettre de sang. En cas de mésusage du matériel il serait passible de la plus haute cour martiale, bla bla bla. Finalement, une dernière clef fut tournée, et un pad numérique se révéla. C'était presque terminé.

«Une heure, colonel ?

— Il ne doit pas être loin, surtout s'il a senti le pingouin. Mettez trente minutes.

— A vos ordres. Préparez-vous à courir.»

Une dernière séquence de chiffres, et un voyant vert s'alluma, suivi d'un rouge, qui clignota deux fois avant de s'éteindre. Pendant un moment, McKlay crut s'être trompé quelque part. Son corps se tendit et il retint son souffle, jusqu'à ce que la diode ne s'allume et que le bip du décompte commença. Au moment où il respirait à nouveau, il distingua les dizaines de paires d'yeux qui reflétaient le rouge de sa diode, tout autour d'eux.

***

Ils se courbèrent par réflexe tant le son était insupportable. Le lieutenant fut le premier à couper l'entrée audio mais ne se déconnecta pas du canal et chercha le responsable du regard, un de ceux qui étaient restés dehors surveiller le périmètre de sécurité.

«Kleod, c'est quoi cette merde ? Arrête de faire le con et replie-toi au pingouin !»

Le dénommé Kleod se retourna lentement et son regard vide accrocha celui de Roy. Sa peau était devenue sombre et se cristallisait à vue d'œil à travers sa visière. Du sang lui avait coulé de la bouche, le même qui avait gelé autour du trou béant qu'il avait là où aurait dû se situer son cœur. Le bruit insupportable qui couvrait toute la fréquence commune était celui de son menton duquel poussaient des cristaux sombre, écrasant son micro alors que son crâne allongé n'était désormais plus adapté à son casque.

«Kleod est tombé, Mari, Lwo, quel est votre statut ?»

Il avait demandé ça sur la fréquence longue portée, que tous devaient avoir en mode écoute. Seul le silence lui répondit, et la respiration haletante de Qweil qui utilisait la même fréquence. Ils portaient tout deux Sénart dont la jambe avait été écrasée dans l'affrontement. Il clopinait aussi vite que possible, sentant déjà le souffle glacial des planétomorphes dans son cou.

Quelqu'un d'autre se brancha sur la fréquence longue portée. C'était Leeroy.

«Lieut, Mari et Lwo montent toujours la garde, mais ils répondent pas. Carter et parti check ce qui va pas.

— Bordel, Carter, tu reviens tout de suite ! C'est des planétomorphes les gars, ils sont déjà perdu. Dégommez-les. On en a au cul, Brum et Kallax, couvrez-nous !»

Il vit l'indicateur de Carter ouvrir son canal vocal et le refermer aussitôt.

«Merde, reprit Leeroy, ils ont eu Carter ! Lev, on se les fait !»

Il entendit plusieurs salves venir de derrière l'appareil. De là où il était, Roy ne distinguait pas l'action mais craignait le pire. Il sauta sur le côté quand Kallax sorti de l'arrière avec sa mitrailleuse lourde et commença à arroser dans leur sillage. Brum courait vers eux pour les exhorter à se presser. Il les dépassa et ouvrit le feu à son tour avec son fusil en hurlant comme un possédé.

En dépassant l'habitacle, le lieutenant pu voir ce qu'il était advenu des autres sentinelles. Lwo avait empalé Carter à l'aide de son bras désormais nu qui avait transpercé son gant et s'était mué en une lance de pierre. Il le soulevait comme un trophée, tandis que les rafales de Leeroy et Levasseur peinaient à transpercer son armure. Il constata que les deux anciens soldats arboraient des blessures semblables à celle de Kleod. Ils étaient tous les trois morts sans même avoir pu émettre le moindre son. Inutile de se demander ce qu'il était advenu de Skiet et Lean, les sentinelles manquantes.

Ses craintes ne firent que se confirmer lorsqu'il vit sortir de trappes déformées d'autres planétomorphes. Ceux-là n'avaient plus aucun vêtement, et se déplaçaient sur les murs comme des araignées. Ils n'avaient plus rien d'humains, quoi qu'ils aient été avant avait été remplacé par une créature malveillante avide de meurtre. Alors que tout était calme jusque là, ils sortaient subitement de tous les orifices, les encerclant complètement.

***

Le cri du colonel fut instantanément noyé dans la pluie de déflagrations qui suivit le choc initial. McKlay était sûr d'avoir entendu «Retraite !» Il avait été plus long à réagir que les autres, son arme était passée en bandoulière et il s'était accroupi pour déployer le dispositif. Il avait voulu les suivre, Mais quelque chose n'allait pas. Son corps était lourd, et il voyait le monde... à l'envers ? Velenn le regardait d'un air horrifié tandis qu'il s'éloignait avec les autres, en marchand sur le plafond. Comme c'était étrange.

Le corps du sergent McKlay tomba dans le chaos général, alors que l'escouade battait en retraite vers l'entrée de la salle des machines. Les soldats déblayaient leur chemin à coup de décharges à pleine puissance, portant peu d'attention aux cellules énergétiques de leurs armes qui s'épuisaient à vue d'œil. Ils étaient assaillis de toute part par les planétomorphes mais, par chance, la plupart était encore en état d'éveil et ils purent sortir de ce piège sans autre perte.

«Pas d'explosifs tant qu'on a pas remonté l'escalier ! commanda le colonel».

Dutrovsky et Velenn étaient en queue de file et déversaient un flot ininterrompu de décharges sans même regarder derrière eux, dans le seul espoir de ralentir leurs poursuivants. Ils couraient maladroitement sur le sol inégal. Culann trébucha plusieurs fois, mais parvint à suivre le rythme. Stern, en revanche, se tordit la cheville et s'étala de tout son long en travers de l'escalier, fauchant Velenn qui fuyait juste derrière lui au passage.

«Non !

— Ne vous arrêtez pas, ! C'est déjà trop tard pour eux !»

L'ordre du colonel fut asséné comme un coup de fouet dans le canal de communication. Stern et Velenn se figèrent, conscients que c'était une condamnation à mort. Mais le commandement avait été si ferme qu'il ne vint à l'esprit d'aucun des soldats de le contredire. Théodore Andrius s'était permis de jeter un regard en arrière lorsque Velenn avait crié. Il avait réalisé que les planétomorphes étaient plus proches qu'ils ne le pensaient tous et avait dû prendre une décision terrible. Sacrifier deux vies pour en sauver cinq.

Grâce à leur sacrifice, ils atteignirent le grand hangar avec de l'avance. Le grand espace où régnait auparavant un silence glaçant était désormais parcouru de grattements, de sifflements et de bruits de course dont les échos se répercutaient sur les parois glacées avant de se perdre à nouveau dans les ombres. C'était comme si le vaisseau entier s'éveillait et frémissait. Juste avant d'expulser les corps étrangers qu'ils étaient.

Descendre avec prudence n'était même pas une option. Ils sautèrent tous dans le vide, réglant le magnétisme de leur combinaison au minimum pour se laisser glisser de la même manière que l'avait fait Qweil un peu plus tôt. Au moment où ils se jetaient tous dans l'abîme, un bruit sourd retenti dans leur dos.

«Merde, ma combi a la mauvaise pol-»

La radio de Dutrovsky se coupa avant qu'il n'ait pu terminer sa phrase. Cette fois, le colonel n'eut pas de regard en arrière. Il n'en avait pas besoin. Il espérait simplement que les abominations qui les poursuivaient n'étaient pas suicidaires au point de se lancer à leur poursuite sur cette paroi verticale. Ses prières furent entendues, car la clameur qui les surplombait finit par mourir loin au-dessus d'eux.

Un cri attira son attention à sa droite. Porter avait heurté de plein fouet l'une des barrières métallique sur lesquelles ils s'étaient reposés plus tôt. La plateforme céda sous l'impact, mais le mal était déjà fait. Coupée en pleine chute, Porter avait été projetée loin de la paroi et plongeait désormais en effectuant des tonneaux sur elle-même. Ses jambes adoptaient des angles qui étaient tout sauf naturels. Son cri d'agonie résonna longtemps dans le casque des survivants jusqu'à ce qu'elle soit trop loin pour maintenir la communication.

Melsh, Culann et Andrius atterrirent saufs à leur point de départ. Le couloir, si calme lors de leur premier passage, grouillait désormais d'activité. Mais au lieu de se ruer dans leur direction, les ombres tranchantes convergeaient vers l'autre côté, vers la baie de lancement et le reste de leur escouade.

«On traverse !, décréta le colonel.»

***

Ils formaient désormais un mur défensif autour de l'appareil, déversant un déluge de feu dans toutes les directions. Leurs anciens camarades avaient été abattus avant qu'ils ne causent plus de victimes, mais la horde qui émergeait de tous les interstices semblait sans fin. Hecker Vort avait remis en route les moteurs au repos et avait confirmé au lieutenant que le transporteur était prêt à partir.

«Le colonel ira jusqu'au bout, mais c'est impossible qu'il puisse revenir à travers ça, insistait Qweil. Il faut qu'on parte !

— On attend encore un peu ! Je sais qu'il va y arriver ! soutint Roy.»

L'imposante masse de Kallax tomba au sol, transpercé de part en part par le membre acéré d'un planétomorphe qui s'était hissé sur la carlingue pour le prendre en traître. Il n'eut pas le temps de savourer sa victoire qu'un tir de Brum lui faisait sauter la cervelle. Les charges explosives en pleine têtes étaient ce qu'il y avait de plus efficace mais ces munitions n'étaient pas infinies, et ils allaient bientôt tomber à court. Quand ce serait le cas, le lieutenant n'aurait pas d'autre choix que de sonner la retraite et d'abandonner ses camarades et son commandant à leur sort. Mais ils devaient tenir le plus longtemps possible. Il lança sa dernière grenade dans un amoncellement de créatures pour tenter de gagner toujours plus de temps. La moindre seconde comptait.

***

Ils se battirent comme de beaux diables, profitant du fait que l'attention des planétomorphes ne soit pas tournée vers eux pour leur causer un maximum de pertes. Leurs armes étaient proches de la surchauffe, mais ils étaient tous de fins tireurs et faisaient compter chaque décharge. Au lieu de l'approche méthodique que son sens stratégique lui dictait, le colonel les fit courir en prenant tous les risques. Il avait conscience que face à une telle marée monstrueuse, le pingouin ne pourrait pas les attendre longtemps. Leur seule porte de sortie se refermait à toute vitesse, et la moindre seconde comptait.

Un bras décharné attrapa le pied de Melsh alors qu'elle franchissait une porte détruite. L'action se passa au ralentit pour ses deux compagnons. Un instant, elle était dans les airs, en train de sauter par dessus un obstacle, et celui d'après elle avait disparu, traînée dans les profondeurs du vaisseau. Les deux hommes n'eurent pas le moindre regard pour l'endroit où l'éclaireuse avait été emportée, mais ils considérèrent leurs sauts avec plus de prudence, leurs armes pointées vers leurs pieds à chaque franchissement.

Le couloir qui leur avait paru si long à l'aller se parcourait en réalité rapidement, en faisant fi de toute précaution. Ils se frayèrent un chemin au travers de la masse rocheuse, abattant tous ceux qui se mettaient sur leur chemin ou qui réalisaient leur existence. Enfin, ils débouchèrent dans la baie de lancement. Culann se précipita en avant, criant dans le canal de les attendre.

Le *Happy Feet* n'y était pas. La rampe de lancement résonnait encore du décollage, emplissant l'air d'un sifflement métallique grave. Ils arrivaient trop tard. Tout était perdu. Ils avaient sacrifié tout le monde, ignoré la douleur de précieux camarades dans l'espoir de sauver leur propre vie, tout ça pour rien. Les oreilles de Culann se bouchèrent, et tout sembla lointain. Il entendait encore le pistolet du colonel cracher décharge après décharge, emportant avec lui le plus de planétomorphes qu'il pouvait dans un baroud d'honneur héroïque. Mais pas Culann. Il se sentait vide, tout espoir envolé. Il lâché son arme et tomba à genoux, priant pour que sa fin soit rapide et sans douleur.

***

Cédric Roy regrettait d'avoir donné cet ordre. Il était convaincu qu'il le regretterait toute sa vie. Mais quand Levasseur était tombé juste à côté de lui, il avait vrillé. Un moment de faiblesse qu'il ne parviendrait pas à se pardonner. Il avait ordonné, hurlé à la pilote Vort de décoller, de les faire sortir de cet enfer. Peut-être avait-il bien fait ? Elle avait mentionné deux planétomorphes qui s'attaquaient au verre renforcé du cockpit. L'accélération subite de la rampe magnétique avait dû les éjecter, les sauvant tous.

Il n'était pas fait pour commander. Il avait été envieux du colonel à de nombreuses reprises, rêvant secrètement de prendre sa place s'il venait à périr au cours d'une mission. Mais il réalisait à présent que les responsabilités qui incombaient à cette position étaient beaucoup plus lourdes que leur ancien commandant ne le laissait paraître. Il s'était fait une raison. Une fois sorti de cet enfer, il démissionnerait de l'armée, et porterait personnellement ses condoléances aux familles de ces hommes. Il n'avait réussi à sauver que cinq de ses hommes, quel chef pitoyable il faisait.

Le refuge devait avoir décollé, à présent. La chose qui se déplaçait sous terre, l'âme de la planète comme certains poètes du refuge l'appelaient, traquait les rayonnement nucléaire. Ils l'avaient réalisé après avoir vu de nombreux vaisseau se faire dévorer par cet espèce de ver de roche gigantesque qui gobait tout appareil utilisant une propulsion nucléaire pour échapper à la force d'attraction de la planète.

L'émetteur que McKlay avait déployé possédait deux fonctions: émettre une signature nucléaire suffisamment puissante pour attirer l'attention de l'entité, puis déclencher une réaction en chaîne qui se propagerait jusqu'aux réacteurs principaux du *Maréchal Stellaire*, et soufflerait tout dans un rayon de plusieurs centaines de kilomètres. Si tout se passait comme espéré, le dévoreur de vaisseau tenterait d'avaler leur vaisseau-mère et se ferait anéantir par l'explosion. Ou dans le pire des cas, serait suffisamment diverti pour que le refuge puisse quitter l'atmosphère sans se faire attraper. C'était un mauvais plan, qui était désespéré et bourré de faille, mais le seul qu'il leur restait face à la pénurie de ressource qui se profilait.

Ils ne regarderaient pas le feu d'artifice, trop occupés à mettre de la distance entre l'épicentre de l'explosion et eux. Vort devait avoir mis les propulseurs à pleine puissance et filait en direction de la trajectoire attendue du refuge. Cédric Roy regarda les quatre survivants la gorge serrée. Aucun ne parlait, ils avaient tous l'air abattus et défaits. Même s'il n'avait pas le cœur à ça, il était le rigolo de la bande, alors il chercha une blague qui pourrait détendre l'atmosphère.

Le grésillement strident qui envahit le canal de communication ne lui laissa pas le temps de trouver un bon trait d'esprit.