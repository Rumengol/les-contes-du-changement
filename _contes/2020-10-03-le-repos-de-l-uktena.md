---
bg: "le-repos-de-l-uktena"
layout: post
title: Le repos de l'Uktena
summary: Deux explorateurs découvrent le repos de l'Uktena, et sont contraints de s'enfoncer dans les profondeurs d'une révélation troublante.
source: “Native American Temples - Snakehead Entrance” par Jonas Hassibi
ref: https://www.artstation.com/artwork/rAEkoJ
date: 2020-10-03
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---

« C'est bon, tu peux descendre !»

La voix d'Anil se perdait dans l'immense grotte, et même en tendant l'oreille, Acick l'avait à peine entendue. Il hocha la tête en direction d'Alan et de leur deux guides, qui resteraient à la surface veiller sur les affaires, puis se saisit de la solide corde qui se perdait dans le gigantesque trou dans le sol. Il testa par précaution la résistance du support, puis entreprit la descente. Les premiers mètres étaient les plus faciles, il n'avait qu'à marcher le long de la paroi. Ensuite, lorsque ses pieds perdirent prise, il se retrouva seul dans le vide, à plus de cinquante mètres du sol, avec une simple corde le séparant de la vie et de la mort. Ses mains serraient beaucoup trop fort, il en avait conscience, mais il ne parvenait pas à les relâcher. Mètre après mètre, sa descente paraissait durer une éternité.

Le soleil à son zénith illuminait la grotte, et la vue était sans doute exceptionnelle, mais ses yeux étaient fermement clos. Il suait à grosses gouttes, des spasmes commençaient à secouer ses bras. Sur le point de craquer, il décida de se laisser glisser le long de la corde. Il ne devait plus être si haut, après tout. Même au travers de ses multiples couches de bandage, il sentait le frottement augmenter terriblement. Une odeur de fumée remontait de ses jambes. S'efforçant de ne pas imager son pantalon prendre feu, il resserra sa prise pour ralentir sa chute.

Le choc fut plus violent qu'il ne l'avait anticipé. Perdu dans sa panique, il n'avait pas entendu Anil le prévenir, et ses tibias accusèrent le coup. Au sol, le souffle coupé, il était néanmoins soulagé d'y être parvenu en vie. Il resta un moment allongé, à vérifier que tous ses os étaient au bon endroit. Anil l'aida à se redresser, et il prit enfin conscience de l'endroit dans lequel ils se trouvaient. Ils avaient donc réussi.

Le Temple d'Apophis, dont les premières traces remontaient aux origines de l'ancienne égypte, 4000 ans auparavant, était devant eux. L'ultime vestige d'un culte vénérant le dieu du chaos, exilé aux confins du monde. Les autochtones faisaient référence à cet endroit comme «le repos d'Uktena», un gigantesque serpent cornu légendaire. Il était difficile de leur donner tort, cela dit. Ils étaient dans une salle aux proportions inhumaines, soutenue par des piliers ayant remarquablement résisté à l'épreuve des âges, et devant eux un serpent gargantuesque reposait dans un bassin profond. Acick dut se dévisser le cou pour discerner le sommet de sa gueule ouverte, qui culminait à une trentaine de mètres.

Dans le passé, cette figure devait être terrifiante. Recouverte de mousse et décolorée, elle n'imposait le silence que par sa taille. En dehors de la couverture érodée par des plantes capables de pousser à même la pierre, le serpent était remarquablement bien conservé. Anil était taiseux comme à son habitude, aussi Acick prit l'initiative.

« On l'a fait.
&mdash; Oui. On devrait appeler Alan pour descendre le matériel, tu crois pas ?
&mdash; Déjà remonter ? Je viens à peine d'arriver, j'aurais pas la force. Autant en profiter pour explorer un peu les alentours.
&mdash; Je l'ai fait pendant que tu t'amusais avec la corde. L'eau n'a pas l'air très profonde, mais elle est infestée de serpents d'eau, c'est même pas la peine d'essayer.
&mdash; Regarde, dans la gueule d'Apophis, on dirait un passage. On pourrait peut-être...
&mdash; Allons-y, puisque sa princesse est trop fatiguée pour remonter»

Levant les yeux au ciel, Acick commença à gravir les marches effacées par le temps. Le souci du détail apporté à cette sculpture géante était proprement incroyable. Même maintenant, il avait l'impression de sentir chacune des écailles de pierre. L'intérieur du serpent était un couloir plongé dans le noir, dont les nombreuses appliques témoignaient d'un éclairage disparu. Il inspectait le mur, à la recherche d'un mécanisme caché, lorsque la voix angoissée d'Anil le détourna de sa tâche.

« Acick ?»

Son compagnon était resté en retrait, mais marchait à présent à reculons dans sa direction. Sur l'îlot où ils étaient descendus, des dizaines de serpents grouillaient, convergeant vers eux. Leurs sifflements emplissaient l'espace, et ils bloquaient tout espoir de sortie.

« On fait quoi ?
&mdash; Pas question de ressortir, on fonce dans le temple !
&mdash; Attends, c'est peut-être dangereux !»

Mais Anil ne l'écoutait plus et courait à présent dans les profondeurs du serpent. Après un bref instant d'hésitation, Acick le suivit. Il y avait peu de chances que des mécanismes millénaires soient encore actifs, et il préférait ce risque à une mort douloureuse, dévoré par des reptiles sifflants. Malgré le travail magistral qui avait été réalisé par les bâtisseurs de l'endroit, le sol était irrégulier et parsemé de trous. Il manqua de chuter plusieurs fois dans le noir, et entendait Anil plus loin jurer. La courbure du corps le prit par surprise et il se précipita dans un mur. Sonné, il lui fallut un temps pour reprendre ses esprits.

Les sifflements furieux s'intensifiaient derrière lui. Il reprit sa course, plus prudemment toutefois. Le couloir était désormais en pente, il s'enfonçait dans le sol. L'explorateur redoubla de prudence lorsqu'il atteignit la partie immergée et ses pierres trempées. Devant lui, Anil cria. Par réflexe il tendit la main en avant, mais perdit l'équilibre et se retrouva à rouler brutalement dans cette pente qui semblait sans fin. Il pouvait sentir chacun de ses os se briser, alors qu'il s'efforçait de ne pas exposer sa nuque. A quoi bon échapper aux morsures des serpents si c'est pour succomber à celles de la pierre ?

Puis ce fut le vide. le sol s'arrêta subitement, mais pas la chute. Le temps se dilata alors qu'il était suspendu en l'air, sans rien pour le retenir. C'était là qu'Anil avait crié. Il s'écrasa douloureusement sur de la terre molle qui amortit partiellement sa chute. Ce n'était pas normal de sentir l'écoulement de son sang, il en était presque sûr. Il était incapable de bouger un muscle. Avant de sombrer dans l'inconscience, sa dernière vision fut Anil fermement retenu par deux hommes à l'apparence de serpent, une dizaine d'autres le regardant fixement de leurs yeux reptiliens.
