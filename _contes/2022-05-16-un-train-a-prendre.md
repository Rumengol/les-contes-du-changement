---
bg: "un-train-a-prendre"
layout: post
title: Un train à prendre
summary: Trois jeunes adolescents cherchent à atteindre un vieux dépôt de trains.
source: “Railways” par Maksym Harahul
ref: https://maksym-harahulin.cgsociety.org/owb7/railways
date: 2022-05-16
categories: posts
tags: ["Un train à prendre"]
type: Conte
author: Rumengol
lang: fr
---
— Le sang s'est déjà arrêté de couler, on peut enlever ton bandage. Tu te rappelles de ce qu'il s'est passé ? 

— Vaguement, jusqu'à l'explosion en tout cas. Le reste est très flou. 

— On va faire le point. Comme il nous l'a appris, sans oublier le moindre détail. 

— OK, je commence. Ça a commencé hier soir, à 22h24. J'étais de garde dehors, avec Pierrot. Il y avait Röwen sur le toit, aussi. Je sais que l'un de nous aurait dû être à l'intérieur, mais tout était up : les caméras de proximité, les détecteurs de mouvement, de chaleur, les senseurs magnétiques... Ferdinand est... était tellement parano qu'il avait même pas voulu ajouter un seuil de détection, et on avait déjà reset les alarmes trois fois depuis qu'on avait pris position. Et j'étais en train de piquer du nez, devant les écrans, alors j'ai voulu sortir un peu. Je sais pas comment ils ont fait, mais ces gars étaient des pros. C'est tout juste s'ils n'auraient pas pu arriver juste sous notre nez sans qu'on les repère.  
A un moment, j'ai cru voir quelque chose briller dans le ciel, alors je suis rentré jeter un œil aux cams. Je crois que c'est grâce à ça que j'ai survécu. J'en ai profité pour check les relevés, vite fait, et j'ai vu que les capteurs de pression étaient tous à quarante kilos de plus que la normale. 

— Pourquoi ils ont pas sonné, alors ? 

— A mon avis, c'est une connerie de différentiel. Le système doit soulever d'anomalie que si des plaques s'éloignent trop du consensus. S'ils ont trouvé un moyen pour toutes les abaisser en même temps, le consensus a bougé avec. 

— Ouais, comme dit Noémie. J'ai remarqué ça, et j'ai voulu le dire à Pierrot. Mais quand j'ai levé la tête, il 'était plus là. Ça faisait trop de bizarreries, alors j'ai appuyé sur le bouton d'alarme. Après ça, le mur a volé en éclats et j'ai été projeté en arrière avant de perdre connaissance. 

— Je reprends à ce moment. Avec Noémie, on était dans notre chambre, on a été réveillées par l'explosion, qui a secoué tout le bâtiment. On a entendu l'alarme juste après, mais on était déjà en train de s'activer. Comme aux répétitions : on s'est habillées, on a pris les affaires essentielles et les pistolets dans l'armoire. J'ai pris le tien, au cas où. J'avais pas encore capté que t'étais de garde. C'était la panique quand on est sorties, il y avait personne pour nous dire quoi faire. 

— On était même les plus calmes, et ça c'est pas normal ! Ils avaient pas reçu d'entraînement, les autres ? 

— Chaque personne a sa façon de gérer les situations d'urgence. Vous avez fait de l'excellent travail dont peu auraient été capables. 

— Si tu le dis. Du coup, on a été chercher le chef d'étage, mais il était visible nulle part. On savait pas trop s'il fallait qu'on monte ou qu'on descende, parce que quand ça s'est organisé il y avait des gens qui couraient dans les deux sens. 

— Alors, j'ai suggéré qu'on aille te retrouver. J'étais inquiète. 

— C'est adorable. 

— Et nécessaire ! On est arrivé au deuxième vers 22h32, et on a vu Rob qui te portait sur son dos. T'avais pas l'air bien, et ton front saignait ! Il a dit que t'avais pas de commotion, mais que t'étais pas en état. On entendait clairement les coups de feu venir d'en dessous, mais il a dit que c'était pas notre combat. Notre mission était de t'emmener en lieu sûr, dans la planque de la station essence. Il nous a dit qu'on contrôlait encore les ascenseurs et qu'on pouvait passer par les souterrains. On voulait rester et se battre, bien sûr, mais il nous a carrément poussées dedans avec toi ! Et après, il a dit qu'il nous rejoindrait dans 10 heures grand maximum. Et si ça n'était pas le cas, on devait aller prendre le train. 

— Ça, là, t'es sûre que c'est ce qu'il a dit ? C'était quoi, ses mots exacts ? 

— « Allez à la station essence, je vous rejoins dans 10 heures max. Si vous me voyez pas arriver, vous devrez prendre le Train. » Il a eu une façon bizarre de le dire, c'était comme s'il mettait une majuscule à Train. Puis il y a eu une explosion en bas et il est reparti en courant. 

— Comment t'as retenu ça mot pour mot dans cette situation ? T'es pas normale... 

— Bon, et après ? 

— Après, on est descendues au -2 et on t'a porté dans les souterrains. On a dû se mettre à deux, t'es vachement lourd quand tu t'y mets ! 

— On s'est doutées que ceux qui nous attaquaient auraient pensé à bloquer tous les chemins de retraite, du coup on est sorties par le passage qu'on a découvert tous les trois, celui que même Rob ne connaît pas. La ficelle était toujours en place, alors on a supposé que personne d'autre n'était tombé dessus. Après ça, on t'a tiré jusqu'à la planque, on t'a soigné et on est restées ici en attendant que tu te réveilles. 

— Personne ne s'est approché, et ça va bientôt faire 11 heures qu'on l'attend. Tu crois qu'il va venir ? 

— S'il a dit 10 heures max et qu'il est toujours pas là, peu de chances. 

— Alors ça veut dire qu'il est... 

— Pas nécessairement. Mais s'il a loupé l'heure de rendez-vous, il va s'attendre à ce qu'on se déplace et que personne ne l'attende. Vous le connaissez aussi bien que moi, il viendrait même pas s'il avait dix minutes de retard. On est là depuis plus qu'assez de temps. Ce train, là, vous savez à quoi il faisait référence ? 

— Il n'a jamais parlé de train avant... 

— Tu crois qu'il parlait de la grande gare ? Un train nous attendrait là-bas ? 

— Non, ça c'est impossible. Il ne nous enverrait pas au suicide. On ne peut même pas approcher le premier checkpoint sans se faire trouer. Personne n'a d'idée ? 

— ... 

— Peut-être qu'il pensait au dépôt désaffecté ? 

— Pardon ? 

— Non oublie, c'est stupide. 

— Noémie, allez. On sait pas quoi faire, alors si t'as une idée, même une piste, on la prend. 

— Il y a un vieux dépôt de trains abandonné, dans la zone indus. Ils voulaient étendre la grande gare jusque là-bas, mais le groupe Solal y a envoyé un wagon avec une tête nucléaire amorcée, alors ils ont plongé toute la zone en stase en catastrophe. Depuis, les trains sont coincés et toute la zone est inexploitable. C'était peut-être un nom de code pour désigner cet endroit ? 

— Nan, ça colle pas. Il nous a clairement dit d'aller à la station essence, pourquoi il utiliserait un code qu'on connaît pas juste après ? Mais ça reste notre meilleure piste. Et même s'il n'y a personne, ça fera toujours une meilleure planque qu'ici. Récupérez les affaires, on y va ! 

Il n'y avait pas grand-chose à récupérer en réalité. Noémie n'avait pas pu supporter le poids de Terrence en plus de celui de son sac, qu'elle avait abandonné. Ne restait plus que celui d'Alita contenant des barres nutritives, deux gourdes et seize chargeurs pour leurs pistolets, en plus de la trousse de premiers secours entamée. Elle avait eu un sens des priorités très fonctionnel, quoiqu'un peu excessif selon le goût de son grand frère d'adoption. Il fouilla rapidement ses poches, en particulier celle de sa veste militaire pour en faire l'inventaire. 

Heureusement, il n'avait pas perdu le pistolet-mitrailleur passé en bandoulière. Il n'avait aucun chargeur, alors il lui faudrait être prudent sur sa consommation, mais c'était déjà une arme bien supérieure à de simples pistolets, en particulier contre un grand nombre d'ennemis. Ses doigts trouvèrent dans la poche près de son cœur la photo écornée de ses parents, lui arrachant un soupir de soulagement. Il avait quelques bonbons, un couteau rétractable et une carte. Encore un de *ses* enseignements qui portaient maintenant leurs fruits. 

Il déploya la carte et essaya de retrouver leur position. La tour, cerclée de rouge, était facile à identifier. Un peu au Sud, la station essence où ils se trouvaient était l'un des nombreux cercles verts. Il n'avait pas de stylo, mais il nota mentalement de le barrer. Les planques mal sécurisées n'étaient utilisées qu'une fois, par précaution. La zone industrielle s'étendait à l'Est de la ville sur des kilomètres. Un dédale d'usines, de cuivre et de béton dans lequel il était impossible de se repérer sans l'aide des locaux qui y travaillaient. Une plus petite zone était recouverte de jaune, avec un sablier dessiné à côté. Des lettres minuscules formaient l'inscription « Berkley's Train Depot ». Il n'avait aucune idée de qui pouvait bien être ce Berkley, mais c'était bien leur destination. Coup de chance, le dépôt était à l'entrée de la zone industrielle, ce qui voulait dire qu'ils n'auraient pas à se perdre dans le labyrinthe mécanique. Il leur faudrait tout de même parcourir une dizaine de kilomètres, sans le moindre véhicule. 

Une fois leur itinéraire tracé, ils rangèrent la carte et observèrent les alentours. Tout semblait calme, du moins à première vue. Ce quartier était laissé à l'abandon depuis des années comme bien d'autres, mais certains habitants refusaient de quitter leur maison. À plusieurs endroits, ces gens avaient développé des habitudes culinaires qui menaient à de violentes confrontations avec les pilleurs et les soldats de la résistance qui tentaient d'échapper aux milices de la cité. Terrence, Noémie et Alita espéraient que la rue qu'ils devaient emprunter avait déjà été nettoyée de ces résidents un peu trop acharnés. 

Ils avancèrent rapidement, en longeant les murs des façades et en contournant les ruelles étroites. L'arme au point et sur le qui-vive, le trio progressait trop lentement à son goût. Personne ne parlait, de peur de briser le silence sacré qui régnait dans l'avenue déserte. Leurs espoirs d'un passage sans encombre fut réduit à néant par le vrombissement d'un drone à l'approche. Sans réfléchir, ils poussèrent la première porte ouverte et pénétrèrent dans un vieil immeuble croulant. Dissimulé derrière les murs décrépits, Alita risqua un regard afin d'évaluer la menace à laquelle ils devaient faire face. 

Par chance, ce n'était qu'un drone de patrouille de routine, le genre de ceux qui arpentaient la périphérie pour détecter et recenser n'importe quel individu pris dans son regard numérique. Rien à craindre tant qu'ils se faisaient discrets, ces drones n'étaient dotés que de capteurs optiques et sonores. Cependant, cela voulait aussi dire qu'ils ne pouvaient pas ressortir avant plusieurs heures, ces engins déployant un large panel de ruses et astuces pour piéger des cibles mal préparées. S'imposant le silence, ils communiquèrent par signes. Terrence fit signe de monter à l'étage avant de montrer l'exemple, rapidement suivi d'Alita puis de Noémie. 

À grand renfort de signes, il fit part de son plan aux filles. De manière générale, les drones volaient à hauteur de toit et scannaient la rue en contrebas, ou bien descendaient au niveau du sol pour adopter une vie rapprochée. S'ils parvenaient à sortir sur les toits, ils seraient capables de se déplacer sans attendre de longues heures, voire de prendre de l'avance sur la machine et ses allers-retours fréquents en zone urbaine. Noémie n'était pas franchement acquise à son plan, mais elle abandonna face aux yeux brillants d'Alita. La plus jeune avait toujours adoré les acrobaties, et les escapades sur les toits étaient ses préférées. 

Ils avaient atteint le quatrième étage lorsqu'un son les fit stopper. Un bruit de mastication sonore, repris par une multitude de bouches. Cela venait d'un appartement dont la porte était grande ouverte sur le couloir et la cage d'escalier. Terrence s'approcha et regarda à l'intérieur avec prudence. Le spectacle auquel il assista manqua de le faire vomir. Les murs de l'appartement étaient repeints de rouge, et la texture visqueuse lui assurait que ce n'était pas de la peinture. 

Au centre du salon, une demi-douzaine de personnes étaient réunies, à quatre pattes, autour d'une immense pièce de viande. Elle était atrocement déchiquetée, mais le garçon pouvait distinguer avec certitude une jambe dont le pantalon n'avait pas entièrement été retiré. Les convives de ce macabre festin mordaient la chair à nu, tels des animaux sauvages. La plupart n'avait même pas de vêtements, ou bien des lambeaux. Comme si leur humanité se perdait en même temps que leur santé mentale. Et ils se baffraient, certains avaient l'estomac si plein qu'il en était littéralement ballant, la peau étirée de leur ventre prenant une teinte violacée. Et pourtant ils ne s'arrêtaient pas, victimes d'une voracité intarissable. 

Il tenta de pousser discrètement la porte, mais sitôt qu'il la toucha, celle-ci grinça. Les bruits ignobles de mastication cessèrent pendant un instant. Il pouvait sentir leurs regards fixés sur le couloir. Sans doute hésitaient-ils à enquêter de peur de perdre leur place et de précieux morceaux de viande. Et comme ils s'étaient arrêtés, les coups de dents reprirent, accompagnés de quelques grognements. Terrence se retourna vers les deux filles, et leur fit comprendre qu'elles devaient passer à l'étage suivant sans jeter le moindre regard à l'appartement morbide. 

Noémie obéit, atteignant sans encombre le balcon suivant vierge de toute infamie. Puis vint le tour d'Alita. En posant le pied sur la première marche de l'escalier, elle se retourna vers Terrence. Ce faisant, son regard embrassa la vision d'horreur du festin macabre, et elle ne put retenir un petit cri. Un tout petit cri, à peine audible. Mais à nouveau les claquements de mâchoire cessèrent, et cette fois lorsqu'ils se tournèrent vers le couloir, les cannibales furent accueillis par la vision d'une fillette paralysée par la peur. 

— Cours ! Hurla Terrence. 

Il claqua la porte et se précipita sur sa petite sœur tout en faisant glisser son pistolet-mitrailleur dans sa main. Il n'avait jamais combattu ces abominations, mais il avait entendu les histoires. Il ne fallait pas se fier à leur physique grotesque, ils étaient incroyablement rapides et pouvaient tordre des barres de fer d'une seule main. Ils montèrent les escaliers quatre à quatre, priant pour voir le toit. Les pas de leurs poursuivants étaient comme le roulement du tonnerre. Le premier qui arriva dans leur champ de vision n'avait plus rien d'humain. C'était une créature, mi-homme mi-araignée, qui se déplaçait sur ses longs membres graciles en gardant son monstrueux abdomen proche du sol. Terrence tira une salve de balles en arrière d'une main, le recul lui disloquant presque l'épaule. Le boucan hésita une minuscule seconde avant de repartir de plus belle. Un de moins. Trop restant. 

Noémie les appela depuis un appartement. Elle se trouvait hors d'une fenêtre et leur faisait signe de la rejoindre. Ils coururent jusqu'à elle et sautèrent sur le toit qui se trouvait en contrebas de la fenêtre, bientôt suivis par les cinq survivants, qui gagnaient du terrain. Là où les adolescents devaient grimper les échelles ou se rétablir à coup de roulades, les choses escaladaient sans mal les murs et se laissaient simplement tomber. 

Intrigué par ce bruit soudain, le drone monta vers les toits depuis une ruelle. Il perturba ainsi la course d'un cannibale qui le percuta de plein fouet, les envoyant tous les deux huit étages plus bas dans un fracas de métal et de hurlement. Les autres ne ralentirent même pas pour assister à la mort de leur camarade. Ils arrivaient bientôt au bout des toits, et à celui du quartier. Ils ne pouvaient pas fuir plus loin, il leur fallait se battre, et gaspiller de précieuses munitions. 

— Maintenant ! 

Comme un seul, le trio se retourna, trois armes à feu braquées sur leurs assaillants. Au prix des dernières balles de son UMP, Terrence parvint à en abattre deux tandis qu'Alita n'avait eu besoin que d'un tir précis entre les deux yeux pour achever sa cible. Noémie, en revanche, rata plusieurs tirs et ne parvint à toucher la sienne que dans l'épaule. La démonstration parut cependant lui suffire, car il cessa son approche et se mis à reculer, non sans grognements rageurs à l'attention du groupe. Dans sa retraite, il prit l'un des cadavres par la jambe avant de disparaître par un velux brisé. 

&nbsp; 

— Tu crois que le drone nous a vu ? 

— Je pense pas, il était tourné vers eux, et tout s'est passé très vite. Au plus il a noté trois personnes se faisant courser par des cannib'. Aux yeux du système, on doit être déjà morts. 

Ils traversaient maintenant une épaisse forêt qui s'étendait entre le quartier qu'ils venaient de quitter et la zone industrielle. Elle avait été un projet de reboisement urbain échoué, qui s'était étendu bien au-delà des limites prévues. Plusieurs quartiers avaient même été absorbés par son expansion galopante, jusqu'à ce qu'elle soit contenue par les pesticides répandus dans la rivière au Nord. Depuis, les super-arbres conçus pour une propagation express envahissaient la campagne hors de la cité, et personne ne s'en occupait. Une faune s'était probablement développée sous la cime des arbres, mais tant qu'ils restaient sur la route ils ne devraient pas être inquiétés. 

Ils avaient fait une première pause en entrant sous le couvert des broussailles, et la moitié des barres nutritives y étaient passées, ainsi qu'une gourde. Revigorés et remis de leurs émotions, ils avaient attaqué la partie suivante de leur périple avec une énergie renouvelée. Énergie qui disparut bien vite lorsque le soleil d'après-midi les accabla, même au travers de l'épaisse végétation. L'atmosphère était humide et lourde, ils avaient l'impression d'avancer dans une mélasse invisible. Ils s'étaient accordés une seconde pause au milieu de l'après-midi, mais à présent leur dernière gourde était quasiment vide, et il leur fallait la préserver. 

Ils arrivèrent enfin en vue de la zone industrielle au soir, alors que le soleil commençait à décliner. Terrence profita d'une dernière pause pour consulter la carte, vérifier le chemin qu'il leur fallait réaliser pour atteindre le vieux dépôt. Le chemin le plus direct consistait à traverser une usine de métallurgie, mais il se faisait tard. Si dans la journée les ouvriers ne se souciaient pas du passage sur leur lieu de travail, laissant parfois même des voleurs se servir, ils avaient tendance à installer toutes sortes de sécurités à la nuit tombée. Elles pouvaient aller de la banale alarme au réseau de mines antipersonnel. Par précaution, Terrence décida de contourner l'usine, choix approuvé par Noémie. 

Le soleil s'était dissimulé derrière l'épaisse chape de nuages mêlée au smog qui planait en permanence au-dessus de la zone industrielle, transformant le monde en nuances monotones de gris. Alita sauta de joie lorsqu'ils rencontrèrent leurs premiers rails, qu'ils décidèrent de suivre. Le voyage avait été riche en émotions, mais ils arrivaient au bout. Si ce n'était pas une fausse piste. Mais ils n'avaient que cet espoir auquel se rattacher. Les cadavres de trains se faisaient de plus en plus nombreux, signe qu'ils avançaient sur la bonne voie. 

Ce dépôt était un labyrinthe en lui-même. Les wagons parfois entassés les uns sur les autres étaient les murs d'un dédale dans lequel les marques à moitié effacées des locomotives étaient les repères. En traversant l'espace entre deux wagons, ils assistèrent à un spectacle unique. Des rails volants dans les airs, à moitié en train de se désagréger. Plusieurs voies se croisaient ainsi, déchirant çà et là les câbles de chemin et fer, qui eux-mêmes paraissaient s'entremêler dangereusement. Aucun ne bougeait. Et au milieu de ce paysage figé dans le temps, un vieux wagon container de métal brun, qui avait dû être rouge par le passé. La porte était en partie soufflée de l'intérieur, et on pouvait y voir une lumière intense ainsi que des particules embrasées tenter de s'en échapper. C'était comme si un soleil tentait de se libérer d'une cage trop petite pour lui, mais que le continuum du temps s'était stoppé juste avant qu'il ne parvienne à briser ses chaînes. La stase n'avait presque jamais été utilisée en dehors des plus beaux quartiers de la cité, et ses effets étaient aussi impressionnants que terrifiants. 

Complétant cette scène surréaliste, une silhouette se tenait juste sous le wagon, droite comme un piquet. De petit gabarit, elle avait relevé la capuche de son imperméable vert et couvert le reste de son visage par un masque à gaz, dont les yeux brillaient comme des lampes torches. Ou des phares. L'homme — ou la femme — tenait dans ses mains un fusil d'assaut qui semblait trop grand pour ne pas avoir été largement modifié. Et juste à côté de son treillis militaire, un chien entièrement noir aux yeux brillants fixait les adolescents, sans esquisser le moindre mouvement. Terrence se demandait si le duo incongru avait été figé avec le wagon lorsque le garde les interpella avec un très fort accent de l'Est, russe, ou ukrainien peut-être. 

— Ici territoire Doroshenko. Pas passer. 

— Heu... bonjour ? 

— Je dis : pas avancer. Et baisser armes ! 

Le chien à son côté commença à grogner, et lui-même braqua son fusil droit sur Alita qui tenait fermement son pistolet. 

— Les filles, rangez ça. On a aucune chance contre lui à découvert comme ça. Et on peut vraiment pas se permettre d'avoir Doroshenko au cul en plus. 

À contrecœur, la jeune fille baissa son pistolet et le rangea dans sa poche. Terrence se retourna vers leur interlocuteur. 

— S'il vous plaît, on ne veut pas d'ennui à Doroshenko, on veut juste passer. On est de la résistance. 

— Vous sourds ? Partez ! 

— Terry, peut-être que le train à prendre c'est une sorte de code, ou un mot de passe. 

— Ça coûte rien d'essayer. Eh, on a un train à prendre, vous pouvez nous laisser passer ? 

— Pas train ici. Dernière chance avant je fâche ! 

— C'est Robert Kingsley qui nous envoie ! 

Si l'exclamation d'Alita avait surpris ses compagnons, elle sembla faire encore plus d'effet à l'homme qui avait ostensiblement baissé son canon. 

— Vous venez de la part du vieux Kingsley ? Mais pourquoi vous l'avez pas dit plus tôt ? 

Toute trace d'accent avait subitement disparu, et sa voix sonnait plus aiguë. Terrence cligna plusieurs fois des yeux, dubitatif. 

— Vous connaissez Robert ? 

— Bien sûr, il nous a pas mal aidé, ici. Fallait commencer par là, enfin ! Allez, venez avec moi. 

Le garde avait rabattu sa capuche et retiré son masque pour dévoiler le visage d'un petit garçon, encore couvert de tâches de rousseurs. Son sourire amusé et ses yeux pétillants tranchaient avec la voix brutale qui les menaçait quelques instants plus tôt. 

— Vous avez pas été suivis ici, au moins ? 

— Non, enfin je ne crois pas. On a évité l'usine de métallurgie à l'Ouest et on est restés sous les radars. 

— Vous avez bien fait, après la fin du travail. Vous savez que c'est ces tarés qui ont popularisé le champ de mines nocturne ? 

Un frisson parcourut Terrence. S'ils avaient tenté de traverser l'usine... Il préférait ne pas l'imaginer. 

— Lukov va s'en assurer, vous inquiétez pas. Suivez-moi, je vais vous amener à la gare. 

Il siffla deux fois et le chien s'activa soudain et se précipita vers l'autre bout des rails, passant entre Terrence et Noémie. Cette dernière ne put s'empêcher de lâcher un hoquet d'étonnement. 

— Un cyber-chien de garde ? Ils existent encore ? 

— On a une connaisseuse, je vois ! C'est l'un des tout derniers modèles encore en état de marche, et je le chéris de tout mon cœur. C'est le vieux Lorne qui me l'a donné. 

— Qui ? 

— Celui qu'on va voir. C'est lui qui gère notre communauté. En fait, il l'a basiquement créée avec le vieux Kingsley un peu après que le dépôt ne se fasse staser. Et c'est aussi l'opérateur du Train, ce qui vous intéresse déjà plus. 

— Je croyais qu'il n'y avait pas de train ? Et au fait, il est passé où ton accent russe ? 

— Haha, je peux faire accent et grosse voix si besoin. Mais tout ça c'est pour de faux, on est même pas avec Doroshenko. Enfin pas vraiment. C'est surtout pour empêcher les gens de venir fouiner. Le flingue est vrai, par contre. 

— Et il vous laisse utiliser son nom comme ça ?  

— Disons qu'il voit son intérêt à protéger cet endroit. C'est du gagnant-gagnant : notre communauté reste neutre et ne lui doit rien, mais officiellement il possède le vieux dépôt. Et puis, je crois qu'il a des vues sur ma mère, aussi. 

— Doroshenko, un cœur à prendre... Décidément, j'aurais tout vu et tout entendu aujourd'hui. 

— Je savais même pas qu'il avait un cœur ! 

— Je serais vous, je ferais attention à ne pas le critiquer trop fort. Les trains ont des oreilles. C'est bon, on y est. 

Le jeune garçon descendit une volée de marche et frappa à la porte. Trois coups secs, mesurés. Un panneau glissa et une paire d'yeux apparut de l'autre côté. 

— C'est toi Tommy ? T'es pas de garde ? 

— C'est Jimmy aujourd'hui. Et j'ai trouvé des gamins perdus, je les emmène voir le boss. Ils viennent de la part de Kingsley. 

— Gamin toi-même, marmonna Terrence. 

— J'ai entendu ! 

La porte s'ouvrit, et un vieil homme en sortit. Un physique de brute fatigué, l'homme devait avoir été au moins boxeur dans sa jeunesse. Il dominait Terrence d'une tête et les dévisageait avec un œil mauvais. Puis il avisa la quincaillerie portée par les trois invités. 

— Pas d'armes. Donnez. 

Après un moment d'hésitation, ils cédèrent leurs pistolets à l'homme patibulaire. Ils ne pouvaient pas les garder de force, et ces gens semblaient être des amis de Rob, après tout. Il libéra le passage et ils purent pénétrer dans la cave dont il gardait l'entrée. En fait de cave, c'était tout un hall de gare souterrain qui se déroulait devant leurs yeux. Ils descendirent la cage d'escalier métallique, le regard rivé sur cette scène qui tranchait avec la morosité extérieure. 

Presque chaque voie était occupée par un long train dont l'intérieur avait été réaménagé selon les envies de ses occupants. Il y avait des modèles de toutes les époques, des vieux wagons de bois jusqu'au TGV désaffecté. Des gens entraient et sortaient continuellement des voitures, portant du linge, des bassines d'eau ou de légumes. Sur les quais pareille activité faisait rage, mais dans un autre genre. Des hommes et femmes en tenues militaires ressemblant à celle de Jimmy révisaient leurs armes, discutaient entre eux autour d'un brasero ou jouaient. Dans un coin, Alita repéra ce qui ressemblait à une forge à ciel ouvert, devant laquelle de très nombreuses balles étaient entreposées. 

Les gens se retournaient sur leur passage, avec un mélange d'appréhension et de curiosité. Ils ne devaient pas avoir l'habitude des nouveaux arrivants. Leur guide ignorait royalement les interrogations muettes qui lui étaient adressées. 

— Et sinon, vous êtes qui pour le vieux Kingsley ? 

— Ses enfants, d'une certaine manière. Il nous a recueilli quand on a perdu nos parents. 

— Alors vous êtes frère et sœurs ? 

— Pas de sang, mais on est une vraie famille, oui. 

— Ça va faire plaisir à Lorne, ça. Il a toujours dit que Kingsley devrait songer à fonder une famille. 

Le jeune soldat les mena à l'autre extrémité de la gare, vers l'office du directeur. Une large horloge figée indiquait à jamais l'heure à laquelle la zone entière était entrée en stase. À 13h07 précises. Dans le bureau, un vieil homme les accueillit. Il était fatigué et grisonnant, ses lunettes rondes semblant prêtes à tomber de son nez à tout moment. Il les regarda avec un sourire triste, et ils reconnurent l'homme qui venait parfois s'entretenir avec Robert en tête-à-tête. Jimmy lui fit un rapport succinct, et l'homme se leva. Il se dirigea vers le trio avec un sourire sans joie ni chaleur.  

— Mes enfants. Vous êtes bien sûrs que Robby vous a dit de prendre le Train ? 

— Certains même. Mais on a aucune idée de ce que c'est. 

— Comme ça a dû lui coûter de prendre cette décision. Mais c'est ce que je lui avais dit de faire il y a bien longtemps. Les alentours de cette cité ne sont pas faits pour des jeunes âmes. La résistance, ses idées de révolutions, c'est un fardeau bien trop grand pour vous. Le Train est le dernier du dépôt encore fonctionnel, celui qui a réchappé à la stase. Sa voie est directement reliée au monde extérieur, vous pourrez vivre une nouvelle vie dans les communautés libres, loin de l'influence du Directoire. 

— Si c'est vrai, pourquoi vous restez, vous et tous ces gens ? 

— Le Train... ne possède qu'un seul wagon. Il nous est impossible de suivre les rails vers l'extérieur à pied, il faut que la locomotive nous tire, mais il n'y a pas assez de place pour tout le monde. Les gens qui vivent ici ont accepté leur sort, ils savent que moi seul sait comment diriger ce train. C'est vous que j'ai choisi comme voyageurs, vous en qui Robby voyait tant de potentiel. 

— Et si nous, on veut pas partir ? 

— Ce serait une très mauvaise décision. La dernière volonté de Robby était que vous puissiez vivre votre vie loin de ces combats futiles. 

— Arrêtez de parler de lui comme s'il était mort ! 

Lorne se tut et retourna s'asseoir à son bureau. 

— Je ne vais pas vous forcer. Le Train sera prêt à partir dès demain. Réfléchissez-y calmement, Tommy va vous montrer vos chambres. 

— C'est Jimmy aujourd'hui, boss. 

— Pardon Jimmy. Emmène-les, veux-tu ? 

— À vos ordres. 

Il les refit traverser le quai, jusqu'au train de la voie 9. C'était un ancien train de marchandises dont les containers métalliques avaient été brillamment réaménagés. Des hamacs et des lits superposés remplissaient la plupart, mais certains autres étaient des cuisines de fortune ou des salons aux sièges visiblement très confortables. Jimmy les conduisit jusqu'au bout, dans le container accolé à la locomotive dévorée par la rouille. Le wagon possédait 6 couches, mais pas de vrai lit ni de hamac, au grand dam d'Alita. 

— Désolé, on a pas encore eu bien le temps d'aménager celui-là. Restez là, je vais chercher de quoi manger. Je vais pas être long, mais évitez de sortir. Les gens d'ici ont tendance à ne pas trop apprécier les étrangers. 

Les trois adolescents choisirent chacun un matelas et s'y allongèrent alors que Jimmy s'éclipsait. Quelque chose paraissait étrange à Noémie, mais la jeune fille était trop fatiguée pour y faire attention. Le garçon réapparut quelques minutes plus tard, brandissant triomphalement une assiette de sandwichs en triangle et une grande carafe d'eau. Il était accompagné de Lukov, que Noémie câlina avec passion. Puis les trois invités se jetèrent sur la nourriture, leurs estomacs affamés de n'avoir mangé que des barres nutritives depuis la veille.  

Peu de temps après, ils s'endormirent. Plongés dans un sommeil artificiel, aucun d'entre eux n'entendit la locomotive se mettre en branle ou ne sentit les cahots des rails en partie détachés. 

 