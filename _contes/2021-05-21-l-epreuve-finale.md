---
bg: "l-epreuve-finale"
layout: post
title: L'épreuve finale
summary: Cinq apprentis affrontent la dernière épreuve de leur parcours.
source: “Desert rays” par Truthbynature
ref: https://www.instagram.com/p/B8AmoHznD2v/
date: 2021-05-21
categories: posts
tags: ["Le monde d'Alkénor"]
type: Conte
author: Rumengol
lang: fr
---
Les cinq hommes se regardaient en silence, anxieux. Aujourd’hui était le jour de vérité. Tout leur entraînement les avait conduit ici, au sommet de l’oasis des crabes. Lâchés au petit matin en plein milieu du désert, ils avaient tout d’abord dû atteindre en moins d’une demi journée l’oasis sous un soleil brûlant. Sitôt atteinte, ils leur avait fallu escalader à mains nues les imposants piliers rocheux qui soutenaient le plateau couvrant l’oasis, avant de traverser l’épaisse forêt qui se trouvait à son sommet.

Les mains écorchées, l’esprit aussi épuisé que son corps était assoiffé, Ilan avait eu toutes les peines du mondes à ne pas se laisser distraire par la magie de l’endroit. C’était là que la plupart des apprentis échouaient. Il se racontait que dans cette forêt maudite, si l’on perdait son objectif de vue, même l’espace d’un instant, on ne pouvait en ressortir autrement qu’en empruntant la cascade qui se trouvait en son centre. Évidemment, un autre maléfice empêchait quiconque de la trouver, et à cause de cela, personne ne vivait dans cet îlot pourtant paradisiaque. 

Zack fut satisfait de constater que les autres n’avaient pas échoué dès le début. Il était incontestablement le meilleur, mais ça n’avait aucun intérêt s’il n’y avait personne à surpasser. Des cinq, il était le premier arrivé à l’oasis, le premier au sommet, le premier à sortir de la forêt. Et pourtant il les avait attendu à chaque fois. Il sourit à Ilan. Ce dernier aussi allait réussir, c’était le plus talentueux après lui. Il s’apprêtait à les féliciter, lorsqu’un cri l’interrompit.

Tous se retournèrent vers les silhouettes que pointait Doran. Il était tout juste midi, ils s’étaient retrouvés juste à temps. Passé le temps de l’émerveillement, les apprentis se préparèrent. Les raies géantes étaient fabuleuses, mais il n’y en avait que trois. Pas assez pour tous. Ces animaux avaient des cycles extrêmement précis, impossible que leurs maîtres aient fait une telle erreur. Le test était loin d’être fini. Elles approchaient rapidement, mais à un rythme régulier. Doran recula pour prendre son élan. Il ne battait pas les autres sur le plan physique, mais s’il pouvait arriver le premier...

Creg s’élança en même temps que Doran. Si le bleuet pensait qu’il pouvait lui damer le pion... Il allait soumettre cette grosse voile, et rentrer avec sa prise. Sa trajectoire n’était pas parfaite, et il chutait un peu trop rapidement. Ce n’était pas un problème, il pouvait facilement corriger le tir avec sa voile personnelle. Mais. Le vent ? C’est vrai, il n’était pas dans le ciel parcouru constamment de bourrasque, il s’était presque élancé au niveau du sol. Il ne pouvait pas se redresser à temps. L’apprenti massif heurta de plein fouet l’aile, qui se brisa presque sous son poids, avant de rebondir dans le vide.

C’était pas beau à voir. Mais Yrvin avait toujours considéré Creg comme un idiot. C’était une perte minime, et un obstacle de moins pour lui. Doran était parvenu sans mal à calmer la raie déséquilibré par la maladresse de son rival, comme il s’y attendait. À présent, c’était deux pour trois. Ces raies-là étaient suffisamment proche. S’il échouait à soumettre la première, il pourrait toujours se rabattre sur l’autre. Zack sauta avec lui. Ilan, malin, attendait la dernière. Qu’à cela ne tienne, ils allaient se battre. L’auto-proclamé génie ne vit pas venir le dard qui lui creva l’œil. Portant les mains à sa tête, il perdit le contrôle de sa chute, atterrit sur le côté de l’aile et glissa vers le sol. Et sans rancune, ça fait partie du jeu.

Ilan était concentré, plus rien n’existait que lui et sa cible. La raie planait placidement, ignorante du tumulte qui agitait ses camarades. Il tenta de créer un lien avec elle, mais rien à faire. La distance était encore beaucoup trop grande. Il sauta au moment opportun. Ouvrant les yeux à mi-course, il sut qu’il était dans l’axe. Il arriva au beau milieu de la tête de la créature, et pausa sa paume contre sa peau asséchée sans perdre de temps. Se connectant à son esprit, il se lia à elle, faisant de la créature son compagnon pour le restant de ses jours. 

Les raies étaient des êtres très intelligents. Elles suivent une même routine toute leur vie, mais se soumettent  aisément au changement. Passer un pacte avec des animaux aussi majestueux était un privilège auquel seuls de rares élus avaient accès, au prix d’un entraînement rigoureux et d’une appétence à la magie de l’esprit. 

Ilan commanda à sa nouvelle monture de s’élever dans les cieux afin de rejoindre le navire volant. Chevauchant vers le soleil, il discernait avec difficulté les deux silhouettes qui le précédait. Il avait ressenti la mort de Creg, alors l’une d’elle devait être Doran, et l’autre appartenait très certainement à Zack. Il avait un peu de peine pour Creg et Yrvin, leur souhaitant de réussir dans leur prochaine vie. 

Maintenant qu’ils s’étaient liés à des raies, les trois vainqueurs n’étaient plus de simples apprentis. Ils étaient désormais d’authentique pirates célestes, maîtres du ciel et terreurs du désert.