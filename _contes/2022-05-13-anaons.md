---
bg: "anaons"
layout: post
title: Anaons
summary: Un équipage de recherche décide d'arrondir sa fin de mois.
source: “Creepy painting 27” par Stefan Koidl
ref: https://www.instagram.com/p/B2TacJaCBn0/
date: 2022-05-13
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---
— Captain ! J'ai un signal ! 

Christian se précipita aux côtés de Lina. Des jours qu'ils recherchaient ce bateau, allant de déconvenue en déconvenue. S'il s'agissait encore d'une fausse alerte, son officière sonar passerait un mauvais quart d'heure. Il était sur les nerfs depuis plusieurs jours, depuis qu'ils s'étaient lancés dans cette traque. Il n'avait plus espoir de secourir qui que ce soit, cela faisait des semaines maintenant que l'*Azurée* avait coulé. Un paquebot de croisière de dernière génération effectue sa première véritable sortie. Une avarie imprévisible et d'un coup, le mastodonte s'était retrouvé par des kilomètres de fond. 

Une histoire pleine d'incertitudes que les médias ressassaient continuellement, accusant tour à tour la société qui a construit le navire, les vérifications de sécurité insuffisantes et les autorités portuaires. De nombreux points restaient inexpliqués, le principal étant qu'aucun survivant n'avait été repéré, et les multiples balises de détresses dont disposait l'embarcation étaient toutes restées silencieuses. Il paraissait inconcevable que dans un tel colosse des mers, personne n'ait été capable de réchapper à la catastrophe. Ajoutez à cela des recherches faisant chou blanc pendant des semaines, malgré le passage au peigne fin des fonds marins sur la course de la croisière, et il n'en fallut pas plus pour alimenter toutes sortes de théories farfelues, la plupart impliquant un enlèvement d'extraterrestres. 

Le drame s'était produit sept semaines auparavant. Le *Bon Chance* de Christian C'hoazic ne participait pas initialement aux recherches, mais il avait été dérouté pour mettre à profit son puissant sonar dont il se servait pour cartographier les fonds marins. Il n'y avait plus de secours, mais la zone de recherches s'étendait toujours un peu plus, en quête de réponses. Le capitaine avait accepté la requête qui n'en n'était pas réellement une plein de doutes. Son employeur l'avait chargé d'une mission importante, et chaque jour qu'il passait à chasser un navire déjà fantôme divisait sa paie. Un jour de plus, avait-il décidé, et il serait déjà déficitaire. Sa réputation lui importait moins que de ne pas perdre son navire sur une expédition désastreuse. 

C'était là, avait-il décidé en s'approchant de l'écran que lui pointa son officière. La dernière chance. Si cette piste aussi s'avérait froide, ils rentreraient à Portland. Trois jours de voyage, et un salaire ridicule. Il aurait perdu suffisamment. Lina savait ce qui l'attendait si elle se trompait. Les colères du capitaine étaient légendaires parmi l'équipage, et il était particulièrement tendu dernièrement. Mais elle n'avait jamais été aussi sûre d'elle. Le point qu'elle avait repéré, à près de 6000 mètres de fond, était trop régulier pour être naturel. Plutôt, on aurait dit qu'il était encastré dans une faille naturelle, et formait un petit mont. 

— Quelle profondeur ? 

— 5742 mètres. On peut oublier les plongeurs, ils arriveront jamais à descendre aussi bas. 

— Non, en effet. Il leur faudrait un sous-marin... Du genre qu'on se trimballe. Vérifie tes scans, je ne veux pas d'erreur. Je vais dire au Bluet et Thélia de se préparer. 

— Pardon ? Vous voulez descendre, Captain ? On devrait pas plutôt avertir les autorités ? 

— Écoute, t'es nouvelle ici, alors je vais t'apprendre comment ça va se passer. Tu vas vérifier, être sûre à 200% que c'est bien l'*Azurée* qui est là-bas, et on va y aller. Parce que les types qui se pavanent sur ce genre de paquebot sont blindés. *A fortiori* si quelque chose là-dessus avait assez de valeur pour qu'un fou furieux fasse exploser la coque. Cette expédition est un échec total, ça fait 8 jours qu'on est paumés au beau milieu de que dalle, alors on va avertir les autorités. Mais avant ça, on va se monter une petite virée dans les abysses, tirer tout ce qui a de la valeur et rentabiliser ce putain de voyage ! Capiche ? 

La pauvre recrue avait les oreilles qui sifflaient. Elle ignorait qu'un humain pouvait augmenter le ton de sa voix aussi longtemps que l'avait fait le capitaine. Tentant de remettre de l'ordre dans ses pensées, elle se retourna vers son poste, et tâcha d'obtenir les images les plus nettes possibles afin de ne laisser aucun doute. Elle sentait l'adrénaline monter, car elle savait déjà qu'elle l'avait trouvé. Ce qu'ils s'apprêtaient à faire n'était sans doute pas très moral, ni même légal, mais si cela lui permettait de mettre du beurre dans les épinards elle s'en accommoderait. Ce n'était pas comme s'ils allaient faire quelque chose de mal, les morts avaient rarement besoin de leurs richesses. 

— La p'tite est sûre ? C'est une sacrée trotte qu'elle nous demande de faire. On aura de quoi faire l'aller-retour une fois, deux au grand maximum. 

— Elle le sera d'ici à ce que tu te sois préparé. Et si c'est une fausse piste, tu seras déjà en tenue pour la foutre par-dessus-bord. 

Le Bluet ricana faiblement. Il avait toujours eu du mal avec l'humour du capitaine. C'était principalement parce qu'il ne savait pas si c'était réellement de l'humour. Il pouvait se montrer extrême dans ses sanctions, mais chaque fois qu'il avait menacé de tuer quelqu'un, la situation s'était arrangée. Le jeune homme était curieux de savoir ce qu'il se passerait si quelqu'un le poussait suffisamment à bout. Il avait le physique le plus imposant de tout l'équipage et n'aurait aucun mal à passer n'importe qui, sauf peut-être Bertrand, par-dessus bord. 

Il enfila sa combinaison de plongée et descendit retrouver les autres dans la cabine principale. Dans le sous-marin, ils pouvaient porter des vêtements ordinaires, mais en cas de problème, il préférait être dans une combinaison adaptée à la plongée en eaux profondes. Non que ça changerait quoi que ce soit, si l'accident survenait par 6000 mètres de fond. Il croisa Thélia, qui avait troqué son habituelle tenue relâchée par des vêtements plus formels, et plus chauds. Il pouvait faire froid sous l'eau, mais c'était trop tard pour faire demi-tour et chercher sa petite laine. 

Trois personnes s'affairaient déjà dans l'étroite cabine autour de l'écran du sonar. Tamir, le second, discutait vivement avec Christian, son accent exotique rencontrant le ton bourru du vieil homme. Il semblait être contre cette idée, mais son supérieur ne voulait rien entendre. Il s'apprêtait à lancer un nouvel argument imparable lorsque Le Bluet posa la main sur son épaule. 

— Laisse, vieux, ça sert à rien. Si le cap'tain à une idée derrière la tête, tu peux pas lui faire changer d'avis. Ça va être rapide, tu verras. On prend ce qu'on peut en bas, un rapide aller-retour, et en deux heures on en parle plus. 

— C'est confirmé ? 

Thélia les avait ignorés pour aller se pencher sur l'écran de Lina. Elle l'observait avec attention, mimant de savoir ce qu'elle devait regarder. 

— Aucun doute possible, fanfaronna la jeune officière. Ou alors c'est un autre paquebot avec les mêmes dimensions de l'Azurée au mètre près, qui a coulé à peu près en même temps. Mais là, je ne réponds plus de rien. 

— C'est anormal, répéta Tamir. On est trop loin du tracé de la croisière, il avait rien à faire ici. Il y a des forces mauvaises à l'œuvre. 

— Tu veux bien arrêter avec tes superstitions de grand-mère et aller descendre le sub ? Tonna le capitaine. 

— Vous le regretterez, et après vous verrez que j'ai eu raison. N'écoutez pas Tamir la grand-mère, même s'il finit toujours par avoir raison... 

Ses derniers mots moururent alors qu'il passait l'écoutille pour s'acquitter de sa tâche. Il n'avait pas entièrement tort, pensait Thélia. Il y avait un réel mystère à élucider dans les circonstances du naufrage de l'*Azurée*. Mais ce n'était pas leur rôle, elle laissait ça à la marine américaine qui débarquerait sur les lieux sitôt qu'ils donneraient leur signalement. Et d'ici là qu'ils découvrent leur prélèvement, ils seraient loin. 

Elle monta dans le sub avec un rictus d'angoisse. Sans être claustrophobe, elle ne trouvait pas l'idée de passer des heures dans une boîte de conserve en eaux profondes rassurante. Au moins, elle avait Le Bluet avec elle. Il savait détendre l'atmosphère, même dans les moments les plus difficiles. Et c'était le seul qui avait de l'expérience avec les crises d'angoisse. Il s'était engagé en tant qu'ingénieur il y a des années, mais elle avait insisté pour qu'il quitte la salle des machines et devienne plongeur avec elle. Il la calmait. 

— Mince, si j'y avais pensé j'aurais pris un bouquin ! 

Exactement le genre de trait d'humour qui lui permettait d'oublier son stress, au moins temporairement. Ils descendaient en silence depuis maintenant dix minutes, en ligne droite vers leur destination. Le sonar détectait de nombreuses formes autour d'eux, sans être capable de clairement les identifier. Elle avait coupé les phares de l'appareil. Ils ne pourraient éviter la vision des corps boursoufflés, gorgés d'eau et en partie dévorés par les poissons lorsqu'ils fouilleraient le paquebot. S'ils pouvaient se l'épargner jusque-là, elle s'en porterait mieux. 

Christian rouspétait dans leur ligne de communication. La seule caméra du sub se trouvait à l'avant qui était plongé dans les ténèbres. Lui aurait aimé voir les dizaines de cadavres qui devaient les entourer. Cet homme avait toujours eu des tendances morbides et une fascination pour les cadavres, en particulier noyés. Bien qu'elle ne l'ait jamais vu commettre de crime pendant les 10 années qu'elle avait fait à son service, l'idée ne la choquait pas particulièrement. 

L'altimètre afficha 5742 mètres et le sous-marin se stabilisa. Il était temps. Thélia souffla profondément pour évacuer son stress. Et Le Bluet activa les phares. Dans un premier temps, rien. Les lances de lumières transperçaient les ténèbres, mais il n'y avait rien à voir. Puis les moteurs de l'engin le poussèrent doucement vers l'avant, et la silhouette gigantesque se dessina. Ils n'avaient vu aucun corps pour l'instant, et Thélia profita de ce répit. Son compagnon mania délicatement l'appareil pour le situer à la proue, où les lettres d'or traçant le mot Azurée étaient en partie détachées. 

— Cap'tain, je crois qu'on a trouvé notre gros X rouge. 

À la surface, Christian exultait. 

— C'est bon ça ! Plus qu'à creuser les gars ! 

Impossible de pénétrer le paquebot à bâbord, à moins d'arracher une partie de la coque, au risque de se faire découvrir. Il n'y avait qu'à espérer trouver un accès à tribord, côté sur lequel le navire était partiellement couché. Le Bluet manœuvra son sub jusqu'à le positionner à l'ombre du pont. Il explora ce côté avec appréhension, qui augmenta quand il trouva ce qu'il cherchait. Il se serait satisfait d'un passage ouvert par un éperon rocheux qui aurait empalé la coque. Il n'avait pas réclamé l'immense trou béant qui avait éventré la coque sur toute la moitié arrière. Cela ressemblait à ce qui se serait passé si le moteur s'était subitement transformé en bombe et avait sauté. 

Il sentait la respiration de Thélia s'accélérer à côté de lui, mais il n'avait aucune blague pour la calmer. Il posa simplement sa main sur son épaule fraternellement, et avança dans l'obscurité de l'épave. Il avait à peine pénétré la coque qu'ils apparurent par dizaines. Des corps, pâles dans la lueur des phares. Ils étaient tous déformés, la plupart avaient perdu leurs vêtements et une bonne partie de leur chair. Ils flottaient immobiles dans l'espace, leur absence totale de mouvement faisant penser à des astronautes à la dérive ayant accepté leur sort. Plusieurs bancs de poissons passaient d'un corps à l'autre, les secouant tandis qu'ils prélevaient de petits morceaux de chair à chaque passage. 

Il ne saurait pas naviguer dans un espace si étroit, alors il confia les commandes à Thélia, bien plus expérimentée. À la place, il prit le contrôle des bras du sub, s'en servant pour écarter les cadavres sur leur chemin. Il essayait de ne pas penser à ces gens qui avaient été humains. Il essayait d'imaginer qu'ils étaient dans la carcasse d'un porte-container transportant des mannequins drôlement réalistes. Il voulut faire part de sa trouvaille à la pilote, mais la concentration qui se lisait sur son visage l'en dissuada. 

Il se concentra lui-même sur sa tâche. Ils voulaient tous deux passer le moins de temps possible dans cet endroit lugubre, alors plus tôt il mettait la pince sur quelque chose de cher, ou de précieux, mieux ce serait. Ils étaient dans ce qui était auparavant la salle des machines et les quartiers de l'équipage. Peu de chances de trouver quoi que ce soit de valeur ici, mais il scrutait néanmoins chaque recoin révélé par les phares. Penser que tous ces corps soient probablement ceux des membres de l'équipage le fit frissonner. Il s'attarda sur l'un des morts, au corps particulièrement décomposé, en se demandant s'il était normal qu'il soit dans cet état après des semaines en mer. 

— Aaaah ! 

— Quoi, qu'est-ce qu'il y a ? 

— Là... là, il y en a un qui a bougé, qui m'a regardé ! 

— T'es qu'un con, Le Bluet ! Déconne pas maintenant ! 

— Non mais je te jure... 

— C'est la poiscaille qui a dû le retourner, sois un peu avec moi merde ! 

La peur dans la voix du Bluet ne paraissait pas simulée. Elle-même devait faire des efforts monumentaux pour ne pas se rappeler constamment qu'ils évoluaient dans une mer de cadavres. Thélia dirigea le sub vers la porte d'un couloir qui semblait se diriger vers les étages supérieurs. Ils allaient devoir faire un peu de travail de démolition, mais cette partie du navire était tellement ravagée qu'elle était certaine que leur participation passerait inaperçue. Elle ne tenait pas vraiment à s'enfoncer dans le ventre de cette bête noyée, mais elle ne pouvait pas non plus remonter les mains vides. Il lui faudrait payer les frais de psychanalyse pour évacuer son traumatisme. 

*Poc* 

— T'as touché quelque chose ? 

— Je suis sûre que non. Mais il y en a tellement, j'ai pu ne pas en voir un. Bon, je vais avoir besoin de tes-*Poc*  

*Poc* *Poc*  
*Poc*  

— .... caméra ... plus rien..... recevez ? 

— Merde, le paquebot doit faire écran, ils*Poc* doivent avoir perdu la connexion*Poc*. 

— *Poc*Attends, ça m'inquiète ces *Poc*petits bru*Poc*its. 

*Poc*  

— Tu*Poc* crois qu'on a*Poc* une fuite ? *Poc*Ce serait pas le mo*Poc*ment.  
*Poc*  
*PocPocPoc*  
*Poc*  
    — Non, on dirait que ça *Poc*vient de l'extérieur.*Poc* 

*Poc* *Poc* *Poc* *Poc*  

— Thélia... *Poc*Regarde de*Poc*hors. 

*Poc* 

&nbsp;  

&nbsp;  

Ils avaient définitivement perdu la liaison. Ça avait commencé avec la caméra qui était subitement devenue hors service, puis jusqu'aux communications audio. Lina tentait de rétablir la connexion, mais le problème ne semblait pas venir de leur côté. Christian était sorti pour évacuer la colère qui le gagnait. Ce n'était sûrement rien, mais il détestait ne pas avoir le contrôle sur les choses. Il sortit une cigarette et l'alluma, le tabac n'avait pas son pareil pour calmer ses nerfs. 

*Poc* 

Ils avaient heurté quelque chose ? Le bruit régulier des vagues s'était interrompu pour laisser place à autre chose. Comme des petits chocs répétés sur la coque. 

*Poc* 

*Poc* 

*Poc* 

 