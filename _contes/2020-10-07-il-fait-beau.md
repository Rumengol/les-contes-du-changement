---
bg: "il-fait-beau"
layout: post
title: Il fait beau
summary: Je me réveille encore d'un mauvais rêve, et des bouts de ciel pleuvent. Il fait encore beau.
source: “今日もいい天気” (Il fait encore beau) par アボガド6 (avogado6)
ref: https://www.avogado6.com/diary2019?lightbox=dataItem-k4wtpwb63
date: 2020-10-07
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---

J’ai encore rêvé que je me noyais. J’étais en classe, le prof parlait mais aucun son ne sortait de sa bouche. On aurait dit un poisson échoué qui se débattait pour retourner dans l’océan. Alors je me suis levée, mais quand j’ai voulu prendre le poisson-prof, il s’est mis à éructer des torrents de dates, de formules chimiques et mathématiques, de principes philosophiques et thermodynamiques. Le tout à rendre pour le lendemain, sous peine de redoubler. J’ai perdu pied dans le flot de connaissances que je n’arrivais pas à absorber. Ils ne m’ont pas aidé, les enfants-éponges, concentrés sur leur propre interrogation écrite. J’étais la seule débordée, je ne remplissais pas ma part du travail, et le sapiofluide avait rempli la salle de classe, noyant tout le monde.

Je recrache un peu d’eau vermeil. Mince, j’ai dû me mordre la langue. Ces rêves sont de plus en plus fréquents, c’est ennuyant. Je n’aime pas quand la fausse vie envahit mon sanctuaire du sommeil. Je suis obligée d’en amener un peu ici, mais ils ne font pas bon ménage. Maman est sortie, ou bien elle dort encore. Sinon le samedi elle serait devant ses émissions, à admirer ses amis de la télé. Je me sers un verre de lait, dans lequel je glisse un chocolat noir. J’aime bien faire ça, le lait adoucit l’amertume du chocolat.

Il fait encore beau, aujourd’hui. Il pleut des petits bouts de ciel bleu. J’enfile mon grand cardigan jaune, qui s’accorde bien à mes bottes. Je n’oublie pas mon parapluie, le transparent de quand j’étais petite. Je sors sans fermer la porte à clef, comme ça papa pourra rentrer quand il reviendra. Le ciel est d’un magnifique bleu ciel, il surveille les petits nuages qui jouent au dessus de la ville, pour ne pas qu’ils tombent, ce serait dommage.

Il fait beau, mais tout a l’air plat aujourd’hui, comme si le ciel était peint sur le plafond, et que le soleil n’était qu’une grosse lampe très puissante. C’est pour ça qu’il pleut des bouts de ciel, la peinture est encore fraîche. Il en tombe tout autour de moi, mais mon parapluie me protège. Je n’ai pas envie d’abîmer mon beau cardigan. J’ai l’impression que je vais les voir, en pleine rue, les peintres en tenue de cosmonautes surélevés par un de ces camions monte-charge. Je leur dirais bonjour, il me salueront d’une voix étouffée avant de se remettre à l’ouvrage. Le ciel n’attend pas, il ne leur reste plus beaucoup à faire avant de passer la seconde couche.

Les gens dans la rue me regardent étrangement, et je leur renvoie leur regard. Qu’ils ont l’air bêtes, avec leurs têtes tartinées de bouts de ciels. J’aimerais bien effleurer le plafond, moi aussi, mais pas avec un rouleau gorgé de peinture, avec mes doigts, laisser ma marque là haut, créer un nuage ou peut-être une étoile. Le collège est encore ouvert, le samedi. Et le bâtiment principal est très haut. Une voiture de police s’arrête à côté de moi. Un officier me demande si je suis perdue. Je lui réponds en souriant que je vais au collège, sans lui dire pour le ciel. Sinon il pourrait se rendre compte que son pare-brise est tout peinturluré et qu’il n’y voit rien.

Apparemment, ma réponse lui convient. Il offre même de m’y emmener, mais je refuse poliment. J’ai envie de marcher, sous ce beau temps. Il lance un dernier regard dubitatif à mon parapluie avant de poursuivre sa course aveugle. C’est vrai que ça fait presque un mois que les peintres manquent d’originalité avec le ciel, il fait toujours beau, avec quelques nuages. Mais aujourd’hui, je n’avais pas envie d’être tachée, tout simplement.

A l’entrée, la concierge me toise méchamment. Elle me dit que je suis en retard. Je réponds en riant que le ciel peut m’attendre encore un peu. La peinture est très fraîche, au dessus du collège. Les cosmonautes viennent de finir la zone. J’éprouve un peu de culpabilité à perturber un ciel aussi récent, mais je n’en n’aurais plus l’occasion après. Toutes les fenêtres ou presque sont recouvertes de peinture. Ça doit être très sombre à l’intérieur. Je sautille en traversant la cour, toute excitée.

Je voyais l’escalier jusqu’au ciel plus long, mais le toit n’est qu’au-dessus du cinquième étage. Ça y est, je peux sentir l’odeur chimique de la peinture. Dans ma hâte j’ai oublié de replier mon parapluie à l’intérieur, les baleines sont un peu tordues. Je m’excuse auprès d’elles et je les laisse se reposer dans la cage d’escalier. Elles l’ont bien mérité, pour m’avoir protégée de la pluie de ciel. Même en sautant, toute tendue, je n’arrive pas à toucher le ciel. Il est pourtant si proche. Le rebord pourra me faire gagner au moins un mètre. Je grimpe dessus prudemment, il est très haut quand même ! Je saute à nouveau, et je crois que j’ai effleuré quelque chose. Je suis si proche d’atteindre le ciel !

Oups. Je crois bien que j’ai glissé.

Tant mieux.
