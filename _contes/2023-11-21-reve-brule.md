---
bg: "reve-brule"
layout: post
title: Rêve Brûlé
summary: Les rêves de Caroline sont particulièrement chauds.
source: “Scorch” par Remorphed
ref: https://www.reddit.com/r/ImaginaryMindscapes/comments/17k6gi9/scorch/
date: 2023-11-21
categories: posts
tags: []
type: Conte
author: Rumengol
note: "Insert 'Je sais pas où je vais' meme."
---
Je fais souvent ce rêve. Je suis sur un chemin de forêt. À gauche, un muret qui m'arrive à l'épaule est là pour m'empêcher de basculer dans le vide de plusieurs mètres. À droite, des branches mortes sortent du couvert des arbres comme des dizaines de mains avides. Le vent les fait frémir, je m'attends à tout moment à ce que les corps à qui elles appartiennent surgissent des fourrés pour m'attaquer. Mais ils ne viennent jamais, ce n'est que du bois.

C'est toujours de nuit. La lune est haut dans le ciel et je ne porte qu'une veste fine, mais je n'ai pas froid. Les étoiles, du peu que j'en vois, ne sont pas à leur bonne place. Pourtant, je reconnais certaines constellations. Je ne suis peut-être pas dans le bon hémisphère. Alors je marche tout droit, sans me retourner. La forêt est trop dense, sans le moindre chemin qui s'y enfonce, et je ne suis pas suicidaire.

Après un moment, tout prend feu. Je ne sais jamais quand ça va arriver. L'instant d'avant, le reflet de la lune est la seule source de lumière, et celui d'après le monde est englouti dans un brasier. Le bois en contrebas est le premier à brûler, il s'embrase et explose dans un milliers d'éclats. Des oiseaux s'envolent, paniqués. Certains me rentrent dedans. Je sens, dans mon rêve, leurs serres se planter dans ma chair et leur bec me picorer le crâne, puis ils disparaissent dans le ciel.

Les flammes n'ont rien de naturel. Elles s'élèvent jusqu'à moi dans un torrent ardent. La chaleur vaporise toute l'eau de mon corps et fait s'embraser spontanément les brindilles tombées sur le chemin. Les arbres à ma droite tentent de résister autant que possible, mais cèdent à leur tour au joug implacable de l'incendie. Je me retrouve alors prisonnière d'une prison incandescente, brûlant de tous côtés. Mon corps se calcine de l'intérieur, et je deviens une torche humaine.

Puis je me réveille, dégoulinante de sueur. Plus que ce que je pensais le corps humain capable. Je me retrouve à étendre mes draps presque quotidiennement, pour ne pas me coucher dans une piscine de sueur. Ces nuits-là, c'est impossible de me rendormir. Le cauchemar est trop intense dans ma mémoire. Je peux encore sentir la morsure des flammes sur ma peau, la déchirer et y ouvrir des cloques qui éclatent aussitôt. J'ai pris l'habitude de monter une bouteille d'eau sur ma table de chevet, mais je suis encore assoiffée après l'avoir vidée. Alors je vais prendre une douche glaciale, et je bois l'eau autant que je me nettoie.

J'en ai parlé à ma psy, et on a tenté ensemble de trouver la signification de ce rêve récurrent. Elle a avancé des théories de conflit interne, d'une situation qui me donnerait la sensation d'être prise entre deux feux sans le moindre espoir d'y échapper. Un traumatisme d'enfance, peut-être. Je comprends son interprétation, mais elle ne me correspond pas. J'ai eu une enfance heureuse, avec des parents aimants qui m'ont toujours soutenue dans mes projets de vie. Je me plais dans mon travail, même s'il est éprouvant et rude avec ma santé mentale, mais rien qui se rapproche de ce qu'elle décrit.

En désespoir de cause, on a regardé dans des traducteurs oniriques. Elle possède quelques livres plus fiables que ce qu'on peut trouver en ligne, sans y croire pour autant. De ses propres mots, c'est souvent « du bullshit qu'on sert aux mémères abruties par l'astrologie ». Elle est très directe avec moi, c'est pour ça que je l'aime bien. Et elle a eu raison, son analyse était bien plus pertinente que tout ce qu'on a trouvé dans les bouquins, et c'était déjà à côté de la plaque. Au bout du compte, elle m'a prescrit des somnifères garantis sans rêves. C'est mieux que rien.

Finalement, les somnifères ne m'empêchent pas de cauchemarder. C'est pareil que rien en fait. Et le rythme s'intensifie. Toujours le même rêve, je finis par le connaître par cœur. Ça m'a pris le temps, mais je parviens enfin à en prendre contrôle. Je ne bouge plus comme un automate, mais je suis maîtresse de mes mouvements. Non pas que ce soit d'une grande aide, ce chemin est sans fin. J'ai beau faire demi-tour, marche arrière, m'enfoncer dans la forêt malgré tout mon être qui essaie de m'en empêcher, rien n'y fait. Le feu me rattrape toujours.

Je me suis renseignée. Lorsque l'on prend contrôle de ses rêves, que l'on devient lucide, on est supposé être comme des dieux. Omnipotents, capables de prouesses limitées seulement pas l'imagination. Alors, j'ai essayé de repousser les flammes, d'invoquer la pluie, un ouragan, de me téléporter loin de là. Sans succès. Même conjurer un simple extincteur est en dehors de mes capacités. Tu parles d'une omnipotence.

La dernière fois, j'en ai eu marre. Je voulais que ça s'arrête le plus vite possible. Alors je me suis jetée par dessus la balustrade, plongeant tête la première vers le sol. Ça m'a fait mal. Très mal. La mort devrait être indolore et rapide, mais là ça a a duré beaucoup trop longtemps. Je me suis réveillée certes sèche, mais avec une terrible migraine qui m'a empêché de me concentrer toute la journée. Le suicide n'est même pas une option.

Je suis à nouveau dans ce rêve. Il fait nuit, et l'air doux caresse mon visage. Ça ne va pas durer, alors j'en profite. Aujourd'hui, je vais tenter une nouvelle approche. La terre n'est pas inflammable et la chaleur sera peut-être supportable, si je me couche contre la rambarde de pierre. Après ça, je n'ai pas d'autre idée. En attendant, je me promène comme si c'était une balade ordinaire, et que rien n'allait se passer.

Quelque chose est différent, cette fois. Au lieu du chemin désespérément vide, un chat est perché sur la rambarde. Il a les yeux rivés sur moi. J'ai l'impression qu'il me dévisage de ses pupilles incandescentes qui brillent dans la nuit comme le brasier qui ne tardera pas à survenir. Il ouvre la bouche, mais c'est directement à mon esprit qu'il s'adresse.

« Tout ce temps, et tu n'as pas été capable de trouver la solution. C'est navrant. »

Je m'arrête, interloquée. Est-ce qu'un chat vient de me parler par télépathie ? J'ai encore du mal à admettre que je n'ai aucun pouvoir surnaturel dans mes rêves, et voilà qu'un matou me parle. Pire encore, il me fait la leçon ! Il y a bien une solution à ce cauchemar qui hante mes nuits depuis des semaines ? Dans ce cas là, j'aimerais bien la voir.

« Ce n'est pas quelque chose que l'on peut te donner. Tu dois la chercher. La mériter. »

Je ne suis même pas surprise qu'il puisse lire dans mes pensées. Après tout, pourquoi pas ?

« C'est bien gentil de me dire ça, mais comment je suis supposée m'y prendre ? Tout ce que je tente échoue.

— Tu confonds l'énoncé du problème avec la question. Prends du recul, réfléchis, fais toi aider. Réellement. Ou transmet ce rêve à quelqu'un de plus compétent.

— Refiler la patate chaude à quelqu'un d'autre ? Si on peut faire ça, je suis preneuse.

— C'est pourtant simple, il te suffit de cesser de vivre. »

Ce sale matou se moque de moi ! Je suis plutôt une amie des animaux en règle générale, mais là je vais faire une exception. Je m'avance vers ces yeux moqueurs quand l'explosion survient, projetant immédiatement la gigantesque colonne de flammes vers le ciel. Le chat se fait vaporiser par le souffle tandis que je me jette contre la rambarde.

Le feu ravage tout. J'entends les arbres se briser sous l'assaut des flammes, je sens l'odeur du bois en feu et surtout je ressens la chaleur. Il fait si chaud que je crois mourir plusieurs fois. Je suis plus résiliente dans ce monde onirique que dans la réalité, sinon je serais déjà morte pour de vrai. Ma veste prend feu spontanément alors je la jette au loin, en me recroquevillant toujours plus. Pas de torche humaine, la fumée ne m'asphyxie pas. Aussi étonnant que ça puisse paraître, je suis toujours en vie. Dans le rêve.

À l'odeur du bois brûlé succède celle des cendres. La température reste infernale, mais elle baisse notablement. Je peux me remettre à respirer normalement, même si j'ai la sensation d'inhaler de la poussière à chaque inspiration. Je me redresse péniblement pour voir le mur de flammes s'éloigner, laissant dans son sillage une désolation lunaire. Combien de temps je suis restée allongée comme ça, exactement ? Des heures ? Ce rêve n'a jamais duré aussi longtemps, mais ma conscience s'éloigne déjà.

Juste avant de sortir des bras de morphée, je distingue au loin la silhouette d'un hélicoptère, mais ses couleurs me sont inconnues et je suis incapable de lire ce qui est écrit sur la carlingue. Les lettres sont nettes, mais dans une langue que je ne connais pas. Je n'ai pas le temps de plus me questionner, car je me réveille, dans une chambre qui ne sent pour une fois pas la transpiration mais l'odeur acre de la cendre encore chaude.