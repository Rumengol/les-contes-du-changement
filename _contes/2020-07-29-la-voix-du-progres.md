---
bg: "la-voix-du-progres"
layout: post
title: La voix du progrès
summary: Le destin obéit à la mélodie d'une petite fille.
source: “Civilization”, par WLOP
ref: https://www.deviantart.com/wlop/art/Civilization2-802432549
date: 2020-01-29
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---

Sous son archet se jouait la mélodie des civilisations. D'un arpège naissait un monument droit et cylindrique que le vibrato faisait pencher irrémédiablement, avant qu'une trille ne le fasse s'effondrer. Chaque fois que sa musique érigeait une nouvelle fantaisie architecturale, des hommes la reproduisaient quelque part dans le monde. Ils les chérissaient sûrement, ignorant probablement que leurs créations tant estimées n'étaient que fleurs éphémères sur le violon de leur monde. Éventuellement, les monuments déchus surgiront ici, dans son environnement de pâle lumière dans lequel elle est si seule.

Démiurge désabusée, elle n'a plus souvenir de ces petits êtres qui couraient autrefois le long des cordes, comme pour tenter de la saluer. Sa fantaisie se perd, ne s'érigent plus que des blocs de métal et de verre qui viennent, de tierce en tierce, peupler la table d'harmonie surchargée. Quand certains apparaissent dans la caisse de résonance et perturbent le son, elle les efface d'un violent glissando. Elle n'en veut pas. Elle se demande parfois qui crée réellement, qui joue et se joue de qui. Le violon ou elle ? Les merveilles d'antan étaient-elles les fruits de son esprit ou ceux de la mélodie éternelle qu'elle s'acharne à varier depuis le premier instant de son existence ?

Lors d'un enchaînement de pizzicati déchaînés, la cathédrale prend feu. Elle se remet aussitôt à jouer pianissimo pour apaiser la braise. Elle ne l'a pas voulu, elle aimait cette relique fragile du temps où elle s'aimait et semait des merveilles. Ce n'est pas le premier incendie, mais c'est le dernier. Plus rien n'existe de son âge d'or. Furieuse, elle enchaîne les piano et forte subito. D'autres bâtiments apparaissent derrière elle, encore fumants de leur destruction récente.

Ses idées fanent, elle se répète beaucoup. Cette tour, identique à la précédente, à peine plus haute d'un ton. Elle fredonne un air, accompagnant la mélodie de sons nouveaux, jouant avec parfois, éveillant des accords imparfaits qui élèvent dans le ciel des nouvelles folies. Elle redevient éclatante, un second âge de création et de découvertes. Ses derniers monuments délaissent le violon surchargé pour peupler l'air, se posant sur les portées de sa voix. Les petits êtres réapparaissent, sur son archet cette fois, l'admirant autant qu'elle les admire.

Oubliés le bois et la pierre des origines, son violon est désormais d'acier et d'étoiles, la pâle lumière de son domaine devenue lactescente rayonne avec elle. Les secondes et les secondes filent ensemble, elle vit un temps à vitesse réelle, figeant le temps d'un point d'orgue, avant de repartir allegretto. Ce monde, ces créations viennent de son essence, elle en est sûre maintenant. Elle tourne sur elle-même, admirant les stations plus extravagantes les unes que les autres qui flottent paisiblement dans le flot de ses notes. Ultimement, elle le sait, la lassitude reviendra, alors peut-être même l'archet ne volera plus sur les cordes. Mais à cet instant, le monde resplendit de son imagination renouvelée.
