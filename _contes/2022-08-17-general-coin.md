---
bg: "general-coin"
layout: post
title: Général Coin
summary: Le grand général de l'armée des canards défend ses hommes.
source: “General” par Andrew Kuzinskiy
ref: https://www.instagram.com/p/CT3JLzPKzKV/
date: 2022-08-17
categories: posts
tags: ["Le monde d'Alkénor"]
type: Conte
author: Rumengol
lang: fr
---
« Tu ne devrais pas t'en servir comme ça. 

— Mais pourquoi ? Général Coin est très fier de la tâche qui lui a été confiée ! 

— Écoute, ma chérie, c'est... ils ne servent pas à ça. Général Coin est là pour te défendre contre les méchants qui voudraient te faire du mal. 

— Mais oui, c'est pour ça qu'il lui faut une armée ! Général Coin ne peut pas être un général s'il n'a pas d'armée. 

— Tu parles des... canetons ? 

— Ne manque pas de respect aux recrues ! Il y a les soldats Pouic, Donald et Mac Nug, le sergent Quackie et le lieutenant Canard, le bras droit de Général Coin ! 

— Je... très bien, fait comme tu veux. Je vais juste t'en faire un autre.» 

La mère d'Ophélie s'éloigna en secouant la tête de dépit. Elle ne pouvait faire entendre raison à sa fille lorsque celle-ci se mettait quelque chose en tête. En revanche, elle allait avoir une petite discussion avec l'auteur de la bouée à tête de canard et au regard méchant. La petite fille se désintéressa de la discussion pour se retourner vers le squelette réanimé menant sa petite troupe palmipède. 

Il avait fière allure, dans son armure dévorée par la rouille et les siècles. Il brandissait une lance au bois massif, maintes fois remplacées, mais à la tête intacte. Il avait été un grand guerrier en son temps, un général en effet, servant le roi d'un empire maintenant oublié. Il avait connu bien des batailles, comme en témoignait les lacérations qui parsemaient sa protection, et sans doute autant de victoires. Jusqu'à cet ultime affrontement où il avait connu sa fin, au prix d'une lutte acharnée où sa lance pris des dizaines de vies. Cet illustre combattant n'avait pas eu l'honneur de voir son nom inscrit dans les grands livres de l'Histoire, mais il lui a été offert une seconde chance, affublé d'une bouée en forme de canard. 

Général Coin se retourna vers sa jeune maîtresse et hocha la tête. La plupart de ses souvenirs avaient été perdus dans les méandres du temps, seul l'essentiel, ses capacités martiales, subsistaient. Son esprit était naïf comme celui d'un enfant, et bien qu'il ne soit pas capable d'avoir conscience de ses actes, il ressentait un certain orgueil à barboter dans cette mare devant cinq canetons. 

Ophélie joua avec son squelette toute l'après-midi avant de rentrer à contrecœur vers le petit hameau. Ses parents lui avaient suffisamment décrits les horreurs qui se tapissaient dans l'ombre et ne sortaient qu'à la lueur de la une. Général Coin ne pouvait la protéger que des paysans et maraudeurs vindicatifs, pas d'êtres défiants la nature elle-même. Lorsqu'elle se leva, il fit mine de sortir de l'eau pour la suivre mais elle l'arrêta. 

« Non, reste là, tu dois veilleur sur ton armée. Ça va aller, la maison n'est pas loin.» 

Le revenant hésita. Il devait obéir sans condition aux ordres de sa maîtresses, mais celui-ci venait en directe contradiction avec la raison pour laquelle il avait été ramené d'entre les morts. Il se détendit finalement en apercevant la silhouette encapuchonnée de Cyriax sur le toit de la bâtisse la plus proche. Quelque chose venu de son passé méprisait l'assassin réanimé et ses dagues courbes, mais il avait reçu les mêmes ordres que Général Coin et serait amplement capable de protéger la petite fille, si un quelconque danger s'approchait. Le squelette hocha la tête et Ophélie lui fit un sourire auquel il manquait une dent avant de courir vers son foyer. 

Il se retourna vers les canetons, qui le regardaient de leurs billes noires innocentes. Le soleil se couchait rapidement, il lui fallait trouver un abri pour ses protégés. Déjà, les joncs bordant la marre frémissaient dangereusement. La menace n'était pas pour lui, aux yeux des créatures de la nuit il était leur semblable. Il n'allait pas pour autant les laisser faire. Il empoigna sa lance et adopta une posture de combat, pointe en avant. Juste derrière lui, le lieutenant Canard était la plus alerte des petites boules jaunes. De son lourd gant, son général lui tapota délicatement la tête pour le rassurer et il laissa échapper un caquètement discret. 

Au bout de quelques minutes, Général Coin trouva ce qu'il cherchait : un rocher plat couvrant un trou rempli d'eau. Il fit signe aux canetons d'entrer dedans. Ils s'exécutèrent tous à l'exception du soldat Pouic, restant bravement monter la garde à la seule entrée de leur refuge. Le squelette essaya de le faire reculer, en vain. Le petit canard refusait de céder à la peur. Le voile de la nuit tombait à présent, et il n'avait plus le temps de convaincre son subordonné. Le général céda alors, le désignant comme garde des autres et lui promettant une promotion s'ils s'en sortaient. 

La première créature ne se fit pas attendre. Flairant sans doute un repas facile, une panthère noire se jeta sur l'abri, ignorant complètement le squelette. Cette erreur s'avéra fatale lorsque la pointe s'enfonça directement dans son crâne, la tuant sur le coup. La nuit commençait doucement, si le premier agresseur était un simple animal, quoique loin de chez lui. Peut-être allait-elle moins terrible que prévu ? 

Il regretta rapidement ces pensées, alors qu'un autre grondement s'éleva sur sa gauche. Et d'autres un peu plus loin. Il se battit toute la nuit contre des aberrations que l'esprit humain pouvait à peine concevoir, ne ressentant pas la douleur des griffes qui s'enfonçaient dans son ses os. Les caquètements paniqués étaient à peine audibles par-dessus la clameur des combats, mais ils flottaient toujours à la lisière de son ouïe, comme pour lui rappeler ce pour quoi il se battait. Sans faiblir, il repoussa des créatures toujours plus immondes, même lorsque son bras gauche lui fut arraché. Il ne flancha même pas lorsque la bouée canard se fit décapiter, l'attaque laissant une nouvelle marque sur son plastron rouillé. 

L'aube pointa finalement au-delà des montagnes, et les monstres refluèrent. Avec eux, la clameur de la nuit, devenue torpeur matinale. Général Coin resta un moment immobile, sur le qui-vive, jusqu'à sentir une petite masse pousser sa jambière. Soldat Pouic était sorti de sa cachette, et à sa suite ses frères le suivaient, jetant des regards inquiets aux alentours. Le squelette chercha d'abord son bras dans l'eau devenue poisseuse et tenta de le remettre à sa place sans succès. Il cala alors sa lance entre ses doigts crispés et passa le tout par-dessus son épaule. 

Le petit canard avait amplement mérité sa promotion. Il allait désormais être caporal Canard, sinon sergent. Il le prit dans sa main libre et se dirigea vers la maison, suivi de ses fidèles canetons ayant repris du poil de la bête. Ils avaient du mal à avancer dans son sillage pourpre, alors il s'arrêtait fréquemment pour leur laisser le temps de le dépasser, ce qu'ils se refusaient à faire. 

Ophélie sortit de sa maison aux premières lueurs du jour. Elle courut vers là où elle avait laissé Général Coin et son armée la veille, pour le voir marcher vers elle, un bras manquant et dans sa main Pouic, qui bombait fièrement le torse. Derrière lui, ses quatre frères d'arme caquetaient joyeusement. La nuit avait visiblement été mouvementée pour eux, mais plus que le bras absent et la bouée dégonflée qui pendait misérablement à sa taille, c'était un autre détail qui attira l'attention de la petite fille. 

« Maman, Général Coin est tout rouge ! » 

 
