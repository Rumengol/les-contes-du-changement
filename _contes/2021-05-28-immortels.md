---
bg: "immortels"
layout: post
title: Immortels
summary: Un couple aimerait en finir avec la vie dans un monde que la mort a renié.
source: “Constancy” par Dmitry Solodovnikov
ref: https://www.artstation.com/artwork/bKJ4VE
date: 2021-05-28
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---

“José, ils se brûlent dans notre rue !”

Le cadavre en putréfaction allongé dans le lit à côté de la vieille femme se tourna péniblement et tenta d’attraper une boîte de cachets, qui se renversa sur la table de nuit. Sa main décharnée tremblante, il serra fort le  médicament et le jeta dans sa bouche édentée. Après quelques minutes, il se sentait assez fort pour parler.

“Et qu’est... ce peut nous... foutre ?

&mdash; Mon José, tu crois pas qu’il serait temps, pour nous ? On pourrait leur demander un jerrycan et une allumette.

&mdash; N...n...non ! *kof* veut mou...rir... dans... mlit.”

Mais la mort fatiguée de son œuvre sinistre, c’en était allée, sans emporter ses victimes avec elle. Depuis des décennies, plus personne ne mourrait. Au début, le monde avait cru à une bénédiction, lorsque des gens renversés par des camions ou abattus sur le champs de bataille se relevaient. Jusqu’à ce qu’il réalise que les nuques brisées ne se ressoudaient pas, et que les organes déchirés par les balles le restaient.

Au final, peu avait changé. Il y avait la même souffrance, les mêmes peines, on ne mourrait simplement plus. Très vite, la vie avait perdu de sa superbe. La perspective de la fin était pour beaucoup la récompense d’une vie de calvaire, une libération. Désormais, seule existait leur prison de chair.

D’autres s’en sont réjouis, confondant sans doute vie et jouvence éternelle. Au grand âge, les corps lâchaient, littéralement dévorés par la maladie. Mais ils ne s’arrêtaient plus. La population vieillissante avait besoin de toujours plus de soins, tandis que le goût de la vie se perdait. Les antidouleurs devinrent une ressource prisée, cœur de la recherche. Mais même la chimie peinait à combattre l’œuvre d’un temps désaxé.

Il paraissait impossible que les corps si faibles des humains ne soient plus capable de faillir. Toutes sortes d’expériences émergèrent dans un deuxième temps. Une tête séparée de son corps ne pouvait survivre, à l’évidence. La pratique démontra que si. Fous de douleurs, les individus décapités n’étaient plus capables de paroles, seulement de pleurs.

Dans ce cas, il suffisait de détruire le cerveau, c’était la seule explication logique. Là encore, la réalité défia toute logique. Incapable d’expression, peut-être de sensation, les bouillies de cervelles vivaient encore. On essaya ensuite le feu, l’acide, tout ce qui pouvait faire disparaître des corps, avec des résultats très mitigés.

L’étape suivante fut celle de l’effondrement. Miraculeusement, les pays s’étaient maintenus jusque là, mais ils étaient devenus moins durables que leurs citoyens. La mort devint un business, service rare réservé aux plus fortunés. Les plus pauvres devaient trouver leur propre méthode pour quitter ce monde, espérant réussir avant d’être trop rongés par la maladie pour pouvoir agir.

José et Gilda n’avaient pas eu cette chance. Déjà vieux avant d’être immortels, ils attendaient paisiblement une mort qui se refusa à eux. Le vieil homme, souffrant le martyre, n’était plus capable de se lever de son lit et pourrissait lentement, peu à peu dévoré par des larves qu’il sentait vivre en lui. Lorsqu’il était encore capable de plaisanter, il murmurait à sa femme que c’était la première fois de sa vie qu’il voyait un homme donner la vie.

Gilda s’en sortait à peine mieux. Elle était capable de se lever quelques instants par jours, trouvant dans de rares occasions la force de traîner sa carcasse jusqu’à la cuisine désertée pour se faire une tisane. Il ne lui restait que ça, de l’eau croupie et des sachets d’herbes. Tout le reste avait été bu, volé ou était parti en poussière.

La vieille femme retourna s’allonger auprès de son mari, qui avait enfoui sa tête dans les oreillers moisis pour tenter d’échapper au tumulte de la foule. En regardant son corps dégoulinant de pus, elle s’estima heureuse d’avoir perdu son sens de l’odorat depuis longtemps.

Leurs chats vinrent se lover contre elle. Les animaux n’étaient pas épargnés par leur triste sort, et les pauvres minous se décomposaient vifs. Leurs os étaient par endroit exposés à l’air libres, et pourtant ils semblaient aussi à l’aise que de leur vivant &mdash; lorsqu’ils étaient en pleine santé. Gilda les enviait, de ne pas subir aussi péniblement le poids des années. Bientôt, elle aussi serait comme José, les jambes coupées par la maladie, incapable d’esquisser le moindre mouvement.

Elle n’avait pas la force de caractère de l’homme de sa vie. Elle ne supportait pas de le voir aussi misérable, mais c’était son choix. Le sien était de partir tant qu’elle était encore capable de le faire. Elle avait pris sa décision. Dégageant le coussin, elle embrassa son mari, qui lui rendit son baiser. Elle lu dans ses yeux vides qu’il avait compris, et qu’il n’allait pas tenter de l’en empêcher.

En retournant près de la fenêtre, elle fut soulagée de voir que la procession flamboyante n’avait pas encore dépassé l’appartement. Elle n’avait pas la force suffisante pour escalader le cadre de la fenêtre, aussi lança-t-elle son corps en avant, laissant son poids et la gravité l’emmener au sol.

Elle s’était brisé plusieurs os. Peut-être tous, en fait. Elle était incapable de se relever, tout juste pouvait-elle gargouiller quelques mots. Deux silhouettes enflammées, hurlantes et tordues de douleur s’approchèrent d’elle. L’un des hommes réussi, entre deux cris, à lui dire des mots qu’elle n’entendit pas.

“Brûlez... moi.”

Elle sentit l’essence versée sur son corps s’enflammer aussitôt. Elle sentit avec horreur la chaleur dévorer chaque grain de sa peau. Elle sentit les flammes s’infiltrer dans son corps en creusant des galeries infernales. Elle sentit la douleur avoir peu à peu raison d’elle, menaçant d’emporter son esprit avant son corps. Elle sentit, enfin, que de cette façon elle serait libre, qu’elle n’avait qu’à scarifier son esprit jusqu’à la démence pour s’affranchir de sa chair, même si son corps restait en vie.

Elle ne sentit plus rien.