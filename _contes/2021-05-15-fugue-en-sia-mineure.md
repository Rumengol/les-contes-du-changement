---
bg: "fugue-en-sia-mineure"
layout: post
title: Fugue en Sia mineure
summary: Un simple conte de la fin d'un seul temps.
source: “Lost Dream” par Alena Aenami
ref: https://www.artstation.com/artwork/nY5blK
date: 2021-05-15
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---
Le ciel semblait se déchirer à l’approche de la tempête. Les couleurs incendiaires du crépuscule fusionnaient avec les teintes profondes de la nuit pour peindre un tableau extraordinaire. Tous les vents du monde s’étaient donné rendez-vous à ce point exact, se lançant dans la lutte acharnée du jour contre le soir. Chaque choc faisait naître un éclair qui zébrait subrepticement le champs de bataille céleste, illuminant l’espace d’une seconde les nuages piégés dans la tourmente. Certains foudroyaient les  impuretés arrachées au sol qui osaient souiller le majestueux espace. 

La température baissait un peu plus chaque seconde, toute la chaleur était drainée, impuissante, vers le cœur du maelström. Aucun des titans ne paraissaient avoir l’avantage sur l’autre, et ni les puissants mistrals ni les terrifiants éclairs n’étaient à même de faire chanceler ces forces inamovibles. Au contraire, les impacts les faisaient tourbillonner toujours plus vite, ravageant le sol désespéré. Les tonnerres successifs en étaient réduits à de faibles bruits de fond, à peine audible par dessus le fracas des cyclones.

Le cataclysme se rapprochait de Sia, qui lui tournait le dos. Rendue presque sourde, elle savourait cette quiété. Enfin, elle pouvait penser sans être distraite. Les vents violents qui souhaitaient par tous les moyens la faire chuter avaient masqué ses yeux d’un rideau de larme dans lequel elle pouvait se contempler. Ou plutôt, contempler ce qu’il restait d’elle. Pas grand chose. 

Il n’y avait plus la fille, d’un père lâche et d’une mère trop jeune. Rien qui sorte de l’ordinaire, dans un village perdu en bordure de route. Elle avait grandi sans l’amour de son seul parent, priant chaque soir de pouvoir quitter la maison délabrée que sa pauvre génitrice n’était pas en mesure d’entretenir. La fille était définitivement partie il y a des années, lors du jet de bouteille ou de la brûlure de cigarette de trop. Elle n’était jamais revenue, même lors des rares moments où celle qui l’avait mise au monde tentait de se montrer charmante. Même une glace dans les mains, les cicatrices ne se disparaissaient pas.

Il n’y avait plus l’amie, non pas qu’elle ai existé longtemps. La seule qui aurait pu la considérer comme telle avait déménagé alors qu’elle était encore une petite fille. Non pas qu’elle puisse lui en vouloir, il n’y avait rien pour qui que ce soit dans ce patelin. Depuis, elle était restée seule, une proie évidente pour les “jeux d’enfants” les plus cruels. Avec la bénédiction des adultes, aveugles à ses souffrances. 

Il n’y avait plus l’enfant, ils y avaient veillé. Elle ne voulait pas y penser.

Il n’y avait plus l’artiste, depuis que le piano avait été retiré de la salle de musique. Un professeur lui avait dit un jour qu’elle avait du talent. Il lui permettait de s’y réfugier lorsqu’elle avait trop peur de rentrer chez elle le soir. Mais lui aussi était parti un jour, et la semaine suivante la salle fut condamnée, l’instrument revendu ou brûlé, sa mélodie et son talent emmurés dans la salle morte.

Il n’y avait plus la rêveuse, son lecteur MP3 reposait sur l’étagère d’une autre. Ses desseins d’avenir avaient étés déchirés et foulés du pied tellement de fois qu’ils avaient perdu leur forme. Elle ne voyait plus de silhouettes enchantées dans les nuages, tout juste pouvait elle imaginer la haine qu’ils se vouaient lorsqu’ils se précipitaient les uns vers les autres.

Sia réalisait qu’il n’y avait plus rien en elle. Sia n’était qu’un corps, une coquille vide désertée de son âme. Elle se demanda si ce corps manquerait à quelqu’un. Probablement que non. Ils n’avaient même pas essayé de la chercher. Ç’aurait été facile de la retrouver, pourtant. Elle ne s’était simplement pas arrêtée sur la route qui rentrait chez elle. Elle avait dépassé la bicoque branlante, puis étaient sortie du village sans se cacher. N’importe qui aurait pu l’arrêter, lui dire qu’il était encore trop tôt pour renoncer. Mais personne ne l’avait fait.

Assise au bord du pont, elle avait attendu. Sa mère affolée aurait pu venir la chercher, fondre en larme ou plus certainement la frapper. Puis elles seraient rentrées, et la vie aurait repris comme avant. A la place, elle était frissonnante, claquant des dents dans un crépuscule aux allure de peinture.

Elle avisa le tag qu’un casse cou s’était risqué à inscrire sur le côté du pont. “Sommes-nous seuls ensemble ? ?”. En souriant, Sia songea qu’elle pouvait, à cet instant précis, être seule avec cet inconnu qui posait des questions avec deux points d’interrogation. Pour la première fois depuis longtemps, elle se sentit moins seule. Elle abandonna son corps à l’ouragan avec cette ultime pensée de réconfort.