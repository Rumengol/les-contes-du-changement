---
bg: "les-spheres-d-oblogga"
layout: post
title: Les sphères d'Oblogga
summary: Les habitants des deux sphères d'Oblogga se rendent à l'endroit où un plongeur de la surface a été aperçu.
source: “Deep Terror” par バシウス (Bacius9)
ref: https://www.pixiv.net/en/artworks/52406511
date: 2020-10-04
categories: posts
tags: ["Les Sphères d'Oblogga"]
type: Conte
author: Rumengol
lang: fr
---

Les deux sphères d'Oblogga étaient en effervescence. Un homme de la surface venait d'être repéré par les sentinelles. Llonari n'était pas encore né lorsque le précédent était descendu aussi profondément dans les abysses. La fête qui avait suivi cet évènement avait été si tonitruante que ses échos étaient parvenus jusqu'à la grande sphère d'Oppollln. Certains scientifiques s'attendaient à ce que d'autres viennent explorer l'Étendue peu de temps après, mais il fallut attendre plus de deux cycles benthiques avant d'en revoir un.

La population d'Oblogga sortait massivement de ses coques, curieux de voir ce visiteur. Llonari retrouva ses amis au centre de la petite sphère. Parmi eux, seul Rhellh avait assisté à la première rencontre, mais il n'en avait que peu de souvenirs. En se dépêchant, ils pouvaient être aux premières loges. Leur petite taille leur permit de se frayer un passage à travers la foule squameuse. Veillant à ne pas traverser la fine membrane qui séparait la sphère de l'Étendue, ils pouvaient se régaler de la sensation d'euphorie provoquée par le déplacement rapide de la sphère. Les sphériers n'usaient de leurs remarquables compétences que très rarement, lorsque la situation l'exigeait. Il était préférable sinon de les laisser réaliser leur cycle naturel.

Les conversations allaient bon train, chacun espérant qu'Oblogga aurait la chance d'être la première à rejoindre le surfacien, même si les deux sphères ne comptaient pas parmi les plus rapides. Les plus optimistes déchantèrent très vite : la sphère de Strke venait de revendiquer le droit du contact. Rien de très surprenant, cette sphère était de loin la plus rapide de cette partie de l'Étendue, grâce à sa petite taille et ses sphériers hors pair. Au moins les Obloggs auraient-ils une meilleure vue.

Une centaine de pulsations plus tard, ils repérèrent l'éclat de lumière qui tranchait les ténèbres. Avec une habileté résultant de cycles d'entraînements, les sphériers positionnèrent Oblogga juste en dessous du plongeur, garantissant le spectacle pour tous. Même avec sa puissante lampe, le surfacien ne pouvait apercevoir la dizaine de sphères qui l'entourait. Llonari repéra les trois sphères d'Ahack, les sphères jumelles d'Éhtivl, ainsi que d'autres sphères uniques qu'il ne parvenait pas à reconnaître. Tous ceux qui pouvaient venir étaient là, alors Strke sortit des ombres devant l'homme qui balayait l'obscurité de son faisceau.

Ça commençait. Quelques rires fusèrent, amusés par le ridicule de la situation. La sphère de Strke gronda sourdement, et des ondes se formèrent autour d'elle. Le plongeur se figea, à l'affût. C'est là que la membrane de la sphère s'illumina lentement, dévoilant au pauvre surfacien l'étendue de son pétrin. Paniqué, il tenta de se débattre, et même de lancer sa lampe au loin dans un geste désespéré. Elle parcourut quelques mètres avant de couler mollement. Avait-il essayé de créer une distraction ? C'était tordant. Constatant la futilité de son geste, il entreprit de fuir, nageant si lentement que c'en était pathétique. En un instant, Strke était sur lui, et refermait ses trois mâchoires sur l'infortuné visiteur.

Des explosions de joie résonnèrent dans les sphères d'Oblogga. La chasse aux surfaciens était facile, mais si rare que chaque occurrence donnait lieu à des moments de liesse. Leur chair étant molle et élastique, elle était laissée aux sphères, leurs habitants se délectant simplement de l'attraction. Llonari avait assisté à sa première chasse, et c'était peut-être la dernière avant plusieurs cycles. Lui et ses amis n'en n'avaient pas loupé un morceau, ils étaient même sortis à la fin pour mieux humer l'odeur du sang excitante. Ils ne pouvaient rester à l'extérieur bien longtemps car le sang de surfacien attirait les reines mûres plus sûrement que tout autre chose, les sphères ne devaient pas tarder à quitter l'endroit.

À la fois galvanisé et apaisé par ce spectacle rarissime, Llonari se dirigeait paresseusement chez lui lorsqu'une nouvelle parvint aux sphères d'Oblogga: un groupe de surfaciens avait été repéré à quelques centaines d'encablures ! Déjà, ils repartaient à la chasse. Des dizaines d'autres messages furent interceptés dans les pulsations qui suivirent. D'innombrables surfaciens étaient détectés par les sentinelles, partout dans l'Étendue. Les pensées de Llonari embrumées par le sang en demandaient plus. Un banquet gigantesque en perspective !
