---
bg: "la-disparition"
layout: post
title: La disparition
summary: Une meute de chien évolue dans un monde où les humains ont disparu du jour au lendemain.
source: “Pumpkin Field” par Giao Nguyen
ref: https://www.artstation.com/artwork/aoZmrR
date: 2023-11-12
categories: posts
tags: ["Après la Brume"]
type: Conte
author: Rumengol
---
La meute s'éveilla aux premières lueurs du jour. La rosée perlait sur la fourrure humide des canidés alors qu'ils étiraient leurs membres ankylosés par le froid. Le chef de meute s'ébroua, puis aboya deux fois, transmettant ses ordres. Ils laissèrent le temps aux deux chiots d'ouvrir leurs petits yeux poisseux, et la meute partit en chasse.

En fait de meute, c'était plutôt un groupe de chiens domestiques subitement abandonnés par leurs propriétaires. Ils provenaient du même pavillon résidentiel, un amalgame de plusieurs races qui se connaissaient de près ou de loin. Ils s'étaient aboyés dessus, mutuellement reniflés, et ils furent ce qui ressemblait le plus à une famille après la disparition de la leur. 

Un beau jour, tous les humains s'étaient simplement volatilisés. Ils s'étaient comme envolés, corps et biens, jusqu'à leur odeur pour la plupart. Après plusieurs jours à attendre patiemment le retour de leurs maître, les chiens étaient affamés. Ils n'avaient pas pris de dispositions avec les distributeurs automatiques comme ils avaient l'habitude de le faire lorsqu'ils s'absentaient de la sorte. Le ventre vide, certains se mirent à dévorer tout ce qui ressemblait à de la nourriture, renversant les tiroirs et arrachant les portes des placards. 

Dans de nombreuses habitations, on aurait dit qu'une tempête était passée. Des paquets déchiquetés jonchaient le sol, leur contenu déversé négligemment, à moitié mangés. Certains n'étaient pas comestibles, comme les pavés de lessive ou les bidons de soude qui n'ont jamais été destinés à la consommation. Au milieu de ce chaos, des chiens et des chats gisaient, victimes de l'abandon et de leur gloutonnerie débridée.

Les autres, ceux qui avaient réussi à s'échapper de leur prison de pierre, s'étaient réunis dans la rue qui traversait leur quartier. Déboussolés, ils cherchaient des traces des humains qui partageaient leur vie, ou même de n'importe lequel, en vain. Trois d'entre eux sentirent flairèrent une senteur familière et se précipitèrent à sa suite sous le regard médusé de leur camarades qui ne ressentaient rien d'autre que l'humidité d'une matinée d'hiver.

Ceux qui restaient en vie s'organisèrent en meute, leur nature profonde refaisant surface. Le gros berger allemand que son collier identifiait sans originalité comme Rex s'imposa comme le chef en gagnant un combat contre un Dobermann dont le nom n'était pas indiqué sur la chaîne qui enserrait son cou. Attristé de quitter leur foyer et effrayés face à l'inconnu, les chiens domestiques obéirent tout de même à leur instinct qui leur dictait de s'éloigner de la ville abandonnée s'ils désiraient se nourrir.

Ils écumèrent la campagne pendant des semaines, vivant de rapine, principalement de petits animaux qu'ils parvinrent à attraper. S'ils restaient à proximité des zones urbaines au début, ils comprirent bien vite qu'aucun homme ne sortirait des maisons closes pour leur venir en aide. Après avoir ravagé les quelques magasins accessibles, ils s'étaient retirés dans la forêt. Blottis les uns contre les autres, ils se réchauffaient mutuellement sous la pluie battante et les nuits froides.

Ils cheminèrent longuement jusqu'à déboucher dans une contrée marécageuse. Le sol était plus boue que terre, et leur marche s'en retrouvait grandement ralentie. A plusieurs reprises il leur fallu se mouiller pour traverser un cours d'eau sorti de son lit qui serpentait au milieu de la campagne détrempée.

Les arbres avaient perdues leurs feuilles, rendues bien trop lourdes et glacée. Elles se mêlaient à la fange propice au développement d'essaims d'insectes qui assaillaient le groupe sans relâche. Ils avaient beau tenter de les chasser, les moucherons s'étaient solidement fixés autour de leurs yeux, se repaissant des excrétions salées et pondant à proximité de cette source de nourriture improbable pour leur descendance. Incapables de s'en débarrasser, les chiens choisirent d'ignorer les parasites et de poursuivre leur route, espérant laisser derrière eux cette terre désolée.

Ils débouchèrent, trempés et fébriles, dans un champs de citrouilles. Les légumes s'étendaient à perte de vue, juteuses et bien au-delà de la maturité. Laissées sans supervision pendant des semaines, elles étaient toutes devenues massives, mais en l'absence de récolte et face aux assauts de l'humidité et des insectes elles commençaient à pourrir et à attirer encore plus de charognards. La chair orange virait au noir, dévorées par les champignons et les cancrelats.

Les chiens évitaient soigneusement les boursouflures colorées comme s'il s'agissait de bombes. Ils étaient affamés, mais ils n'en étaient pas encore réduit à manger de la chair de légume en putréfaction. La conspiration de corbeaux qui avait élu domicile sur le toit de la ferme n'avait pas ces réticences, et chaque citrouille avait son oiseau attitré qui piquait dedans à intervalles réguliers. Ils lançaient des regards soupçonneux à ces nouveaux venus qui s'invitaient sur leur territoire comme s'il leur appartenait.

Si la meute ne se risqua pas à mordre dans les citrouilles, les corbeaux étaient une autre histoire, bien plus appétissante. Lorsqu'ils passaient à proximité d'un volatile absorbé dans sa dégustation, ils se jetaient sur lui toutes griffes dehors, avide de la moindre viande sur laquelle ils pouvaient mettre la patte. Malheureusement pour eux, les corbeaux se révélèrent d'agiles adversaires. Chaque fois qu'un canidé s'approchait un peu trop près, sa proie s'envolait hors d'atteinte, disparaissant dans la brume dans un concert de croassements moqueurs auxquels répondaient des aboiements rageurs.

Les ombres voletaient au dessus de leur tête, dissimulées par l'humidité en suspension mais suffisamment éclairées pour projeter leur silhouette sur le sol, narguant la meute affamée. Les chiens, eux, pataugeaient dans la boue et glapissaient lorsque le sol meuble les faisait trébucher. Ils ne répondaient à cette humiliation que par plus de hargne aveugle. 

Leur chef avait choisi de s'approcher de la maison délabrée dont les fenêtre brisées laissaient entre la brume et le froid. Il espérait trouver une nourriture que les voraces auraient laissés, focalisés comme ils l'étaient sur les citrouilles. Il doutait cependant du bien-fondé de cette décision, car les croassements sinistres s'amplifiaient au fur et à mesure de leur approche. Bientôt, c'était des centaines de paire d'ailes qui battirent à leurs oreilles. Les limiers ne savaient plus où donner de la tête, et certains tentèrent même de la couvrir de leurs pattes pour faire cesser ce tourment infernal.

Finalement, un corbeau fit une erreur d'inattention. Il se laissa planer trop près du sol, à portée du vieux border collie qui n'était plus aussi agile que dans sa jeunesse, mais pas dépourvu de réflexe. Il assomma l'oiseau d'un coup de patte et entreprit de satisfaire son appétit. Aussitôt, une rumeur parcouru la nuée qui fondit sur le pauvre animal. Il se retrouva entièrement recouvert par une masse d'oiseaux noirs trois fois plus grande que lui. Leurs serres lui lacérait le cuir tandis qu'une myriade de becs le pinçaient jusqu'au sang, emportant à chaque fois une partie de sa chair.

Rex se porta à son secours, n'hésitant pas une seule seconde à se jeter dans la mêlée. Lui aussi se retrouva dépassé, son intervention héroïque ne faisant que déporter une partie de l'attention de la conspiration sur lui. Ses cris de rage se muèrent bien vite en glapissements d'agonie alors que son beau poil brun cédait la place à une peau sanguinolente mise à nue. Le pauvre border collie qu'il tentait de secourir était déjà mort et son tour suivrait.

Le reste de la meute s'enfuit la queue entre les jambe, dérapant sur le sol inégal et se perdant de vue dans la brume qui s'épaississait. Ils ne s'arrêtèrent qu'une fois le champs macabre hors de vue pour s'apercevoir que leur petit groupe avait été amputé de la moitié de ses membres. L'humidité ambiante masquait les odeurs, et les chiens étaient trop apeurés pour oser appeler les leurs. Tête baissée, ils quittèrent le charnier, espérant pouvoir se réunir un jour.

La meute ainsi diminuée chemina au travers de la campagne embrumée pendant toute la journée sans faire la moindre rencontre. Avec l'heure qui avançait, les caractères devenaient irascibles et les altercations fréquentes. Le Dobermann qui n'avait pas réussi à s'imposer face à Rex décida de faire cavalier seul, laissant derrière lui les restes de ses compagnons.

Lorsque les chiens épuisés et affamés se retrouvèrent face à des humains pour la première fois depuis des semaines, leur réaction ne fut pas celle qu'ils auraient eu quelques temps plus tôt. C'était une famille qui avançait au milieu de la forêt, portant de lourds sacs à dos sur leurs épaules. Ils avaient un équipement fait pour la marche, et étaient armés avec de redoutables armes à feu. Il fut un temps, le tir de pistolet en l'air les aurait fait fuir.

Mais c'était bien avant que les canidés ne sombrent dans un tel désespoir que rien n'était en mesure de les arrêter. Ils retroussèrent leurs babines, prêts à fondre sur leur proie. Ils ne voyaient plus rien que la chair fraîche acculée devant leurs yeux, n'entendaient plus rien que le sang battant dans leurs tempes. Ils se précipitèrent en avant, ne ralentissant même pas quand l'une de leur proie appela le nom de l'un d'eux, un nom venu d'une époque qu'il avait définitivement oublié.