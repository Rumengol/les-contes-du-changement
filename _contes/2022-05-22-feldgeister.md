---
bg: "feldgeister"
layout: post
title: Feldgeister
summary: La vie d'un Haffermann, de sa naissance jusqu'à sa mort.
source: “The Midday Man” par Janna Sophia
ref: https://www.deviantart.com/jannaphia/art/The-Midday-Man-701729442
date: 2022-05-22
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---
Lorsqu'il est né, le ciel était orageux. Il se trouvait dans un champ de blé, la céréale couleur d'or s'étendait à perte de vue. Des éclairs zébraient le ciel, quelques-uns frappant le sol cultivé. Sa naissance avait-elle été le fruit d'un pareil coup d'éclat ? Le blé autour paraissait en effet avoir été carbonisé. Mais il ne comprenait pas. Quel était le sens de son existence ? À quoi rimait sa soudaine apparition ? 

Il marcha longuement parmi les épis de blés, errant dans son berceau qui devint un foyer pour l'esprit nouveau-né. L'orage au-dessus de sa tête déchaînait sa fureur et couvrait la campagne d'une obscurité éclipsée par les éclairs occasionnels. La pluie tombait à la verticale dans le ciel sans vent, mais il n'en sentait pas la fraîcheur. Un chapeau à large bord prévenait les gouttes de frapper son visage et projetait une ombre sur ses yeux étincelants. 

D'autres lances de foudre frappèrent le champ, donnant naissance à deux formes fantomatiques. Des loups au corps de fumée et aux yeux de braise. Ils montraient les crocs, dessinant sur leurs gueules un sourire carnassier. Il voulut les imiter et sourit lui aussi, découvrant ses rangées de dents inégales. Les deux esprits ne semblaient pas partager sa conscience, mais il reconnaissait ses semblables. Il les caressa, provoquant des grognements de satisfaction. Puis il reprit sa route, ses nouveaux compagnons trottinant derrière lui. 

Un jour passa. L'orage cessa, et des hommes vinrent. Il les regarda errer comme il le faisait dans le champ, mais différemment. Ils n'étaient pas comme lui. Pas nés de l'orage. Ce n'était pas ici chez eux, ils venaient avec un but en tête. Ils fauchaient les blés qui l'avaient vu naître. Il ressentit de la colère à l'égard de ces inconnus qui ravageaient sa maison. Le sourire factice qu'il arborait s'inversa tandis qu'il avançait vers les hommes en train de réunir les brins abattus. À ses côtés, les loups grognèrent méchamment. 

Les paysans les ignorèrent, occupés à leur travail. Il découvrit une main griffue et attaqua la première figure qui se présenta à lui. Ses doigts crochus passèrent au travers de l'homme comme s'il n'existait pas. Son bras était bien là, il le sentait toujours, même au travers de l'individu qui ne l'avait toujours pas remarqué. De rage, il retira sa main et tenta de mordre son épaule comme les loups faisaient. Là encore, il ne rencontra aucune résistance et trébucha en avant, traversant entièrement le corps de l'impudent. Il déclencha enfin une réaction, l'homme frissonnant. Un autre se mit à en rire. 

— Eh bien, Fynn, tu deviens frileux avec l'âge ? 

L'esprit fulminait malgré l'hilarité générale. Les loups n'avaient pas eu plus de succès, alors il les appela et ils s'éloignèrent, incapable d'en supporter plus. Lorsque le ciel s'assombrit, les hommes repartirent, et ils furent de nouveau seuls. Il s'aventura jusqu'à l'extrémité de l'immense champ, sans quitter le confort du blé. Il le pouvait, bien sûr. Mais il n'en avait pas envie. Les loups non plus d'ailleurs, ils se contentaient de passer leurs têtes de prédateurs entre les brins avant de faire demi-tour. 

Il comprit bien sûr que ni les hommes qui ravageaient son champ ni les femmes qui le longeaient en portant de lourds seaux ne pouvaient ressentir sa présence. Sa rage initiale dépassée, il se contentait à présent de les observer. Plusieurs jours passèrent ainsi, jusqu'à ce qu'un petit être n'apparaisse sur le chemin qui passait à côté des blés. C'était comme une version miniature des paysans, court sur pattes et l'air niais. Mais ce petit homme le regardait dans les yeux. Il le voyait. Le sourire qu'il n'avait plus travaillé depuis sa naissance reparut sur son visage sans qu'il ne fasse d'effort. 

Il tendit la main, d'un geste se voulant accueillant. Le garçon s'en saisit, et aussitôt il referma son emprise et emporta l'homme miniature dans les profondeurs du champ. Comme il était bon de toucher quelque chose pour de vrai ! Il appela les loups pour leur présenter sa trouvaille. Le petit homme dans ses bras était paralysé par la peur, incapable de hurler. Il sourit de plus belle. Les familiers arrivèrent rapidement, aussi surpris et heureux que lui. Ils ne firent pas grand cas de la résistance désespérée de leur proie et la dévorèrent. Lui se contenta de prélever les yeux, deux globes écarquillés qu'il savoura longuement. 

Le lendemain, les hommes revinrent au champ, mais en retard. La fatigue et l'inquiétude se lisait sur leurs visages. En s'approchant, il put entendre des conversations à propos d'un enfant disparu sur le chemin longeant le champ. C'était donc ainsi que cela s'appelait. Un enfant. Il avait adoré l'expérience d'hier avec l'enfant, et attendait avec impatience une nouvelle occasion, qui ne tarda pas à se présenter. Une femme miniature cette fois, qui batifolait dans le blé fraîchement fauché. D'autres enfants la cherchaient, mais il la trouva en premier. Comme hypnotisée, elle le suivit sans se débattre et il put lui présenter cet endroit dans lequel il vivait avant que les loups ne sautent sur leur nouveau festin. Il préleva sa part, extatique. 

À nouveau, les paysans étaient perturbés. Les yeux de celui qui se prénommait Fynn, celui qu'il n'avait réussi qu'à faire frissonner, étaient rouges à force de pleurer. L'homme s'attelait à la tâche sans entrain, avec un mouvement mécanique. Aucun des autres ne parla, mais ils regardaient tous le pauvre Fynn, inquiets. C'est alors qu'il comprit quelque chose. Il ne pouvait les blesser indirectement, mais il existait plusieurs façons de leur faire du mal. S'en prendre aux enfants en était une. En une semaine, il fit deux victimes de plus avant que les enfants soient interdits de s'approcher du champ de blé. Il entendit plusieurs mots qu'il ne connaissait pas. Getreidemann, et Roggenwolf. Apparemment le nom des coupables pour les disparitions d'enfants. Il se demanda si ces noms servaient à le désigner. Était-il un Getreidemann ? Il n'aimait pas le terme. Ce n'était pas des grains qui le constituaient, mais sa propre personne. 

Il recommença à se sentir seul. Les enfants ne venaient plus jouer avec lui, et même les hommes et les femmes évitaient les alentours du champ. Contemplant le village depuis l'extrémité du champ, il se demandait s'il devait aller chercher les enfants lui-même. C'est alors qu'une petite fille s'approcha de lui. Elle portait un panier rempli des fleurs bleues et rouges qui poussaient un peu partout dans le champ, entre les épis de blé. Souriant, il s'apprêtait à se saisir d'elle lorsque quelque chose l'arrêta dans son regard. 

Elle le fixait sans la moindre peur, mais sans être sous son charme non plus. Alors il la regarda à son tour. Et alors il sut ce que les loups qui se tenaient immobiles avaient remarqué avant lui. Ce n'était pas une petite fille comme les enfants du village. Elle était comme eux, une semblable. Elle lui sourit, d'un sourire sincère et chaleureux, bien loin du faux sourire qu'il brandissait devant elle. Fouillant dans son panier elle en sortit une gerbe de fleurs et les lui tendit, avant de s'éloigner en fredonnant. Lui n'avait pas bougé, sauf pour prendre le bouquet dans sa main. 

Perplexe, il resta un long moment immobile à considérer cette rencontre. Il ignora les aboiements inquiets de ses familiers jusqu'à ce qu'il réalise ce qui attirait leur attention. Les villageois avaient décidé d'incendier le champ pour faire partir le mauvais esprit. Peu lui importait en réalité. Il n'était pas enchaîné à ce lieu en particulier. Au contraire, brûler son foyer ne le rendrait que furieux. Il s'avança pour quitter le champ en feu, mais quelque chose l'en empêcha. Il n'en avait plus envie. Pourtant, il voulait s'échapper de cet endroit. Mais il ne voulait pas. Il y était lié, c'était chez lui. Mais son chez lui brûlait. Il ne souhaitait pas périr dans les flammes. Mais il ne souhaitait pas partir non plus. 

Alors il se souvint de la signification des fleurs rouges et bleues que la jeune fille lui avait données. Les hommes les avaient apportés pour apaiser la mémoire des enfants morts. C'est pour ça qu'elles fleurissaient autant en bordure de ce champ. Il les contempla un instant et déposa la gerbe à ses pieds, pour apaiser sa mémoire. 

 