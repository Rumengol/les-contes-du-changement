---
bg: "qui-va-a-la-chasse"
layout: post
title: Qui va à la chasse ?
summary: Une équipe d'intervention part à la chasse au dresseur d'ombre.
source: “Racing” par Valera Lutfullina
ref: https://www.artstation.com/artwork/Pma9N8
date: 2021-05-20
categories: posts
tags: ["Les Ombres"]
type: Conte
author: Rumengol
lang: fr
---
La meute s’élança dès que la note du cor silencieux retentit. En un instant les énormes molosses arrivèrent à la hauteur du camion lancé à vive allure.  Ils se calèrent sans difficulté sur sa vitesse, leur excitation difficilement contenu. Le conducteur avait toutes les peines du monde à garder son sans froid, et son copilote sursautait à chaque fois que la patte grosse comme un tronc entrait dans la lumière des phares. Si ce n’était le monstrueux appendice, ils pourraient se croire seuls sur la route. Mais si leurs yeux les trompaient, ils avaient bien conscience des machines à tuer qui les accompagnaient.

Yuri aurait mille fois préféré avoir un dresseur chevronné à ses côtés, plutôt qu’une chiffe molle apeurée. Il n’avait jamais apprécié ses horreurs, et le gamin ne faisait qu’accentuer son propre inconfort. Mieux valait distraire le petit, se dit-il, ou il risquait la crise cardiaque avant même qu’ils n’arrivent.

“C’est ta première chasse, gamin ?

&mdash; O-oui m’sieur.

&mdash; Appelle moi Yuri plutôt. Tu sais c’que tu fais là, au moins ?

&mdash; Oui m’sieur Yuri, on protège notre patrie et l’humanité des individus dangereux en les chassant ! Seulement...

&mdash; Je t’écoute. Et c’est Yuri.

&mdash; Le danger, il ne vient pas justement des ombres ? Alors pourquoi est-ce que l’armée emploie des dresseurs ? Et des dresseurs de loups, par dessus le marché !

&mdash; Parce que c’est justement les plus dangereux. Il n’y a qu’une ombre pour en détruire une autre. Nous autres, on peut éliminer les dresseurs et les contractants, mais si on laisse l’ombre repartir dans la nature, ça recommencera. Ils t’apprennent pas ça, à l’instruction ?

&mdash; Si, mais quand même...”

Le reste du trajet se passa dans le silence. Au moins, le petit avait arrêté de trembler comme une feuille et de sursauter. Ils s’arrêtèrent sur le chemin menant à une ferme, au beau milieu de nulle part. Yuri coupa le moteur. Ils se retrouvèrent enveloppés par les ténèbres, et la température de l’habitacle sembla chuter de quelques degrés. Claquant des dents, le chauffeur se pencha vers son collègue.

“Et tu vois, si on agit toujours juste après minuit, c’est parce que c’est à ce moment là que les ombres sont les plus fortes. Nos loups sont invincibles jusqu’au matin.

&mdash; Ça je le sais, inutile de me le rappeler, rétorqua sèchement le novice.”

Yuri se fendit d’un sourire. S’il était assez brave pour être agacé, il ne s’enfuirait peut-être pas la queue entre les jambes. Ça lui faisait toujours mal au cœur d’abattre les déserteurs dans le dos. Alors que même leur course effrénée avait été parfaitement inaudible, le grondement des loups s’élevait peu à peu. Comme un râle sourd venu d’outre-tombe, il résonnait de chaque côté du véhicule, menaçait. Ils sentaient la présence de leur proie, et le dresseur devait avoir toutes les peines du monde à les contenir. 

Le soldat se les imaginait tournant autour d’eux, piaffant d’impatience à l’image des chevaux dont il s’occupait dans une autre vie. Il se demandait si le gamin se ferait dessus avant minuit. Trois minutes, c’était jouable. Il paria contre lui-même le café tiède qui les attendait à la base. Un choc. Au bruit, il devait y en avoir un contre le pare-brise, les regardant les yeux pleins d’appétit. Une odeur dégoûtante lui apprit qu’il avait perdu son pari. Cet imbécile de néophyte avait mis ses lunettes de vision nocturne avant le début de l’opération. C’était le meilleur moyen de ne plus dormir jusqu’à la fin de ses jours.

Il regrettait déjà son café en se bouchant le nez quand un sifflement strident retentit. Ça commençait. Yuri se saisit de son fusil, abaissa ses lunettes et descendit à la suite du commanda qui patientait jusque là à l’arrière. Il fut satisfait de voir le gamin reprendre contenance et suivre le mouvement. Le dresseur était reconnaissable entre tous. Blond, gringalet et vêtu d’un long manteau, il n’avait pas la posture d’un militaire. Un confrère européen, apparemment, venu observer les tactiques d’intervention russes. 

Pas n’importe qui non plus, le vétéran s’était étonné de savoir que l’homme qui ne payait pas de mine était capable de contrôler seul trois loups d’ombre. Même s’il n’était pas un compatriote, Yuri devait admettre que c’était rassurant d’avoir un dresseur comme lui sur une telle opération. Leur cible était apparemment une famille de dresseurs, manipulant des ombres depuis plusieurs générations.

Le domaine était silencieux. Trop silencieux. Des dresseurs expérimentés devaient savoir que leurs ombres disparaissaient s’ils dormaient à minuit. Ils n’auraient pas fait cette erreur. L’angoisse de Yuri était partagée par la plupart des membres du commando. Ça puait le piège. À moins qu’ils ne se soient déjà enfuis. La vingtaine d’hommes entoura sans un bruit la vieille bâtisse, veillant à ne laisser aucun angle mort. 

Les loups s’approchaient lentement de leur cibles, leurs ombres prêtes à bondir. Ils sentaient quelque chose et n’étaient pas prêts à laisser leur proie s’échapper. On aurait pu entendre une mouche voler dans ce silence de mort. Puis une forme sombre et massive s’éleva du toit bien trop fragile pour la soutenir. Une ombre, monumentale. Quand elle se redressa, Yuri put admirer pour la première fois de sa vie un grizzly d’ombre, qui devait attendre les cinq mètres debout. 

Les ombres dans la nature sauvage avaient tendance à être plus grosses et puissantes, mais un grizzly, c’était une première. L’apparition tétanisa les hommes, mais ne parut pas impressionner les loups, qui se jetèrent simultanément dessus. C’était une vision hors du commun, quatre figures grandes comme des maisons, s’affrontant sur le toit d’une grande isba. Le plus déconcertant, c’était probablement de ne pas voir la tôle du toit s’ouvrir ou simplement céder sous la violence des coups.

Yuri se dirigea prudemment vers la porte d’entrée avec une moitié des troupes, le reste surveillant les fenêtres, prêt à abattre le premier imprudent qui passerait devant. Le grizzly était sans aucun doute le plus puissant gardien que la famille contrôlait, mais ça ne voulait pas dire qu’ils ne possédaient pas d’ombres plus modestes.

Sur ses gardes, il se jeta au dernier moment sur le côté comme la silhouette griffue se jetait sur lui. Le soldat derrière lui n’eut pas le même réflexe et s’effondra, la gorge tranchée, avant d’avoir pu réaliser ce qu’il se passait. Balayant la grande pièce du regard, Yuri cherchait désespérément le dresseur en se relevant. Déjà l’ombre repartait à la charge, maintenant les soldats à l’extérieur et le repoussant à l’intérieur du même coup. 

Une balle frôla sa nuque. Remerciant sa chance indécente, le militaire se retourna face à l’homme qui venait de le manquer, et ne fit pas cette erreur. Un seul tir, entre les deux yeux, et le dresseur tomba raide mort. L’ombre dans l’entrée arrêta soudainement de s’agiter. C’était toujours comme ça, lorsque le dresseur mourrait. Son ombre se figeait pendant une dizaine de minute, avant de se ressaisir et de s’évanouir dans la nature.

À l’extérieur, le combat faisait toujours rage. Les loups avaient l’avantage, mais le grizzly était un adversaire redoutable qui refusait de se laisser faire. À tous les coups, c’était un genre d’esprit ancestral qui protégeait les fermiers depuis des siècles. S’il continuait à se battre, c’est que l’homme ne le contrôlait pas. Il y avait au moins un autre dresseur dans la maison, ils devaient le trouver.

Les hommes investirent les lieux, fouillant chaque endroit, vérifiant si le plancher n’était pas creux. Yuri monta dans la mezzanine pour se retrouver face à deux jeunes enfants tremblant de peur et une femme trempée de sueur, le visage figé dans une concentration intense. Sans attendre, il abattit la mère. Le grizzly avait beau être résistant, s’il se figeait sur place les loups n’en feraient qu’une bouchée. 

Faisant taire son cœur, il rechargea silencieusement et pointa son arme en direction des enfants recroquevillés l’un contre l’autre.