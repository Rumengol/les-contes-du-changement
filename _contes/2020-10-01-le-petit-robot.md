---
bg: "le-petit-robot"
layout: post
title: Le petit robot
summary: Un adorable petit robot se fait réactiver après des millénaires d'inconscience par un météore de passage.
source: “Interval” par Matt Dixon
ref: https://www.mattdixon.co.uk/transmissions/interval
date: 2020-10-01
categories: posts
tags: ["Et factum est machina"]
type: Conte
author: Rumengol
lang: fr
---

Le météore traversa le ciel, laissant derrière lui une traînée de poussière étincelante. Cette poussière retomba comme de la neige au sol, devenant presque indiscernable. Mais quelques grains à peine, chargés de l’énergie de leur descente, effleurèrent la carcasse métallique d’un petit robot laissé à l’abandon depuis bien longtemps. Cette énergie, aussi infime soit-elle, suffit pour rallumer une étincelle de vie à l’intérieur du châssis grisâtre.

Les yeux du petit robot clignotèrent deux, trois fois, avant de se stabiliser. Sa batterie de secours, qui venait d’être activée, l’alimenterait pour plusieurs années. Son premier réflexe fut de regarder le ciel. Un ciel nocturne inconnu mais magnifique. Chaque étoile était incandescente, et sa vue barrée par une bande nacrée. A côté, il pouvait discerner les lumières fugaces d’autres galaxies. Il eut l’impression de souiller par son regard la perfection immobile de ces entités astrales. Honteux, il baissa sa tête et reporta son attention sur l’endroit où il était assis.

Il se trouvait au sommet d’un petit talus herbeux, perdu dans une large plaine. Il était venu ici il y a bien longtemps, et il s’était arrêté pour faire une pause, sa batterie demandant à être rechargée. Des milliers, peut être des milliards de cycles s’étaient écoulés depuis. L’herbe avait peu poussé autour de lui, son vernis s’étant avéré toxique pour les plantes. Au delà de son cercle de poison, elle était désormais deux fois plus haute que lui. Heureusement, il n’avait pas rouillé pendant son long sommeil. Dépourvu d’objectif, il décida de retrouver le météore qui l’avait réactivé. Écartant les premières herbes hautes, il débuta son périple.

À la plaine se substituait un désert. Une mégalopole s’élevait avant ici, c’était visible dans les stigmates du sol, craquelé et assoiffé. Une fois, il trébucha dans une crevasse, et manqua de chuter profondément. Sur les flancs des falaises, l’armature d’acier d’un immeuble affleurait. Dévorée par la rouille, elle tomba en poussière au passage des pierres délogées par le petit robot. Il frissonna à cette vision, au destin inéluctable des structures en métal de l’ancien monde. Lui avait échappé à ce terrible destin, mais combien de ses semblables ont eu cette chance ?

Parfois, les sommets de bâtiments autrefois immenses dépassaient du caveau de terre. Alors il s’amusait à comparer leurs antennes à la sienne. Certaines étaient plutôt semblables, et alors il prenait le temps de les dépoussiérer avant de passer son chemin. Il ne pouvait s’empêcher d’éprouver de la peine pour ces géants enfouis à jamais, condamnés à s’éroder lentement jusqu’à disparaître sans laisser de marques.

Il avait grimpé sur un satellite écrasé pour se repérer. La ville engloutie par le sol était loin derrière lui désormais, mais le désert semblait infini. Les nuit étaient courtes et la chaleur des jours impitoyable. Même sans capteur de température il se sentait chauffer sous le soleil de plomb. Au plus fort de la fournaise, ses pas s’enfonçaient dans le sol dur comme de l’ardoise. Il avait frôlé un buisson, un jour, celui-ci s’était immédiatement enflammé. Du haut de l’engin abandonné, il n’avait rien vu, sinon d’autres du même genre. C’était un désert littéral, dans lequel aucune trace ne persistait longtemps.

Les nuits étaient des moments de répit, où les étoiles n’étaient plus occultées par la lumière du soleil. De temps en temps, il s’arrêtait, et admirait fugacement la toile de l’univers. Il lui semblait que les étoiles filantes étaient chaque fois plus nombreuses, et s’émerveillait à imaginer ce que ces astres en perdition pouvaient receler de mystères. Parfois, il parvenait à capter le clignotement furtif d’un satellite en mouvement. Avant qu’ils ne s’éteignent, ils formaient des constellations mouvantes incroyables, fidèles à la tâche, même si plus aucun appareil ne réceptionnait leur signal. De la guirlande des diodes rogue-vert-jaune, seule les plus braves étaient encore visibles.

Quand exactement le désert avait laissé place à la savane ? Il n’en était pas sûr, la transition s'était faite en douceur. A la savane s'était substituée une jungle clairsemée. Les arbres lui semblaient gargantuesques, mais ils étaient inhabités, vidés de leur substance. Le vent sifflait en passant dans leurs troncs calcinés, jouant une mélodie sinistre. Leur forme se tordait de plus en plus, les couleurs se fanaient. La nuit, la forêt devenait terrifiante. Un soir, le grondement sourd d’une éruption lointaine le fit paniquer. Il courut aussi vite que ses petites jambes le lui permettaient, et sortit du même coup de la forêt.

Il avait débouché dans des collines rouges aux allures de montagnes. La poussière s’insinuait dans ses articulations et les gênait, mais son but était proche. Quelque part aux alentours, le météore était tombé. Le petit robot ne savait pas encore ce qu'il ferait une fois son but atteint. Il aviserait. Les étoiles filantes se faisaient de plus en plus nombreuses, éclairant désormais le ciel comme une aube prématurée. Escaladant des rochers plus grands que lui, à la force de ses bras mécaniques, il se rapprochait toujours plus. Le mouvement de son antenne ralentissait aussi. Il avait épuisé sa batterie de secours plus vite qu'il ne s'y était attendu. Retrouver le météore serait peut-être la dernière chose qu'il ferait.

Enfin, dans le contrefort d'une petite vallée, il perçut un cratère. Il en était certain, c'était son météore, celui qui l'avait ramené à la conscience. Il dégringola la pente, achevant la descente en roulant sur lui-même. Au fond du cratère, aucune météorite ne l'attendait. Sa carcasse de métal roussie par la traversée de l'atmosphère, le satellite n'avait rien de remarquable. Un satellite météo comme il y en avait des milliers du temps des humains. Celui-ci était tombé hors service depuis longtemps et la gravitation l'avait lentement attiré vers la Terre jusqu'à ce qu'il frôle l'atmosphère et s'écrase au bout d'une révolution.

Le petit robot leva les yeux. Dans le ciel nocturne, ce n'était pas des étoiles filantes, mais des satellites fantômes s'écrasant par dizaines, retournant dans la croûte qui les avait vu naître. Les derniers vestiges de l'antique civilisation disparaîtraient bientôt, tout comme lui, ultime représentant de la dernière race à peupler cette Terre.
