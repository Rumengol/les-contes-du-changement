---
bg: "coeur-d-or"
layout: post
title: Cœur d'or
summary: Stuart Goodwill récupère Stuart Goodcan, un automate qu'il pensait perdu.
source: “The Machinist” par Erik van Helvoirt
ref: https://www.artstation.com/artwork/zPOllD
date: 2023-11-19
categories: posts
tags: []
type: Conte
author: Rumengol
notes: "La planète Jarilo-IV (Honkai: Star Rail) a été une source d'inspiration assez importante dans la création de l'univers, même si l'histoire n'a rien à voir. Quoique, il y a peut-être une tempête éternelle à l'origine pas très naturelle..."
---
L'échoppe était déserte. Des dizaines d'artefacts mécaniques et d'automates reposaient sur les étagères ou encombraient les allées. Le bois ployait sous le poids des mécanismes de précisions entassés sans le moindre soucis pour leur intégrité. Les plus précieux, ceux qui étaient aboutis, trônaient derrière des vitrines de verre renforcé, dominant les passants qui ne pouvaient que contempler de tels chefs-d'œuvre sans même espérer en posséder.

Même en l'absence de mouvement, la pièce était loin d'être silencieuse. Des dizaines d'horloges accrochées au mur emplissaient l'air du tic-tac de leurs aiguilles. Chacune était unique, le résultat de longues heures de travail pour répondre à une commande précise. Les modèles exposés attendaient impatiemment que leurs propriétaires viennent les chercher pour être la pièce maîtresse d'un salon, d'une aile ou même d'un manoir. Pour tromper l'ennui, elles indiquaient le temps à une précision qui atteignait parfois la fraction de seconde, à l'aide d'aiguilles simples, raffinées ou fantaisistes.

Dans la vitrine, des figurines de cire dansaient au son mécanique d'une boîte à musique, sous le regard vigilant d'une demi-douzaine d'automates. L'odeur de l'huile qui brûlait au fond de leurs yeux pouvait se montrer incommodante, et d'autres ateliers cherchaient à la dissimuler à grand renforts d'encens et de parfums fleuris contrastant brutalement avec le métal froid de leurs produits. Ici, cette senteur était source de fierté, elle flottait dans l'atmosphère rendue moite et graisseuse.

Derrière la porte mécanique se séparant en deux moitiés qui disparaissaient dans le mur, l'arrière boutique révélait le cœur de l'atelier, là où la magie opérait. Dans un labyrinthe d'étagères débordant de boîtes elles-mêmes remplies d'écrous, de vis et d'engrenages résidaient les différentes stations de travail de Stuart Goodwill, l'artisan prodigue et maître de ces lieux. Tous ses bureaux étaient couverts d'outils, de composants d'automates ou de traités sur le sujet dont il était pour la plupart le principal auteur. Soudeuses, étais, pinces, marteaux, tournevis et bras mécaniques luttaient pour le peu d'espace  disponible. Rien n'était gâché, puisque les plateaux n'étaient pas retenus par de simples pieds, mais par des rangées de tiroir ou de caisses empilées tant et si bien qu'elles supportaient le bois qui ployait sous le poids de la technologie. Il s'agissait pourtant de meubles de première qualité, conçus pour résister à l'épreuve du temps et de l'artisan le plus impitoyable.

L'atelier était si long qu'il possédait des fenêtres donnant sur la rue parallèle à celle de la devanture. Les larges vitres n'offraient cependant qu'une faible luminosité, recouvertes des substances qui flottaient dans l'air et trouvaient dans le verre une surface agréable où s'installer. De la graisse, surtout, mais aussi des dépôts de souffre, de colle et de lime métallique, amalgamé dans un revêtement opaque qui n'avait pas été nettoyé depuis des années, s'il l'avait déjà été. Ainsi et malgré leur grande taille, l'atelier était continuellement plongé dans la pénombre, même aux heures les plus clémentes du jour. Ce mélange agissait de façon insoupçonnée comme un isolant, préservant du même coup les pièces les plus fragiles des températures glaciales de l'extérieur.

Tout au fond de l'atelier, devant justement l'une de ces fenêtres, Stuart Goodwill s'attelait avec attention à une tâche de la plus haute précision. Il portait sa tenue de travail, une chemise blanche protégée par une salopette de cuir ignifugée qui lui avait sauvé la vie à de nombreuses reprises. Sa barbe Van Dyke grisonnante était taillée avec précision, il portait toute son attention capillaire à sa pilosité faciale depuis que son dernier cheveu était tombé. Sa paire de lunette favorite sur son nez, il pouvait discerner le plus fin des détails comme s'il avait à nouveau vingt ans.

Le vieil homme était penché vers un travail de la plus haute importance. Assis sur une chaise semblable à celle qu'utilisaient les médecins pour ausculter leurs patients, un automate de fer était figé, face vers le miroir placé au dessus de lui. Des bras mécaniques étaient disposés autour de lui, attachés à la tuyauterie qui parcourait le plafond, tel une pieuvre cherchant à se saisir de l'infortunée création. Certains plantaient des tiges d'acier dans son crâne pour le maintenir immobile, tandis que d'autres portant des miroirs d'argent reflétaient et intensifiaient la faible lumière du jour par un habile jeu d'optique.

Le visage de l'automate était ouvert, révélant sa mécanique interne. Les deux lentilles qui lui servait à appréhender son environnement formaient d'angoissants yeux proéminents. Les fibres qui servaient d'armature flexible pour les modèles requérant différentes expressions finissaient de donner à la créature de métal un aspect humain, et il n'aurait pas fallu longtemps à un intrus pour déduire que Stuart Goodwill s'adonnait à la torture d'un homme dont il aurait arraché le visage. Mais l'attention de l'ingénieur n'était pas sur cette face source de cauchemar.

Armé d'un stylet chauffé à blanc, il traçait les dernières instructions sur le plastron de sa création. Une fumée accompagnée d'une odeur de métal fondu s'élevait là où il posait son outil pour tracer des lignes minuscules à la surface d'une plaque d'un matériau différent. C'était toujours là le passage qui demandait le plus de précision. Les lignes couraient côte à côte mais ne devaient jamais se chevaucher, ni même se toucher, sous peine de mélanger les informations et d'obtenir une construction folle. Pour cette raison, l'ensemble de ses pairs rechignaient à les tracer eux-mêmes, utilisant à la place des machines pour maintenir la plaque et une pointe de diamant maniée par un bras d'automate.

Ils recouraient à de tels artifices car ils manquaient son talent. Leur processus était long et limité, mais peu risqué. En usant de telles fonctions, les plaques d'instructions étaient basiques au mieux. Il fallait l'adresse et l'audace d'un véritable maître pour expérimenter et innover. Même lui était imparfait. Cela lui arrivait d'échouer, de briser une pièce ou que le tracé théorique d'une instruction diffère de la pratique. Il se retrouvait alors à jeter la précieuse plaque, devenue irrécupérable. Cependant, qui irait le lui reprocher ? Il avait tant apporté, même les maîtres artisans œuvrant directement au service du directeur général étaient d'anciens disciples de son atelier.

Ce qui l'occupait et qui l'avait mené à fermer boutique dès la mi-journée n'était ni une expérience, ni un nouvel échec en perspective. C'était le projet de sa vie. L'aboutissement de décennies de recherches, à confectionner des horloges pour des nantis insouciants et des outils de haute précision à des citoyens ignorants. C'était pour cela qu'il avait négligé sa famille, qu'il n'était pas là lorsque Léona avait rendu son dernier souffle, qu'il avait laissé sa progéniture le détester. Les résultats étaient arrivés près de quinze ans trop tard pour qu'il la sauve une première fois, mais il y avait toujours une seconde chance.

La chandelle qu'il utilisait pour relire et remettre au goût du jour l'un de ses anciens traités se consumait, dégoulinant de cire brûlante qui coulait sur les pages. Rien d'important. Ce n'était que des mots, il pouvait toujours les réécrire. Tant que les pages ne prenaient pas feu, il ne couperait pas court à sa concentration. Cela dit, il doutait que la chaleur soit suffisante pour que le papier atteigne une température suffisante. La chaudière était encore une fois tombée en panne. Rien qu'il pouvait faire, cette coupure affectait toute la rue. Mais il n'était pas dérangé par le froid mordant qui pénétrait par les rares trous dans les joints des fenêtres. Son cœur battait avec assez d'excitation pour réchauffer tout son corps, et son bras ne tremblait pas le moins du monde.

Plus tôt dans la matinée, trois hommes avaient bravé le blizzard pour frapper à sa porte. Ils portaient sur leur dos un automate, le même qu'il était à présent en train de réveiller. Ils l'avaient trouvé dans la carrière, à moitié enseveli sous la neige. D'après eux, il avait tenté d'escalader les parois de glace au Nord du site. Au delà il n'y avait que des plaines enneigées à perte de vue et les températures les plus basses que ce monde connaisse. Les mineurs avaient conclu au dysfonctionnement d'un automate de minage, mais sa carrosserie différait de ceux employés traditionnellement dans la carrière, et il ne portait pas de sceau de propriété, seulement la signature des ateliers Goodwill. 

Celui-ci avait adopté son expression la plus perplexe, brodant un mensonge aussi facilement qu'il respirait. Il avait soutenu ne pas reconnaître cette construction, mais qu'il allait tout de même la réparer avant de la rendre à son réel propriétaire. Il leur avait donné cinq craies de charbon chacun, en les invitant chaleureusement à lui ramener tout automate non identifié qui porterait sa signature. Les mineurs s'étaient ébahi de son apparente générosité, ignorant qu'ils venaient de lui rendre un service bien plus précieux que quinze craies.

Le haut du corps de son automate était méchamment cabossée et il s'était retrouvé à court d'énergie, mais en dehors de ça il était parfaitement fonctionnel. En toute logique, l'automate aurait dû être capable de se relever et de rejoindre la ville de lui-même, au lieu de s'effondrer sous la neige. Le vieil homme l'avait péniblement installé sur sa chaise de travail, connecté à ses réserves de carburant et d'huile avant de l'inspecter. Le crâne n'avait subi aucun dommage structurel, mais ce n'est qu'en ouvrant la plaque protégeant la mécanique située au niveau du cœur qu'il fut assuré de sa trouvaille.

Il finit de tracer la dernière instruction dans la plaque inachevée. Les quelques lignes manquantes, imperceptibles même pour les plus habiles ingénieurs, empêchaient le redémarrage autonome de de l'automate et entravaient ses capacité de communication. Cet affront volontaire corrigé, il referma la trappe et tourna la clef qu'il avait enclenché un peu plus tôt. L'humanoïde mécanique s'anima soudain et tenta par réflexe de se redresser avant d'en être empêché par les barres de métal le trépanant. Il tourna alors autant que possible son visage toujours ouvert aux quatre vents vers son créateur.

« Stuart, s'exprima l'automate d'une voix mécanique et rugueuse. Il semblerait que je sois de retour dans l'atelier.

— Stuart, répondit le vieil artisan d'une voix qui masquait à peine son impatience. Dis moi vite, qu'en est-il de ta mission ?

— Il me serait plus aisé de te montrer. Puis-je retrouver la maîtrise de mes membres ?

— Bien sûr, bien sûr. »

Stuart Goodwill se pencha par dessus l'automate pour dévisser la barre qui retenait son bras droit contre l'accoudoir. Il s'attaqua ensuite au bras gauche tandis que sa création effectuait des moulinets dans une tentative maladroite d'imiter un homme dont on viendrait de retirer les menottes. Sans un mot, l'être mécanique remplaça ses doigts par une panoplie d'outils et entrepris de s'ouvrir lui-même le ventre.

Les vis qui retenaient la trappe vers cette cache requéraient six types d'outils différents, certains créé pour l'occasion. A sa connaissance, Stuart l'automate était le seul capable de déverrouiller sa trappe. Une fois la protection retiré, il enfonça sa main là où se trouveraient ses viscères s'il était vivant et en extirpa une boîte métallique, dans laquelle reposaient trois cylindres. Le temps et les éléments n'avaient pas été tendres avec eux, ils étaient dans un état déplorable et l'un d'entre eux était percé, versant un étrange liquide qui ne ressemblait à rien de connu.

« Qu'est-ce donc ?

— J'espérais que tu le saches, j'ai failli perdre plus que mes fonctions motrices pour extirper ces cylindres du sépulcre. Si tu y vas toi-même, tu seras sans doute en mesure de déterminer leur utilité. Mais sans doute auras-tu trop à faire à déchiffrer les inscriptions minuscules gravés sur les côtés.

— Je te remercie, Stuart. Tu m'as été d'une aide inestimable, bien plus que ce à quoi je m'attendais en te donnant la vie. 

— A t'entendre, tu as déjà de la suite dans les idées.

— C'est exact.

— Alors, où dois-je aller cette fois ? Et si tu pouvais me libérer de cette entrave crânienne.

— Oui, naturellement. Auparavant, j'aimerais savoir quelque chose. Les mineurs qui t'ont ramené m'ont informé que tu tentais d'escalader la muraille de glace. Qu'espérais-tu trouver au-delà de ces murs ?

— Oh, vieil homme. Je priais pour que tu ne me poses pas la question. Tout aurait été plus simple. »

La suite se passe très rapidement. L'automate gesticula sur la chaise, comme agité de spasme. Il tourna frénétiquement sa tête dans l'espoir de détacher les barres qui la maintenant sur place, tout en fouettant l'air de ses bras. Ses pieds étaient eux aussi fixés au sol. Le bois gémissait sous ses assauts répétés pour libérer ses jambes. Devant la vacuité de ses efforts, il se mit à gémir, puis à hurler, produisant un son qu'aucune gorge humaine ne pouvait imiter.

Stuart Goodwill réagit aussi vite que ses os vieillissants le lui permettaient. Il s'empara du stylet qui fumait toujours et le planta directement dans le cœur de la créature devenu folle. La protection métallique céda rapidement et la pointe pénétra sa carapace pour toucher la plaque d'instructions. Celle-ci fondit instantanément, pulvérisée par la puissance dévastatrice de la pointe surchauffée. L'automate cessa aussitôt de s'agiter. Ses membres retombèrent, inertes. Si toute sa logique était concentrée dans son crâne, hors d'atteinte, ses fonctions motrices et la plaque chargée de connecter toutes les autres étaient superposées au niveau du cœur. Elles détruites, ce n'était qu'une carcasse de métal sentiente, incapable du moindre mal.

L'inventeur de génie soupira en regardant sa création. C'était elle aussi un échec, au final. Cet automate s'était montré plus performant que les autres de sa génération, mais lui aussi avait tenté de tuer son créateur et de prendre sa place. Il ignorait ce qui rendait les machines ainsi folles, cela avait été sa principale problématique lors de la conception de la génération suivante. Il ne savait pas encore si elles avaient eu plus de succès, aucune n'était encore revenue. Si cela devait arriver, il prendrait les mêmes précautions.

Au moins, cet échec avait eu le bon ton d'accomplir son objectif avant de dysfonctionner. Ces cylindres contenaient forcément, d'une manière ou d'une autre, la clef qui lui manquait pour réaliser son rêve. Les plaques d'instructions étaient écrites depuis longtemps, plus de quinze ans, mais il n'avait jamais pu se résoudre à les installer. Échouer ici reviendra à faire s'envoler deux décennies de recherche. Il ne lui restait pas ce temps là à vivre, alors il ne pouvait prendre aucun risque.

Sa main tremblait légèrement. Il ne pouvait pas transcrire en instructions les évènements de la journée ni la kyrielle d'hypothèses qu'il établissait à propos des mystérieux cylindres. Au lieu de ça, il extirpa d'un des tiroirs un carnet vieilli par les années et se saisit d'une plume. Il relata par écrit tout ce qui lui paraissait pertinent. Les pages se noircirent à vue d'œil tandis que les yeux de Stuart l'automate le fixait avec une haine qui ne devrait pas être possible au travers d'orbites si inexpressives.

« Tout va bien, Stuart ? jaillit une voix de l'autre bout de l'atelier. J'ai cru entendre un cri terrible.

— Ce n'est rien, Stuart. Un automate de génération deux est revenu. Dysfonctionnel, comme les autres. Mais il a rapporté quelque chose, viens voir. L'un d'eux est abîmé, mais quelque chose me dit que ces cylindres contiennent la clef que nous recherchons depuis si longtemps. »