---
bg: "decollage"
layout: post
title: Décollage
summary: Un groupe d'insurgents décide de s'emparer de vaisseaux spatiaux pour s'enfuir de leur planète.
source: “Launch site” par Martin Parker
ref: https://www.artstation.com/artwork/YB6YeK
date: 2023-11-20
categories: posts
tags: ["Les 5 ciels"]
type: Conte
author: Rumengol
notes: "Pour être honnête, la fin devait être très différente. Plus de suspens, une enquête dans les couloirs du bunker et la découverte choquante du destin de ses occupants. Mais il se fait trop tard pour finir comme je veux. C'est possible que je revisite cette histoire plus tard, pour la compléter."
---
Zarranck contempla la figure majestueuse des vaisseaux spatiaux avec envie. La dernière fois que l'un d'eux avait connu le vide interstellaire, il n'était pas encore né. Ses parents en étaient descendus alors qu'ils n'étaient eux-même que des enfants, portés par les espoirs de leurs grands-parents. Des colons impatients de s'établir sur un monde intouché par la souillure de l'espèce humaine, un nouveau départ pour toute une civilisation.

Ils avaient tous embarqué à bord de sept vaisseaux transportant l'intégralité de leurs possessions, de leur technologie, de leurs connaissances. Des gens ordinaires, de tous horizons. Certains fortunés, d'autres sans le sou, saisissant à deux mains l'opportunité de recommencer leur vie dans des terres plus propices à leur épanouissement.

Alors ils s'étaient posés sur le sol verdoyant de Ceteles, et leur première réalisation fut de gigantesques buttes de bétons creusant profondément la terre pour installer leurs vaisseaux sur ce qui devait être leur ultime repos. Les portes des bunkers furent scellées, et les pionniers désireux de s'affranchir de leurs liens technologiques se lancèrent candidement dans l'aventure de leur vie.

Le rêve avait viré au cauchemar bien plus vite qu'aucun d'entre eux, mêmes les soldats vétérans, n'auraient pu prédire. La planète n'avait rien du paradis promis, ce que la première génération de colons ne réalisa que trop tard. Certains souhaitaient vivre en communion totale avec la nature, dans des habitats éco-conçus et rejeter toute forme d'électronique. D'autres considéraient que la technologie était indissociable de la civilisation et cherchèrent à assembler les robots de constructions enfouis dans les bunkers. D'autres encore, plus nuancés, luttaient pour un entre-deux, un futur proche de la nature appuyé par des outils très avancés.

Avant la fin de la première année, les colons s'étaient fracturés en une dizaine de factions luttant pour ses idées. Par le verbe d'abord, lors d'assemblées hebdomadaires qui laissaient la part belle aux ambitions et aux projets d'avenir. Puis, quand le compromis tarda à venir, les actes prirent le pas sur le dialogue. Les partisans de la technologie avaient conservé sur eux certaines armes, ce qui accentua la tension générale. Le nom de celui qui tira le premier fut oublié, tant cela aurait pu être n'importe qui. Mais le premier naturaliste abattu enflamma la poudrière.

Malgré leur armement supérieur, les technophiles étaient très largement dépassés par le nombre. Assaillis de toutes parts, ils se replièrent dans les tréfonds des bunkers et activèrent les systèmes de sécurité, pour ne plus jamais reparaître. Les deux camps se séparèrent, pansant leurs blessures et comptant leurs nombreuses pertes. Par la suite, la maladie et les prédateurs s'étaient chargés d'achever la communauté naturaliste mourante.

Il leur avait fallu deux génération pour abandonner tout espoir de coloniser un jour la planète. Les parents de Zarranck croyaient encore en cet idéal, aux vertes prairies et aux collines boisées qu'ils pensaient léguer à leurs enfants. Lui n'en n'a jamais été convaincu, les histoires du ciel et du peuple des étoiles qu'ils lui contaient le faisait bien plus rêver que les après-midi de pêche et les course-poursuites pour échapper aux crocs d'un liviel.

Leur communauté vivait dans des cabanes bâties à même les immenses troncs des Cimes Étoilées. Les accidents, les constructions fragiles et les altercations avec les autres habitants des arbres réduisaient chaque année un peu plus leur nombre. Mais lorsque sa sœur succomba d'une grippe qui avait ravagé la communauté, quelque chose se brisa en lui. Il partit, contre l'avis de ses parents et accompagné d'amis et d'insurgés qui, comme lui, se rebellaient contre leurs géniteurs qui avaient forcé leur mode de vie à leurs enfants.

Ils ne possédaient que leurs paquetages, une vieille carte et leur connaissance des étoiles pour retrouver le point d'atterrissage. La route avaient été longue et périlleuse. A peine quittèrent-ils la forêt qu'un marécage se dressa sur leur chemin. Marin et Gioele insistèrent pour le traverser, tandis que Zarranck parvint à convaincre les autres de le contourner. Ce fut un détour de plus d'une semaine qui épuisa leurs ressources bien plus vite qu'ils ne l'avaient anticipé, mais il préféra la prudence à la témérité. Ils ne surent jamais si les deux têtes brûlées s'en étaient sorti. De l'autre côté de l'étendue inhospitalière, personne ne les attendait, et rien sur le chemin n'indiqua un passage récent.

Par la suite, le terrain devint rapidement accidenté. La carte était imprécise, ils ne possédaient comme seule information que le fait que les vaisseaux se trouvaient dans une vallée encaissée entre de nombreuses collines. Peter supposait que d'aussi grandes constructions se verraient forcément de loin, mais les Bourrelets de la terre formaient un relief traître, où il était possible de gravir un talus de quelques mètres pour réaliser qu'il dévalait ensuite des centaines de mètres en a-pic. Démunis, ils n'eurent d'autre choix que d'explorer chaque recoin des Bourrelets méthodiquement, priant pour ne pas tomber à court de nourriture avant d'accomplir leur objectif.

Les proies se faisaient rares, ou plutôt elles avaient appris un large éventail de méthodes pour échapper à des prédateurs bien plus dangereux qui rôdaient dans les collines. Eux s'étaient spécialisés dans la furtivité et les attaques surprises. Le groupe de jeunes adultes avançait avec prudence, guettant le moindre rocher susceptible de dissimuler un monstre de crocs et de griffes. Régulièrement, ils s’aplatissaient au sol pour se soustraire à la vue aiguisée d'un Roual.

Les nuits étaient mouvementées, ils dormaient peu et montaient beaucoup la garde. Les recoins manquaient malgré le relief important, et ils se retrouvaient souvent à se reposer dans des arbres bien trop bas à leur goût. Même s'il était improbable que les animaux arboricoles qui leur menait la vie dure dans les Cimes s'aventurent dans un terrain aussi peu boisé, ils ne savaient pas si les prédateurs locaux étaient capable de grimper aux arbres. Mieux valait ne pas prendre de risque et se préparer au pire.

Par chance, ils ne firent aucune mauvaise rencontre les trois premiers jours. Peter avait estimé qu'il leur faudrait deux semaines, au plus, pour explorer les cuvettes les plus à même d'abriter les appareils. Certains de leur groupe s'étaient montré désespérés à cette annonce, mais elle n'avait fait que renforcer la détermination de Zarranck.

« Raison de plus pour s'y mettre maintenant ! avait-il dit. »

Il décréta qu'ils allaient se séparer, chacun arpentant une crête différente, pour maximiser le terrain couvert. Cheminer ainsi était incroyablement plus dangereux, mais le désespoir et la témérité l'avaient poussé à prendre cette décision. Il avait vu dans leur regard l'abandon, l'envie de rentrer chez eux et de s'excuser. Le groupe ne resterait pas uni deux semaines, et il ne pourrait pas piloter le vaisseau seul.

Ils se donnaient chaque matin un point de rendez-vous qu'ils devraient atteindre le soir. Rien de très loin, pour s'accommoder aux plus lents et permettre à tous de se reposer au mieux. C'était aussi là qu'ils s'échangeaient les informations, leurs observations sur le terrain et la faune locale, tout ce qui pouvait être d'intérêt. Ludmilla n'arriva jamais au rendez-vous du quatrième jour. Personne ne se risqua à aller la chercher. Draïna hasarda qu'elle était peut-être rentrée chez eux. Aucun d'entre eux ne la contredit.

La mi-journée du cinquième jour de recherche arrivait. Le ciel était haut dans le ciel, les quelques nuages translucides ne parvenaient pas à masquer ses rayons qui révélaient les couleurs vives de la végétation de Ceteles. Zarranck somnolait à moitié, épuisé par la nuit dernière où il n'avait pas réussi à fermer l'œil. Lui-même commençait à douter de son entreprise. Ses pieds le faisaient souffrir, tout son corps réclamait un pause, et sa gourde était déjà vide. Il se faisait un point d'honneur à toujours arriver au rendez-vous en premier, mais cette résolution devenait de plus en plus dure à tenir.

C'est alors qu'il capta un reflet rouge éclatant du coin de l'œil. Il crut d'abord qu'il hallucinait, rendu fou par le manque de soleil. Mais non, il ne rêvait pas. Au fond d'une cuvette encerclée par des collines à l'allure de montagnes, quatre magnifiques étalons de métal se reposaient, leur carcasse aussi resplendissante qu'au premier jour. Ils se dressaient fièrement au sommet de buttes d'une terre ocre grignotée par la mousse empiétante. Des armatures métalliques les maintenaient sur place, le nez orienté vers le ciel, comme prêts à décoller.

Un brin de déception le traversa. Ils lui paraissaient subitement petit, depuis son point de vue en altitude. Les structures sur lesquelles les vaisseaux trônaient étaient bien plus grandes, en perspective. Ce n'est qu'en remarquant le lac qu'il réalisa son erreur. Ce qu'il avait plus tôt pris pour de la mousse était en réalité des carrés d'arbres qui escaladaient péniblement les buttes titanesques. Le moindre de ces appareils ferait de l'ombre au plus haut arbre des Cimes Étoilées, il pouvait le voir à présent. Les jambes flageolantes, il appela ses compagnons de grands gestes avant de s'engager dans la pente. Il lui tardait de voir ses palais volants de plus près.

Il s'y connaissait très peu en mécanique, cette connaissance étant méprisée dans sa communauté de naissance. Il était incapable d'identifier l'utilité de la plupart des pièces qu'il pouvait isoler. Les moteurs, au nombre de six, étaient situés à l'arrière, mais c'était la partie facile. Sous la carapace vermeil, un enchevêtrement de câbles noirs était tressé comme des intestins apparents. La partie inférieure de la coque reposait au sol. C'était comme si on avait ouvert le ventre de la bête qui gisait sur son lit de mort. Une analogie que Zarranck n'espérait pas prémonitoire.

Il venait d'atteindre le bas de la pente lorsqu'il entendit derrière lui les cris de surprise et de joies de ses amis. Ils dévalèrent la pente en riant, soulagés que leur rêve n'était pas une chimère.

« On l'a fait, déclara sobrement Peter lorsqu'ils atteignirent son niveau.

— Oui, répondit le leader. Maintenant, plus qu'à prier pour qu'ils nous laissent entrer et qu'on ne se fasse pas abattre à vue.

— Tu crois qu'ils seront accueillant ? questionna Zarya. Ma mère disait que les technophiles sont tous des fous furieux sanguinaires.

— Et ta mère était persuadée que les moknas ne lui voulaient aucun mal, asséna sèchement Régis.»

La jeune femme fit une moue blessée et lui tourna le dos.

« Pardon, je ne sais pas ce qui m'a pris. Ce que je veux dire...

— C'est qu'on ne peut pas se fier aux histoires qu'on nous racontait, le coupa Zarranck. Enfin, vous avez vu cette merveille ? Ça n'a rien à voir avec les immondices qu'ils nous décrivaient dans les cours d'histoire.»

Ils traversèrent la distance qui les séparait de l'entrée du bunker le plus proche en discutant des différentes visions qu'ils avaient de la chose, de ce qu'ils attendaient de l'autre côté des portes blindées, et de comment ils devraient se présenter. Le trajet fut plus long qu'ils ne s'y attendaient, trompés par les dimensions gargantuesques des constructions. Le soleil entamait son déclin quand ils atteignirent finalement la lourde porte en fer.

L'entrée était perdue au milieu du bois qui poussait au pied de la butte de béton. Ils auraient eu beaucoup de mal à la trouver si l'ouverture ne faisait pas trente mètres de haut. Ils commencèrent par étudier l'entrée depuis le couvert des arbres, guettant le moindre piège ou un système de sécurité qui s'activerait pour les carboniser. Après plusieurs minutes, aucun d'eux ne remarqua quoi que ce soit de dangereux, alors Peter s'avança à découvert, les deux mains en l'air. Il se dirigea droit vers une demi-sphère de verre enfoncée dans le mur.

« Bonjour, s'annonça-t-il. On voudrait parler. On n'est pas des naturalistes. Vous... Vous pouvez nous ouvrir ?»

Une nouvelle minute s'écoula sans que rien ne bouge. Zarranck sortit à son tour de la protection des arbres pour rejoindre son ami qui baissa ses bras endoloris. Un par un, les renégats les rejoignirent, curieux de l'absence de réponse de la part des habitants de l'endroit.

« Et maintenant quoi ? demanda Zarranck.

— Peut-être qu'ils ne regardent pas la caméra en permanence ? Si je me souviens bien du manuel, il doit y avoir un interphone pour leur signaler notre présence.

— Tu es sûr que c'est dans notre intérêt ? Zarya s'avança. S'ils n'ont pas quitté la planète après tout ce temps, c'est qu'ils n'en ont pas envie. On ne devrait pas plutôt profiter du fait qu'ils ignorent notre présence pour s'infiltrer et le dérober ?

— Elle n'a pas tort, acquiesça Zarranck. Peter, trouve le moyen d'ouvrir la porte sans les alerter si possible.

— Je vais voir ce que je peux faire.»

Il se rapprocha du mur et examina une plaque de métal sur laquelle figuraient plusieurs boutons, ainsi qu'une plaque noire séparée en neuf section par de fines lignes bleutée. Après un moment d'hésitation, il apposa sa main dessus. Une lumière turquoise s'alluma sur la plaque d'encre, balayant de haut en bas. Après trois aller-retours, l'entièreté du carré s'illumina en rouge et un texte apparu au dessus.

« EMPREINTE NON RECONNUE. VEUILLEZ RÉESSAYER.»

Peter grimaça, conscient des conséquences désastreuses que son erreur pouvait déclencher. Mais après un instant, la plaque vira au vert et les verrous de la porte s'ouvrirent un à un, avant que l'immense bloc de métal ne coulisse, révélant derrière un couloir tout aussi gigantesque, plongé dans le noir. Un nouveau message s'afficha sur la console.

« MESURES D'URGENCES DÉCLENCHÉES. SÉCURITÉ DÉSACTIVÉE.»

« Qu'est-ce que ça veut dire ?

— Peu importe, on peut entrer, c'est ce qui compte.»

Ils pénétrèrent dans le bunker, suivant Zarranck qui avait pris la tête. Aucun d'entre eux ne resta suffisamment longtemps devant l'écran pour voir que le texte se mis à coulisser, révélant la fin du message.

« DANGER BACTÉRIOLOGIQUE LÉTHAL.»