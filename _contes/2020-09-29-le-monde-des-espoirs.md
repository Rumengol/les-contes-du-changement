---
bg: "le-monde-des-espoirs"
layout: post
title: Le monde des espoirs
summary: Un voyageur dimensionnel débarque sur un monde à l'augure désespérée.
source: “The God of Despair” par Adrian Biłozór
ref: https://www.artstation.com/artwork/LbB90
date: 2020-09-29
categories: posts
tags: ["Alcyerna"]
type: Conte
author: Rumengol
lang: fr
---

Cette dimension avait quelque chose d'effrayant. Et de fascinant. La première fois qu'il l'avait atteinte, en vagabondant au hasard dans l'éther, il s'était immédiatement senti mal à l'aise, sans être capable de l'expliquer. Les êtres qui y vivaient étaient mornes, des automates de chair aux yeux éteints. Pourtant, les nombreuses ruines qu'il avait traversées devaient autrefois être majestueuses. Ce monde était silencieux, affreusement silencieux. Aucun animal ne criait, aucun vent pour agiter les arbres rouges dont aucune feuille ne tombait pour crisser sous un pied. Rien que du silence.

Il n'était resté que quelques heures, il lui fallut pourtant des jours pour se remettre de cette expérience. À son retour, il s'était cloîtré chez lui, la vie paisible de sa dimension d'origine semblant tout à coup tonitruante. Sans en parler à personne, il était reparti dans cette dimension mystérieuse. Là, il retrouva le village sans vie. Les habitants au visage creusé s'étaient pour la plupart pendus, d'autres se laissaient dépérir, étalés au milieu de la route. L'état de ces pauvres hères était désespéré. Il avait abandonné les corps gisants, résistant à l'envie croissante de se rouler en boule et de pleurer.

Il comptait ses pas, au début, pour s'occuper dans le paysage monotone des arbres rouges. Lorsqu'il perdit le compte, il réalisa à quel point ses pieds le suppliaient de s'arrêter. Il ne reconnut pas le paysage. Il n'y avait là que des arbres rouges épars, et cette terre rougeâtre à perte de vue. Il faisait encore jour. Il n'avait jamais fait nuit non plus. S'effondrant presque, il se demanda ce qu'il faisait là, ce qu'il espérait accomplir. Rien, de telles notions n'avaient plus cours sur cette dimension, contrairement à la faim. Il songeait à retourner sur Alcyerna, lorsqu'il ressentit un chamboulement dans l'équilibre de la dimension, comme si toute une partie de son énergie venait de disparaître.

Ignorant la faim qui le tenaillait, il se remit en route, puisant dans ses réserves de volonté pour ne pas s'arrêter. En arrivant à l'endroit de la perturbation, il ne vit rien, si ce n'est les feuilles écarlates qui formaient un tapis de sang sur la plaine, et au loin, une étendue d'eau trop sombre pour la luminosité ambiante. Son mal-être s'accentuait à chaque pas, son corps était à bout, seule la force de sa volonté lui permettait désormais d'avancer. Le ciel semblait s'assombrir, de noirs nuages couvrant bientôt jusqu'à l'horizon. Puis enfin, grâce aux jeux d'ombre et de lumière des rayons qui perçaient les nuages, il le vit.

Sa vision suffit à le faire tomber à genoux. Une divinité au corps brumeux indiscernable était assise en tailleur dans le ciel. Il s'était déjà confronté à des dieux, certains plus imposants encore, mais ce qu'il avait devant lui était différent. Son abdomen et son crâne lunaire étaient transpercés comme par une lance aux proportions démesurées. Il portait des épaulières de chair en forme d'arc de cercle, lacérées et sanguinolentes, à l'image de son être de fumée. Des branches mortes sortaient de chaque orifice, comme si une vie avaient tenté de se développer à l'intérieur de cet organisme dégénéré, puis de s'extraire désespérément, avant de céder. Il faisait soudainement très froid.

Les mains du dieu pendaient nonchalamment sur ses jambes, pointant l'homme comme par coïncidence. Il n'entendait même plus sa respiration, tout comme il ne ressentait plus son cœur battre à la chamade. Dans un mouvement qui sembla durer une éternité, la déité leva son bras gauche jusqu'au ciel, par delà les nuages. Le mortel réduit à sa plus simple condition était incapable d'esquisser le moindre mouvement, les larmes inondaient son visage sans qu'il puisse les contrôler. Il tenta désespérément de quitter cette dimension, pour aller autre part, n'importe où ; mais c'était sans espoir, ses pouvoirs étaient bloqués.

Une traînée de fumée noire descendit du ciel. Des heures s'étaient déroulées sans la moindre activité, et soudain une lance de fumée noire était descendue si vite et l'avait transpercé sans qu'il ne puisse se défendre.

Non ? La fumée n'était pas descendue du ciel, elle s'échappait de lui, attirée par ce que le dieu portait par delà le plafond nuageux. Et avec elle sa vie, ses souhaits, ses souvenirs. Peu à peu il perdait tout, son essence était absorbée, ou plutôt elle le fuyait. Cette situation était sans espoir, il le savait. Il ne chercha pas à se débattre, ou à fuir. Il en avait pourtant le pouvoir. Même scellé sur cette dimension, il était assez puissant pour abattre une divinité.

Mais sans espoir, à quoi bon se démener. Rien de ce qu'il puisse faire aurait jamais un réel impact sur la marche de la réalité. Il mourra, comme tout le monde, lorsque son temps viendra. Nul n'est éternel, et tout le monde est remplaçable. Le cycle de la vie et de la mort est tout autant le fardeau de la plus éphémère des mouches que des dieux soi-disant éternels. Après eux, un autre se substituera, ni pire ni meilleur, une autre existence inutile qui ne laissera aucune trace dans la trame des événements. L'espoir pousse en avant les existences et leur donne l'illusion qu'ils importent. Personne n'importe, surtout pas lui. Une coquille vide, sans passé, sans avenir, errant sur une dimension perdue parmi tant d'autres. Le lot de tous, qu'ils le réalisent ou non.
