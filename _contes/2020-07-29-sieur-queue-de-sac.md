---
bg: "sieur-queue-de-sac"
layout: post
title: Sieur Queue-De-Sac
summary: Le champion du peuple des souris combat leur ennemi endémique.
source: “...For the Fallen”, par Vance Kovacs
ref: https://www.artstation.com/artwork/qABawP
date: 2020-02-09
categories: posts
tags: ["Les Divinépées"]
type: Conte
author: Rumengol
lang: fr
---

Sieur Queue-De-Sac s'arrêta un temps en atteignant le sommet du mur. Il avait déjà accompli un si long chemin pour le salut de son peuple. Tous, sous terre, comptaient sur lui. Il n'était pas le plus fort ni le plus menaçant des chevaliers, mais il était brave et habile. Là où Godeffroy Dos-Gras s'était fait repérer par un rapace en quête de déjeuner, là où Dame Vive-Queue avait chu lorsqu'une pierre s'était détachée, lui avait triomphé de l'ascension. Et là où Sieur Courte-Lame s'était fait happer par leur ennemi endémique, lui triompherait aussi.

Trop longtemps le seigneur du château avait cru qu'il pouvait dominer et asservir les peuples rongeurs. Naïf, ou trop confiant qu'il était, il s'était déjà fait surprendre. Courte-Lame lui avait entaillé l'œil. Pas assez pour l'éborgner, mais suffisamment pour créer une faille. Et lui, Sieur Queue-de-Sac, l'utiliserait pour asséner un coup fatal à la bête. Équipé des gants de son père et de la cape de sa mère, en ses mains le septième aiguillon et en son cœur la bravoure, il se sentait capable d'abattre même des montagnes s'il le fallait.

Il dégaina sa lame et la posa verticalement au sol. La flaque d'eau limpide devant lui était l'un des sanctuaires de son peuple. Il pria les anciens de lui accorder leur force. Le vent qui hurlait dans ses oreilles en aurait fait vaciller plus d'un, mais sa détermination était inébranlable. Ses yeux perdirent leur habituelle bonhommie. Il n'était plus que volonté et concentration. La patience était la clef de sa victoire. Il n'en manquait pas.

La bête arriva enfin, marchant nonchalamment en contrebas du mur, comme si le monde lui appartenait. Ce moment, le combat final était enfin venu ! Sieur Queue-De-Sac sentit l'adrénaline affluer dans ses veines. Il prit quelques pas d'élan et s'élança dans le vide en hurlant la rage de son peuple.

"Pour ceux qui sont tombés !!"

Ce fut là sa première erreur. Le seigneur Cha l'entendit de ses sens affûtés et il esquiva au tout dernier moment la lame qui visait sa pupille verticale. Le chevalier myomorphe se rattrapa in extremis, le septième aiguillon parvenant à se planter dans la peau pourtant épaisse de son adversaire. Il ralentit et échappa de peu à une chute bien plus tragique. L'immense créature feula de surprise plus que de douleur, mais déjà son agresseur s'était ressaisi et entreprenait l'escalade de ses poils touffus pour atteindre le point faible, l'œil d'Achille. Le Cha se grattait, essayant de déloger l'importun qui osait le déranger dans sa balade. Il le toucha plusieurs fois, le griffant douloureusement, l'éventrant presque, mais sans jamais réussir à s'en saisir.

Enfin, le petit guerrier était perché sur la tête de sa Némésis. Il eut alors une idée. Levant bien haut sa lame, il l'abattit pointe en avant sur le crâne de l'animal, qui broncha à peine. En revanche, il secoua vigoureusement la tête, forçant Sieur Queue-De-Sac à s'accrocher de toutes ses forces. Décidément, la peau du seigneur était bien trop épaisse pour que même le plus pointu et aiguisé des aiguillons ne parvienne à la percer. Qu'il en soit ainsi, se dit-il. Il était à l'œil. Celui-ci le fixait avec agacement et ennui.

Piqué au vif par ce désintérêt vexant, le chevalier partit à l'assaut de toute son âme. Dents, aiguillon, griffes. Tout était bon pour élargir la fine ouverture de Sieur Courte-Lame. Quand enfin une goutte d'eau s'en échappa. Sieur Queue-De-Sac eut la certitude qu'aucun de ses prédécesseurs n'était mort en vain. Sans réfléchir plus, il s'élança dans cette bille aqueuse et l'écorcha si bien qu'elle se déchira. Le seigneur Cha se pliait de douleur, cherchant à poser sa patte griffue sur son œil autant pour limiter l'épanchement que pour attraper ce satané rongeur, mais trop tard.

Le héros avait atteint son cerveau, et son aiguillon l'avait tailladé encore et encore. Dans un ultime sursaut désespéré, la bête convulsa avant de s'effondrer, morte. Il était victorieux. Plusieurs liquides visqueux collaient à ses poils et du sang s'écoulait des plaies à son flanc, mais elles importaient peu. Seule comptait ce soir la victoire du peuple des souris, et la défaite du pompeux Cha. Il serait acclamé et admiré pour des générations, un sanctuaire lui serait même dédié. Sans un regard pour la dépouille, il prit le chemin des souterrains.
