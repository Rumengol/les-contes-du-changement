---
bg: "la-croisade"
layout: post
title: La Croisade
summary: La volonté de Dieu s'accomplira à travers l'espace et le temps.
source: “Journey On”, par Mike Winlemann
ref: https://www.beeple-crap.com/
date: 2020-02-06
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---

La colonne de chevaliers progresse dans un silence absolu. Tels des fantômes, les croisés avancent, imperturbables, vers la prochaine croisade. Aucun trait de peau n'est visible. Des armures mobiles, habitées uniquement par le fanatisme religieux et leur foi. Leurs yeux sont les fentes de leur casque, leurs mains des épées. Nul ne se plaint, nul ne faiblit. Ils évoluent dans la voie de la foi, jalonnée par les statues des templiers tombés au combat. Chaque traversée est plus longue que la précédente. Mais le temps n'est pas un obstacle. Ils taillent leur chemin dans le cœur des mondes païens jusqu'à la terre promise, le Paradis. Derrière eux, Oustamor n'est plus que feu et cendres.

Un millénaire à peine s'écoule. Un nouveau monde se dessine déjà à l'orée du couloir divin. Pour la première fois depuis 100 ans, la marche invariable de l'armée faillit. Tous s'arrêtent, déposent leurs armes et joignent les mains. Ils prient Dieu de leur donner force et courage pour la tâche qu'ils s'apprêtent à accomplir. Ils ne demandent aucune repentance pour leurs péchés, car ils savent qu'ils sont justes. Et si Dieu leur en donne la force, alors ils exécuteront la volonté céleste sur cette nouvelle planète.

Les hérétiques osent renier l'existence du Tout-Puissant. Ils possèdent une technologie si puissante qu'elle est capable de désintégrer la matière, et pourtant ils ne sont rien face à la volonté de Dieu. Les croisés se battent avec une agilité surnaturelle, et leurs lames effilées tranchent des parois dures comme du diamant. Chaque bâtiment purifié est surmonté de la croix rouge, pour que Dieu miséricordieux accepte la repentance des âmes infidèles.

Nombreux sont ceux qui implorent la merci de la Croisade, hurlant leur repentance et leur abjuration à leurs croyances hérétiques. Ce ne sont là qu'opportunistes, relaps en demeure. Ces êtres ne connaissent que le mensonge scientifique, et même à l'aune de leur mort sont incapables d'abjurer avec sincérité. Deux templiers faillissent. De temps à autre, lors des épurations, la foi de quelques croisés vacille. La plupart se reprennent vite et se flagellent pour expier leurs doutes, mais certains apostats refusent d'abattre leur épée, et renoncent à leur foi. Seule l'épée divine d'un autre templier peut alors les absoudre. En détachant leur tête de leur corps, leur âme  s'élève, abandonnant ses péchés au sol.

La tête de l'hérésiarque est plantée sur un pic, avant que le feu purificateur ne nettoie ce monde souillé. La Croisade repart sur le chemin de la foi, une autre planète purgée de son hérésie. La justice du Très Haut est rendue. Pas un mot n'est prononcé, chaque vœu de silence respecté. La lugubre procession reprend, le sang disparaîtra de lui-même car le Père de tout homme ne saurait les laisser ainsi souillés.

Un nouveau millénaire s'écoule, rythmé par les pas de la Croisade. Io n'est plus qu'un vague souvenir. Un nouveau monde se dessine déjà à l'orée du couloir divin. Une planète familière, quelque chose en la forme de ses continents fait remonter des souvenirs enfouis. La Terre n'est en rien celle qu'ils ont laissé longtemps auparavant. Si ses habitants ont à nouveau perdu la foi en le Dieu Tout-Puissant, leur sort sera d'être purifié. Ô Dieu, guide mon bras et j'accomplirai ta volonté !
