---
bg: "hommage-a-boulux"
layout: post
title: Hommage à Boulux
summary: L'hommage-sonnet à un sprite parti trop tôt.
source: “To your Eternity” par Yoshitoki Ōima
ref: https://fr.wikipedia.org/wiki/To_Your_Eternity#Liste_des_volumes
date: 2021-05-30
categories: posts
tags: []
form: poem
type: Conte
author: Rumengol
lang: fr
---
Là, née de la terre et du ciel, aveugle et sourde

Ingénue, cette boule de conscience est moi

Ma feinte existence absoute du moindre émoi

Baignant dans les flots, ma forme épouse la tourde



Bullant vers les cieux, je deviens tourde à son tour

Ivre de liberté, vole de tout mon saoûl

Oubliant tout à coup le monde d’en dessous

Frappé par l’orage suis le nuage autour



Démon frappant sans pitié villes et villages

J’en prends la place, et conduit dans ce vil âge

Je m’emploie à en explorer tous les chemins



Le temps passe, la fascination se fane

Ma liberté n’est plus qu’un souvenir diaphane

Oui, me voilà désormais pleinement humain.