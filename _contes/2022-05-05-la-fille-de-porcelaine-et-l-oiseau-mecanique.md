---
bg: "la-fille-de-porcelaine-et-l-oiseau-mecanique"
layout: post
title: La fille de porcelaine et l'oiseau mécanique
summary: Une fille de porcelaine est réparée dans la douleur sous le rire moqueur d'un oiseau automate.
source: “Kintsukuroi” par Nekro
ref: https://twitter.com/NekroXIII/status/1521806209501827075/photo/1
date: 2022-05-05
categories: posts
tags: ["Et factum est machina"]
type: Conte
author: Rumengol
lang: fr
---
Enfermée à l'intérieur d'un minuscule jardin, il y a cette fille qui ne fait rien. Elle est immobile, son regard tourné vers l'intérieur. Elle aimerait parler, cette fille, mais sa bouche est fendue. Elle souhaiterait embrasser son existence, mais elle n'a pas de bras. Elle rêve de marcher loin de cet endroit, mais elle ne sait faire obéir ses jambes de bois. Ses yeux larmoyants ne révèlent rien de son âme, piégée à l'intérieur de son enveloppe de porcelaine. Malgré sa carapace brisée, elle ne peut s'envoler. 

Dans ce beau jardin, il y a aussi un oiseau mécanique. D'airain et de cuivre, il moquait la pauvre fille. Elle était si grande, disait-il, qu'il lui fallait plusieurs tours de clefs pour atteindre son front craquelé. Et pourtant elle était clouée au sol, elle qui n'avait pas d'aile. Et lorsque par son silence elle manifestait de l'agacement, il se moquait de plus belle. 

— Comment veux-tu m'attraper, toi qui n'as pas de bras ? Comment veux-tu me dire d'arrêter, toi qui n'as pas de mot ? 

Le volatile la tourmentait de ses mots blessants pour tromper l'ennui. Il ne se taisait que lorsque son créateur pénétrait le jardin. Elle ne l'avait jamais vu, mais l'oiseau l'appelait « le maître » avec déférence. De lui, elle ne connaissait que les mains. Ces grandes mains calleuses, marquée par le labeur. Elles étaient souvent humides, charriant la poussière que l'eau n'avait su séparer de la peau. Elles se déplaçaient sur son corps avec délicatesse, comme s'il craignait de la blesser en posant sa paume plus franchement. 

Elle avait appris à craindre ces mains, qui l'avaient dérobée à son propre jardin. Leur toucher grossier, avec toutes les imperfections qui étaient tout ce qu'elle ressentait. Les séances de tortures qu'elles amenaient chaque fois, avec elles. Les blessures encore mal refermées étaient rouvertes pour être comblées par des morceaux qui ne seraient jamais elle. Elle pouvait ressentir la résine qu'il injectait dans ses failles et qui la brûlait. 

Puis elle avait appris à aimer ces mains, lorsqu'elles la confortaient. Alors que le liquide visqueux séchait et colmatait les brèches en se faisant une place non désirée, elle souffrait le martyre. Son seul réconfort venait de ce toucher qui devenait soudain si compatissant, comme si elles comprenaient ce qu'elle subissait et souhaitaient se faire pardonner. Et la douce fille leur pardonnait, à ces mains contraintes d'agir contre leur volonté. Mais son ressentiment envers leur propriétaire ne faisait que grandir. 

Elle guérissait lentement. Ses épaules étaient devenues assez larges pour servir de perchoir à l'oiseau de malheur. Il ne cessait ses moqueries, martelant de vilains mots à l'oreille à demi brisée de sa victime impuissante. Parfois il rentrait en elle, par l'une des ouvertures que la résine n'avait pas colmatées. Il mimait de la picorer, puis s'en allait, savourait sa liberté et l'étalait devant la prisonnière du minuscule jardin. 

Les nuits étaient paisibles. L'oiseau mécanique ne lui adressait pas la parole le soir venu. Sans doute dormait-il. Peut-être son maître si adoré l'emmenait voir d'autres filles de porcelaine à tourmenter. Au matin, il revenait. Ne quittait-il jamais cet endroit, au final ? Sa voix nasillarde s'élevait sans qu'elle n'entende l'insupportable ronronnement de ses ailes. 

Un jour, les mains caressèrent sa tête avec insistance. Elle avait craint ce moment. Celui où son corps ne suffirait plus. Les doigts humides glissèrent sous les courbes élégantes de son visage jusqu'à sa bouche fendue. Ils remontèrent jusqu'à effleurer son œil gauche. Et enfin, ils tracèrent le contour de son crâne ouvert. Elle aimait cette ouverture, qui donnait un peu de lumière à ses yeux. Elle voulait la conserver. Elle ne voulait pas être plongée à nouveau dans le noir. Mais que pouvait-elle faire, elle qui était incapable de se débattre ? 

Il s'en prit d'abord à sa bouche, l'emplissant de résine. Son goût était immonde et tapissait les moindres recoins de sa gorge. Elle étouffait et sentait le mucus poursuivre son chemin jusque dans son ventre. Il ne prit qu'un mouchoir pour éliminer le surplus. Seulement ce qui se voyait. Il n'avait peut-être même pas conscience d'en avoir versé autant. Sa lèvre fendue allait guérir à son tour, il lui fallait simplement du temps. 

Il se saisit ensuite de ses yeux, de pauvres billes portant toute sa souffrance, et les avait fait rouler dans leurs orbites. Le processus avait été intolérable, mais était encore bien peu de choses en considération de ce qui l'attendait. La résine coula, perçant son œil gauche de part en part. La vue encore brouillée, elle discerna à peine la pièce d'argent qu'il avait adapté à la forme de son crâne. La quantité de laque nécessaire pour maintenir un fragment de cette taille dépassait ce à quoi elle s'était résignée. 

Ses yeux voyaient, mais sa vue n'était que douleur. Pendant un jour entier, elle ne ressentit que ça. Même le babillage incessant de l'oiseau ne l'atteignait plus. Ensuite la douleur s'estompa, comme chaque fois. Elle voyait le monde, pour la première fois depuis un temps dont elle ne se rappelait pas. Les couleurs lui avaient tant manqué. Elle fut même heureuse de pouvoir enfin distinguer son tourmenteur. 

C'était un automate mécanique, pas bien grand, de la couleur du bronze, bien qu'il clamât être d'airain et de cuivre. Son bec était acéré et décoré de porcelaine, de tous ces morceaux dérobés à la fille immobile. Malgré ses ailes battant pendant son vol, ses déplacements semblaient mus par l'étrange vibration qui le parcourait continuellement. Une clef à ressort était ancrée dans son dos, le manche tournait lentement comme pour le maintenir en mouvement. Il s'arrêta au niveau de ses pupilles, stupéfait que ces billes auparavant sans vie soient capables de le suivre. 

— Comment t'appelles-tu ? 

Était-ce là sa voix ? Elle était délicate et cristalline, coulait dans l'air comme un fin filet d'eau brillant. Elle se réfléchissait sur les murs et résonnait magnifiquement, les enveloppant tous deux dans une vibration mélodieuse l'espace d'un instant. La magie s'envola aussi vite qu'elle était arrivée, le son s'était épuisé à force de rebondir et mourait doucement au sol. 

Le volatile avait reculé jusqu'à l'autre bout de la pièce dans sa surprise. Ses paupières mécaniques clignèrent plusieurs fois dans un mouvement qui n'avait rien de naturel avant qu'il ne regagne son calme. Puis il se rapprocha d'elle avec curiosité. 

— Le maître m'appelle Tidi. Et toi, qui me suis enfin de tes yeux. Et toi, qui parles enfin. Quel est ton nom ? 

— Je l'ignore. Je ne sais pas si j'ai un nom. 

La douce mélodie emplit à nouveau l'espace, et à nouveau l'oiseau vacilla. 

— Trouve-t-en un. Ou mieux encore, laisse au maître cet honneur. 

— Alors je serais Katia. 

— Katia-qui-voit-et-qui-parle, mais qui ne se meut toujours pas. Dis-moi Katia, comment est-ce la vie clouée à son propre corps ? 

— Arrête, je te prie. 

— Tu as peut-être des mots, mais comment veux-tu me faire taire, si tu n'as pas de bras ? 

Le maître entra peu après et Tidi se tut. Il ne s'exprimait jamais en sa présence, sûrement que sa voix nasillarde insupportait son propre créateur. La fille de porcelaine ne dit mot, et tourna d'elle-même ses yeux vers l'intérieur. Elle ne souhaitait pas le voir. Elle refusait que son image soit celle qui marquerait son esprit. Celle de l'infect Tidi était amplement suffisante. 

La nuit, l'oiseau mécanique, qui avait accompagné son précieux maître, revint. Il faisait déjà noir au dehors, mais Katia profitait de chaque image que ses yeux voyaient. L'automate se hissa dans un perchoir, presque hors de vue de la fille de porcelaine, puis s'affaissa dans un claquement métallique. Depuis qu'elle voyait, il ne cessait de voleter partout, ne se posant que pour de très brefs instants. Et voilà qu'il restait coi de longues minutes. Elle remarqua quelque chose chez son compagnon endormi. La clef à ressort dans son dos était elle aussi parfaitement immobile. 

Les semaines passèrent, chaque séance de torture étant un peu plus supportable que la précédente, entrecoupées de querelles avec l'oiseau mécanique. Peu importait à quel point Katia était en colère, sa voix restait égale, douce et chaleureuse. Elle se sentait plus entière, aussi. L'homme en avait terminé avec son buste, et commençait à assembler ses bras. Une tâche aussi douloureuse qu'elle avait été délicate, au vu des innombrables rainures dorées qui parcouraient le premier membre qu'il fixa. 

Avait-elle déjà su ce que c'était que d'avoir des mains ? Elle ne pouvait encore saisir que l'air, mais son bras bougeait avec tant d'élégance qu'il lui paraissait grotesque d'avoir pu exister sans. Lorsque le second fut fixé, elle se sentit presque entière. Pour la première fois depuis qu'elle avait ouvert les yeux elle ressentit de la reconnaissance. Le maître l'avait longuement enlacé alors, et malgré la résine qui séchait en lui griffant la peau, elle lui avait rendu son étreinte. Il avait pleuré, alors. Et elle avait serré plus fort. Ils étaient restés figés ainsi un long moment avant qu'il ne se dérobât à l'étreinte. 

Après cela, il cessa de venir pendant plusieurs jours. Elle aurait pu se sentir seule, si Tidi ne lui était pas resté aussi fidèle. Elle ignorait ce qui était le pire. Au moins, ses nouveaux bras encore maladroits lui permettaient de chasser l'impudent lorsqu'il prenait trop ses aises. Il se montrait moins cassant, ces derniers temps. Presque amical, par moments, comme s'il cherchait à se rapprocher de la fille de porcelaine. 

Puis, un jour, il débarqua dans le minuscule jardin en caquetant plus fort qu'il ne l'avait jamais fait. Elle l'attendait. 

— Katia, c'est terrible ! Le maître a succombé ! 

Il n'avait pas prêté attention à sa trajectoire, et s'était dirigé droit sur elle. Ses deux mains délicates se refermèrent sur l'armature de métal de l'oiseau, coupant court à ses élucubrations. Il tenta de se débattre, piailla à tout rompre, jusqu'à ce qu'elle retire la clef à ressort de son dos. D'un coup, il s'arrêta de bouger, et l'éclat dans ses yeux disparu. Il avait enfin cessé de parler. Et il était à sa merci. 

Avec application, un jour durant, elle étudia les ailes de Tidi. Les plumes de métal s'imbriquaient dans un mécanisme complexe, qui portait en son centre un cube qui semblait régir le tout. La fille de porcelaine retira une à une les fines plumes coupantes comme des rasoirs. Plusieurs de ses doigts étaient sévèrement entaillés lorsqu'elle eut fini, mais une douleur si bénigne n'avait plus d'emprise sur elle. Elle conserva les petites lames précieusement sur la table à son côté. Puis elle inséra la clef dans l'oiseau mécanique, et la tourna. 

Sitôt qu'elle lâcha la poignée, Tidi revint à la vie. Ses paupières exécutèrent le même clignement dérangeant, puis il réalisé qu'il se trouvait dans la paume de sa victime de porcelaine. Il chercha ses mots, écarta ses ailes pour prendre son envol, mais elles se refermèrent sur le vide. Ce n'était désormais plus que des cadres métalliques contrôlés par d'habiles mécanismes, bien incapable de soulever même le plus léger des oiseaux mécaniques. Il se figea dans son mouvement et son regard se planta lentement dans celui de la fille, qui esquissait presque un sourire. 

— Comment veux-tu voler, toi qui n'as pas d'aile ? 

— Katia, ne te rends-tu pas compte ? Le maître est... 

— Ce maître qui n'a été pour moi synonyme que de souffrance et de peine ? Tu ignores comme sa mort me réjouit Tidi. Mais t'avoir au creux de ma main est sans doute le plus beau cadeau qu'il puisse me faire. 

— Ne comprends-tu rien, stupide porcelaine ? Il te soignait. Jour après jour, il t'appliquait un remède qui pour lui était un poison. Il se sentait tellement coupable qu'il a négligé sa santé pour te faire revivre, jusqu'à ce que cela lui coûte la vie. 

— Il pensait sans doute que je le pleurerais comme tu le fais. Mais j'ignore tout de son visage et de lui je ne connais que les mains. De toi en revanche je sais tout. Puisque je ne peux marcher, et que tu ne peux voler, il ne nous reste qu'un long moment, seuls, dans ce jardin. 

Elle retira à nouveau la clef. Parfois, elle s'amusait à ne faire qu'un quart de tour, et se délectait de la panique qui s'emparait de l'oiseau pendant son court temps d'éveil. À d'autres, elle lui retirait le bec, ou les yeux, et moquait son impotence. Elle apprit que Tidi devait être remonté régulièrement, ou son intégrité se perdait. Alors elle espaçait ces instants de réveil toujours un peu plus. Il lui plaisait de voir l'insupportable jouet perdre l'esprit et devenir tour à tour fou, misérable puis incohérent. 

Dans une maison reculée on pourra retrouver la dépouille d'un homme intoxiqué, dans son atelier délabré se tiendra une fille de porcelaine couverte de rainures d'or, et déposé dans l'une de ses mains le cadavre mutilé d'un oiseau mécanique. 

 