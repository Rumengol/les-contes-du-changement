---
bg: "un-conte-pour-enfants"
layout: post
title: Un conte pour enfants
summary: Rien d'autre qu'un simple conte.
source: “1476” par Yuri Hill
ref: https://www.artstation.com/artwork/dOV3gJ
date: 2022-05-17
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---
Mon père m'a toujours interdit d'aller dans le marais. Il y rôde, paraît-il, un monstre abject qui dévore les enfants, et en particulier les petites filles. Enfant, le monstre m'effrayait. Je croyais voir sa silhouette moribonde dans l'ombre des arbres tortueux qui marquaient l'entrée du marais. Lorsque j'en faisais des cauchemars la nuit, il s'asseyait au bord de mon lit et me réconfortait. Il m'assurait que la bête ne sortait jamais du marécage, qu'elle résidait en son cœur. Puis il dévoilait un cadeau, acheté pour moi pendant son dernier voyage. Il paraissait en avoir une quantité intarissable, car à chaque nuit tourmentée j'obtenais un nouveau jouet. 

En grandissant, j'ai appris à modérer mes peurs. Bien sûr, l'idée même d'approcher ces marais me terrifiait toujours, mais mes nuits échappaient à ces peurs. Plus exactement, d'autres vinrent les remplacer. La maladie de ma mère qui empirait de jour en jour, et la guerre approchant qui empêchait le médecin de se rendre jusqu'à notre village. Nous avions encore des remèdes, mais ils se limitaient à ralentir le mal qui la rongeait. 

Mon père avait cessé ses activités de marchand et passait le plus clair de son temps à son chevet, m'aidant parfois à tenir la boutique. Il se lamentait souvent du triste sort qui avait affligé une herboriste d'un mal incurable par ses décoctions, et je ne pouvais qu'abonder en son sens. Nous faisions aussi bonne mesure que possible, mais sans les voyages réguliers de mon père, notre réserve d'herbes se faisait mince. Le reste du village nous aidait comme ils pouvaient, mais les temps étaient durs pour tout le monde, et je m'aventurais de plus en plus loin dans la forêt lors de mes cueillettes, m'approchant parfois dangereusement du marécage. Mais j'arrivais à un âge où je ne croyais plus aux contes pour enfants, et la santé de ma mère prenait le pas sur la mienne. 

Puis la guerre arriva. Le transporteur qui nous livrait les remèdes régulièrement cessa ses trajets. Rien d'étonnant à cela, puisque la plupart des chevaux avaient été réquisitionnés pour l'effort de guerre. Notre village a été épargné car situé en dehors des routes conventionnelles, mais les hameaux voisins n'ont pas réchappé aux ordres de mobilisation. Pour une fois, notre absence de la moitié des cartes nous a été bénéfique. 

L'état de ma mère empira soudainement, alors qu'il ne nous restait plus qu'un dernier flacon de remède. Sans y repenser à deux fois, mon père enfourcha son cheval et s'apprêta à partir. Je voyais dans son regard qu'il n'était pas serein à cette idée, et ne pus m'empêcher de le questionner. 

— Où vas-tu ? lui ai-je demandé. 

— Chercher le médecin. Ou au moins des remèdes. 

— Est-ce sûr d'emprunter la route ? Les soldats... 

— Je ne vais pas prendre la route. Je vais couper par le marécage. Ça me fera deux jours de voyage en moins, et j'éviterai les soldats. 

— Et la bête alors ? Ne vas-tu pas entrer dans son territoire ? 

— Je dois te révéler quelque chose, Élina. La bête n'existe pas. Ce n'est qu'un conte que le village se transmet pour effrayer les enfants et les tenir écartés des dangers du marais. Et puis, ne t'inquiète pas, si jamais elle venait à se manifester, je sais me défendre ! 

Il avait flatté l'épée à son flanc et s'était retourné avant que je ne puisse dire un mot. Tout juste ai-je pu lui crier bonne chance, mais j'ignore s'il m'a entendu. Je me suis occupée de ma mère les jours suivants. S'il économisait deux jours de trajet, il n'aurait pas dû prendre plus de cinq jours à revenir, quand bien même le médecin se serait montré réticent. À la fin de la semaine, j'ai commencé à m'inquiéter. Mais j'avais confiance en mon père, alors je patientais deux jours de plus. 

Puis l'attente devint trop forte, et notre dernier flacon de remède était vide. Quelque chose lui était arrivé, c'était certain. Peut-être avait-il fait une mauvaise rencontre dans le marais ? Et si le monstre l'avait attrapé ? Forte d'une détermination que je ne me connaissais pas, j'ai réuni quelques amis braves, parmi ceux qui se vantaient d'avoir déjà exploré le marais, puis je suis partie à sa recherche. Je n'avais qu'une dague, mais je savais m'en servir. Les autres possédaient de réelles épées, Wolfrid avait même pris la lance de son père. 

Nous nous serions lancés à l'aveugle si Arthur n'avait pas de tels talents de pisteur. Même vieille de neuf jours, il était parvenu à retrouver la trace du cheval de mon père et à remonter sa piste. Il s'était enfoncé tout droit dans les méandres pestilentiels, suivant le chemin de terre vaguement tracé par les hasards du terrain. Le trajet fut silencieux. Même si personne dans notre petite équipée ne croyait réellement à la créature, son fantôme nous mettait tous mal à l'aise. 

Au bout du chemin, le sol redevenait solide. Nous n'avions marché que sur des bandes de sable, des rochers à fleur d'eau ou des troncs abattus jusque-là, c'était plaisant de pouvoir à nouveau fouler un véritable sol en terre. Nous déchantâmes aussitôt. Nous avions posé le pied sur un tertre macabre, où des dizaines de bustes humains étaient ligotés aux arbres par des cordes épaisses. La plupart étaient décharnés et anciens. Certains crânes avaient été défigurés, des bois de cerfs leur donnant un aspect loufoque, mais tous avaient eu la mâchoire inférieure arrachée. La peau tombante sur leur bouche les bloquait à jamais dans un cri de douleur muet. 

Jöert a rendu son déjeuner, et d'autres détournaient le regard en hoquetant de dégoût. Mais je n'avais pas le temps pour ça. Je scrutais chaque buste désacralisé, à la recherche angoissante de celui qui pouvait appartenir à mon père. La plupart étaient trop amochés pour être reconnaissables, mais je savais que je le retrouverais. Mon regard volait d'un cadavre à l'autre, ignorant le haut-le-cœur qui montait chaque fois que mes yeux se posaient sur ces amas de chair coiffés de bois ridicules. 

J'ai d'abord vu l'épée. Elle était tirée, la lame en partie recouverte de boue. Même à cette distance, je vis que du sang avait coulé dessus. Attaché à l'arbre au-dessus de l'arme, un torse tout autant mutilé que les autres, la mâchoire pareillement arrachée. Ses vêtements avaient tous été déchirés, à l'exception du petit pendentif que j'avais réalisé pour mon père à l'aide d'un coquillage qu'un vagabond m'avait un jour donné. Je ne sais pas comment la fragile ficelle avait enduré le supplice qui lui avait été infligé. C'était comme si le vicieux bourreau de mon père savait que j'allais venir, et avait tenu à ce que je n'ai aucun doute sur ce qu'il avait fait. 

Mes jambes me lâchèrent et mes genoux frappèrent le sol lourdement. Les pleurs étaient bloqués dans ma gorge, incapables de sortir. J'avais du mal à réaliser ce qu'il m'arrivait, mais mon corps le savait. C'est alors que l'un des autres cadavres, un de ceux avec les bois plantés dans le crâne, bougea, arrachant plusieurs cris de surprise. Mêlée aux autres cadavres, la créature était restée immobile depuis notre arrivée. L'atmosphère macabre de l'endroit nous avait fait ignorer l'évidence : l'un de ces cadavres n'était pas attaché à son arbre ! 

L'abomination s'est écartée du tronc, et nous avons pu constater l'étendue de son horreur. Elle était immense, haute comme deux hommes sur ses membres décharnés, et se déplaçait à quatre pattes. C'était impossible qu'une chose aussi imposante ait pu se dissimuler derrière un arbre aussi fin, et pourtant personne ne l'avait remarquée jusque-là. Sa tête était identique à n'importe quel autre crâne de ses victimes, mais le reste de son corps était anormal. Il lui manquait peau et chair, elle n'était qu'un sac d'os retenus ensemble par des muscles atrophiés et une quantité extravagante de tendons. Des os qui ne devraient pas être là sortaient de son corps à de multiples endroits, en particulier sur son dos. 

Elle resta là à nous regarder, bougeant à peine. Son abdomen qui ne semblait être fait que de muscles sanguinolents se gonflait et dégonflait à intervalles réguliers, comme si elle respirait. Mais c'était impossible, son cou exposé n'était fait que de vertèbres enrobées dans de la chair, et elle n'avait pas de réelle bouche. J'étais paralysée par la peur, aucun de mes membres ne me répondaient, puis ce fut au tour de mon esprit de devenir blanc lorsque je croisais ses yeux, deux orbites aussi vides que la mort. 

Je ne sais trop comment, mais son cri me réveilla. C'était un hurlement d'outre-tombe, qu'aucune créature vivante ne pouvait produire. Il m'a comme fouettée et je me suis remise sur pied instantanément. Mes compagnons avaient eux aussi réagi, mais pas de la même manière. Empoignant leurs armes, ils se sont précipités sur la chose qui les attendait. Pas moi. J'ai honte de le dire, et cette culpabilité me suivra jusqu'à ma mort, mais je me suis enfuie. Sans regarder derrière moi, même lorsque leurs cris de souffrance me sont parvenus. J'ai couru sans m'arrêter jusqu'à l'entrée du village où je me suis effondrée.  

De nombreux villageois sont venus à mon secours. Beaucoup d'amis et de parents me demandant pour leur fils. Je n'ai pas eu le cœur de leur dire tout de suite. Je me suis simplement évanouie. Ils apprirent par la suite, et les mères m'en voulurent. Les sœurs surent me pardonner, une fois leur deuil achevé. Elles comprirent jusqu'où j'avais été prête à aller pour sauver mon père, et auraient sans doute fait la même chose à ma place. Je n'ai rien dit à ma mère, le choc l'aurait tué. Elle s'est éteinte douloureusement, mais convaincue que son mari allait revenir pour la sauver. J'ai préféré qu'elle parte de ce monde pleine d'espoir. 

Quant à moi, j'ai noté tout ce dont je me souvenais de cette rencontre. Puis j'y suis retournée. Je l'étudie régulièrement, en restant à bonne distance. Elle m'étudie aussi, je crois. J'ai compris que tant que personne ne monte sur le tertre, elle n'est pas agressive. Le marais n'est pas son territoire, juste ce bout de terre. Je me demande si cette chose est une sorte de gardien, peut-être protège-t-elle une tombe ancienne, ou bien un trésor ? Pour être tout à fait honnête, je n'en ai cure. Mais si je parviens à le découvrir, je pourrais attirer ici des aventuriers, voire des soldats en capacité de la détruire. Ce monstre sait-il que j'œuvre à sa perte, et que chacune de mes visites le rapproche de son glas ? Je l'ignore. Mais je ne laisserais pas d'autres petites filles subir ce que j'ai vécu. 

 