---
bg: "le-jardin-des-saisons"
layout: post
title: Le jardin des saisons
summary: Les saisons se succèdent, le jardin des haikus est éternel.
source: “🍃”, par guweiz
ref: https://www.instagram.com/p/B7kesMInG4B/?utm_source=ig_web_copy_link
date: 2020-02-04
categories: posts
tags: []
form: poem
type: Conte
author: Rumengol
lang: fr
---

Pollen dans le vent

Cette quiétude troublée

Mes yeux sont fermés.

\*\*\*

Les rayons solaires

Me réchauffent corps et âme

Du travail, j'y vais.

\*\*\*

Pour qui ces pétales

Tombent-elles ? Ah, les voilà

Le souffle me guide.

\*\*\*

Qu'est-ce, neige ou sang ?

Le calme froid à nouveau

Sol souillé, ménage !

\*\*\*

Ce jardin divin

Fanera-t-il comme les autres ?

Non, je le protège.
