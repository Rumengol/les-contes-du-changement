---
bg: "sabotage"
layout: post
title: Sabotage
summary: Deux terroristes font sauter un pont
source: “Bridge” par Karl sisson
ref: https://www.artstation.com/artwork/RY8zxW
date: 2021-05-19
categories: posts
tags: ["A.I.D.E."]
type: Conte
author: Rumengol
lang: fr
---

**🝏** - Comme une toile tissée par une araignée invisible, les larges filaments de métal s’assemblaient consciencieusement. Non, c’était plutôt comme un blob dont le corps s’étendait, se contorsionnait et se déformait pour trouver la conformation la plus efficiente. L’immense entité d’acier était secouée de spasmes, comme si elle respirait paisiblement. A chaque inspiration, de nouveaux filaments étaient créés, d’autres gonflaient ou s’attachaient ensemble. Puis, venait l’expiration, où les mésappariement et les accidents étaient corrigés, les structures excédentaires éliminées.

Dans ces moments, il arrivait qu’une pièce de métal soit jugée inutile. Elle se détachait alors du complexe et chutait silencieusement pendant plusieurs secondes avant de provoquer un fracas phénoménal en touchant le sol. On pouvait rester des heures admirer le fascinant ballet organique, qui à chaque pulsation donnait un peu plus forme à la structure démentielle.

Des silhouettes se dessinaient sur le toit de la montagne ronflante, minuscules même face aux plus petits des filaments. Ils faisaient un travail périlleux, devant constamment veiller à ne pas être sur le chemin de la construction, dansant presque pour esquiver les nouvelles formations. Parfois, l’un des hommes était surpris et se retrouvait projeté à des dizaines de mètres de hauteur avant de retomber lentement au sol. Ces inconscients ne devaient leur salut qu’à leurs combinaisons hautement technologique, capable de leur sauver la vie même s’ils chutaient de la construction.

C’était encore difficile à dire, mais l’organisme en pleine effervescence serait, d’ici quatre mois, une hyperroute traversant les terres désolées pour relier les plus grandes métropoles. Le projet mené conjointement par Sericco et l’A.I.D.E. ambitionnait à terme former une frontière infranchissable entre les villes du Dominium et les camps rebelles qui s’était réfugiés dans les parties les plus inhospitalières de la planète.

“Saleté de métal vivant. D’ailleurs, c’est que du marketing, il y a rien de vivant là dedans. Des matrices de synthèse et des IA de contrôle soit-disant “optimisées”. Regarde moi ce gâchis.

&mdash; Oh, c’est toi qui me gâche le spectacle. Tu veux pas juste apprécier la vue ? On aura pas d’autre occasion, si tout se passe bien.

&mdash; Tu sais, de mon temps, on construisait des ponts avec du vrai acier, de vrais matériaux. Et on pouvait voir des ouvriers et des ingénieurs sur le chantier, pas des gardes surarmés.

&mdash; “De mon temps”, t’as quoi, trente ans ? Tu parles comme un vieux.

&mdash; Et toi gamine, tu devrais respecter un peu plus tes aînés. On devrait pas rester là, on commence à se faire remarquer.”

**🝏** - Lila sauta au bas de la structure de métal mort, suivie par Nael. Juste avant de s’écraser au sol, elle appuya sur sa ceinture qui ralentit instantanément sa chute. Elle se posa gracieusement, récupérant son lapin dans ses bras. Darren, plus prudent, avait entrepris de désescalader l’échafaudage, malgré son souffle court et la demi-heure de grimpe qui lui avait été nécessaire pour atteindre le sommet. D’en bas, elle pouvait entendre, entre deux fracas métallique, son compagnon râler. Contre la technologie, contre la folie des grandeurs des hommes, contre elle et sa tendance à courir partout...

La descente fut plus courte, mais aussi plus douloureuse que la montée. Les bras en compote et trempé de sueur, il s’apprêtait à incendier la jeune femme lorsqu’elle l’interrompit :

“T’avais raison, regarde. Ils nous ont repéré.

&mdash; Qu’est-ce que je t’avais dit, fichue gamine ?”

**🝢** - Levant les yeux au ciel, il agrippa son bras et se mit à courir dans le dédale de métal acéré. Il n’avait pas vraiment pu voir combien étaient leurs poursuivants, mais probablement cinq, s’ils n’avaient pas été reconnus. Dans le cas contraire, tout était fichu. Il dissimula Lila sous une plaque encore chaude, dont le cœur pulsait encore faiblement. Ça voulait dire que d’autres risquaient de tomber dans les environs, mais elle serait invisible à leurs détecteurs. L’ancien militaire confia sa protection à Nael avant de retourner sur ses pas.

Se déplaçant de couvert en abri, il parvint à se rapprocher de la petite équipe de gardes sans être repéré. Heureusement, ils n’étaient que cinq. L’un d’entre eux scannait la zone, mais Darren avait pris les devants. Il avait activé ses brouilleurs, et se doutait qu’à proximité d’un chantier de métal vivant, les détecteurs étaient suffisamment perturbés pour qu’une anomalie de plus passe inaperçue.

“R.À.S. chef. Fausse alerte, visiblement.

&mdash; Eh, toi, fit sèchement celui qui sembla être leur chef à l’un de ses hommes, ça te fatigue tant que ça d’être sur le chantier ?

&mdash; Non, chef, bredouilla une voix féminine déformée par son casque. Je vous assure que le scan a détecté des formes de vie dans le cimetière, et j’ai vu quelque chose tomber de la structure.

&mdash; Bah, c’était certainement des rats-écureuils-piaf ou autres saloperies. Quoi que c’était, c’est plus là, conclut le premier en terminant son scan.”

**🝢** - Ils s’éloignèrent ensuite trop loin pour que Darren puisse continuer de les écouter par dessus le tonnerre constant, même avec leurs amplificateurs. Au moins, il avait appris plusieurs informations utiles. Les gardes n’appartenaient pas à l’armée régulière du Dominium, puisqu’ils n’employaient pas de grades. Une milice privée de Sericco ou de l’A.I.D.E. donc. Il espérait sincèrement le premier, mais leurs uniformes étaient identiques sur cette planète.

Ensuite, ils étaient sur leurs gardes, mais ne s’attendaient pas à être attaqués, sinon ils auraient cherché la zone avec plus de zèle. Bon point. Et enfin, ils appelaient la zone où tombaient les débris du pont le cimetière. Toujours dans le lexique du métal vivant, ça faisait du sens. Il retrouva Lila et lui fit comprendre qu’elle devait arrêter de prendre des risques aussi inconsidérés. Pourquoi ne voulait-elle jamais rien prendre au sérieux ?

Le plus dur restait encore à faire. Le bruit était si fort ici qu’ils ne pouvaient plus s’entendre parler, et les émanations toxiques soulevées par les plaques de métal réduisaient significativement leur espérance de vie. Lila sortit de sa sacoche un étrange appareil qui rappelait à Darren un parapluie miniature. Il s’activa en bourdonnant, et il se sentit nauséeux lorsque l’onde le traversa. Passé l’inconfort initial, il fut forcé de reconnaître l’utilité de ce gadget. Il purifiait l’air qu’ils respiraient, leur évitait d’être écrabouillés par la chute d’une pièce, et comble du bonheur, il réduisait le boucan infernal et leur permettait de s’entendre, à condition d’hurler.

**🝏** - C’était intimidant d’entrer dans l’ombre de la construction. D’un coup, elle s’imposait dans tout l’espace, et même en se dévissant le cou c’était impossible de voir le ciel. La pluie de métal s’accentuait aussi, et la petite ingénieure ne pouvait qu’espérer que le bouclier antigrav qu’elle portait à bout de bras ait suffisamment de puissance. Comme elle aimerait disposer d’un véritable dispositif anti-gravité, et pas ce simulacre qui ne faisait que manipuler l’air ambiant.

Nael, dans son autre bras, tremblait lui aussi. Le pauvre n’était pas dans son élément, et il devait détester ne pas contrôler la situation. Un peu comme un autre grognon de sa connaissance. Elle le fit grimper sur son épaule afin de changer le bouclier de bras et reposer un peu l’autre qui s’était épuisé. Ronchon avait proposé de le porter, mais elle avait refusé. Sa fierté ne se rendrait pas si facilement !

Plus ils s’approchaient du pied de la structure, plus la cacophonie muait. Le concert de métal était peu à peu couvert par un bourdonnement assourdissant. Lila voyait la bouche de Darren s’ouvrir, ses lettres remuer, mais elle était incapable d’en saisir la moindre parole. Devant son air ahuri, il arrêta de se torturer les cordes vocales et opta pour leur code en signes. C’était un dérivé de celui de l’armée, aussi l’ancien militaire le maîtrisait parfaitement, mais la jeune femme avait encore des difficultés à le comprendre.

De ce qu’elle en retirait, il lui demandait ce que c’était, combien de temps ils devaient encore marcher, et à quelle heure le portail se fermerait. Pour toute réponse, elle hocha les épaules. En particulier pour la dernière question, elle n’avait aucune idée de ce qu’il voulait dire. En revanche, elle était particulièrement satisfaite de l’agacement de son protecteur.

Il fallait tout de même reconnaître que la base de l’édifice était plus loin qu’il n’y paraissait. Ils progressaient à un bon rythme depuis deux heures, et la silhouette titanesque semblait toujours inatteignable, perdue dans la brume. Pour ne rien arranger, la diode qui signalait l’état du bouclier se mit à clignoter. Du même clignotement que celui indiquant une batterie à plat. C’était pas bon. Elle fit maladroitement comprendre à Darren qu’ils leur fallait se mettre à courir, et s’élança sans attendre sa réponse. Il suivrait. Avec un peu de chance, le bouclier tiendrait cinq minutes avant de lâcher.

**🝢** - Quoi qu’il se passait, pour que Lila arbore un air aussi effrayé, c’était pas bon. Le soldat, fidèle à ses vieux réflexes, n’avait pas cherché à discuter et avait emboîté le pas de la jeune femme. Elle avait beau être en excellente condition physique pour une gamine, elle manquait d’endurance. Son épuisement ajouté au terrain accidenté manquait de la faire trébucher à chaque pas. Il comprit enfin son empressement lorsque la bulle de protection du gadget diminua, le forçant presque à se presser contre elle pour ne pas sortir du champs d’action.

Il aurait préféré ne pas en venir à ça, mais ils avaient encore un bout de chemin à parcourir et ils n’avaient plus de temps. Dans une manœuvre périlleuse, il activa les systèmes de ses bottes. Il sentit le biofluide se mettre en mouvement, et soudain ses pas furent plus légers. En quelques instants, le miracle de ce matériau faisait effet. Sans qu’elle puisse protester, il prit Lila dans ses bras, s’assura que le lapin était avec eux, et accéléra le rythme.

**🝏** - Passé la surprise initiale, elle ne savait si l’expérience qu’elle vivait était fantastique ou effrayante. Darren, tout en la portant, courait à une vitesse surhumaine et effectuait des bonds de plusieurs mètres, comme si c’était parfaitement normal. Les obstacles qui auraient pris des dizaines de minutes à contourner étaient maintenant avalés en quelques secondes. Elle devait admettre que c’était agréable d’être portée comme une princesse, et de laisser tout le boulot à Darren. Il s’en sortait bien, pour un vieux.

Le bouclier antigrav rendit finalement l’âme. La rotation de ses pâles s’arrêta un peu trop soudainement, se qui perturba l’équilibre dans la main de Lila, qui ne parvint à le retenir. Il y avait tant d’heures de travail dans ce petit bijou de technologie, et à peine était-il tombé de sa main qu’elle l’avait perdu de vue. Et pire encore, ils étaient à présent complètement vulnérables à la pluie de métal.

A partir de là, elle s’arrêta sur la terreur. Darren allait encore plus vite, elle devait s’agripper de toutes ses forces à lui pour ne pas se sentir glisser. Lui-même était en train de repousser ses limites, il réalisait des acrobaties avec une souplesse qu’elle ne lui connaissait pas, changeant plusieurs fois de direction en plein saut. Elle était comme Nael finalement, elle ne supportait pas ce sentiment d’impuissance. Elle ferma les yeux, et plaça toute sa confiance en Darren.

Lorsque le soldat la déposa enfin, elle ne saurait dire combien de temps s’était écoulé. À en juger par la douleur qu’elle ressentait au niveau de la gorge, elle avait dû crier. Beaucoup. Mais elle était rendue sourde par le bourdonnement du métal, et Darren aussi, alors ça ne l’avait sans doute pas dérangé. Quand il la lâcha, elle tenta de se mettre debout, fit quelques pas mal assurés, avant de s’étaler par terre. De son côté, le trentenaire n’en menait pas large non plus, il était épuisé et éprouvait toutes les difficultés du monde à respirer.

Il était cependant parvenu à les emmener dans une zone sûre, où le sol était stable et aucune plaque d’acier ne tombait. Sans l’atténuation sonore du bouclier, le bourdonnement était à la limite du supportable, et elle sentait déjà une terrible migraine réduire ses capacité cognitives. Elle tentait de repositionner l’envers de l’endroit quand elle heurta un mur de métal. Encore plus sonnée qu’elle ne l’était, elle leva la tête. Il semblerait qu’ils y étaient arrivés, au pied du mur. Là, l’acier était stabilisé dans sa conformation définitive.

Darren la regardait, l’air de dire “et maintenant ?”. Avec un large sourire, elle lui indique le haut. Il leur fallait encore grimper pour atteindre leur objectif. Dans d’autres circonstances, elle aurait savouré l’exaspération qui marquait le visage de son ami. Mais là, elle n’en avait pas le force. Heureusement, ils n’allaient pas escalader la paroi quasiment lisse. Les grappins servaient à ça. En plus de reconnaître et de se fixer au métal vivant actif, ils étaient suffisamment long pour les amener au sommet, si besoin.

Ils visèrent une zone peu visible de l’extérieur, masquée par la brume. Après plusieurs angoissantes secondes, les filins se rigidifièrent, signe que la tête avait touché une région active. Ils fixèrent l’autre extrémité au sol et préparèrent se collèrent magnétiquement aux deux filins presque verticaux. Lila s’attacha par sa ceinture, et Darren au niveau des chevilles. Outrepassant les consignes de sécurités, ils grimpèrent déraisonnablement vite. Chaque seconde comptait, puisqu’à tout moment le métal pouvait détecter le corps étranger et l’éjecter.

Lila pensait que son mal de tête ne pouvait pas être pire, mais elle avait tort. L’espace d’un instant, elle avait oublié que les limitations existaient pour éviter ce genre de vertige. S’il n’y avait pas l’idée d’être suspendue à des centaines de mètres du sol, avec pour seule sécurité une ceinture magnétique, elle se serait évanouie.

Elle eut à peine la présence d’esprit de ralentir en approchant le sommet, sinon elle et Darren se serait écrasés au plafond bien stupidement. A demi consciente, elle se sentit soudain à nouveau d’aplomb. Enfin, sa mission commençait. Si le rôle de Darren était de s’assurer qu’elle arrive saine et sauve jusqu’au chantier, le sien était encore plus important. Elle sortit de sa sacoche un mainteneur de métal vivant reconditionné. Il envoyait des informations avec des identifiants de maintenance, de sorte que l’IA lui ouvre un passage jusqu’à son cœur.

Elle fit signe au soldat de monter la garde à l’extérieur et lui confia Nael. Même suspendu au filin, il ne risquait presque plus d’être éjecté, puisque l’IA avait diminué son activité en vue de la maintenance. Les protocoles de sécurité des ouvriers étaient particulièrement drastiques, même sur les chantiers sans ouvriers. Les mesures anti-piratage, en revanche, étaient beaucoup plus laxistes. Tant mieux pour elle.

Elle s’aventura à quatre pattes dans l’étroit tunnel gentiment ouvert par l’IA. Mieux valait ne pas être claustrophobe, surtout lorsqu’il se réduisait tellement qu’elle était obligée de ramper. Délicate attention ou conditions normales, le bourdonnement se faisait beaucoup plus ténu à l’intérieur, et il ne faisait pas aussi chaud qu’elle s’y attendait. La chambre principale était en fait d’un froid glacial. Le cœur énergétique était disposé jusqu’à côté de l’imposant dispositif abritant l’IA qu’il alimentait. Lila était presque certaine qu’en séparant les deux éléments dans des chambres différentes, il n’y aurait pas besoin de refroidir aussi sévèrement la salle informatique. Mais elle n’était pas là pour juger les choix techniques douteux des ingénieurs.

Un clavier holographique s’afficha devant la façade noire à son approche. L’A.I.D.E. avait tant d’argent à gaspiller pour installer des dispositifs aussi coûteux sur des machines qui n’étaient en théorie jamais utilisées ? Ses doigts tremblaient à cause du froid, et vu la chaleur à l’extérieur, elle était bonne pour passer les jours suivants clouée au lit.

Mais pour l’heure, elle avait plus important à penser. Dans la console, elle ouvrit un port, dans lequel elle inséra un cube aux reflets métalliques. Elle donna ensuite l’ordre à l’IA d’attendre dix minutes, puis de redémarrer à partir de la partition contenue dans le cube. Ça avait été si facile. Mot de passe “root”, sérieusement ? En 2502 ? Pas de sécurité complémentaire, c’était presque perturbant. Elle effaça ses traces par acquis de conscience, puis reparti par le tunnel d’où elle était venue.

Darren avait repris des couleurs à son retour. Elle lui fit signe de redescendre. Une fois n’étant pas coutume, ils y allèrent bien trop rapidement et eurent la nausée à l’arrivée. Le soldat se repris le premier, se mettant directement à chercher un abri tandis que Nael bondissait dans les bras de Lila. Darren fini par dénicher une crevasse assez profonde recouverte par un épais bloc de métal. C’était impossible de prévoir la suite des évènements, alors ils avaient besoin de l’abri le plus solide possible.

**🝤** - Le virus de Lila fit des miracles. Malin, il avait entièrement pris le contrôle de l’IA dans laquelle il avait été injecté, avant de se propager en utilisant ses identifiants sous couvert d’une mise à jour système. Il empruntait le même protocole que les véritables mise à jour, alors aucune ne s’est douté de quoi que ce soit. Et une fois le réseau sous son contrôle, ce fut un feu d’artifice fantastique.

Chaque secteur de métal vivant recevait des instructions contradictoires, incohérentes voire corrompues, leurs capteurs étaient hors-service mais leurs protocoles de sécurité étaient inactifs. Alors les intelligences rendues folles s’étendaient sans restriction, dans toutes les directions, même à l’intérieur de la structure. Des figures complexes entraient en collision et s’écrasaient mutuellement, emportant des tonnes de matériau dans leur course.

Juste avant, le chantier ressemblait à un seul organisme se dessinant harmonieusement, mais là ce n’était plus qu’une kyrielle d’individus se battant pour exister. Les gardes, incapable de saisir ce qui se passait, étaient comme des poulets sans têtes ou des pantins désarticulés projetés en l’air pour finir écraser sous des immeubles de fer. C’était tout bonnement magnifique. quoiqu’il serait mieux si Lila ignorait le dernier point.

Le carnage arriva à son apothéose lorsque trois des arches qui supportaient la structures se désolidarisèrent. Tout un pan de la structure s’effondra dans un fracas monumental, ouvrant une brèche de plusieurs centaines de mètres. Et les quelques cœurs encore en fonctionnement cherchaient à combler l’espace en créant une forêt chaotique de filaments aveugles.

La folie dura toute l’après midi et une partie de la nuit, avant de se calmer suffisamment pour que Chloé puisse partir à la recherche de ses héros. Coup de chance ou comportement codé dans le virus, la zone où ils avaient trouvé refuge était relativement épargnée. Avec la plupart des gardes morts ou blessés, il n’y avait plus de risque à s’approcher avec le _Vadrouilleur_, mais elle ne devait pas tarder, sinon les renforts risquaient d’arriver.

Chloé se targuait d’être l’une des meilleures pilotes de la planète, mais même pour elle, la navigation dans ce chaos avait tout du suicide. Et la poussière métallique soulevée au cours de la journée n’aidait en rien sa visibilité, elle devait esquiver au dernier moment des obstacles pourtant immenses. Miraculeusement, elle arriva à destination sans encombre. Le silence qui régnait à présent tenait du surnaturel.

Inutile de les appeler, ils devaient être sourds ou évanouis, voire les deux. Mais ils avaient Nael avec eux, lui reconnaîtrait le signal. Quelques minutes plus tard, l’adorable boule de poils bondissait joyeusement vers elle. Chloé la saisit et le câlina en rigolant. Derrière lui, Lila et Darren faisaient peine à voir. Couverts de poussière, du sang coulait de leurs oreilles et de plusieurs dizaines de petites blessures, et ils avaient l’air plus morts que vifs. Elle les aida à grimper à l’arrière du véhicule, et même le soldat bourru se laissa faire. C’est dire s’il était épuisé.

Ils méritaient des semaines de repos, mais ils n’auraient que quelques jours, elle en avait peur. Après ce coup d’éclat, le Dominium allait forcément lancer des représailles, ciblant indistinctement toutes les colonies du désert. Elle se demanda s’ils devrait quitter la planète, le temps que les choses se calment. Enfin, ils auraient tout le temps de penser à la suite demain. Ce soir, ils fêtaient leur victoire.
