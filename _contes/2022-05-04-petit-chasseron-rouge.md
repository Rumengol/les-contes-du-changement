---
bg: "petit-chasseron-rouge"
layout: post
title: Petit chasseron rouge
summary: Un chasseur tout vêtu de rouge pénètre profondément dans une forêt pour rendre visite à une vieille femme.
source: “Baba Yaga's hut” par Stanislav Sherbakov
ref: https://www.artstation.com/artwork/zzaxm
date: 2022-05-04
categories: posts
tags: ["Le monde d'Alkénor"]
type: Conte
author: Rumengol
lang: fr
---
Il s'était peut-être trop enfoncé dans le Bois Hurlant, cette fois. Le chasseur avait perdu tous ses repères. Les arbres noueux s'étaient épaissis et obstruaient le passage. Il espérait ne pas avoir dévié du chemin. Les dernières lueurs du jour ne parvenaient plus à percer l'épaisse canopée et il avançait presque à l'aveugle. Il se sentait exposé, à présent qu'il était dépossédé de l'un de ses seuls avantages. 

Il ne chassait pas par plaisir. Personne n'entrait dans cette forêt sans y être contraint. Pour lui, c'était une simple question de survie. Ce que lui et les autres faisaient n'était pas réellement du braconnage, car aucun seigneur ne pouvait régner ici. Ils chassaient leur subsistance dans des terres sauvages, sans roi ni loi. 

La cape d'Herrick s'était coincée dans un fourré de ronces qu'il aurait juré n'être qu'un buisson l'instant d'avant. Il parvint à la retirer des griffes de bois, non sans y laisser d'importants lambeaux de tissu. Il grimaça. Cette cape était sa protection contre les flèches malencontreuses dans le dos. Non que cela importe aussi profondément dans la forêt. 

Le rouge aidait à être vu de loin, pour qu'un autre chasseur ne tente pas un tir hasardeux après avoir entendu un fourré remuer. Cette couleur permettait également de reconnaître facilement un camarade en péril, si un chasseur altruiste passait à proximité. Ils avaient tendance à se faire rares récemment. Trop d'accidents. 

Certains avaient soulevé, à l'époque, que des capes aussi voyantes feraient d'eux des cibles de choix pour les prédateurs du Bois Hurlant. Les vétérans, Herrick en tête, leur avaient montré que les créatures, ou les choses, les plus dangereuses étaient complètement aveugles. Sourdes aussi, pour ce que ça changeait. Elles avaient d'autres méthodes pour saisir leur environnement, et lorsqu'elles désignaient un malheureux comme leur proie, ce n'était pas la couleur de sa cape qui lui permettrait d'être épargné. 

Depuis, la cape qu'il avait propagée était devenu plus qu'un moyen de se reconnaître entre eux dans la terrible forêt, mais également dans les villages au-dehors. Si le seigneur détournait le regard, les villageois respectaient les chasseurs et leur donnaient toutes sortes de requêtes. Pour certains des messages à délivrer aux fous qui avaient choisi de s'exiler dans la forêt, pour d'autres des demandes de gibier. 

C'était une requête du genre qui l'avait poussé à s'aventurer aussi loin. La jeune femme qui l'avait accosté était bien trop richement parée pour vivre dans la région. Son teint mat et la soie exubérante qu'elle arborait ne laissait que peu de doutes sur ses origines exotiques. Une robe de soie coûteuse laissait ses épaules découvertes, et une ribambelle de fils dorés couraient dans sa chevelure d'ébène. Une princesse promise au rejeton du seigneur, ou une riche marchande prospectant des opportunités financières, avait pensé Herrick.  

Malgré cela elle s'était avancée directement vers lui, comme si elle savait ce que l'atour carmin signifiait. Elle lui avait parlé avec un accent manifestement étranger, mais avec des mots de la région. Elle lui avait chargé de remettre une lettre ainsi qu'un panier de fruits à l'allure étrange pour sa grand-mère, qui vivait recluse dans le Bois Hurlant. Il avait semblé suspect au chasseur grisonnant qu'une vieille femme puisse survivre dans ce bois qui même pour lui était synonyme de mort. 

Puis la femme avait sorti des innombrables replis de sa robe une bourse ainsi qu'un bracelet d'or et les avait tous deux posés dans sa main, coupant court à ses protestations. Elle lui demandait de partir au suicide, mais quel homme ne ferait pas au moins cela pour plus d'argent qu'il n'en avait jamais possédé ? Il avait toutefois refusé de porter le panier de fruits. Si les choses qui rôdent là-dedans ne voyaient ni n'entendaient, leur odorat ne pâlissait en rien à celui des plus fins limiers. Les fruits au fort parfum ne manqueraient pas de les attirer.  

Sans insister, elle avait simplement récupéré le panier, affirmant que seule la lettre importait, mais que sa grand-mère pouvait se montrer acariâtre si l'on se montrait à sa porte sans présent. Chargé de son paiement, Herrick se disait que s'il survivait à son périple, il pouvait affronter l'ire d'une grabataire. Elle avait acquiescé, puis indiqué un chemin qui s'enfonçait dans la futaie. Les chasseurs évitaient les chemins de manière générale. Ils n'étaient pas naturels, arpentés seulement par ce qui régnait sur ce monde horrifique. Mais les plus effrayants étaient ceux qui sortaient de la forêt, car ils signifiaient que quelque chose battait la terre suffisamment de fois pour laisser cette marque. Quelque chose qu'aucun d'entre eux n'avait envie de rencontrer. 

Mais la femme lui avait assuré que c'était le seul sentier fiable pour rejoindre la cabane de sa grand-mère, et que nul bête ne s'y aventurait. De toute évidence, elle ignorait de quoi elle parlait, mais Herrick n'avait pas le choix. Il n'allait pas se perdre des jours durant à la recherche d'une bâtisse probablement indiscernable des troncs environnants. Il lui faudrait simplement se montrer prudent. 

Sa connaissance des bois l'avait aidé, au début du trajet. De nombreuses pistes de chasse suivaient le chemin qu'il devait emprunter dans la zone externe de la forêt. Ces tracés approximatifs offraient à peine plus de couvert que le sentier principal, mais il s'y sentait plus en sécurité. Superstition, il le savait, mais il lui fallait ça pour ne pas succomber à la panique. 

Puis les pistes avaient bifurqué et le chemin continuait. À contrecœur il s'était aventuré dessus, craignant qu'une immonde forme se jette sur lui l'instant où son pied toucherait le sol. Tendu, son coutelas dégainé, il s'était figé sur le qui-vive quelques secondes. Mais rien n'était venu. Les feuilles elles-mêmes avaient retenu leur souffle. Calmé sans être détendu, il avait repris sa progression. D'abord sur la pointe des pieds et au bord de chemin, il fut contraint d'adapter sa démarche lorsque celui-ci se rétrécit. 

Auparavant direct, le tracé se tordait en une succession de virages et devenait à certains endroits si étroit qu'il devenait impossible de ne pas marcher à côté. Par deux fois le chasseur avait perdu la piste avant de la retrouver quelques mètres plus loin, derrière un buisson touffu. Pour ne rien arranger, la nature elle-même semblait décidée à lui refuser le passage. En plus des branches basses qu'il ne remarquait qu'au moment de les percuter, il devait désormais considérer les buissons qui se changeaient en ronciers et l'obscurité qui le privait de l'un de ses meilleurs sens. 

S'il avait pu prévoir, il aurait emporté avec lui des torches, ou bien serait passé par chez lui pour récupérer son briquet à amadou. Il avait été aveuglé par l'appât du gain, et maintenant par l'épais feuillage. Il devinait son chemin plus qu'il ne le voyait, et avançait avec lenteur de peur de perdre le sentier. Plus loin, il disparaissait complètement. Il n'y avait plus de terre, seulement un parterre de feuilles en décomposition et d'herbe au toucher anormal. Herrick se guidait grâce aux ouvertures dans la végétation, les bords du sentier étant trop denses pour qu'il puisse passer au travers sans ruiner sa précieuse cape. 

Au crédit de sa commanditaire, elle avait dit vrai. Il avait marché plus longtemps et plus loin que n'importe qui avant lui, et aucune des choses qui peuplent le bois n'était venue à sa rencontre. C'était toujours ça de pris, tentait-il de se convaincre alors qu'une branche acérée écorchait sa manche. Il remarquait un changement dans les bois alentour. De plus en plus d'arbres avaient été déracinés, et les autres avaient perdu leurs feuilles depuis bien longtemps. Le parterre n'était plus fait de feuilles, mais seulement de cette herbe si étrange. 

Le point positif de cela était qu'il pouvait enfin voir où il s'aventurait. L'orangé de la fin de journée perçait au travers de la cime toujours plus éparse, jusqu'à ce qu'il puisse profiter des derniers rayons du soleil avant que celui-ci ne disparaisse derrière un horizon qui semblait bien lointain à l'homme isolé. Il ne lui restait plus que la pâleur du jour qui refusait pour quelques heures encore de laisser place à la noirceur de la nuit. C'était peu, mais suffisant. 

Il discernait mieux son environnement. Les troncs reposaient de part et d'autre de ce qui devait être son chemin, formant des murs de bois parfois hauts comme trois hommes. Lui qui n'avait jamais eu d'hésitation à se terrer dans des trous grands comme lui, voire à s'enfermer dans une caisse de bois pendant plusieurs heures pour échapper à des poursuivants, il était soudain frappé d'un sentiment de claustrophobie. Il n'aimait pas l'idée de ne pas avoir de solution de fuite. Si quelque chose décidait de lui tendre une embuscade, il n'aurait que peu de chances de s'en sortir. 

Soudain, il lui sembla voir la lumière d'une flamme plus loin sur le chemin. En s'avançant, il put distinguer la silhouette d'une chaumière en surplomb. La cabane de la vieille femme devait être accrochée à un arbre, ce qui expliquait comment elle avait échappé à la plupart des créatures. Pour une recluse, les quatre flambeaux qui brûlaient devant sa porte étaient plutôt accueillants. D'un autre côté, elle devait savoir que cela ne la rendait pas plus voyante aux yeux de ses voisins indésirables et ne se souciait pas d'avoir des visiteurs. 

Plus il s'approchait, plus Herrick sentait que quelque chose n'allait pas. La silhouette de la cabane se découpait complètement, à mi-hauteur d'un tronc qui n'était pas visible au-dessus. Et en fait d'un tronc, c'était quatre piliers filiformes qui soutenaient l'habitation. Mais ce n'était pas des arbres non plus. Ils se mouvaient. Continuellement, comme les pattes d'un insecte. Non, ce n'était pas cela, se corrigea-t-il. Les griffes recourbées qu'il pouvait à présent apercevoir ne laissaient aucun doute. C'était des serres de rapaces, chacune assez grande pour lui arracher la tête. 

Pris de terreur, Herrick voulut tourner les talons pour fuir, mais lorsqu'il s'arrêta, il réalisa qu'il s'était précipité devant la monstrueuse bicoque. Il distinguait à la perfection les quatre pattes graciles qui supportaient la cabane de bois dans laquelle un brasier semblait s'être allumé. Des larmes perlèrent aux creux de ses yeux. Il sentait ses jambes lâcher lorsqu'une voix sèche lui parvint de l'intérieur de la petite hutte. 

— Eh bien, entre. Tu ne vas pas me faire attendre plus longtemps ? 

Comme si elles n'attendaient que ça, les pattes de la maison se plièrent et s'effacèrent dans le sol, abaissant la porte jusqu'à ce qu'elle soit au niveau du sol. Hésitant, Herrick poussa la frêle porte en bois et se contorsionna pour pénétrer dans l'ouverture bien trop petite pour son gabarit. A l'intérieur de la petite cabane il n'y avait qu'un grand chaudron et une femme de l'autre côté. 

Sa robe était en tous points identique à celle de la femme qui lui avait donné la lettre, jusqu'aux fils d'or emmêlés dans les cheveux. Mais au lieu d'une magnifique jeune femme, le chasseur avait devant lui une vieille sorcière rabougrie aux rides si prononcées qu'on pouvait les confondre avec des cicatrices. Sa peau avait troqué son teint exotique pour un pâle orné de nombreuses larges taches de rousseurs. 

Confus, Herrick porta la main à sa poche intérieure pour en sortir la lettre. Ce faisant, il sortit de la même poche le bracelet et la bourse qui lui avaient été donnés en paiement. Le bijou désormais fait de terre cuite s'effrita tandis que le sable contenu dans la bourse s'écoulait au travers des mailles décousues. Son air effaré n'attira qu'un soupir d'agacement à la vieille femme. 

— Tu as pris ton temps, il me manquait l'ingrédient principal. 
