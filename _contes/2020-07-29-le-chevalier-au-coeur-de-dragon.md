---
bg: "le-chevalier-au-coeur-de-dragon"
layout: post
title: Le chevalier au cœur de dragon
summary: Un brave parmi les braves est envoyé pacifier un territoire barbare.
source: “Combat Thebalt martyrdom” par Piotr Foksowicz
ref: https://www.artstation.com/artwork/0XGrVe
date: 2020-02-11
categories: posts
tags: ["L'empire des confins"]
type: Conte
author: Rumengol
lang: fr
---

Jamais son épée n’avait failli. Jamais son bras n'avait tremblé. Il avait triomphé de chaque ennemi, ramené de fabuleux trésors de chaque conquête. Au fil du temps, il avait gagné le surnom de chevalier au cœur de dragon. Cette fois n'était qu'une campagne de plus. Un peuple barbare s'était soulevé contre la volonté de l'empereur, qui envoya l'un de ses plus braves vassaux les mater.

Charlerai menait cinquante hommes. Un excès de prudence sûrement, là où vingt auraient amplement suffit. Mais il n'avait pas triomphé tant de fois en se comportant comme un jeune arrogant. Un seul de ses hommes pourrait venir à bout de cinq barbares dépenaillés, mais il risquait de ployer sous un nombre trop important.

Les premiers villages étaient sinistrement vides. De toute évidence, ils étaient attendus, et ces sauvages attendaient que les chevaliers s'enfoncent plus loin dans leurs terres. Roffenard, son capitaine, s'approcha.

«Que fait-on, seigneur ? Le piège est trop évident et-

– Brûlez le village. Et tous les autres qui seront vides après celui-ci. Ces lâches vont apprendre ce qu'il coûte de fuir leur destin.»

La bonté et la merci ne valaient que pour ceux qui combattaient honorablement. Le bûcher illuminerait les environs de sa lumière purificatrice jusqu'au lendemain. Sans un regard en arrière, la colonne se remit en marche à travers la forêt. Les chemins escarpés ralentissaient leur progression, et Charlerai s'attendait à tout moment à ce qu'une flèche se plante dans son armure. Mais les deux éclaireurs qui chevauchaient à une centaine de mètres plus avant ne détectaient rien, et aucune attaque ne survint, même lorsqu'ils durent gravir une colline bien trop abrupte.

Trois autres villages furent incendiés. Le seigneur était de plus en plus perplexe. Qu'espéraient donc les Freltes ? Que la troupe impériale se fatigue, qu'elle abandonne ? C'était bien mal connaître la volonté d'acier de son commandant. Il fut tenté, à plusieurs reprises, d'incendier ces forêts qui faisaient l'orgueil des barbares. Mais l'empereur souhaitait sécuriser son territoire, pas régner sur une terre calcinée.

Peut-être les Freltes avaient-ils simplement pris peur ? Il était bien plus facile d'étripailler et de crucifier un collecteur de taxe qu'une troupe lourdement armée. Il les imaginait sans peine regarder ces hommes de métal marchant d'un pas de conquérant, tremblant comme des feuilles mortes. Il les foulerait de son pied comme il ferait pour un insecte.

Si le seigneur ressentait une frustration grandissante, Roffenard, en revanche, était de plus en plus téméraire. Il envoyait des petits groupes en reconnaissance de plus en plus loin, s'approchait déjà des villages torche à la main, se plaisant presque à ravager ainsi les habitations. Charlerai laissait faire, bien qu'il n'approuvait pas cet excès de zèle. Lui-même, malgré la prudence qui le caractérisait, se sentait en terrain conquis. La brume qui se faisait de plus en plus dense l'encourageait dans cette pensée. Ils approchaient du littoral, et si les Freltes fuyaient devant eux, alors ils seraient bientôt acculés à l'océan.

Un mois après le début de leur périple, ils arrivèrent sur un lieu de culte païen. Les Freltes vénéraient des dieux anciens, et effectuaient des rituels blasphématoires dans des cercles de pierres levées. Sacrifices d'animaux ou d'enfants, orgies dégénérées... Malgré l'interdiction de l'empereur, ces pratiques avaient toujours cours ici. Et ce cercle était encore entretenu. Cette vision posa un voile rouge sur le regard du chevalier au cœur de dragon. Il ordonna que ces pierres soient abattues sur le champ et le cercle brûlé jusqu'aux cendres.

Les premiers cris furent de surprise. Les suivants de souffrance. Les derniers d'agonie. Des figures squelettiques avaient surgi du sol, armées de lames de pierre ou de métal rouillé. La plupart étaient vêtues de cuir et de fourrure, certaines possédaient des plaques de métal cabossées. Il était impossible de distinguer leurs traits à travers le brouillard, si épais qu'une épée pouvait le trancher. Dans les premiers instants de l'embuscade, plusieurs soldats tombèrent. Mais bien vite les réflexes acquis pendant des années d'entraînement prirent le dessus et ils se regroupèrent, lame au clair. Une quarantaine de guerriers contre cent, peut-être deux cent Freltes. La victoire était assurée.

Charlerai s'autorisa un bref rictus. Le temps sembla ralentir, chaque groupe jaugeant l'autre, chaque homme choisissant son adversaire avec le peu de visibilité disponible. Ils ne voyaient pas à deux pas devant eux, mais pour tuer un barbare, c'était largement suffisant. Le seigneur hurla un cri de bataille, reprit par Roffenard puis par le reste de ses hommes. La mêlée débuta dans une cacophonie d'acier. Les lames s'entrechoquaient, coups de poings et de pommeaux s'étouffaient dans la nappe humide.

Les corps tombaient un à un, à un rythme qui satisfaisait la soif de sang du commandant. Il exultait intérieurement à chaque cri d'étonnement que poussait un Frelte lorsqu'une lame impériale le traversait. Justement, son adversaire, mieux équipé que les autres, lui avait offert un meilleur combat qu'il ne l'aurait cru. Mais c'était la fin. Sa garde s'ouvrit, d'un coup de poignet il le désarma avant de lui transpercer le cœur. Il ne fit aucun son en mourant. Maintenant qu'il le réalisait, alors que l'impérial vociférait et hurlait en assénant ses coups, son adversaire restait muré dans un mutisme perturbant.

Tout aussi perturbant que le coup de bouclier qui le saisit en pleine tête, lui arrachant son casque. Il tituba en arrière, arrachant son épée sans la moindre difficulté. Avant qu'il ait eu le temps de comprendre d'où venait le coup, le bouclier s'abattit à nouveau sur lui. Il esquiva, et frappa de toutes ses forces le bras qui tenait la pièce de bras, qui se brisa. Sans attendre, il fit volte-face et décapita l'ancien propriétaire du bouclier. Son corps chancela, puis tomba enf- non ?

« Sainte mère de Dieu... murmura Charlerai.»

Devant lui, un corps sans tête venait de se ressaisir. Il se repositionna et le menaça de son unique bras. La brume lui jouait-elle des tours ? Oui, sans doute avait-il raté la tête et simplement fait voler le casque de son ennemi. D'une ultime charge, il se jeta sur lui de tout son poids et écrasa l'homme entre un rocher et une armure de métal. Une succession de craquements sinistres se fit entendre, et son adversaire se disloqua.

Sans s'appesantir sur cette étrangeté, il se retourna. Le brouillard se dissipait, et il put se rendre compte de l'état du champ de bataille. Au sol gisaient une cinquantaine d'armures, et un peu moins de silhouettes le scrutaient, debout en haillons. À peine cinquante hommes, voilà pourquoi il n'a eu à en combattre qu'un. Mais pourquoi ses soldats reposaient-ils au sol, tous vaincus par des barbares Freltes mal armés ?

Il repoussa ses questions jusqu'à la fin de la bataille. Il allait se confronter à plus d'hommes qu'il n'avait jamais combattu. Et s'il mourrait, il aurait l'éternité au ciel pour se questionner. Brandissant son épée des deux mains, il entama un hurlement guerrier, qui se coupa net. La flèche qu'il venait de se recevoir dans le dos ne l'avait pas blessé, mais suffisamment secoué pour le déconcentrer. Deux autres se fichèrent dans son épaule gauche. Une parvint à percer l'articulation de l'armure. Aussitôt il sentit une vive douleur et perdit les sensations dans son bras. Il sentait un filet de sang courir sur sa peau à l'intérieur de son armure.

Alors que les hommes qui avaient vaincu les siens s'approchaient, la dispersion de la brume lui permit de mieux les distinguer. Ils n'avaient plus rien d'humain. Ce n'étaient que des squelettes, des cadavres réanimés dont pendaient ça et là quelques lambeaux de chair. Les Freltes, au comble de la lâcheté, avaient usé de la nécromancie pour s'opposer à l'empire. Les fous... Voilà comment ses hommes étaient tombés. Ce n'était pas les Freltes qui poussaient des cris d'étonnement, mais bel et bien les humains, constatant que leurs coups mortels étaient restés inefficaces. Lui même avait été surpris.

« Au nom du Père, ceux qui s'adonnent à la nécromancie sont maudits pour l'éternité !»

S'il devait mourir, ce ne serait pas sans bravoure. Levant haut son épée, Charlerai au cœur de dragon réunit les forces qui lui restait pour un ultime combat dont il ne pouvait sortir vainqueur.
