---
bg: "reve-d-eveille"
layout: post
title: Rêve d'éveillé
summary: Un homme croit se réveiller d'un cauchemar, pour réaliser qu'il l'a suivi dans le monde éveillé.
source: “Osiah - Triangle Pieces” par Lewis Hewison
ref: https://www.artstation.com/artwork/ybZPon
date: 2020-09-30
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---

L'herbe encore un peu humide de la forêt caresse mes pieds nus. L'odeur de pluie s'estompe, mais elle est encore présente. Le ciel est recouvert d'un nuage blanc laiteux, qui filtre la lumière comme un abat-jour. Une légère brume flotte paresseusement au dessus du marais. Je ne reconnais pas l'endroit, pas plus que je ne me souviens comment je suis arrivé ici. En me retournant, je ne vois que des arbres et de l'herbe humide. Un courant d'air frais me fait frissonner, comme pour me signaler que je suis torse nu.

Claquant des dents, je me mets en mouvement. Il n'y a pas de soleil dans cette forêt, mais rester dans l'ombre des bois ne va pas me réchauffer. J'entame une petite course, pour me réveiller. Les rochers affleurant à peine me forcent à les éviter régulièrement au dernier moment. En quelques minutes à peine, je ne sens plus le froid. Il fait même bon, maintenant. J'ai couru tout droit en espérant sortir des bois, mais j'ai l'impression que l'inverse s'est produit.

Les arbres sont plus resserrés, plus épais, et leurs racines sortent du sol comme si elles essayaient d'en extraire leur masse prodigieuse. Les branches sont plus basses, aussi. Je m'arrête un instant pour reprendre mes forces. C'est seulement maintenant que je prends conscience de la quiétude angoissante de la forêt. Je n'entends que ma propre respiration, aucun oiseau ne piaille, aucun insecte ne s'est encore jeté sur moi. D'ordinaire, je me serais considéré chanceux, mais là, cette absence de nuisibles n'a rien d'agréable.

Ma respiration est redevenue régulière, je me remets en marche. Mes pas résonnent de plus en plus fort dans la cathédrale des troncs. Les yeux fermés, je ne vois plus, je ressens. La pulsation du bois, les ondulations de l'herbe naissant sous mes pieds, l'odeur de la pluie plus récente ici... et d'autres choses. Je ne suis plus seul. Soulagé, mon instinct me dicte pourtant de ne pas ouvrir les yeux. J'entends neuf murmures distincts sans parvenir à saisir leurs sens. L'un d'entre eux tient plus du grondement. Ils s'approchent de moi, m'encerclent. L'odeur de pluie devient plus forte, envahit mes narines jusqu'à la nausée. Je comprends une chose, ils me veulent, cherchent à me saisir. Je n'ai aucune échappatoire, je le sais. La seule sortie est bloquée. Et quand bien même, elle s'est déjà refermée. Ils me touchent presque. Le parfum d'averse me fait défaillir, les murmures se sont mêlés en un bourdonnement insupportable.

Je me réveille en sursaut, trempé de sueur. Un sacré cauchemar, cette nuit. Ah non, on est déjà le matin. 6h55, le jour pointe timidement et j'ai encore une bonne heure pour me préparer. Je saute dans mon jogging blanc, encore un peu chamboulé par ce réveil. Les rêves ont toujours un sens, apparemment. Je suis bien curieux de connaître la signification de celui-là. Les yeux mi-clos, je prépare le café, et j'en renverse une partie sur le plan de travail. Naturellement. Le temps qu'il chauffe, j'entame mon bol de céréales. Un peu molles, j'ai mal refermé le paquet hier.

La cafetière arrête enfin son boucan. J'espère que les voisins n'étaient pas en train de dormir. Et voilà qu'elle débloque, elle se met à gronder méchamment. Ça ne pouvait pas être une bonne journée. Tiens, non ? On dirait qu'elle s'est éteinte normalement, et que le bruit ne vient pas d'elle. Une minute, je reconnais ce grondement ! Mes yeux s'écarquillent probablement de terreur lorsque je regarde vers la chambre. J'avais laissé la lampe allumée par habitude, mais je ne vois que des ténèbres. Je reste paralysé par la peur quelques instants, suffisamment longtemps pour que la masse noire se propage dans la moitié du salon.

Paniqué, je fais demi tour, attrape tout juste mes clefs, me cogne contre l'angle du mur et me rue sur la porte. Mon genou me lance atrocement, mais je l'ignore. Le plus important est de mettre le plus de distance entre moi et cette chose. Une fois dans le couloir, je ferme la porte à double tour, triple si le loquet ne m'avait pas arrêté. La chose dans mon appartement gronde moins fort, juste ce qu'il faut pour que j'entende les murmures venant du couloir plongé dans le noir.

C'était un rêve, rien qu'un rêve ! Quoi que soient ces choses, elles ne pouvaient pas m'avoir suivi dans le monde réel. Je dois sûrement être toujours endormi. Je me pince aussi fort que je peux, jusqu'aux larmes. Rien n'y fait. Je me rue dans les escaliers, jusque dans le parking souterrain. Elles m'attendent sûrement à l'entrée. Mes pieds nus s'écorchent sur le sol rugueux recouvert de petits morceaux de verres. Ça aussi, je verrais plus tard.

Il a sans doute plu cette nuit, vu l'odeur qui imprègne même ma voiture. Je démarre au quart de tour, emboutis le bolide de 212. Tant pis, je m'excuserai plus tard. Il faut que je m'éloigne, autant que possible, de la ville. Je roule à tombeau ouvert, ignore les feux rouges et la signalisation. Je soupire de soulagement en dépassant le panneau de sortie. J'accélère encore, plus que jamais. C'est une question de ma vie ou ma mort, je le ressens au plus profond de moi. Le ciel s'est couvert d'un linceul blanc uniforme, mais au moins il ne pleut pas.

La radio crachote un murmure incompréhensible. Je pile violemment. Même au travers des interférences, je peux reconnaître leur murmure, et cette odeur entêtante. Elles m'ont suivi. Peut-être qu'elles étaient dans la voiture depuis le début, ou alors elles se déplacent aussi rapidement ? Je ne suis plus en sécurité. Les pneus crissent sur des centaines de mètres. Je fais une violente embardée, et la voiture bascule dans le fossé.

J'en sors un peu sonné, mais suffisamment lucide pour savoir où je dois aller. De l'autre côté de la route, dans les bois. L'herbe encore un peu humide de la forêt caresse mes pieds nus. L'odeur de pluie s'estompe, mais elle est encore présente. Le ciel est recouvert d'un nuage blanc laiteux, qui filtre la lumière comme un abat-jour. Une légère brume flotte paresseusement au dessus du marais. Avec horreur, je réalise que je sais où je suis. Elles m'ont emmené exactement où elles le voulaient. Et elles m'attendent, les neufs ombres, dans la clairière où mon cauchemar a débuté. Flottant au dessus du sol, on dirait des silhouettes dissimulées dans des voiles noirs transparents. Mais elles n'ont aucun corps. Leurs murmures se transforment en bourdonnement comme elles s'approchent de moi. Cette fois je ne peux plus me réveiller pour leur fuir, je suis totalement à leur merci. Si j'avais compris le sens de mon rêve, est-ce que j'aurais pu leur échapper ? C'est trop tard, de toute façon. Trop tard pour tout.
