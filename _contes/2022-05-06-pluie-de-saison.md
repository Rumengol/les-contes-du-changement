---
bg: "pluie-de-saison"
layout: post
title: Pluie de saison
summary: Un officiel se rend dans un bosquet anormalement hydraté en pleine sécheresse.
source: “Medusa” par Caffe Tylo
ref: https://www.artstation.com/artwork/xzqdxW
date: 2022-05-06
categories: posts
tags: ["Les chasseurs de démons"]
type: Conte
author: Rumengol
lang: fr
---
Déconcerté. Oui, c'était bien ça. Il était déconcerté. Il lui avait fallu tout le voyage, à l'arrière d'un chariot terriblement lent pour retrouver ce mot. Il avait passé suffisamment de temps ballotté à l'arrière de la charrette pour refaire le monde plusieurs fois, mais ce mot lui avait échappé jusqu'à sa destination. La sécheresse avait frappé durement cette année, et il avait pu en constater les effets. Les rizières étaient engorgées de boue, et le sol de la plaine craquelait sous le pâle soleil. Mais cela l'avait préoccupé. 

Son chauffeur, un vieil homme aux traits tirés qui malmenait ses bêtes, avait eu la conversation la plus ennuyante qu'il avait entendue. Il n'avait jamais lu de sa vie, ni écouté de la poésie ou même une pièce musicale digne de ce nom. Tout ce dont il était capable, c'était frapper les deux bœufs fatigués qui tiraient de leurs maigres forces un véhicule chargé de tonneaux d'un vin trop cher. Il ne jugeait pas là le colporteur, qui avait bien assez à faire avec sa triste vie. Non, il en voulait à son pays, qui avait accepté de commercer avec ces étrangers au teint trop pâle. Quel besoin y avait-il d'importer de l'alcool, quand celui de son pays suffisait amplement ? Cela l'avait définitivement agacé. 

L'absence de nuages au milieu de l'après-midi rendait l'atmosphère étouffante. Les volutes de chaleur déformaient l'espace jusqu'à ce qu'il se trouble trop pour voir. Ils avaient croisé plusieurs voyageurs, et tous semblaient avoir eu leur visage dérobé par ces jeux de lumière. Les faces qui s'étaient tournées vers lui étaient troubles, déjà absorbées par la fournaise. Couplé à l'épuisement, cela l'avait inquiété. 

Mais son ordre de mission reçu plusieurs jours auparavant l'avait, plus que tout, déconcerté. Bien sûr, il s'ennuyait après avoir posé son sabre pour la plume du fonctionnaire, et il se montrait souvent volontaire pour les missions de terrain. Mais il avait su développer un nouvel intérêt dans les lettres. Il lisait beaucoup, allant jusqu'à échanger longuement avec le frère de l'empereur qui avait reconnu en lui un digne interlocuteur. 

Il ne l'aurait pas repoussé, alors se pourrait-il que l'empereur lui-même ai cherché à éloigner l'un des soutiens de son frère ? De telles pensées étaient ridicules, nul complot ne se tramait, et quel bénéfice l'empereur pouvait-il tirer de l'élimination de son propre frère ? Il se pouvait simplement que l'expertise d'un agent du gouvernement était nécessaire, et qu'il était parmi les plus fiables. C'était bien la procédure habituelle. Le shogun refusait de croire à l'œuvre des yokais et ne voulait entendre parler des chasseurs. Alors l'empereur avait dû prendre les choses en main. 

Lorsqu'un évènement que la raison ne parvenait à expliquer lui était signalé, il envoyait un officiel sur place, qui devait rendre compte de la situation et décider s'il était nécessaire de missionner un chasseur. Ryûgaku considérait le shogunat comme irresponsable de préférer fermer les yeux plutôt que de prendre le problème à bras-le-corps. Les faits étaient indisputables, et l'activité de ces démons augmentait. Il n'était même pas impossible qu'en réalité la brutale sécheresse soit de leur fait. 

D'autant plus étrange était le bosquet de Kiriyama. Là où toute la végétation de la région s'était asséchée, la forêt était plus florissante que jamais. Les villageois avaient appris à craindre cet endroit, qui serait devenu le repère de la créature ayant volé l'eau du ciel et des rivières. L'intitulé avait beau être inquiétant, il n'était pas exclu que ce ne soit qu'une anomalie naturelle. Kiriyama était une forêt de bambous particulièrement résistants, et il pouvait exister un puits souterrain dans lesquels ils parvenaient à s'abreuver. A lui de s'en assurer. 

Il descendit du chariot sans échanger plus qu'un grognement mutuel avec le conducteur. La vue obstruée par une vague de sueur, il distingua au loin une tache verdoyante. Il pouvait y être avant la nuit, s'il se dépêchait. Un village se trouvait de l'autre côté du bosquet, mais le vieux superstitieux avait refusé de s'approcher des bambous et avait insisté pour réaliser un détour qui lui aurait coûté un jour entier de voyage. À pied, il ne faudrait que quelques heures à l'ancien samurai pour atteindre le bourg en traversant le petit bois. Il commencerait son investigation le lendemain.  

Le trajet se révéla plus important qu'il ne l'avait anticipé. Le ciel avait déjà passé l'horizon lorsqu'il atteignit l'orée de Kiriyama. Il hésita à pénétrer la forêt de bambous plongés dans la pénombre, effleura son sabre. Il avait beau ne plus s'entraîner avec autant d'ardeur qu'auparavant, il n'avait que peu perdu de son talent d'épéiste, et se sentait capable de repousser n'importe quel bandit qui se tapirait entre les troncs longilignes. Et s'il s'agissait d'un yokai, il serait capable de prendre la fuite. Rien de déshonorable à fuir un combat que l'on ne peut gagner. 

Alors qu'il passait la frontière de la végétation, il posa sa main sur l'un des bambous. Celui-ci était humide, comme si une douce pluie venait de le caresser. L'herbe au sol se colla autour de sa chaussure, pareillement mouillée. Ryûgaku pouvait pourtant jurer qu'il n'avait plu ce jour-là, ni celui d'avant. Le ciel était cruellement vide de nuages, et la terre craquelée agonisait jusqu'à quelques pas derrière lui. Il n'y avait plus de doute possible, ni de retour arrière. S'il avait pénétré le territoire d'un yokai, il préférait se dépêcher d'avoir le soutien de la milice locale que d'être seul en rase campagne lorsqu'il lui tomberait dessus. 

Une dizaine de lueurs attirèrent son regard. L'une d'elle dansait comme une luciole, alors que les autres étaient fixes. Cela était intriguant. Il s'approcha en se faisant aussi discret que possible, prêt à dégainer son katana au moindre signe d'hostilité. Une part de lui regrettait son impénétrable armure, tandis que l'autre lui rappelait qu'il n'aurait pu être silencieux avec, et que rien n'est impénétrable face aux pouvoirs démoniaques. 

Il fut surpris par une silhouette sur sa droite. Il ne l'avait pas entendu approcher. Il s'était figé, la main crispée sur la poignée. Il ne réalisa que de longues secondes plus tard que ce qui l'avait effrayé n'était rien d'autre qu'une statue de pierre. Particulièrement détaillée, elle représentait une fermière au visage buriné et taillé par les années, son chapeau de pierre glissant légèrement sur le côté comme elle s'épongeait le front. Le souci du détail était proprement phénoménal. Il passa un instant à admirer le chef-d'œuvre avant de reprendre sa route. 

Il voyait maintenant l'origine des lumières, des lanternes de papier disposées autour d'un autel. Ryûgaku ignorait à quelle divinité il était dédié, mais il ne pouvait ignorer le groupe de statues qui y était rassemblé. Une pareille attention y avait été apportée, et chaque figure représentée dans la roche semblait honorer le dieu à sa manière. Étonnamment, ils arboraient tous des traits étrangers, et aucun n'était vêtu comme ceux qu'il avait pu rencontrer. Des habits sobres, des chemises et des robes, et leurs regards qui lui paraissaient exorbités fixés sur l'autel. 

D'autres encore surplombaient la scène, et d'autres se dessinaient à la lisière des lumières. Mais la plus frappante était celle qui reposait sur l'autel. Petite, recroquevillée comme sur le point de bondir, elle toisait les autres de sa hauteur. Elle portait une armure de samurai pourpre à sa taille, celle d'un enfant, ainsi que plus de lames qu'il ne devait être possible d'en transporter. Derrière son masque de pierre inexpressif, deux yeux brûlaient par la flamme de deux bougies. 

Posé dans sa longue chevelure blanche, un être encore plus petit avait été représenté. Il rappelait au fonctionnaire un criquet, tant ses vêtements amples et ses antennes lui donnaient des allures d'insectes. Ses yeux aussi étaient illuminés, mais son masque avait été gravé pour lui donner plus de personnalité qu'à sa monture. Les tracés autour des fentes embrasées lui donnaient un air malicieux. C'est lui qui tenait la lumière dansante, dans la forme d'une lanterne de verre et de métal qui se balançait au bout d'une canne de bois. 

L'évidence le frappa sur le tard, lorsque la plus grande des étranges statues tourna sa tête pour lui faire face. Peut-être était-ce la fatigue, peut-être l'atmosphère éthérée avait-elle perturbé ses sens ? Celle-ci aussi possédait deux antennes, qui s'étaient auparavant fondues dans les bambous à l'arrière-plan. La pierre des autres statues fonça, adoptant le luisant de la roche trempée par la pluie. Ryûgaku aussi sentait l'humidité ambiante, comme si une pluie qu'il ne sentait pas se déversait sur lui. 

La lanterne grinça, et l'ancien samurai, comme frappé par la foudre, s'élança. Le démon avait de petites jambes, il ne le rattraperait peut-être pas. Il frappait les bambous sans ralentir, sautant au-dessus des racines qui tentaient de le faire trébucher, usait de sa carrure pour défoncer les branches basses. Pas une fois il ne pensait à dégainer son sabre et à se battre. Il courait pour sa vie, c'était ce qui importait. 

Après ce qui avait pu être une éternité à courir, de nouvelles lueurs lui apparurent. Elles différaient des précédentes. C'était les lumières du village de Kôn, la sortie n'était plus très loin. Galvanisé, il redoubla d'efforts malgré ses forces diminuantes. 

Elle l'attendait là, perchée sur la branche d'un érable. Toujours aussi immobile, la lanterne tenue par son compagnon criquet avec ce même balancement. La statue démoniaque lui montrait qu'elle pouvait le tuer lorsqu'elle le désirait. Ou bien il y en avait plusieurs. Qu'importe, il était déjà lancé. À cette distance, qu'il s'éloigne ou non ne changerait rien, alors il persista sur sa trajectoire, juste en dessous du yokai. Alors qu'il passait à la hauteur de la figure penchée sur lui, il entendit des mots semblant venir de partout à la fois, glissant sur lui comme une rosée matinale. 

— Pars vite, humain. *Elle* n'aime pas les intrus. 

 