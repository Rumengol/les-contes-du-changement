---
bg: "ClockMaster"
layout: post
title: Le Maître des horloges
summary: La folie des hommes mène le dieu du temps à réinitialiser le cycle.
source: “Watchmaker” par Mirekis
ref: https://www.instagram.com/p/B8vpIT1lcQH/?utm_source=ig_web_copy_link
date: 2020-02-12
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---

L'approche de la fin les effrayait. Ils souhaitaient tous avoir plus de temps. Du temps pour vivre, pour se réaliser. Ils ne cherchaient pas à repousser l'inévitable, simplement à se figer dans un état de survie éternelle. Ils refusaient de voir cette nécessité, ce changement de cycle qui n'apportait pas le désastre, mais le renouveau. Confrontés à leur impuissance, ils ne savaient plus s'ils désiraient un messie ou un bouc émissaire.

A leur yeux, j'étais les deux. Maître des horloges donc du temps, ma tâche était de veiller à ce que cette mécanique interne du monde soit visible pour tous. Les maîtres des horloges n'avaient pas le privilège d'y échapper, contrairement à la pensée commune. Mon propre mentor avait été vaincu par l'âge quelques années auparavant, et je n'étais que le dernier d'une lignée vieille comme l'humanité.

La tour de l'horloge, grâce à son aspect terrifiant de mécaniques et d'aiguilles, était parvenu un temps à les tenir à l'écart. Ils me pensaient capable de puissants artifices temporels, mais je n'étais pas un mage. Lorsque, poussés par le désespoir, ils entrèrent dans la tour, je les accueillis. Qu'il était étonnant de voir l'archimage vociférer, le visage ravagé par la folie. Lui qui s'était acheté des millénaires de vie, aucune de ses potions ne pouvait le sauver de l'Éclipse, prophétisée par un devin avant même sa naissance.

J'ai tenté de discuter. Mais à peine l'air entrait dans mes poumons qu'en ressortait une gerbe de sang. Le magicien fou ne souhaitait rien entendre, m'avait-il dit. Il m'ordonnait de créer la plus puissante des horloges, une mécanique qui surpasserait les autres, un dispositif capable de neutraliser la marche morbide du temps. La horde irraisonnable qui se massait derrière lui, brisant et saccageant les plus anciens artefacts temporels de ce cycle se retournait de temps à autre pour approuver.

Les pires bourreaux furent chargés de me motiver. Si pour mon malheur mon travail n'était pas assez rapide, ils me torturaient quelques heures. C'était aussi un moyen pour eux de se distraire. Messie et bouc émissaire. Mon esprit tanguait entre travail et douleur. La mécanique du temps, m'avait enseigné un jour mon maître, est une forme de magie que nul mage ne parvient à saisir.

« Pour capturer chaque instant du temps, il ne suffit pas d'assembler des engrenages, ni de puiser dans le mana. La véritable source du temps est physique. Elle réside en toi, mais aussi autour de toi. Sans la matière, il n'y a pas de temps. Sans temps, il n'y a pas de vie. Et sans vie, pas de magie. Le temps se nourrit de la matière et alimente la vie. Les mages insufflent leur vie dans leurs sorts. Pour saisir le temps, tu dois lui insuffler de la matière.»

Ces mots me revenaient au travers de ma demi-folie. Pour de simples horloges, dont le rôle était de capturer le temps, des cristaux de quartz ou de célestite étaient un catalyseur suffisant. Mais pour agir sur le temps, il fallait plus. Il était nécessaire de toucher l'essence de la matière. Le mécanisme que je créais ne ressemblait en rien à une horloge. Il s'agissait d'une clef, capable d'ouvrir l'accès au pouvoir de la grande horloge. Cette horloge était, tout comme la tour, l'ultime création de l'archimage temporel. Celui qui avait créé le temps dans l'espace.

Son horloge, mystérieux héritage, se situait dans une temporalité étrange, aléatoire. Aucun maître des horloges n'avait compris ce qu'elle indiquait, ou s'il était possible de l'utiliser. Moi si. Elle était le temps. C'était à travers elle qu'il s'écoulait dans notre monde. Elle avait probablement le pouvoir de le ralentir, ou même de l'arrêter. Mais ce cycle était corrompu, et ce monde ne méritait pas d'être sauvé.

Le jour de l'Éclipse, alors que tous me pressaient de coups de bâton, d'étranges souvenirs me vinrent. L'origine des royaumes, les premiers temps, l'animation, seconde par seconde, de chaque horloge. Je compris alors. Ma décision était la bonne. Je l'avais déjà faite tant de fois. J'avais par la suite tant échoué. Des milliers de vies s'écoulèrent en moi, sans qu'aucune ne puisse m'indiquer quel choix faire, quel évènement changer.

Pas d'indication. Juste un geste, déjà accompli plus de mille fois. Un simple mouvement de poignet. Je ne pus tourner la clef intégralement, comme prévu. Mon corps matériel n'existait plus, consumé par le temps. La tour de l'horloge et tout ce qui y résidait avait subi le même sort. Les intrus n'étaient pas véritablement morts, leur esprit était simplement séparé de leur corps. Tout comme le mien, d'une certaine manière.

Mais j'étais différent. Mon esprit, libéré de sa matière, avait fusionné avec l'horloge du temps. D'un coup, j'existais partout et à tout moment. J'étais tout. Je ressentais la dégradation de chaque grain de sable, chaque particule d'air qui se déplaçait. J'étais plus lucide que jamais. Les quelques jours de torture endurés n'étaient qu'une égratignure sur des millénaires d'existence. Non. Un temps infini d'existence.

L'Éclipse. C'était donc ça, cette fameuse catastrophe. J'abandonnais ce cycle. Privé de son horloge, le temps n'abondera plus. La magie, la vie, tout cela cessera d'avoir sens. Il n'y aura là plus que matière inerte, inconsciente. Un nouvel échec. Mais le temps n'est même plus un paramètre.

Un nouveau cycle. J'observe cette matière figée, immobile. Un temps en dehors du temps, je suis le seul être mobile. Puis une des aiguilles de l'horloge s'active. Et une autre. Bientôt, le temps se déverse dans ce cycle, il se meut, il s'agite, ne vit pas encore. Des millions d'années avant que la vie n'apparaisse à présent. Ils passeront comme un battement de cil. Puis je rétablirai la tour de l'horloge, repositionnerai l'horloge. Mais ce cycle n'aura pas de magie. Peut-être que la source de la corruption des précédents était ce paramètre.

Je me dissous dans le flot du temps. Peu avant la prochaine Éclipse, un nouveau vaisseau naîtra, naïf. Je l'habiterai pour constater si ce cycle est une réussite. Ou un énième échec.
