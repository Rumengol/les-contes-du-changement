---
bg: "le-coeur-du-monde-la-cite-maudite"
layout: post
title: Le Cœur du monde - La cité maudite
summary: Le prince Sorgyen a retrouvé la trace du cœur du monde dans l'antique cité maudite d'Anshankar.
source: “Chadar Logoth”, par Sergey Shikin
ref: https://www.artstation.com/artwork/VdXWe5
date: 2020-02-02
categories: posts
tags: ["Le coeur du monde"]
type: Conte
author: Rumengol
lang: fr
---

"La cité maudite d'Anshankar, on y est. Tu es bien sûr de toi Sorgyen ?"

"Mes sources sont sûres, mon ami. Mais si tu crains les fantômes, tu peux toujours rentrer au palais la queue entre les jambes."

"C'est ça, moque-toi. J'aime juste pas cet endroit."

Anshankar, glorieuse capitale de l'empire Téléoste. Des siècles plus tôt, alors qu'il était à son apogée, le palais impérial se retrouva piégé au cœur d'un cyclone magique qui anéantit la cité et emporta l'empire dans la tourmente. Des révoltes éclatèrent dans de nombreuses provinces, tous les dirigeants avaient disparu et l'armée était éclatée, profondément affectée par le cataclysme. Il s'effondra en quelques années à peine, les fondations sapées.

Chose étrange, l'histoire avait rapporté que les quelques survivants d'Anshankar avaient trouvé le palais impérial intact, toujours aussi éclatant, rayonnant du contraste avec sa ville en ruines. L'intérieur était immaculé, sans une âme vivante. C'était comme si tous, famille impériale, gouverneurs, soldats et esclaves s'étaient volatilisés. Déclarée comme maudite, peu de gens lucides s'y aventuraient encore. Peut-être Sorgyen n'avait-il plus toute sa tête, pour y chercher le cœur du monde, un artefact que la légende disait si puissant qu'il apporterait la bonne fortune à tout royaume le possédant. Personne ne savait qui l'avait dissimulé dans les entrailles d'Anshankar, ou même si l'empire Téléoste le détenait depuis le début, mais les rapports des quelques explorateurs s'aventurant dans les faubourgs concordaient avec le mythe : des lignes d'énergie bleutées qui semblaient jaillir du sol, une atmosphère littéralement pesante et crépitante.

Sorgyen et Rorris constatèrent ces mêmes faits au moment où ils passèrent la lourde porte sud aux trois quarts démolie. D'un coup, il leur sembla supporter le poids d'un homme sur leurs épaules, et leurs cheveux s'agitaient sans vent, comme animés d'une volonté propre. Les chevaux se tendirent. Seul leur entraînement implacable les empêcha de prendre la fuite. Par sécurité, les deux cavaliers rabattirent les œillères.

Ils ne tardèrent pas à apercevoir la première ligne d'énergie. Elle paraissait crever le sol comme une racine, et pourtant il ne s'agissait, ainsi que Sorgyen le confirma quand il s'en approcha, que de lumière. Un phénomène optique étrange, s'il en était, qu'une faille dans le pavement puisse produire l'effet d'une racine. Il tenta de déterminer la source de cette luminescence, mais fut bien vite aveuglé par l'intensité qui s'en dégageait. Rorris grogna son désagrément et lui dit de remonter en selle.

Il régnait dans les murs une atmosphère étrange. La pierre était étranglée par des arbres morts et du lierre séché. Les récits concernant la destruction de la cité avaient dû être grandement exagérés, ou bien elle était restée inchangée tout ce temps. Certes, il n'y avait là que des ruines, mais certainement pas millénaires. Ils évoluaient dans des quartiers qui avaient dû être autrefois magnifiques, dont la splendeur était à présent décolorée et fanée.

Ils avaient la désagréable sensation d'être observés, tantôt de voir une ombre courir dans les murs, tantôt d'entendre des murmures portés par le vent. Une brume fine nappait les rues, s'épaississant à l'approche du palais. Approche laborieuse, les rues principales étant souvent bloquées par des montagnes de gravats et de murs effondrés, ils étaient forcés de prendre de multiples détours, jusqu'à parfois même sortir des murs pour quelques centaines de mètres.

En passant l'arche qui devait marquer l'entrée d'une des innombrables places ou cours intérieures de jadis, Ils furent tétanisés par une voix qui semblait venir d’outre-tombe. C’était comme si elle surgissait de partout et nulle part à la fois, des craquelures des murs comme du sol. Elle s'exprimait dans un dialecte inaccessible aux deux hommes, guttural et résonnant. Les chevaux se cabrèrent, manquant de désarçonner leurs cavaliers. Une silhouette s'avança dans la lumière pâle, perchée sur le rebord d'un ancien vitrail. L'écho de la voix résonna encore quelques instants avant de s'estomper.

L'être qui venait d'apparaître était encapuchonné dans des lambeaux de haillons et des bandages souillés par le temps. D'une main il menaçait Sorgyen et Rorris d'un sabre émoussé par l'âge, et de l'autre il portait un bâton au bout duquel une braise semblait consumer ses dernières forces. Le prince semblait distinguer l'éclat métallique de divers équipements arnachés chaotiquement. L'individu, ou la créature, était ramassée sur elle même, comme prête à bondir. Elle continua de les invectiver dans cet obscur langage connu d'elle seule, mais sans la puissance donnée par les résonances des ruines, sa voix paraissait fragile et nasillarde.

Les aventuriers crurent percevoir un mot dans ces borborygmes ineptes. Était-ce "Partez", un avertissement de la part de ce mystérieux gardien ? Ou bien leurs sens leur jouaient des tours et ce n'était là que paréidolie. Quoiqu'il en soit, Sorgyen n'était pas d'humeur à se laisser décontenancer. Flattant doucement l'encolure de sa monture, il la força à avancer vaille que vaille. Rorris, la mine déconfite, lui emboîta le pas.

La chose bondit - ou plutôt se laissa tomber - au sol. La chute d'une dizaine de mètres qu'elle venait de réaliser paraissait l'avoir à peine ébranlée. Chancelante mais tenant toujours fermement ses armes, la créature les menaça de plus belle, agitant son bâton braisé comme une torche. Ses gesticulations aléatoires eurent pourtant de l'effet, effrayant le cheval de Sorgyen et désarçonnant Rorris. Le premier parvint à contrôler sa jument mais fut forcé de mettre pied à terre pour confronter son adversaire.

Avant qu'il ait eu le temps de se remettre en garde, le sabre antique s'abattit sur lui et se brisa sur son épaulière. La lame, émoussée et fragilisée par l'âge, ne pouvait lutter contre de l'acier royal Cytarien. Grognant, l'autre bondit en arrière d'un geste incroyablement leste. Il détacha le grappin-faucheur de sa taille et commença, tout en gardant une distance respectueuse entre lui et le prince, à le faire tourner autour de sa tête.

Quand le coup partit à la vitesse de l'éclair, Sorgyen était prêt. Il fit décrire à son épée un large mouvement ascendant qui vint percuter de plein fouet le grappin, qui fut projeté presque à la verticale. Un second mouvement sec brisa net la chaîne rouillée. Avec un feulement de rage, son adversaire lui envoya au visage deux boules de terre cuite - à en juger par l'odeur de souffre qui s'en dégageait, elles avaient dû être des bombes fumigènes un jour - avant de dégainer deux dagues et de se jeter sur lui. Mais cette fois son ennemi était prêt, et bien meilleur combattant. Sorgyen esquiva un premier coup tout en plongeant sous la garde de son adversaire qu'il décapita d'un mouvement circulaire parfaitement exécuté. La tête roula quelques mètres plus loin tandis que le corps désarticulé s'effondrait.

Le prince déchu rejoignit son ami qui se remettait difficilement de sa chute.

"Nom d'Altor... Qu'est-ce que c'était ?"

"Aucune idée, mais on va vite le savoir, viens."

Il sépara péniblement le capuchon de la tête tranchée et des bandages, pour découvrir un visage séculaire, si ravagé par les rides qu'il était difficile de deviner la forme qu'il avait à l'origine. Desséché, le teint cadavérique, il était incroyable qu'un tel individu ait survécu et puisse combattre dans cet état. Était-il au moins humain à l'origine ? Plongés dans leur réflexion, ils réagirent au dernier moment à l'être qui les chargeait d'une lance au manche inégal. Rorris bloqua la course de l'arme, permettant à Sorgyen d'éliminer facilement l'importun.

"Encore un ? grogna le maître d'armes. Mais qui sont ces gens ?"

"Peut-être les habitants du palais disparus dans la légende. Mais peu importe, ils sont rapides mais fragiles, et leurs lames sont émoussées. Inutile de faire dans la finesse, on laisse les chevaux ici et on s'équipe."

Ils se débarrassèrent de leurs manteaux et sortirent de leurs sacs de selle des armures de plates en pièces. Ils s'en équipèrent laborieusement, regrettant l'absence d'écuyers dans ces terres maudites, s'aidant l'un l'autre. Leurs préparatifs terminés, ils étaient méconnaissables, guerriers de métal engoncés dans une armure impénétrable. Ils abandonnèrent les chevaux, préférant avancer à pied, une chute leur aurait probablement été fatale.

Plusieurs fois ils furent attaqués par des groupes de vieillards à peine vivants qui venaient s'écraser contre une défense bien trop résistante. Ne préférant pas laisser de place au doute, ils les passèrent tous au fil de leurs lames. Tant bien que mal, souvent ralentis ou interceptés, ils avançaient vers le palais. Quand ils l'aperçurent enfin à travers le brouillard, ils furent étonnés de la conservation du bâtiment, qui était comme s'il avait été vidé la veille. Plus ils s'en approchaient, plus leurs assaillants redoublaient de vigueur. Ils perdirent vite le compte des vies ôtées et oublièrent la frayeur éprouvée lors de leur première rencontre.

S'était-il écoulé des heures, des jours, lorsqu'ils arrivèrent aux portes du palais ? Ils étaient exténués, combattre et marcher entre de lourdes plaques de métal drainant bien plus d'énergie que les fanfarons de la cour ne voulaient l'admettre. La fatigue n'épargnait pas leur esprit, qui perdait ses repères dans un labyrinthe baigné d'une brume azurée si dense qu'elle en masquait le ciel. Ce qui les avait perturbés à leur arrivée dans ce dédale n'étaient désormais que des repères pratiques, les lignes lumineuses qui pourtant furent plus massives et nombreuses au fil de leur progression n'étaient rien d'autre que des marqueurs.

Aussi s'accordèrent-il une pause en haut des gigantesques escaliers qui menaient au palais. Ils avaient dû être très empruntés et entretenus durant l'âge d'or de l'empire Téléoste, mais à présent les marches étaient branlantes et inégales. Curieusement, alors qu'ils avaient été harcelés sans relâche depuis des heures, les attaques avaient cessé sitôt qu'ils avaient posé le pied sur ces mêmes marches. Ils dormirent à tour de rôle, par prudence, bien qu'aucun ennemi ne soit apparu. Ils mangèrent un peu, puis se préparèrent à pénétrer l'édifice.

L'intérieur était inexplicablement vide, comme s'il s'agissait d'un tombeau, à cela près que pas un grain de poussière n'était visible, là où même le seuil était indiscernable sous la crasse du temps. L'atmosphère ici était plus pesante encore qu'à l'extérieur. Après quelques pas, un bourdonnement sourd les stoppa. Il leur fallut un temps d'accoutumance pour comprendre qu'il s'agissait d'un battement qui semblait venir des profondeurs de la terre.

L'excitation des guerriers grandissait : ils avaient enfin trouvé le Cœur du monde ! L'artefact mythique devait être protégé depuis des siècles par des gardiens liés à lui et à ce lieu. En l'obtenant, non seulement Sorgyen récupérerait sa légitimité royale, mais en plus ils libéreraient les âmes de tous les malheureux qui rôdaient dans la cité. Ceux qu'ils n'avaient pas déjà tué, du moins. Ils auraient probablement couru, si le poids de leurs armures ne les en empêchait. Et préférant ne pas perdre de temps à les enlever, ils prirent un rythme de marche forcée en se fiant aux battements incessants, jusque dans les tréfonds du palais, dans des sortes de catacombes dénués d'ossements, pour enfin déboucher dans une salle aux proportions de cathédrale.

En son centre, une sphère de la taille de cinq hommes pulsait. Une lumière bleue émanait de son cœur, et de temps à autre, un filament de lumière s'en échappait pour aller se faufiler dans une des nombreuses craquelures du sol. Des câbles retenaient la sphère a mi-hauteur, et semblaient l'alimenter, provoquant les pulsations, à moins qu'il ne s'agisse du phénomène inverse. Les deux hommes d'armes restaient interdits. Leur but, le cœur du monde, ressemblait à cela ? Avec du recul, il semblait évident que le cœur du monde ne serait pas une petite bille qui tiendrait dans la paume d'une main, mais si grand... Il faudrait un énorme chariot, et au moins quatre bœuf pour le tirer. Quant à le faire descendre si profondément...

Non, Sorgyen ne pouvait se résoudre à échouer alors qu'il touchait au but ! Il avait un trône à réclamer, et cet artefact en était la clef ! Il s'en approcha, à la recherche d'un quelconque mécanisme d'activation, en vain. Se hissant sur le piédestal qui se trouvait sous la sphère, il posa sa main dessus. Aussitôt, la salle se chargea d'une tension électrique sans pareille. Rorris se précipita pour écarter son prince de la sphère. Coincés dans leurs armures, ils risquaient de rôtir sur place si cet objet maudit se mettait subitement à lâcher des éclairs de foudre ou toute autre arcane maudite. Mais le gant de son compagnon était aimanté à la surface qui semblait... s'éveiller, comme si elle était vivante. La sphère presque parfaite fut parcourue de bosses et d'excroissances qui naissaient ça et là, se déplaçaient, éclataient parfois pour se reformer plus loin. La force qui les attirait était si forte qu'ils étaient incapables de bouger, tout juste pouvaient-ils s'échanger des regards horrifiés.

À la surface, hors de portée des deux hommes, un cyclone de foudre se formait, identique à celui qui détruisit Anshankar des siècles plus tôt. Partout dans la ville, les fantômes du passé le contemplaient avec un mélange d'angoisse et de soulagement. Leur tourment allait prendre fin. Punis par la faute d'un empereur avide, et condamnés à une éternité d'errance, la sottise des nouveaux venus était leur libération. Enfin, leurs âmes allaient pouvoir s'élever au ciel si longtemps convoité.
