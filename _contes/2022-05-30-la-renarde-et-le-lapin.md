---
bg: "la-renarde-et-le-lapin"
layout: post
title: La renarde et le lapin
summary: Seolhwa est envoyée arbitrer la sentence sur le clan des Hare, mais elle n'en a pas envie.
source: “Asian Fortress” par Diekyers
ref: https://www.artstation.com/artwork/lR6gOV
date: 2022-05-30
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---
Le clan des Hare avait gravement fauté. Leur transgression de la loi du murim ne saurait être pardonnée malgré les supplications de leurs émissaires. Les clans Bei et Gosu s'étaient prononcés et la sentence était tombée. Pour leur insolence vis-à-vis des lois éternelles, les Hare seraient détruits. Et l'arbitre qui avait été choisi pour exécuter la sentence était Seolhwa, une jeune fille de la secte divine qui ne semblait s'intéresser à rien. Les anciens l'avaient choisi pour éveiller sa curiosité au monde extérieur, espérant que le clan renégat se montrerait à la hauteur. 

— Tu penses qu'ils seront amusants, Kuratori ?  

Le corbeau se contenta de fixer la petite fille de ses yeux turquoise. Elle était assise sur une corniche surplombant la forteresse volante qu'elle devait anéantir. Cela l'ennuyait déjà. Les arbitres divins n'étaient pas ce qui manquait, c'était une perte de son temps de lui confier cette tâche. D'autant qu'il faisait froid dans leur royaume, sa robe légère à manches courtes la laissait sensible à la morsure du vent. Elle était venue avec son compagnon à plumes, mais il manquait de conversation. Les esprits gardiens étaient supposés être des amis et des protecteurs, mais le sien était aussi flegmatique que sa jeune maîtresse. 

L'île volante sur laquelle la forteresse s'était bâtie n'était accessible que par un étroit chemin de pierres en lévitation. Puis s'étalaient des petits villages au pied des murs, et enfin le bastion. Tous les bâtiments partageaient la même architecture, avec leurs toits pourpres particuliers. Les bâtiments les plus importants étaient décorés de cornes d'ivoire supposées représenter des oreilles de lapin, mais l'imitation était grotesque. Au loin, elle discernait les contours d'une autre île volante, plus petite, couverte de grandes tours pareillement décorées. Le château du patriarche. Cela aurait été plus simple de ne faire que l'éliminer, mais le crime incombait au clan entier, et c'était son annihilation qui était réclamée. La seule idée découragea la petite fille. 

— Qu'est-ce que t'en dis, si on s'en va loin ? 

Cette fois, il se retourna et fit vibrer ses plumes de colère. Même son attitude flemmarde ne surpassait le sens du devoir qui lui avait été inculqué à sa création, il n'autoriserait pas Seolhwa à tourner les talons à son devoir. Elle soupira. 

— Je plaisante, pas besoin de te mettre dans cet état. Mais j'ai envie de rentrer, cet endroit ne veut pas de moi. 

L'oiseau la fixait de son regard perçant et plein de reproches. 

— Je sais ce que tu penses. J'imagine que plus tôt ça sera fait, plus tôt je pourrais rentrer. 

Elle leva la main, et l'oiseau qui était posé dessus s'envola, avant de tourner autour de sa tête. Seolhwa se releva et réajusta le katana qu'elle portait en bandoulière. L'arme était presque aussi grande qu'elle, pourtant c'était une arme classique. Elle songea aux hallebardes maniées par les adultes de sa secte et pensa qu'il lui faudrait un long moment avant d'être assez grande pour en prendre une en main. Même les lances étaient trop encombrantes pour son corps frêle. 

Le croassement de Kuratori la fit se retourner brusquement, la main sur sa lame. Derrière elle, un petit garçon au masque de lapin l'observait, à moitié dissimulé dans un buisson. Il tentait de disparaître derrière la masse du feuillage, mais ses longues oreilles taillées dans le bois blanc étaient visibles comme le nez au milieu de la figure. Seolhwa s'immobilisa, hésitant entre rire et abréger le jeu. Finalement, c'est l'enfant qui sortit de sa cachette, intimidé. 

Il était plus jeune qu'elle de quelques années, son masque ne possédait même pas sa deuxième moustache. En général, les enfants aussi jeunes ne sont pas autorisés à s'éloigner autant du domaine familial. S'était-il enfui ? Il n'était vêtu que d'une tunique blanche et marchait pieds nus, un bien piètre fugueur si c'était le cas. Il se triturait les mains nerveusement, et la tête baissée comme s'il était honteux. Il osa finalement relever la tête pour la regarder, et poussa un petit cri en voyant le masque de Seolhwa avant de s'enfuir à toutes jambes. Elle le rattrapa en un instant et le coupa dans sa course. 

— Eh là, mon lapin. C'est très malpoli de s'enfuir sans dire bonjour. 

— Tu veux me dévorer ? C'est toi le kitsune ? 

C'était tellement inattendu qu'elle en éclata de rire, et libéra le petit bras de son emprise. Essuyant ses larmes de joies, elle retira son masque de renard pour dévoiler son visage au petit. 

— Hahaha, je ne suis pas plus Kitsune que tu n'es Gyokuto. Mais c'est vrai qu'à tes yeux, je suis une divinité, même si je ne veux pas te manger. Je m'appelle Seolhwa, et toi ? 

— G-Guma. Tu es très jolie. 

— Merci, Guma. Je ne suis pas censée retirer mon masque quand je suis en mission, alors ça sera notre secret, d'accord ? Sinon Kuratori va se mettre en colère. 

Le corbeau voletait déjà derrière elle avec des croassements de reproches, qu'elle ignora. Elle avait enfin trouvé quelque chose d'amusant à faire, le reste de son clan pouvait bien attendre quelques temps. 

— Dis, c'est quoi ta mission ? 

— Eh bien tu vois, je suis une arbitre, et ton clan a fait quelque chose de très méchant aux autres clans, alors je dois le punir. 

— Mais qu'est-ce qu'on a fait de si mal ? 

— Tu es petit, tu l'ignores peut-être, mais les grands lapins se sont accaparés les grands pouvoirs de la lune en pactisant avec le culte démoniaque, ce qui est un péché capital. 

—Mais mon père dit que c'est une manigance des baies pour s'emparer de notre montagne sacrée. 

— Des Bei, tu veux dire ? Et qui est ton père, petit Guma ? 

— C'est le patriarche de tous les Hare, Ryokoru Hare ! 

Quelle belle prise, avant même d'avoir commencé sa sinistre besogne. Cependant, quelque chose la dérangeait. Le clan Bei avait dénoncé les exactions des Hare et mené la procédure avant de déclarer la sentence, suivi du seul clan Gosu. Les autres factions s'étaient contentées de ne pas réagir, par prudence. Même si les preuves présentées étaient maigres, il était très dangereux de défendre quelqu'un accusé de frayer avec le culte démoniaque et le murim ne pardonnait pas les égarements. Cette histoire était trop complexe pour que Seolhwa s'attarde à la comprendre, mais elle refusait d'être le bras armé d'une stupide lutte de territoire. Et surtout, elle ne pardonnerait pas à ceux qui l'avaient forcée hors de chez elle si elle n'était pas là pour rendre la justice. 

Elle repoussa les injonctions incessantes de son oiseau alors qu'elle tentait de réfléchir. À court de patience et incapable de faire sens seule, elle allait se tourner vers Guma quand des cris lui parvinrent depuis le couvert du feuillage. 

— Protégez le jeune maître ! 

Trois masques de lapin surgirent des fourrés. Un homme et deux femmes, brandissant des lames courtes. Ils se précipitaient sur Seolhwa, qui repoussa son nouvel ami derrière elle avant de riposter. Tout se déroula très vite. Elle fit coulisser son fourreau tout en se baissant pour esquiver le premier coup. Son arme en main, elle effectua une large frappe circulaire qui sépara deux bustes de leurs jambes. La troisième attaquante avait bondi en retrait juste à temps, mais elle n'eut pas l'occasion de se rétablir avant que la lame affûtée ne lui transperce le cœur. D'un geste sec, Seolhwa éjecta le sang qui maculait l'acier, mais ne rengaina pas son arme. Elle se retourna vers un petit garçon terrifié et le prit dans ses bras. 

— Ça va aller, les méchants ne sont plus là. 

— Lâche mon fils, démone ! 

— Non mais ça va pas recommencer ! Attendez, votre fils ? Vous êtes Ryokoru Hare ? Fantastique, ça me fait gagner un temps fou. 

— Comment connais-tu mon prénom ? Un instant, ce masque... Tu es l'arbitre, alors ça veut dire que la sentence est tombée. 

— C'est justement à ce propos que je souhaitais vous parler. 

— Nous ne nous rendrons pas sans combattre ! 

D'un geste de la main, l'homme imposant fit apparaître à ses côtés une dizaine de soldats, qui se jetèrent sur la petite fille au masque de renard sans ménagement. Seolhwa comprit qu'elle avait eu tort d'espérer de l'amusement de la part des Hare. Si le patriarche était entouré de ses meilleurs guerriers, ce n'était qu'une perte de temps. Elle n'avait même plus envie de combattre. Un seul de ses adversaires attira son attention, un jeune garçon au jeu de jambes rapide qui parvint à échanger quelques frappes avec elle. Mais cette brève distraction ne dura pas, son inexpérience coûta au fier guerrier sa tête. Le chef de clan était désormais seul, mais pas encore vaincu. Une grosse hache dans la main, il s'apprêtait au combat. La jeune fille soupira. 

— Avant de vous tuer, je voudrais juste savoir quelque chose. Vous avez vraiment traité avec le culte démoniaque ?

— Bien sûr que non, pour qui prends-tu les Hare ? Mais vous autres les arbitres, vous n'êtes que les exécutants d'ordres dont vous ne saisissez même pas les implications, n'est-ce pas ? 

— Ah, c'est comme ça qu'on nous voit ? Ce n'est pas si surprenant. Mais je ne suis pas comme les autres, j'ai horreur d'être utilisée. Tu viens Guma, on s'en va. 

Le jeune garçon se releva sur ses genoux, tiraillé entre son père et la peur que lui inspirait la fillette. Finalement, le patriarche décida pour lui. 

— Tu ne me prendras pas mon fils ! 

Il projeta sa hache, retenue par une chaîne de métal. Elle frappa de plein fouet le sol là où Seolhwa se trouvait l'instant auparavant. Mais elle l'avait pris de vitesse et combla la distance entre les deux en une fraction de seconde. C'était trop tard pour le patriarche. Elle essuya le sang qui commençait à adhérer à sa lame en utilisant l'épais tissu qui enveloppait le cadavre du chef de famille, puis elle tendit la main au garçon dont les larmes coulaient sous son masque. 

— On peut y aller, maintenant ? 

Une petite main tremblante se posa dans la sienne. Le corbeau ne cessait de voleter en piaillant et croassant, si bien qu'il énerva Seolhwa qui le trancha en deux avant de rengainer son arme dans le fourreau qui n'avait pas bougé de son dos. L'enfant avait sursauté face à ce nouvel accès de violence, mais elle prit sa voix la plus douce pour le consoler. 

— Ne t'en fais pas, Kuratori ne peut pas mourir. Il finira par revenir nous embêter, mais en attendant on est que tous les deux ! Je préfère, tu ne peux pas être moins bavard que lui de toute façon. 

Main dans la main, la fillette au masque de renard et le petit garçon au masque de lapin descendirent la montagne, tournant le dos à la forteresse intacte des Hare. 

 