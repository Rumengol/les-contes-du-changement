---
bg: "un-bon-client"
layout: post
title: Un bon client
summary: L'Alchimiste se remémore un client régulier dont il n'a plus de nouvelles.
source: “Potion shop” par Tarmo Juhola
ref: https://www.artstation.com/artwork/8wPdGm
date: 2022-05-19
categories: posts
tags: ["Le Sanctuaire de l'Alchimiste"]
type: Conte
author: Rumengol
lang: fr
---
Ce soir-là, il pleuvait. Pas une petite pluie ni une averse, mais une bonne grosse pluie londonienne qui pénètre les vêtements et la peau. Je n'ai pas beaucoup de crédit pour m'en souvenir. Il pleut tous les soirs à Londres. Ce dont je me souviens nettement, en revanche, c'est du carrosse qui s'est arrêté devant mon établissement. *Le Sanctuaire de l'Alchimiste* est ma fierté, et l'une des plus énigmatiques de la capitale. Beaucoup de gens se demandent comment je peux posséder d'aussi grands locaux sans être ouvert ni attirer de clients. Et pour cause, depuis huit années que je suis installé, ma boutique n'a jamais été ouverte que la nuit, et ma clientèle est unique. 

La nuit, Londres se transforme. Lorsque les bonnes gens dorment et que les gendarmes regardent ailleurs, toutes sortes de monstres dansent dans le noir. Ils refluent au matin, disparaissant dans les caves et les égouts qu'ils ont investis. Certains hommes se sont plongés dans ce monde nocturne et en ont adopté les codes. Mages et sorciers de tous bords, chasseurs, cultistes... Tous ces gens utilisaient des décoctions secrètes ou réclamaient des ingrédients rares, dont j'étais le seul possesseur. On m'appelait l'Alchimiste alors, et j'avais bâti une petite fortune sur les besoins occultes de ces gens de l'ombre, et ma réputation de neutralité faisait affluer une clientèle aux opinions et desseins variés. 

Si les chasseurs de la reine voyaient d'un mauvais œil mes affaires avec les cultistes qu'ils traquaient, ils avaient trop besoin de mes services pour me faire fermer boutique. C'était même leur organisation qui me garantissait la pleine propriété de mon établissement. En échange, je les laissais parfois faire leurs petites réunions dans l'arrière-boutique. Tous les alcools ne devaient pas nécessairement finir dans les potions. 

Non, ce qui m'apportait parfois des ennuis étaient mes marchés avec quelques-uns des monstres les plus intelligents, en particulier la gent vampire. Une famille notamment demandait régulièrement la fleur de *Tacca chantrieri*, que je faisais importer directement de Birmanie à un coût faramineux. Malgré tout, l'opulence incroyable des Sittles me garantissait de toujours rentrer dans mes frais, d'autant qu'ils acceptaient de me céder en contrepartie un ingrédient au moins aussi précieux que la fleur, mais que les chasseurs répugnaient à me céder : du sang de vampire. Il fallait jouer finement dans ces eaux troubles, et j'ai perdu plusieurs coursiers dans de malheureux malentendus. Les aléas des affaires. 

Mais même les prestigieux Sittles étaient des vampires inférieurs, l'une des plus faibles familles de la capitale. Je n'avais jamais rencontré de vampire supérieur, à mon grand soulagement. Ils étaient, paraît-il, d'un genre bien moins tolérant envers les humains qui versaient dans les arts de la nuit. Des dires des quelques chasseurs qui étaient parvenus à survivre à de telles rencontres, il était impossible d'ignorer l'identité de l'un de ces vampires lorsqu'il se tient devant vous. C'est pourquoi l'homme qui descendit de cette voiture me terrifia. 

Chose assez inhabituelle pour être remarquée, il n'avait pas de valet à ses côtés, et ouvrit lui-même la portière de la voiture. Alors qu'il traversait les quelques mètres qui le séparaient de ma porte sous une pluie battante, je remarquais qu'il n'y avait pas de cocher. Cet homme était, selon toute vraisemblance, venu seul. Ce point en particulier n'est pas hors de l'ordinaire, dans un métier tel que le mien. J'avais déjà côtoyé plusieurs mages qui se plaisaient à enchanter leurs affaires pour se dispenser du salaire de servants. En revanche, lorsque ma porte s'ouvrit d'elle-même pour le laisser passer, je pus observer qu'il n'était pas trempé, et semblait au contraire sec comme dans un désert. 

Il avait l'apparence d'un noble tout à fait ordinaire, quoiqu'une allure trop altière pour la noblesse moderne. Malgré ses atours ordinaires, une veste et un chapeau sobre ainsi qu'une canne au pommeau doré, il dégageait l'aura d'un roi. Et malgré ses traits de jeune homme portant une élégante moustache noire, il me semblait voir le passage du temps dans ses yeux, comme si je me trouvais face à l'allégorie de l'histoire. Cela ne fait pas beaucoup de sens, j'en conviens, mais décrire une telle sensation n'est pas chose aisée. 

Je me doutais bien que mon existence n'était pas inconnue des entités de son acabit, je n'avais pas fait d'effort particulier pour être discret. Au contraire même, mais je ne m'attendais pas à recevoir une visite aussi prestigieuse. Les vampires les plus anciens ne considèrent les humains que comme des existences passagères indignes de leur attention. J'ai pensé que la visite de l'un d'entre eux, *a fortiori* s'il dissimulait sa prestigieuse lignée par des atours discrets, ne pouvait être que de mauvaises nouvelles. 

Sans m'adresser la moindre attention, il commença à déambuler dans mes rayonnages, examinant les potions, remèdes et poudres que j'exposais. Pour la plupart des produits de faible effet ou des plantes communes. Pour une partie de ma clientèle habituelle, tuer, parfois en masse, n'était qu'une formalité. Alors un vol... Tous les mélanges puissants ou revêtant un quelconque intérêt pour un initié reposaient derrière mon comptoir, dans des vitrines scellées par un mage de ma connaissance, en échange d'un gros service rendu. 

Le noble vampire ne tarda pas à le réaliser, et c'est avec ce que je décelais être un léger agacement, il se dirigea vers moi. Je me suis saisi, hors de sa vue, d'une décoction d'ail. Contrairement aux idées reçues, cette plante est complètement inoffensive pour les vampires, en revanche son odeur leur est hautement inconfortable. Plutôt qu'un moyen de défense, c'était un parfum capable de les dégoûter suffisamment pour qu'ils décident de s'en aller. En dernier recours, je disposais également d'un arsenal de pièges et de mécanismes que je pouvais activer, mais je doutais qu'un seul d'entre eux ait la moindre efficacité contre l'individu qui s'approchait. Rien à ma portée, hélas. 

C'est pourquoi mon soulagement fut grand lorsqu'il se contenta de marcher jusqu'à moi et de s'arrêter, avant de prononcer d'une voix morne, sans la moindre intonation : 

— Quelle est votre arme la plus efficace contre les vampires ? 

Naturellement, cela devait être un test. Sans doute souhaitait-il estimer ma dangerosité afin de décider s'il devait m'éliminer ou non. Je tentais donc un bluff, faisant mine d'être offensé, sans toutefois monter le ton trop haut. 

— Excusez-moi, monsieur, mais vous êtes ici dans un établissement respectable ! Je vends toutes sortes de potions et je peux vous fournir toutes les plantes que vous désirez, mais je ne suis pas un vulgaire artisan de mort. 

C'était plutôt osé, mais la pression de la situation ne m'avait pas permis de trouver plus fin. Sans grande surprise, ce fut un échec cuisant, et sa voix trembla de rage. 

— Ne joue pas avec moi, Alchimiste ! Je ne reposerais pas ma question une seconde fois. 

Il n'avait pas encore achevé sa menace que j'avais déjà disparu dans l'arrière-boutique. Il est des forces avec lesquelles j'avais appris à ne pas me mesurer, et ce vampire devant moi était clairement l'une d'entre elles. Malgré ma bravade, je possédais en réalité une large gamme de potions adaptées à la lutte contre sa gent, ainsi qu'une collection d'artefacts que j'employais comme sécurité. Après tout, ils étaient la plus grande menace de Londres suivie des goules. Et toute décoction ou objet ayant un effet contre les vampires serait efficace contre leurs semblables sans cervelle. 

Je triais avec hâte pour ne garder que les articles ayant les effets les plus spectaculaires, et les lui présentais un par un. D'abord mes propres recettes, dont des élixirs de renforcement qui semblèrent l'amuser. Il montra un intérêt particulier pour la potion d'apathie, un composé très volatile à base de fruits d'*Actaea pachypoda* et de feuilles de *Welwitschia mirabilis*. Cher à produire, ce mélange avait l'étonnant effet d'affaiblir fortement les facultés surnaturelles des vampires qui le respiraient. Voyant son intérêt, je lui proposais une démonstration et lui fit inhaler le parfum délicat. Bien que manifestement vampire, il était étrangement coopératif lorsque je lui proposais des solutions supposées lui faire grand mal. Peut-être savait-il que mes talents étaient inefficaces sur lui. 

Il parut néanmoins convaincu par ma potion d'apathie au point de m'en acheter cinq fioles ainsi que le bulbe de Saint Eusèbe, mon artefact défensif le plus efficace contre tout être de la nuit. Infusé de pouvoir divin, il était capable d’émettre une lumière sacrée lorsque tenue par un homme pieux. Lors de ma démonstration, sa peau commença à se craqueler et à apparaître comme brûlée à certains endroits. Il demanda à ce que je cesse aussitôt, mais semblait tout de même fasciné par cet objet. Je fus réticent à m'en séparer, mais malgré ma résistance il sut trouver des arguments... convaincants. Mon amour pour l'argent était limité, quoique vaste, mais pas celui que je portais aux traités d'alchimie de l'ancien temps. Malgré mes avertissements qu'une entité impie — sans vouloir l'offenser — ne pouvait tenir le bulbe qu'au travers d'une étoffe et trouverait probablement sa proximité inconfortable, il proposa non pas un mais deux traités rarissimes que je n'avais jamais contemplés avant. 

Je me risquais à le questionner sur les raisons qui le poussaient à faire de tels achats, et il me répondit simplement : 

— Un différent. 

Je n'ai pas insisté, et il est reparti dans la nuit. Sa voiture repartit, et ce fut comme si le temps se remettait en marche. J'avais manqué de le remarquer, hautement préoccupé par ce client peu orthodoxe, mais l'infernal grondement de la pluie ne s'était plus fait entendre depuis qu'il était entré. Le jour suivant, j'oubliais tout, et me retrouvais bien plus riche et en possession de deux ouvrages dont j'ignorais tout de la provenance. 

Il revint plusieurs fois, après cela. Chaque fois, la rue se vidait de son activité et le temps cessait de tourner l'espace de sa visite. La mémoire me revenait toujours lorsqu'il passait le pas de ma porte, et je me souvenais alors de ce client si régulier. Il me confiait la préparation de potions, d'élixirs et de bombes particulièrement efficaces dans l'élimination de vampires. Je m'exécutais sans le réaliser durant mes périodes d'amnésie, et sa commande était toujours prête pour sa visite suivante. Il parlait peu, la conversation étant souvent à sens unique. Je pris peu à peu confiance en ce terrifiant individu, assez pour lui parler de plus en plus franchement. Mais la seule réponse que je n'obtins jamais à mes questions fut, lorsque je le questionnais à nouveau sur la raison qu'il avait de continuer ses achats malgré le différent visiblement résolu. 

— Pour le sport. 

Du reste, j'ignore tout. Je ne sais à quelle famille cet homme qui ne m'a jamais donné son nom appartient. Je n'ai aucune idée de l'utilisation qui est faite des articles que je lui fournissais, bien qu'il se consacrât, en toute vraisemblance, à la chasse au vampire. Cette routine mystique perdura un an, où j'oubliais chacun de ses passages. Cependant, ce matin, je me rappelle de tout. Je revois en détail son visage, et chaque interaction que nous avons eue. Et cela me terrifie. 

Si l'emprise qu'il avait sur moi s'est soudainement envolée, je ne fais pas grand cas de la vie de ce mystérieux vampire. Si ses meurtriers apprennent l'identité du responsable de toutes ces décoctions, s'ils apprennent que je fus le fournisseur du vampire qui chassait les siens, il y a fort à parier qu'ils me paieront une visite. J'ai fait appel à tous les amis me devant une faveur. Certains chasseurs ont accepté de me protéger sans me questionner. Je suis en danger. J'écris ces lignes pour que mon éventuel successeur sache ce qu'il m'est arrivé, et pourquoi. J'espère que mon heure n'est pas venue. Je crois qu'oublier me convenait bien, toutes choses considérées. 

 