---
bg: "dame-aux-hiboux"
layout: post
title: La Dame aux hiboux
summary: Un enfant apeuré, une mystérieuse forêt, l'esprit du bois.
source: “お茶会 (Fête du thé)”, par よしおか
ref: https://www.pixiv.net/en/artworks/60855983
date: 2020-01-17
categories: posts
tags: ["Le monde d'Alkénor"]
type: Conte
author: Rumengol
lang: fr
---

"Le hibou me dit qu'un visiteur est arrivé."

Les bruissements d'ailes remplaçaient de temps à autre le silence assourdissant de la forêt par un tonnerre de plumes. Le petit garçon sursautait à chaque nouveau bruit, cherchant désespérément à en identifier la source. Il avait peur, il regrettait maintenant d'avoir pénétré dans la forêt. Il avait entendu dire qu'elle était la demeure de la dame aux hiboux, et que nul ne ressortait jamais de ce bois. Toutes sortes de légendes circulaient sur elle. Pour certaines elle était une très vieille sorcière qui se nourrissait des âmes des égarés, pour d'autres il s'agissait de l'esprit des forêts ayant pris la forme d'une immense chouette accueillant en son sein les orphelins et les enfants rejetés.

C'était ce qu'il était parti chercher dans ces bois, du réconfort. Son père n'était jamais revenu de la guerre et sa mère venait d'être emportée par la maladie. En pleurs, l'enfant avait couru vers les arbres, trop vite pour que quiconque puisse le rattraper. Il était désormais perdu dans une forêt en sommeil. Étrangement, malgré l'hiver et ses vêtements en loques, il n'avait pas froid. Il voulait partir, mais le chemin n'existait plus, et chaque pas le rapprochait un peu plus du cœur de la forêt.

Il déboucha soudainement sur un sentier couvert de broussailles et décoré de banderoles éliminées. Il y régnait une agréable odeur de pétrichor, malgré l'absence de pluie des derniers jours. C'est là qu'il vit le premier hibou. Posé en face de lui, sur une branche basse, il le fixait obstinément. Sa tête se tourna rapidement à l'horizontale, arrachant un petit cri de stupeur au garçon, puis il s'envola dans un hululement. L'un des côtés du sentier, celui par où le hiboux s'était enfui, irradiait d'une nitescence hypnotique. Le petit homme suivit ce chemin, émerveillé par ce sentier de douceurs, remarquant à peine les hiboux qui se posaient sur les arbres alentours, toujours plus nombreux.

Comme s'il s'éveillait d'un profond sommeil, il leva les yeux, désorienté. Devant lui se trouvaient plus de hiboux qu'il n'en avait jamais vu.

"Hoo~Hoo."

Les hululements emplissaient l'atmosphère à un point qu'il n'était plus capable de dire s'il était devenu sourd ou non. Et sur une branche qui lui arrivait un peu au dessus de la tête était assise la dame aux hiboux dans son manteau de plumes. Elle était humaine semblait-il, bien que son visage soit caché par son capuchon pourvu d'oreilles. Ses longues jambes nues flottaient élégamment. Elle tenait une très belle tasse de thé fumant dans ses mains délicates et, près d'elle, à côté du premier hiboux qu'il avait vu, une théière. Un peu plus loin, un couple d'oiseaux tenaient en équilibre sur leurs têtes une assiette de biscuits. Et tous le clouaient sur place de leurs regards implacables.

La brume chatoyante qui avait attiré le garçon enveloppait les lieux, noyant toutes les formes et les silhouettes, n'épargnant que l'élégante dame, îlot de stabilité dans un monde mouvant, presque onirique. Elle prit une gorgée de thé, lentement, distinctement, puis s'exprima de la voix fluette d'une toute jeune enfant, et au même instant, tous les oiseaux se turent.

"Bienvenue dans les profondeurs de cette épaisse forêt. Tu es un visiteur plutôt inhabituel..."

Elle rit doucement, amusée sans être moqueuse.

"A partir de maintenant, je te prierais de suivre l'étiquette de la forêt."

"Vous êtes la dame aux hiboux ? C'est pour vous voir que je suis entré dans la forêt ! Mes parents sont morts, et je n'ai aucun endroit où aller."

Il sentit sa voix se briser sur ses derniers mots. Elle avait l'air gentille, peut-être lui proposerait-elle de rester à ses côtés. Elle rit encore de sa voix cristalline.

"Désolée, mais ici tu es un visiteur sans invitation. Tu ne peux ni rester, ni partir ainsi."

"S'il vous plaît, aidez moi."

Il suppliait, à présent à genoux. Il ignorait s'il lui demandait de ne pas le tuer, ou à l'inverse d'achever ses tourments. La dame aux hiboux releva légèrement la tête, et il put apercevoir son magnifique sourire.
