---
bg: "la-tempete-eternelle"
layout: post
title: La tempête éternelle
summary: Un groupe d'exorcistes scelle une tempête primordiale
source: Lac Maracaibo, Venezuela
ref: https://fr.wikipedia.org/wiki/Foudre_de_Catatumbo
date: 2021-05-26
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---
Le ciel au-delà des montagnes était sans cesse parcouru de traînées vermeils. Le doute ne fut plus permis, nous approchions de la tempête éternelle. Nos guides avaient refusé de nous porter au pied de la chaîne montagneuse, nous forçant à effectuer le reste du voyage à pied. De tous mes compagnons, j’étais sans doute le moins apte physiquement, et nous n’avions même pas débuté l’ascension que je réclamai une pause. 

Je compris pourquoi les locaux nous eurent recommandé de ne pas tenter d’y aller à dos de cheval. Après une dizaine de mètres, la pente devenait particulièrement escarpée, et le seul chemin discernable était à peine assez large pour qu’un humain s’y aventure. Je me demandai comment y faire passer notre homme-oiseau lorsqu’il déploya ses larges ailes et partit en reconnaissance. Bien sûr, les elkens n’avaient pas les mêmes considérations que nous, pauvres êtres cloués sur le plancher des vaches.

De retour, Erry confirma mes craintes : le chemin que j’eus décelé était le seul permettant de progresser. Je profitai autant que possible de mon dernier moment de répit avant ce que j’anticipai être un long moment d’angoisse et de crampes. La montagne ne me déçu pas. Non content de longer un à-pic, le sentier se réduisit parfois tellement que nous ne pouvions procéder que de profil, en agrippant la roche. 

Erry voleta dans notre dos, veillant à nous prévenir d’une chute fatale. Je sentis sa présence particulièrement derrière moi, une sensation aussi vexante que rassurante. Par chance, la roche était solide et le vent nous épargna, aussi le rythme fut correct. Nous parvînmes à triompher de cette première épreuve sans dommages autre que nos mains durement éprouvées. Après cela, nous pûmes respirer, la route empruntant une vallée encaissée entre plusieurs montagnes.

L’air y fut frais et doux, les arbres nous offrirent le couvert lors d’une ondée passagère. Estella sembla  troublée dès notre entrée dans la vallée. Son lien à la terre étant le plus fort de nous tous, nous tînmes compte de son inconfort, mais elle-même l’imputa à notre proximité avec la tempête éternelle. À son crédit, les roulements de tonnerres qui nous parvenaient plus violemment chaque instant incommodèrent toute notre petite troupe. Et de fait, une fois à l’autre extrémité de la vallée, le paysage qui se dessina sous nos yeux ne prêta pas à l’apaisement.

Les braises ardentes repérées à midi étaient désormais occultées par un voile de nuages obscurs. Nous n’étions qu’en milieu d’après-midi, et pourtant nous semblions plongés dans la nuit. Ironiquement, la tempête qui obscurcissait le ciel nous permis également d’y voir clair. Chaque instant, des dizaines d’éclairs zébraient le ciel, frappant sauvagement la surface agitée du lac, s’élevant depuis la cime des arbres qui le bordaient ou s’épandant dans les nuages d’orage. 

Le grondement fut si puissant au dehors de la vallée que Dænalia conjura un sort de silence pour nous en prémunir. Autour d’elle, les sons fanèrent pour ne parvenir qu’étouffés, comme derrière plusieurs épaisseurs d’étoffes. Le désagrément de ce sortilège était que pour couvrir aussi efficacement le tonnerre, la bulle asphyxiait totalement nos paroles et drainait la pauvre magicienne de son énergie.

Nous avisâmes un village en contrebas. Il était donc vrai qu’un peuple fit le choix de demeurer au bord du lac d’éclairs, isolé des âmes simples que l’endroit effrayait. Vénéraient-ils également la foudre, comme le colportaient les rumeurs ? Nous nous tenions prêts à cette éventualité dans notre descente vers ce qui ressembla, après plus ample observation, à un hameau proche de la ruine. Estella et moi-même préparions des sorts de protection, sur nos gardes. Un mot inaudible de notre part et notre groupe se retrouvait enveloppé d’un manteau d’énergie doublé d’une muraille de roche.

Nos précautions s’avérèrent inutiles, lorsqu’ils fut évident que les habitants ne furent pas agressifs, mais plutôt effrayés par notre accoutrement singulier. Je libérai ma magie, mais ne pu m’empêcher de remarquer qu’Estella resta parée à conjurer son sort. Elle n’eut pas tort de maintenir son attention, mais je ne pus discuter avec le chef du village avec un sort au bord des lèvres.

Il sortit justement de l’une des habitations, un vieil homme muni d’un bâton et décoré d’ossements. Probablement un shaman, médium entre les vivants et les morts, mais peu redoutable. Je fis signe à Dænalia de lever son sort, et elle s’exécuta avec soulagement. Le roulement assourdissant revint aussitôt, et une violente douleur me vrilla la tête à cet instant. Le chef sembla saisir ma souffrance, et nous conduisit à l’intérieur de la masure dont il était sorti.

L’isolation sonore de ce qui ne sembla être qu’une cabane de bois fut impressionnante. Mon mal de crâne se soulagea rapidement, et je pus m’entretenir avec le vieux sage. Manœuvrant subtilement, je commençai par lui demander s’il fut au fait de l’origine de la tempête éternelle. Nos employeurs avaient seulement mentionné des rumeurs d’un archimage ayant conjuré un enchantement d’une ampleur incroyable, ou de dieux souhaitant dissimuler aux mortels une île recelant de fabuleux trésors.

Il s’exprima péniblement dans notre langue. Il eut les approximations de celui qui maîtrisait un langage mais ne l’avait pratiqué pendant trop longtemps. Un sourire édenté éclaira son visage à la mention des légendes qui circulaient sur le lac, avant qu’il ne les balaye d’un revers de main. Selon lui, ce n’était là œuvre ni d’homme ni de dieux, mais de la terre-mère elle-même. 

Sa théorie fut fascinante à écouter, mais dérangeante pour notre mission. Lorsque je lui expliqua que nous venions dans le but d’exorciser la tempête éternelle, une expression de rage déchira son visage, et une veine paru sur son front. J’eus anticipé cette réaction, aussi Jura conjura instantanément un sort de restriction, neutralisant les velléités du vieil homme.

Il nous importa peu, en vérité, de savoir que ce fabuleux spectacle était fait de main d’homme, de dieu ou un phénomène naturel. Laissant le village derrière nous, nous profitions de cette scène aussi splendide que terrifiante. Estella et Jura partagèrent mes regrets quant à nos actes futurs, et Erry se sentit humble face à cette entité, maître des cieux implacable dominant sa race sans même concevoir leur existence.

Une fois le rituel décidé, Dænalia nous enveloppa d’une nouvelle bulle de silence afin que nous puissions nous concentrer pleinement. Chacun de nous conjurait dans une harmonie silencieuse un fragment du sortilège qui se traça sous nos yeux. L’extrême complexité du procédé ne tolérait pas la moindre erreur, mais malgré cela je me permis d’être ébloui par l’intensité de la forêt étincelante d’arbres si éphémères.

Ce fut un véritable crève-cœur de sceller une telle merveille, mais nous exécutions un contrat. De plus, cela permettrait d’apporter le repos à la région éprouvée par le tonnerre incessant. Comme si elle pressentit notre action, la tempête se déporta dans notre direction. Hélas pour elle, ce fus déjà trop tard, le rituel était accompli, et le sort conjuré. Elle s’éteignit lentement, se débattant et libérant les derniers éclairs dont elle fut capable.

Longtemps après le dernier impact de la foudre, le tonnerre résonna encore, amplifié par les contreforts des montagnes. Mais ce ne fut qu’un vain râle d’agonie, et déjà les nuages se clairsemèrent, révélant un ciel crépusculaire qui calma les ultimes ardeurs de la tempête dont l’éternité vint de s’achever.