---
bg: "ker-arbath"
layout: post
title: Ker Arbath
summary: Un coursier se dirige vers la citadelle pour la ravitailler.
source: “Nhagruul's Bane” par Käri Christensen
ref: http://www.karichristensen.com/y8ooxlifz75bmmh5pzm2r80rhntnc4
date: 2021-05-26
categories: posts
tags: ["Le monde d'Alkénor"]
type: Conte
author: Rumengol
lang: fr
---
Les habitants de la forteresse de Ker Arbath n’avaient jamais eu la vie facile. Bâtie, ou plutôt sculptée dans un rocher culminant au dessus du Gouffre, elle marquait l’ultime bastion de la civilisation humaine dans les terres désolées d’Ankäshær. 

Les occupants du château se faisaient appeler les frontaliers, des guerriers sans pareils. On disait d’eux qu’ils étaient passés maîtres dans le maniement de toutes les armes existantes, et que nul homme ne pouvait les vaincre. Ils partageaient leur combat avec de grands mages aptes aux arcanes les plus obscures, parfois interdites. 

Depuis des siècles, les combattants de Ker Arbath repoussaient sans relâche les monstruosités démoniaques qui remontaient du Gouffre. Dragons, hordes morte-vivantes, araignées aussi grandes qu’une ville et d’autres abominations dont la simple description suffirait à faire cauchemarder le plus endurci des guerriers.

C’était tous de grands hommes et femmes à la détermination inépuisable, mais il en existait d’autres plus admirables encore. Eux ne s’illustraient pas dans des combats épiques aux ires apogées, mais leur mérite était égal. Ils étaient les ravitailleurs de la citadelle.

Si les terres d’Ankashær ne sont pas parcourues par les terreurs innommables du Gouffre, elles grouillent tout de même de monstres de moindre envergure. Des petits démons ou créatures des ténèbres suffisamment insignifiantes pour passer entre les mailles du filet magique de Ker Arbath.

Les frontaliers, aussi redoutables soient-ils, ne commettaient jamais l’imprudence de s’aventurer seuls hors des murs de la forteresse. Et pourtant les coursiers réalisaient de long voyages à travers ce territoire désolé pour apporter des vivres et du matériel au château loin de toute civilisation.

C’était un métier dans lequel on ne vivait pas vieux, et il était commun qu’un coursier n’arrive jamais à destination. C’est pourquoi la plupart du temps, trois groupes d’un ou deux cavalier partaient de Ker Durmann, la forteresse gardant la frontière entre le royaume d’Érénor et les terres d’Ankashær. 

Dargryll était l’un de ces valeureux. À trente-trois ans, il était l’un des plus vieux coursiers encore en activité dans cette région, et se préparait pour son huitième voyage. Chaque coursier avait sa méthode pour arriver sain et sauf à Ker Arbath. La vitesse, la force, la furtivité, la magie... Toutes échouaient au bout d’un moment. 

C’est pourquoi il ne s’était jamais maintenu sur une seule voie, mais les avait toutes explorées. Il n’était ni le plus rapide, ni le meilleur combattant, ni le plus discret et encore moins le plus doué avec la magie, mais il avait toutes ces compétences. 

Le jour se couchait comme il franchissait les portes de Ker Durmann. Même si une grande partie des créatures étaient nocturnes, celles dont il se méfiait le plus étaient diurnes. Il allait peu dormir pendant les quatre jours que dureraient son périple, aussi il s’était reposé autant que possible et se sentait prêt à abattre des montagnes. 

Son cheval traça à travers les terres rocailleuses sans faiblir pendant plusieurs heures, les plus tranquilles du périple. Plus Dargryll s’enfonçait dans Ankashær, plus il ressentait l’hostilité de menaces toujours plus grandes. 

La première créature à oser l’attaquer fut un scolopendre aussi grand que lui. Enfoui sous le gravier, il avait tendu une embuscade qui avait manqué de désarçonner le coursier. Ce dernier avait tenté de lui rendre la pareille en tranchant ce qui ressemblait à une tête, mais son épée avait rebondi sur la carapace chitineuse de l’insecte.

Refusant de perdre du temps dans un combat vain et incertain, le cavalier opta pour la fuite, et distança rapidement son adversaire. Dans les heures qui suivirent, il fit de nombreuses rencontres, passant certaines au fil de sa lame, évitant d’autres.

Le territoire monotone prêtait à l’inattention, il ne trouvait aucun détail auquel se rattacher quand tout était en nuances de gris. Le ciel était en permanence masqué par une épaisse chape de nuages. Il se demandait parfois s’il ne se trouvait pas dans une gigantesque caverne, si le ciel dont il se languissait n’était pas qu’un mur de pierre.

Il abandonna ses considérations philosophiques en dépassant la dépouille éthérée. Un mage coursier, tué des dizaines d’années auparavant en pleine incantation, laissant derrière lui son corps encore brillant de magie. Il servait désormais de marqueur à ceux qui empruntaient ce chemin. Au delà, le périple prenait une tournure encore plus dangereuse.

Dargryll fit claquer les rênes et donna un coup de talon dans les flancs de son cheval. À partir de là, il comptait avaler le plus de distance possible avant de rencontrer quoi que ce soit. Il fut immédiatement pris en chasse par une meute d’énormes loups.

Résolu à pousser sa monture jusqu’au bout, le coursier la pressa à nouveau. L’étalon fit illusion un moment, mais lui non plus ne s’était pas reposé depuis la nuit dernière. Leurs poursuivants comblaient l’écart, Dargryll pouvait déjà sentir leur haleine putride dans son dos. 

Au moment où il était perdu, il se mit debout sur la selle, et sauta aussi loin que possible, déséquilibrant le cheval qui emporta dans sa chute plusieurs loups. Il avait rempli son rôle jusqu’à la fin, l’avait porté loin et et se sacrifiait à présent pour son cavalier. 

Le coursier s’éloigna rapidement du lieu du festin des loups. Son anneau de stockage était un peu éraflé, mais son contenu n’avait pas l’air endommagé. Il continua sa route à marche soutenue sur le terrain escarpé, bien plus lentement qu’avant. Il avait perdu sa monture plus tôt qu’il ne l’avait prévu, mais ça ne devrait pas le mettre trop en retard.

Une rumeur sourde lui parvint devant lui, et une forme massive plongea la région dans l’obscurité. Quelque chose arrivait. Pourtant, il était encore loin de Ker Arbath. Comme la créature avançait à pas de géants vers lui, il put discerner sa silhouette. Il en avait déjà vu, des araignées géantes dans le genre. Malgré leur aspect effrayant, elles n’étaient pas de taille face aux défenseurs de la citadelle.

Dargryll se replia dans un renfoncement rocheux. Quelques minutes plus tard, une énorme patte velue frappa le sol près de son refuge, suivie d’un abdomen grotesque. Plus de doute, le monstre n’avait pas pu se faufiler sans être détectée. Quelques chose était arrivé à Ker Arbath, quelque chose de suffisamment grave pour arriver à une telle situation.

La peur au ventre, le coursier se pressa vers la citadelle, multipliant les détours et les abris pour se soustraire aux regards des immenses monstres qui émergeaient du Gouffre. 

Il arriva pour contempler les ruines de Ker Arbath. L’ultime bastion qui protégeait le monde des horreurs du Gouffre était tombé. Un âge sombre se profilait, et Dargryll ressentit un bref soulagement de savoir qu’il ne serait pas là pour y assister. Car il n’y aura pas de voyage retour pour le coursier.