---
bg: "la-sorciere-du-nord"
layout: post
title: La sorcière du Nord
summary: L'épopée précipitée d'une jeune magicienne bannie dans un territoire mortel.
source: Par Piotr Foksowicz
ref: https://www.artstation.com/piofoks
date: 2020-02-07
categories: posts
tags: ["Le monde d'Alkénor"]
type: Conte
author: Rumengol
lang: fr
---

Pour survivre parmi les monstres, deviens comme eux. Cette phrase avait guidé la vie de Nidarael pendant les vingts longues années de son exil. Son seul crime, qui lui avait valu la peine capitale alors qu'elle venait d'avoir dix ans, était d'avoir manifesté une étincelle de magie. N'importe où ailleurs, elle aurait été exécutée. Mais le meurtre, peu importe sa forme, était proscrit en Sélenie. Douce hypocrisie que de réserver aux pires criminels un sort pire que l'échafaud sous couvert d'éthique. Encore petite fille, elle fut conduite à la frontière septentrionale pieds et poings liés. On la jeta dans la fange en compagnie de deux hommes sitôt ses liens retirés. Le discours de l'homme qui se tenait en haut des remparts hanterait nombre de ses nuits.

"Sam dit "Le Noir", pour viols et assassinats, Ethel Vignefred, pour tentative de régicide, et Nidarael de Sygneferl, pour détention de capacités magiques, vous avez étés jugés et déclarés coupables. Vous êtes des monstres et en tant que tels, vous êtes condamnés à l'exil dans les Terres de Vormyrach. Vous n'êtes plus sujets de Sélenie, et serez dès le prochain lever du jour considérés comme ennemis du royaume et abattus à vue. Je ne vous souhaite pas bonne chance, abjectes pourritures, je vous souhaite de mourir dans d'atroces souffrances."

Les soldats postés sur la muraille les moquaient, les invectivaient, certains crachèrent même de mépris. Le nobliau semblait désemparé, le regard mi-fou, mi-vide, et la petite fille avait fondu en larmes. Seul l'homme au visage noirci par la suie gardait contenance. Il attrapa par le cou ses compagnons d'infortune comme s'ils ne pesaient rien et les tira sous le couvert des bois. La neige mêlée à la boue formait des flaques de mélasse épaisse à la profondeur trompeuse. Le colosse attendit un temps que les deux autres se calment puis, à court de patience, les gifla.

"Vous savez où on est, là ? Ethel secoua mollement la tête. C'est Vormyrach, les terres maudites. J'en ai entendu parler dans l'milieu. Le dépotoir des Dieux, à ce qu'on dit. Tout c'qu'ils ont raté, ou qui était trop moche pour le monde, ils l'ont foutu là. Y a des trucs là-dedans, c'est pire que la mort."

Il se pencha vers eux avec un air conspirateur.

"Mais on dit qu'il y a un moyen d'en sortir, de se barrer d'ici."

Ces derniers mots captèrent immédiatement l'attention d'Ethel et interrompirent les sanglots de Nidarael. Fier de son effet, Sam Le Noir poursuivit.

"Pour survivre parmi les monstres, deviens-en un toi même, qu'on dit. Le sergent là, nous a traité de monstres, mais même moi j'suis un gamin de Sylla à côté de c'qui rôde dans l'coin. Faut dev'nir pire que tout si on veut avoir une chance."

Puis il se redressa et lâcha d'une voix forte et nonchalante une phrase qui fit l'effet d'une douche glacée.

"Mais bon, c'est qu'des conneries. Pas un seul corniaud pour rev'nir et raconter. Tous les trois, on y passe c'te nuit, alors autant s'faire plaisir jusqu'à la fin, non ?"

Il lança un regard entendu au régicide raté, qui se tenait à présent la tête en pleurant. Déçu de l'absence de réponse, Le Noir se retourna vers Nidarael, les yeux à présent empli d'une lubricité malfaisante. Elle tenta de reculer mais trébucha dans la panique. L'autre enserra sa jambe dans un étau de fer. La petite fille cria et se débattit, mais c'était peine perdue. Son adversaire aurait pu la briser d'une main, sans effort. Mais ses projets pour elle étaient tout autres. Il l'écrasait sous le poids de sa jambe pour la maintenir immobile pendant qu'il déboutonnait son pantalon. Sans cesser de lutter, Nidarael fermait déjà les yeux, pour ne pas voir ce qui allait se produire.

Une lumière aveuglante embrasa soudain le sous-bois. Même à travers ses paupières fermées, l'enfant était blessée par cette nitescence subite. Le poids qui lui compressait la poitrine s'était envolé. Ouvrant prudemment les yeux, elle constata avec soulagement que la montagne de muscle qui s'apprêtait à lui dérober son innocence avait disparu. Pas totalement cependant, sa botte était encore droite, le pied toujours à l'intérieur. Elle n'avait aucune idée de ce qui venait de se produire, mais au vu du regard horrifié que noble resté en retrait lui lançait, elle en était la cause.

Elle regarda ses mains sans trop comprendre. Était-ce vraiment de son fait ? Et qu'avait-elle fait, exactement ? Elle doutait même d'être capable de magie. Elle n'avait auparavant été capable de produire qu'une petite sphère lumineuse inconstante, qu'elle avait baladé dans sa chambre avant qu'elle ne s'éteigne. Elle avait même cru à l'apparition d'un esprit. Mais quand cette sphère laiteuse était réapparue en pleine rue à ses côtés, l'Inquisitoire s'était emparée d'elle et l'avait condamnée sans plus de procès.

Des mouvements dans les fourrés firent tomber un peu de neige et coupèrent aux interrogations de la petite fille. L'homme survivant hésita, ses yeux paniqués oscillant entre le buisson et Nidarael. Il était visiblement en proie à un dilemme, ne parvenant à décider ce qui l'effrayait le plus. Soit il outrepassa sa peur superstitieuse de la magie, soit celle de l'inconnu était plus forte. Il prit doucement, en évitant tout geste brusque, la main de la fille toujours sous le choc et l'aida à se relever. Il fit une grimace qui se voulait rassurante et indiqua le fourré, qui continuait à s'agiter.

"Petite, tu peux nous débarrasser de ça aussi ?"

Il eut pour toute réponse le regard interdit de Nidarael. Elle ne comprenait pas ce qui se venait de se passer, ce qu'elle avait fait.

"Tant pis. Courons !"

Il l'empoigna aussi fermement qu'il en était capable et commença à détaler au hasard. Ils trébuchèrent de nombreuses fois, s'étalant au sol parfois même. Ce qui se dissimulait dans le buisson enneigé se jeta à leur poursuite. La chose n'était heureusement pas très rapide, mais bien plus sereine et adroite. Elle gagnait peu à peu du terrain, jouant avec ses proies. Dans un éclair de lucidité - ou bien de désespoir - Ethel fit grimper la petite à un arbre dont les branches étaient suffisamment hautes et solides pour l'abriter. Il escalada ensuite un arbre voisin, préférant ne pas confier leurs deux vies à un arbre mort depuis des lustres.

Il n'était visiblement pas rompu à cet exercice et, plus lourd que l'enfant, brisa plusieurs branches et s'écorcha les mains avant de pouvoir être hors de portée de leur prédateur, à condition qu'il ne sache pas grimper aux arbres. La créature arriva aux pieds des arbres peu de temps après. Elle n'avait pas de forme précise, plutôt un amoncellement informe de chair duquel dépassait là un bras, là une jambe. Du sang, du pus et une autre substance étrange dégoulinaient de son simulacre de corps, souillant la neige dans son sillage. Il était impossible de dire comment il se déplaçait, mais il était clair que cette horreur était incapable de monter à un arbre. Il n'était même pas certain qu'elle soit pourvue d'yeux. Après avoir erré longuement à leur recherche dans les alentours, elle sembla renoncer et s'éloigna.

La nuit tombait et il n'était plus question de descendre. Pour combattre leur terreur, ils se parlèrent, apprirent à se connaître, en parlant le plus doucement possible. Ethel était le premier fils de la famille Vignefred, destiné à reprendre les rênes de la famille. Mais lui était un rêveur et ne s'intéressait qu'à l'art des marionnettistes. Son père le méprisait pour sa passion "paysanne", et le soir où Ethel était venu lui annoncer qu'il ne souhaitait pas lui succéder et qu'il comptait quitter le pays, son père lui prévint qu'il ne tolérerait pas un tel déshonneur. Le lendemain, il se retrouvait accusé de régicide et envoyé dans les terres de Vormyrach.

De son côté, il apprit à connaître la petite fille qui l'avait d'abord effrayé, et découvrit que la magie n'était pas cette arcane maudite portée par les démons comme on lui avait enseigné. Ils parvinrent, de cachettes en refuges, à survivre. Nidarael apprit à contrôler et utiliser son pouvoir, et ils purent enfin cesser de se terrer. Elle se découvrit, grâce à Ethel, un don tout particulier à insuffler des étincelles de vie dans l'inanimé. Ainsi, l'homme fabriquait des poupées et marionnettes de bois et de feuillage, qui prenaient par la suite vie. Ces petits êtres servaient de sentinelles, d'éclaireurs et de compagnons de chaque instant. Plus tard, elle anima des êtres difformes, des amas de bois à forme vaguement humaine. Leur apparence dégoûtait Ethel, mais elle ne l'écoutait pas. Pour survivre parmi les monstres, deviens comme eux.

Elle délaissait les poupées des premiers jours au profit d'être aux formes des plus odieuses, des monstres pour combattre les monstres. Ces nouveaux serviteurs étaient assez forts pour leur bâtir une véritable maison de bois au milieu d'un marais souvent verglacé. Un caprice de l'homme vieillissant, qui souhaitait pouvoir se reposer sous un toit. Ils s'étaient accommodés à la rudesse du Nord et ne se formalisaient désormais plus du froid ni du vent. La rancœur de Nidarael devenant adolescente grandissait de jour en jour, au dam de son compagnon d'infortune devenu comme un père d'adoption.

Un jour, il tomba malade. Quinze ans durant, Nidarael avait œuvré pour devenir la plus dangereuse des monstres des terres de Vormyrach, afin de protéger Ethel. Mais c'est finalement un ennemi plus pernicieux qui l'abattit. Un mal terrible, lors d'un hiver pourtant peu rigoureux. Mais l'âge frappait durement dans ces contrées, surtout pour ceux n'y étant pas préparés. Il souffrit beaucoup, ayant à la fois chaud et froid, ses entrailles le brûlant. Il mourut deux semaines plus tard, après que Nidarael ait tenté tout ce qu'elle pouvait pour le sauver, en vain. Elle fit de cette maison qu'il avait tant désiré son bûcher funéraire, avant de la quitter.

Sans rien pour la réprimer, sa haine envers les humains qui les avaient bannis était à son paroxysme. Ils ne l'avaient pas tué, car ainsi était la loi Sélenienne. Ils allaient chèrement payer cette erreur. Cinq ans elle erra dans l'immensité des terres de Vormyrach, constituant une armée de monstres de bois, de pierre et de chair. Chaque fois qu'elle allumait une étincelle de vie dans un pantin inanimé, elle sentait une part de son âme la quitter. Mais peu lui importait, tant que sa colère demeurait intacte. Enfin, quand elle estima que ses troupes étaient suffisantes, elle retrouva le chemin de sa terre natale. La Sélenie, Sygneferl, tout brûlerait jusqu'aux cendres, elle s'en faisait la promesse.
