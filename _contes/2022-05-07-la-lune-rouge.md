---
bg: "la-lune-rouge"
layout: post
title: La lune rouge
summary: Il a tenté de les avertir. Ils ont refusé de le croire.
source: > 
    [Sans Titre] par Abdullah Evindar
ref: https://www.instagram.com/p/B0tBv0PHehM/
date: 2022-05-07
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---
Ils avaient refusé de me croire. Déjà, quand je prophétisais la lune rouge ils me riaient au nez. J'étais le seul, au travers de mon télescope ont-ils dit. Quel diplôme d'astronomie je pouvais leur montrer pour prouver mes dires, m'ont-ils demandé. Comme si les diplômes faisaient tout. J'aurais pu en acheter un pour quelques centaines d'euros, et ils se seraient tus. Mais pas besoin d'un diplôme ridicule pour voir l'évidence.

Quand leurs rires se faisaient trop blessants, j'allais sous mon arbre, pour penser. C'était un grand arbre, possédant des branches se ramifiant par centaines. Mais mon arbre était mort, tué par la maladie lorsque j'étais enfant. Il n'était jamais tombé, mais ne fleurirait plus. Ses feuilles étouffées étaient toutes tombées. Enfant, je m'étais amusé dans ce parterre automnal en plein mois de juillet, ignorant la signification de cet évènement tragique. Mais cet arbre nu, fragile, était resté debout et droit toutes ces années. J'aimais venir en haut de cette colline et poser mon dos contre le tronc.

Je conversais avec lui, confiais mes pensées les plus sombres comme les plus joyeuses avec ce cadavre arboricole. Il ne m'a jamais répondu, bien sûr. Il est mort. Et c'est bien pour cela que je lui racontais tout. Seuls les morts sont capables de garder des secrets. Mais il n'était d'aucune aide lorsque je me suis plains des brimades que me faisaient subir ceux qui ne me croyaient pas. Je lui en ai voulu au début de rester planté là. Mais ce n'était pas sa faute. C'est un arbre mort.

Plus la lune rouge se rapprochait, et plus je montais la petite colline. J'étais de moins en moins chez moi, je ne venais plus au travail. C'est pour ça qu'on m'a viré, je crois. Peu importe, l'argent ne sera plus un problème lorsqu'il sera évident que j'ai été le premier à avertir les gens de l'approche de la lune rouge. J'avais suffisamment d'économies pour quelques mois. Il me suffisait d'attendre.

Mais ses effets délétères ne l'attendirent pas pour se déclencher. Avant même qu'elle ne soit un point visible à l'œil nu, les gens commencèrent à changer. D'abord dans leur comportement, ils devenaient irascibles ou instables. Schizophrènes, d'une certaine manière. Ça ne touchait pas tout le monde, encore heureux. Mais même les normaux faisaient comme si tout était normal. Peut-être étais-je le seul à rester lucide dans l'anticipation du chaos ?

Malgré mes avertissements, la situation n'a fait que s'aggraver. De plus en plus de gens changeaient, et les altérations étaient de pire en pire. J'en venais à ne plus reconnaître des amis de longue date, voire ma propre grand-mère. Je connaissais la harpie mieux que personne. Elle était acariâtre, impossible à satisfaire et ne manquait jamais une occasion de me rabaisser. Puis un jour, soudainement, elle est devenue mielleuse, me suppliant de lui pardonner son comportement. Tout le monde disait qu'elle avait changé comme si elle s'était simplement remise en question. Mais je savais. Ce n'était plus la même personne.

Elle était morte deux semaines plus tard, soi-disant d'un cancer en phase terminale. Mais moi, je sais ce qu'il s'est réellement passé. Son corps trop faible n'a pas pu supporter la transformation et s'est écroulé. Je ne vais pas dire que je la regrette, mais dans le fond elle n'était qu'une victime. Personne n'a voulu me croire lorsque je leur ai dit. Ils m'ont lancé des regards dubitatifs, voire inquiets. Ils s'inquiètent pour moi ! Alors que je suis le seul suffisamment pragmatique pour m'inquiéter pour le monde entier !

J'étais encore sous l'arbre, ce soir-là. Pas pour pleurer, je n'avais ni larme ni tristesse. Juste une profonde colère pour leur ignorance. C'est là qu'elle est apparue dans le ciel. La lune pâle qui nous berçait de ses rayons célestes avait laissé place à un autre astre, bien plus grand et bien trop proche. Un astre entièrement rouge. Je pouvais la voir au travers des branches fournies de mon arbre, cette teinture qui obscurcissait le ciel. Je m'étais reculé pour mieux voir, mais cette nouvelle lune était impossible à embrasser du regard tant elle recouvrait l'horizon. La lune rouge.

Les lumières du village étaient en partie allumées comme à leur habitude. Comme si rien d'inhabituel ne se passait. Pourtant ils n'avaient pas encore tous été remplacés. Peut-être que les derniers normaux n'avaient pas vu la lune. Je devais leur montrer. Leur montrer que je n'étais pas un illuminé, que tout cela était réel, et que la seule folie avait été d'ignorer ce qui nous fonçait irrémédiablement dessus. J'avais débarqué dans le bourg en criant ma victoire comme un forcené, pointant la lune rouge qui surplombait la campagne.

Ils avaient ri, ces imbéciles. L'un d'eux s'était approché de moi en me parlant d'une voix apaisante. Il avait craché un non-sens répété à propos d'un évènement rare mais parfaitement naturel, qu'on devait admirer et non pas craindre. Bien sûr, il avait été remplacé en premier, l'instituteur. Ainsi il pouvait enfoncer ses mensonges dans le crâne des enfants. C'en était trop, je l'ai frappé. Un bon crochet du droit, qui l'a cueilli en pleine mâchoire. Je crois que j'ai senti un os craquer, et ce n'était pas à moi. Avant que les autres remplacés ne puissent se jeter sur moi, j'étais déjà remonté dans ma voiture pour rentrer à toute allure.

Ils ont encore refusé de me croire. Mais ce n'était pas moi. Elle était déjà morte bien avant que je n'arrive. Elle était l'une des seules à rester à mes côtés malgré le monde qui me tournait le dos. Je savais qu'elle ne pouvait pas succomber si facilement aux mensonges. C'est pour ça que je ne peux pas l'avoir fait. Elle n'était déjà plus là. Je n'ai pas appelé la police, c'est vrai. J'aurais pu, mais eux non plus ne me croiraient pas. Et puis, ils pouvaient en être aussi.

Je me suis réfugié sous mon arbre. J'ai attendu là plusieurs heures. L'aube commence à poindre, mais la lune rouge est toujours là, inchangée. Elle restera là pour toujours maintenant. Elle n'a pas fait tout ce voyage pour se soumettre au ballet des astres ordinaires. Dans le lointain, les rayons timides du soleil percent la nuit sans nuage. L'horizon se teinte de rose puis d'orange, alors même que la campagne est toujours plongée dans le noir.

Les lumières du village se multiplient. Ils se réveillent. Tous doivent avoir été remplacés à présent. L'influence de la lune rouge est si forte. Je m'interroge. Comment est-ce possible que je n'ai pas été affecté ? Était-ce mon arbre qui m'avait protégé, de ses ramifications qui formaient un filet capable de retenir les mauvaises influences ? En ce cas je ne pouvais plus le quitter. Elle était trop forte. Peut-être si elle détournait son regard de moi je pourrais tenter de m'échapper. Mais elle n'en a pas l'intention, elle est toujours plus proche.

Je distingue des lueurs en contrebas. Elles sont nombreuses. Elles se dirigent droit vers moi, vers mon arbre. Ils ne peuvent avoir déjà trouvé ce qu'il reste d'elle. Mais ils savent. Ils savent que je suis là, que je ne suis pas comme eux. Ils obéissent à la lune rouge. Elle est incapable de m'atteindre sous la protection de l'arbre, alors elle a envoyé son armée pour me déloger. D'ici quelques minutes, ils abattront l'arbre et m'emmèneront loin. Ils me saisiront, m'attacheront peut-être. Ils me forceront à être remplacé, tout comme eux. Ça y est, je sens les larmes venir.

Un dernier regard à mon sanctuaire, puis j'enfouis ma tête dans mes bras. Je ne veux pas les voir arriver. Je ne veux pas qu'ils voient que je pleure. Je les entends. Ils seront bientôt là.