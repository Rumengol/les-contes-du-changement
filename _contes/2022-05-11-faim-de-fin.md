---
bg: "faim-de-fin"
layout: post
title: Faim de fin
summary: Un ancien dieu se libère de son emprisonnement millénaire et regagne la civilisation.
source: “Wukong” par Shan Qiao
ref: https://www.artstation.com/artwork/baQaKa
date: 2022-05-11
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---
— Ça y est, le village de Gongbu a été touché. C'est une famille entière, cette fois. 

— Alors c'est tout le sud de la rivière Yalong qui est menacé. C'est lui, il n'y a aucun doute ! 

Il y a de cela treize millénaires, un pouvoir malicieux s'était levé sur la terre des hommes, mettant en danger son existence. Cinq dieux avaient pris les armes et s'y étaient opposés. Long Siyun le dragon du Sud protecteur des cieux ; Lin Sifeng l'enfant du renouveau, protecteur des campagnes ; Hadress qui régnait sur la mort et l'Ouest ; Ryeladd du Nord ; et le roi des singes, protecteur du Wudang. Ensemble, ils avaient combattu héroïquement la menace trois millénaires durant. Ils étaient puissants, des combattants sans pareil même parmi leurs pairs. Ils se battaient pour le monde des humains, répondant à leur promesse intemporelle. 

Leur bonté et leur amour pour les humains n'avaient aucune limite. Ils versèrent plus de sang qu'il n'en faut pour remplir les rivières, reçurent plus de cicatrices que la terre ne comptait de montagnes. Et malgré le silence de leurs pairs divins, ils firent front. Pour chaque blessure, une dix fois plus douloureuse était infligée à la malice. Même la disparition de Lin Sifeng ne suffit à entamer leur détermination, et ils vainquirent. 

Une victoire amère. Les milliers d'années de lutte sans relâche avaient pris leur taxe, et les esprits des dieux-héros s'étaient érodés tandis qu'ils laissaient libre court à leur rage sanguinaire. Plutôt que de fêter leur succès, les combattants s'apprêtaient à s’entre-tuer lorsque la sagesse de Long Siyun apaisa le chaos. Le dragon avait été le seul à ne pas se perdre et son esprit immortel avait été préservé. Il ramena ses amis à la raison, refroidissant leurs ardeurs. Hadress fut prompt à prendre congé, son royaume le réclamant. Ryeladd partit veiller sur les tribus durement éprouvées par des millénaires sans patron, et le dragon s'éleva dans le ciel commander aux tempêtes qui parcouraient le monde de s'arrêter. 

Le roi des singes resta seul sur l'ultime champ de bataille, au cœur du Wudang qu'il était de son devoir de protéger. La désolation l'entourant, son esprit n'avait pas guéri aussi vite que celui des autres. La malice l'avait touché plus profondément, corrompant non ses pensées mais son cœur. Au fond de lui, il sut qu'il n'aimait plus les hommes. Il les haïssait. Il méprisait cette espèce inférieure pour laquelle il avait donné jusqu'à son essence, et qui ne lui offrait que des louanges en échange. Il avait été sévèrement blessé en portant le coup fatal, et son bras portait des stigmates indélébiles, sous la forme d'arabesques brûlantes qui rongeaient sa chair. 

En une nuit, le travail de trois mille ans de guerre fut réduit à néant, et le Wudang disparut. Les adorateurs du roi des singes étaient nombreux, et leur art martial avait tenu en respect une grande partie des sous-fifres de la malice. Mais ils n'étaient rien contre l'incarnation de la martialité. Le carnage entourait le roi de singes où qu'il allait, ne laissant dans son sillage que montagnes désolées et terres arides. 

Là encore, les protecteurs des cinq royaumes survivants se rallièrent pour affronter cette nouvelle menace. Ils étaient peinés d'affronter leur ancien compagnon, mais ils savaient qu'ils devaient arrêter la bête dans sa folie furieuse avant qu'elle ne touche leurs territoires. Le combat était déséquilibré. Le roi des singes avait toujours été le plus puissant combattant. Les cantiques du Wudang, lorsqu'il existait encore, affirmaient qu'il aurait vaincu pareillement la malice, eut-il été seul à lui faire face. 

Hadress tomba lorsque le bâton acéré le transperça vingt-sept mille fois. Pour un jour, la mort n'eut plus d'emprise sur le monde. Ce qui n'empêcha pas Ryeladd de périr lorsque, saisissant chaque bord de sa mâchoire, le roi des singes la sépara en deux. Long Siyun se révéla être un prodigieux adversaire, repoussant le déicide dans ses retranchements. Jouant de sa taille et de sa maîtrise des cieux, il força le roi des singes à livrer une bataille défavorable. On dit aujourd'hui que le Wudang aurait pu être repeuplé avant leur affrontement, mais que le déchaînement de leurs pouvoirs avaient tant heurté la terre qu'elle avait fait émerger des dents de pierres tranchantes dénuées de toute vie en réponse. 

Nul dragon ne pouvait vaincre le roi des singes, et de ce fait nul être vivant. Au bout de trois jours et trois nuits, le corps infini du dragon s'empala sur les griffes rocheuses. Il était mort bien avant de toucher le sol, défait par la menace qu'il avait juré d'éliminer. Le roi des singes, insatisfait de l'issue de la bataille, l'aurait démembré et entrepris de dévorer sa chair dans un accès de folie. Et depuis sept mille ans, il se repaissait du cadavre du plus grand des dieux. Il s'agissait là de l'ultime présent de Long Siyun à l'humanité, une distraction qu'il ne pouvait qu'espérer suffisante dans les derniers instants de sa très longue vie. 

Telle était l'histoire racontée aux enfants du pays de Si. Le roi des singes, dont le crime fut si odieux qu'il en a perdu son nom, se terre dans l'ancien Wudang, et il viendra te dévorer si tu ne finis pas ton repas ! Un conte, une histoire pour faire peur. Rien de sérieux, le territoire n'était même pas interdit d'accès, juste déconseillé. Les montagnes y étaient escarpées, et plus d'un alpiniste avait trouvé la mort après un pas malheureux. La légende avait presque été oubliée, jusqu'à ce qu'un historien fasse la corrélation avec les récentes disparitions qui survenaient au Nord-Est du Wudang. 

— Muwon, ne me dit pas que tu crois à cette légende urbaine ? Si un dieu-singe existait et qu'il dévorait un dragon infini, tu ne crois pas qu'on serait au courant ? Haha, si je l'avais en face de moi, je... 

— **qUe FeRaIs-Tu Si Je Me PrÉsEnTaIs En FaCe De ToI, pEtIt HuMaIn ?** 

La voix rauque venait d'entre les deux amis, qui s'écartèrent brutalement. Sans même faire sens des mots, le ton lugubre et distordu suffit à les effrayer. Mais la réelle terreur ne vint qu'au moment où ils virent clairement l'individu qui s'était immiscé entre eux. Il était très grand. Deux mètres, peut-être trois, mais son dos s'était courbé plus qu'il ne paraissait possible pour placer sa tête entre les deux adolescents. Vêtu comme les moines qu'on pouvait retrouver dans les livres d'histoire, jusqu'à la gourde à la ceinture et au rudraksha à son cou. Mais le plus perturbant était son corps.  

Horriblement poilu, il était mal proportionné et heurtait le regard. Ses mains comme ses pieds nus sont griffus et beaucoup trop longs. Son gros orteil formait un angle étrange, qui ne devrait pas être possible. Son visage était dissimulé par sa longue chevelure blanche qui touchait presque le sol, tachée d'innombrables gouttes de sang séché depuis des lustres. Jusqu'à ce qu'il ne tourne la tête pour révéler un visage qui n'avait rien d'humain. 

Même en ignorant sa pilosité abondante, c'était un faciès simiesque qui fixait Muwon. Sa face était blafarde, comme malade, mais ses crocs avaient l'air suffisamment acérés et ses yeux brûlaient d'une rage terrible. Entre ses deux orbites, une cicatrice en forme de croix pulsait agressivement, comme si elle était sur le point d'exploser. Il avisa un instant Chen, qui s'était évanoui, et reporta son attention sur son ami qui tentait maladroitement de sortir son téléphone. 

À peine eut-il le temps de s'en saisir que celui-ci explosa dans ses mains. L'extrémité d'un bâton qu'il n'avait pas remarqué avant était apparu à quelques centimètres de son visage. 

— **tU eS aUdAcIeUx De VoUlOiR m'AtTaQuEr AvEc UnE aRmE sI pAtHéTiQuE.** 

Muwon voulut le corriger, dire que son téléphone n'avait rien d'une arme, mais les mots moururent dans sa gorge. Il ouvrit la bouche plusieurs fois et la referma, incapable de produire le moindre son. Il recommença plusieurs fois. La quatrième, son crâne explosa. 

Le dieu libéré s'éloigna, encore un peu plus frustré. Cette fois non plus il n'avait obtenu aucune réponse. Le monde avait changé pendant qu'il était emprisonné dans son propre esprit. Le maudit dragon avait usé d'un éclat de malice pour le lier à son être jusqu'à ce que plus rien ne reste de lui. Les os lui avaient pris un temps fou à digérer. De nouveaux dieux avaient sûrement profité de l'occasion pour occuper les postes de protecteurs laissés vacants. Il devait en apprendre plus sur ce nouveau monde qu'ils avaient créé, s'il voulait le détruire comme il l'avait fait avec le précédent. 

 