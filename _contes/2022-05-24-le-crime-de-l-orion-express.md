---
bg: "le-crime-de-l-orion-express"
layout: post
title: Le crime de l'Orion Express
summary: Alors que Lurien Astante embarque à bord de l'Orion Express pour une croisière historique, un meurtre survient.
source: “Galaxy Travelling” par Sungwoo Lee
ref: https://www.artstation.com/artwork/Qz4leZ
date: 2022-05-24
categories: posts
tags: ["Les 26", "Les 5 ciels"]
type: Conte
author: Rumengol
lang: fr
---
« Retrouvez la sensation vintage des trains du XXe siècle le temps d'un voyage jusqu'aux étoiles de Bételgeuse, Rigel ou Bellatrix. Embarquez à bord de l'Orion Express et retombez dans l'âge d'or de la voie de fer pendant huit semaines. L'intérieur d'un train antique et la capacité de propulsion du fleuron de la flotte spatiale Éole, à un prix plus abordable que jamais ! » 

Je ne pensais pas être si sensible à la publicité, mais cette croisière me touche particulièrement. L'intérêt pour l'histoire se perd parmi les habitants de la Terre, à mon grand désespoir. C'était un pari plutôt osé de la part d'Éole de proposer une telle croisière de luxe, même si ceux qui vivent dans la lointaine Orion témoignent d'une passion réconfortante pour notre passé. Tout de même, le prix n'avait rien d'accessible et je faisais probablement un excès. Je tentais de me rassurer en me convainquant que je devais de toute façon me rendre sur Rigel, et emprunter l'Orion Express ne rajoutait qu'une semaine au trajet. Les autres pourraient bien attendre ça, et le chef comprendrait. 

En arrivant sur le tarmac, j'admets m'être inquiété. Nulle trace du train spatial promis par la brochure, le seul vaisseau d'Éole ressemblait à un gros porteur pouvant accueillir plusieurs milliers de personnes. C'était pourtant là que les agents du spatioport me dirigeaient. Réflexe professionnel ou paranoïa développée par des années de traque, je me suis méfié et m'apprêtais à m'enfuir lorsque deux stewards se sont présentés à moi, dans une mise d'époque parfaite. Cela suffit à endormir ma méfiance, si c'était un piège aussi finement élaboré, j'acceptais avec joie de tomber dedans. 

Je questionnais mes accompagnateurs qui me confirmèrent qu'il s'agissait de leur dernier modèle de gros porteur, modifié et complètement métamorphosé à l'intérieur. Certaines limitations techniques du voyage imposaient de conserver une certaine forme et revêtement de coque, ce que j'entendais sans difficulté. La navette dans laquelle ils me firent monter m'amena dans un coin isolé du spatioport, où une réplique de gare avait été montée. L'illusion était saisissante, bien que certains détails ne datent que de la fin du XXe siècle, début XXIe. Je m'abstins de tout commentaire. J'étais là en tant que simple amateur d'histoire, pas expert. 

Sans grande surprise, nous étions peu nombreux sur le quai. Certains portaient des habits normaux, mais la plupart jouaient le jeu et étaient vêtus à l'ancienne, avec des approximations historiques qui me crispaient. J'avais moi aussi choisi un manteau synthétique et un chapeau dont le style imitait le style des années 1980, bien loin de celui de 1920 qui était celui du décor de la croisière. Cela me frustrait, mais je ne pouvais pas me permettre d'être trop parfait, juste au cas où. J'avais tout de même ressorti pour l'occasion ma serviette de l'époque. Mon matériel y était en sécurité, et elle faisait illusion, pour une fois. 

J'en profitais pour observer mes compagnons de voyage. La plupart n'avaient pas eu à briser leurs tirelires pour obtenir leurs tickets. Je ne reconnus pas beaucoup de visages, mais celui de l'héritier des Viscenti était difficile à manquer. Flamboyant dans son manteau de fourrure rare qui devait coûter le prix du vaisseau, ses serviteurs composaient la moitié des passagers. Je remarquais aussi quelques-uns des voyageurs qui n'avaient pas l'air à leur place. Ils étaient richement vêtus, mais l'homme aux rouflaquettes et la femme au grand chapeau étaient inconfortables dans des vêtements trop neufs. Ils pouvaient vouloir simplement se fondre dans la haute société, mais je retenais de les garder à l'œil. 

Après une dizaine de minutes, le vaisseau se mit en branle et s'avança vers nous. Son flanc s'aligna avec le quai, dévoilant les voitures d'un train d'époque. Je dus m'avouer impressionné par la ressemblance avec la réalité, comme s'ils avaient mis la main sur un réel véhicule. Je pénétrais dans mon wagon à la suite de l'homme aux rouflaquettes. Le couloir était trop étroit pour que deux personnes se croisent sans se coller l'une à l'autre, quelle attention du détail. Il y avait bien assez de compartiments pour que chacun ait le sien, sans doute plus par manque de réservations que volonté. 

Un steward attendait devant chaque suite. L'un d'eux ouvrit la porte de celle que je choisis, et me délivra un discours savamment rôdé tandis que je retirais mon manteau. 

— Merci d'avoir choisi la compagnie Éole. Vous voyagerez avec nous pour les huit semaines à venir à bord de l'Orion Express. Vous avez pu remarquer la taille de notre vaisseau. Il se découpe en deux parties : vous vous trouvez dans celle calquée sur le style des trains du XXe siècle. Vous disposez de cette voiture à votre entière disposition. Un wagon-restaurant d'époque se trouve au bout du couloir. Vous pourrez y déguster des plats typiques du monde de cette période. Notre menu comporte près d'un millier de choix parmi lesquels votre bonheur se trouvera à coup sûr. 

» Si l'époque ne vous sied pas ou si vous souhaitez vous en éloigner un temps, un étage entier digne des meilleures croisières de luxe moderne est accessible via l'élévateur au-delà du restaurant. Toutes les commodités sont à portée de main, et vous pouvez faire appel au personnel en pressant ce bouton. Nous vous souhaitons une excellente traversée des étoiles. 

L'homme se volatilisa peu après avoir déclamé son texte. J'embrassais la pièce du regard. Des banquettes de cuir jusqu'aux porte-bagages en hauteur, tout me rappelait l'ancien temps avec une exactitude effrayante. Ce n'était pas du simili cuir, mais bien du vrai, toutefois altéré pour rendre l'assise plus confortable. L'immersion était presque parfaite, complétée par le côté fenêtre de l'habitacle. Dans le cadre en bois, une large fenêtre était dessinée, et donnait sur le tarmac qui se vidait peu à peu. J'en déduisis qu'il devait exister un étage mobile, puisque j'avais pénétré le faux train de l'autre côté du vaisseau. Il n'était pas non plus impossible qu'il s'agisse d'un habile jeu de miroirs ou d'écrans, mais la sensation de profondeur était saisissante. 

Une vingtaine de minutes plus tard, le commandant de bord fit une annonce dans les haut-parleurs crachotants pour signifier notre départ imminent. Le formidable engin pivota, puis alla s'aligner sur la piste de lancement. Ce type de vaisseau était incapable de s'arracher de l'emprise gravitationnelle de lui-même, pas sans pousser son moteur au-delà des limites du raisonnable. De ma fenêtre, je pus voir la piste magnétique s'enclencher, puis nous étions dessus. J'eus la désagréable sensation d'être aspiré en arrière par une force irrésistible qui me fondait dans mon siège. Puis une décharge soudaine m'y plaqua, alors que nous prenions plusieurs G d'accélération. La rampe s'inclina, et nous passâmes dans les airs, suspendus un bref instant en perte de vitesse importante. 

Les anneaux de propulsion reconnurent la signature magnétique de la coque juste avant que le ralentissement ne devienne trop important et reprirent le relai. Il n'y en avait que deux, sur Lonémia, mais ils étaient amplement suffisants pour projeter l'immense vaisseau en orbite. Une nouvelle accélération nous fit dépasser l'atmosphère en moins de trois minutes. La capacité de ces vaisseaux à absorber la force d'accélération et à l'épargner aux passagers. Je ne ressentis pas plus d'1G tandis que nous nous propulsions à une vitesse démente. 

La pression s'atténua à notre entrée dans l'espace, où des réacteurs contrebalançaient la poussée magnétique. Un ciel étoilé se dévoilait de l'autre côté de la fenêtre tamisée pour protéger l'intérieur des rayons du soleil. L'embarcation dans la fronde céleste prit environ une heure, après quoi notre transport fut à nouveau propulsé à une vitesse proche de celle de la lumière. La poussée initiale me provoqua un haut-le-cœur, et nous étions partis. La voiture commença à cahoter et à trembler, comme si nous étions sur des rails. J'avais perdu cette sensation qui m'était si familière autrefois. Une attention aux détails qui me toucha tout particulièrement. 

Je profitai un moment de la quiétude de ma cabine, avant de sortir pour me mêler aux passagers. Même à la vitesse la plus rapide de la galaxie, le voyage serait long. Autant rencontrer mes compagnons de voyage et tenter de rendre l'expérience aussi plaisante que possible. À peine sorti de mes quartiers, un jeune homme me bouscula et courut en direction du restaurant. Laissant échapper un juron à propos de l'impétuosité de la jeunesse, je suivis sa direction générale et manquai de percuter un homme qui sortait d'une autre cabine. 

C'était un grand homme, au costume noir parfaitement repassé. Ses cheveux grisonnants et ses traits tirés témoignaient d'un âge avancé. Il portait une moustache fournie qui masquait presque entièrement sa bouche et remuait lorsqu'il parlait.  

— Bonjour, monsieur. Nous n'avons pas eu l'occasion d'être présentés, je suis Ferid Löppelmann, majordome de Monsieur Viscenti. Puis-je demander à qui ai-je l'honneur ? 

— Bonjour à vous, monsieur Löppelmann. Je me prénomme Lurien Astante, journaliste au City News et détective privé à mes heures perdues. Je me dirigeais vers le wagon-restaurant, oserais-je vous inviter à ma table ? 

Le mot « détective » arracha une grimace au visage stoïque du majordome. Sa main commença à trembler, tic qu'il dissimula rapidement dans son dos avant de reprendre ses esprits et de me répondre très poliment. 

— Je regrette, mais Monsieur m'envoie quérir ses affaires. Ce serait avec plaisir si l'occasion se présente à nouveau, monsieur Astante. 

Il s'éloigna dans la direction opposée, me jetant quelques regards en coin. Bien que peu naturelle, cette discussion formelle avait quelque chose de rafraîchissant dans la vulgarité à laquelle j'étais quotidiennement confronté. Sa réaction à l'évocation de ma profession était révélatrice d'une conscience lourde. Les puissants usaient souvent de leurs serviteurs comme tueurs à gages à moindre frais, aussi je ne m'en suis pas formalisé outre mesure. Nombreux sont les coupables d'un quelconque crime dans le vaste univers. Je n'étais pas zélé au point d'enquêter sur un parfait inconnu rencontré lors d'un voyage d'affaire. 

Le wagon-restaurant était complètement vide, à l'exception du serveur qui m'accueillit. Il me dirigea à une table pour une personne sur laquelle trônait un menu en faux papier. En réalité un écran qui permettait de voyager dans la large gamme de choix. Il y avait quelque chose de pressant à ce qu'il se tienne à côté de moi jusqu'à ce que je choisisse, mais je décidais de l'ignorer. Mon choix s'est finalement porté sur une blanquette de veau, un plat que je n'avais pas mangé depuis trop longtemps pour m'en souvenir, accompagné d'un vin rouge grand cru « cuvée spéciale » probablement survendu. 

Tandis que je mangeais, la dame au grand chapeau violet traversa le wagon au pas de course, avant de disparaître dans l'élévateur. Elle triturait son petit sac nerveusement, comme s'il était bouillonnant. Je n'y accordai pas plus d'importance et passai au dessert, une tarte au citron délicieusement meringuée. Je m'étonnais de n'avoir croisé personne lors de mon repas. Ces gens avaient-ils donc tous mangé avant d'embarquer, ou bien les repas étaient-ils horriblement surfacturés ? L'idée m'arracha une grimace. 

De retour dans ma cabine, j'admirais les étoiles en lisant un roman policier. Papier, pour être dans le thème. Ils se faisaient rares mais j'en possédais encore quelques-uns que je n'avais pas encore lu. Rien d'époque, malheureusement. L'intrigue n'était pas des plus inspirées, mais elle avait le mérite de me ramener mentalement à une période qui n'existe désormais que dans les souvenirs d'une poignée d'individus. Alors que l'horloge située au-dessus de la porte du couloir indiquait minuit passée, je dépliais la couchette pour la nuit. Bien que délicate, elle était loin du confort moderne, et je doutais qu'un grand nombre de passagers y passeraient plus d'une semaine. Mais c'était tout ce qu'il me fallait. 

Je fus réveillé en plein milieu de la nuit par un cri déchirant. Il ne me fallut pas beaucoup de temps pour être dans le couloir, pleinement réveillé. D'autres sortirent de leurs voitures respectives, les yeux ensommeillés. La femme au chapeau était prostrée contre le mur du couloir, fixant désespérément l'intérieur d'un compartiment plongé dans le noir. Son bras tremblant pointait quelque chose hors de ma vue. J'avançai avec précaution, et jetai un regard dans l'obscurité. Les lumières de l'espace étaient insuffisantes pour y voir clair, alors j'appuyai sur l'interrupteur et me reculai aussitôt. 

Dans la voiture gisait Viscenti, baignant dans une mare de sang. Il avait été lacéré de multiples entailles perçant son épais manteau de fourrure. Son visage était figé dans un rictus à mi-chemin entre la surprise et la colère. Aucune arme n'était visible, mais toute la pièce était couverte de sang qui n'avait pas encore entièrement séché. Malgré le carnage qui avait eu lieu, aucune trace de lutte n'était visible, comme s'il avait été surpris dans son sommeil. Mais cela ne collait pas avec les vêtements épais qu'il portait encore. Je ratais quelque chose. Je m'avançais pour étudier le cadavre, mais sans matériel adapté je ne pouvais pas en tirer beaucoup d'informations, et je ne voulais pas souiller la zone du crime. 

Les autres commençaient à approcher, mais je pris les devants et appuyai sur le bouton d'appel du personnel. Un jeune steward apparut d'un coup à côté de moi, me demandant en souriant ce qu'il pouvait faire pour m'aider. 

— Il y a eu un meurtre dans cette cabine. Ne laissez personne d'autre que moi y entrer. À moins qu'il n'y ait un service d'investigation compétent sur votre vaisseau, je prends la charge de l'enquête. Je suis détective à Lonémia. 

La couleur disparut de son visage alors qu'il prenait la pleine mesure de ce que j'étais en train de lui annoncer. Sa bouche s'ouvrait et se fermait, mais aucun son n'en sortait. Je pris donc ça pour un consentement. 

— Vous avez des caméras de surveillance dans cette partie du vaisseau ? 

— N-non, nous tenons à respecter l'intimité de nos clients et la fidélité du lieu autant que possible. Les seules caméras sont dans le wagon-restaurant, je peux vous faire apporter les enregistrements si vous le désirez. 

— Ce sera mieux que rien, j'imagine. Il va falloir faire ça à l'ancienne. Je vais interroger chaque passager, en commençant par vous, madame ? 

— De Rivellont. 

— Si vous voulez bien me suivre. 

Je la conduisis jusqu'à ma propre cabine et l'assis en face de moi. Passée la surprise de la découverte, elle semblait déjà remise de ses émotions. Avait-elle vu beaucoup de situations dans le genre, pour y être à ce point insensible ? À bien l'y regarder, elle était très jolie, mais son visage portait les stigmates des épreuves que la vie avait dû lui imposer. Elle arborait avec expérience un masque d'indolence pour couvrir ses failles qui apparaissaient subtilement, et je devais admettre ne pas être insensible à son charme. 

— Désolé que vous ayez eu à assister à ça, madame de Rivellont. Mais puis-je vous demander ce que vous faisiez dans cette voiture ? Je devine que ce n'est pas la vôtre. 

— Oh, ne vous excusez pas. J'ai été surprise, c'est vrai, mais pas traumatisée. Viscenti était une ordure qui a mérité ce qui lui est arrivé. Voyez-vous, il m'a escroqué une grosse somme d'argent, contre quoi il avait promis d'investir dans mon entreprise. Une preuve de bonne foi, disait-il. Mais le salaud n'a jamais mis le moindre centime, et j'ai dû mettre la clef sous la porte. Alors oui, je voulais le confronter pour qu'il me rende au moins l'argent que je lui avais donné. Je voulais faire ça à la faveur de la nuit, pour éviter d'attirer trop d'attention, mais quelqu'un a été plus rapide. 

— Vous réalisez que votre témoignage ne joue pas en votre faveur ? 

— Ridicule, enfin ! Je voulais me faire rembourser, et seul Viscenti était au courant de notre accord. Maintenant qu'il est mort, je peux dire adieu à mon argent, et je n'ai plus qu'à repartir de zéro — mais littéralement, je suis sans le sou — dans la constellation d'Orion. 

Je réfléchissais à notre échange en allant chercher l'homme aux rouflaquettes. En dehors de moi, c'était le seul à s'être rendu sur les lieux déjà habillé. Peut-être avait-il les mêmes habitudes de sommeil que moi, mais elles étaient suffisamment peu communes pour que j'en doute. Lorsqu'il s'affala dans la banquette, il était trempé de sueur. À l'évidence, il aurait préféré être n'importe où sauf ici avec moi. 

— Commençons par les présentations, vous êtes monsieur ? 

— Aurélien Zurich. Écoutez, je sais ce que vous pensez, mais vous n'y êtes pas du tout. Je n'ai pas tué monsieur Viscenti. 

— Aurélien, mmh. Je ne vous ai encore accusé de rien, j'aimerais simplement savoir ce que vous avez fait cette nuit jusqu'au cri de madame de Rivellont. 

— Au point où j'en suis... Vous me promettez que ce que je vais dire restera entre nous ? 

— Si vous n'êtes pas coupable. 

— Bon. Vous avez dû le deviner maintenant. Je ne suis pas de la haute comme le reste de ces gens. Je suis journaliste, dans Vylaudé. Vous ne devez pas connaître, sauf si vous lisez les journaux à scandale. J'ai réussi à obtenir un ticket à bord lorsque j'ai su que Viscenti comptait embarquer. Ce type est... était une véritable mine d'or de scandales, c'était couru que j'aurais de quoi écrire une dizaine d'articles avant la fin du trajet. Mais comme il ne sortait pas de sa voiture, je suis parti chercher d'autres histoires, sur le pont supérieur notamment, mais il n'y avait personne et j'ai fini par boire un martini en compagnie du serveur. 

— Et vous n'auriez pas eu envie de créer le scandale plutôt que d'attendre qu'il vienne à vous ? Ça s'est déjà vu. 

— Pour qui me prenez-vous ? Je ne vais pas risquer ma carrière et ma vie pour un bête article ! Il n'y a pas beaucoup de prix décernés, dans ma catégorie de journalisme, vous savez ? Maintenant, si vous n'avez plus de questions, j'apprécierai que vous ne révéliez mes activités aux autres passagers. Cela rendrait le voyage... désagréable. 

— À condition que vous n'écriviez rien sur moi dans votre torchon. J'aime ma tranquillité. 

Zurich sortit de la pièce sincèrement vexé, ou bien c'était un acteur remarquable. Je le soupçonnais de cacher quelque chose, mais je ne m'attendais pas à un renifleur de scandale. J'allais devoir être prudent avec lui. Le troisième de mes suspects était le majordome. Je ne manquerais pas d'interroger tout le monde, mais je tenais à commencer par le trio qui soulevait le plus de questions. 

— Je regrette que nous nous retrouvions en de telles circonstances, monsieur Löppelmann. Mes condoléances pour la disparition de votre employeur. 

— Pas autant que moi, monsieur Astante, pas autant que moi. Puis-je être d'une quelconque assistance à votre enquête ?  

— En vérité, vous le pouvez. J'aurais deux questions pour vous. Tout d'abord, j'aimerais savoir ce que vous êtes allé chercher dans les affaires de monsieur Viscenti. Tout à l'heure, dans sa cabine, il n'y avait aucune forme d'affaires justifiant un voyage en soute, et j'ai peur que son meurtrier s'en soit emparé. 

— Oh. Eh bien, je suppose qu'il n'y a plus de mal à en parler désormais. Monsieur se droguait. Beaucoup, et pas avec les drogues un peu illégales qu'on trouve partout. Il s'injectait de grandes quantités de Krylamine, et restait ensuite des heures, voire des jours catatonique. C'est moi qui m'occupais du dosage, je m'assurais toujours qu'il soit le plus haut possible en évitant tout risque d'overdose. Nous ne pouvions bien sûr pas porter les flacons sur nous, mais Monsieur avait le bras suffisamment long pour éviter la fouille de ses bagages. Le reste des doses se trouve dans ma cabine personnelle, si vous désirez vous en assurer. 

— Cela explique l'absence de traces de lutte sur son corps, il n'était simplement pas en état de se défendre. J'aimerais effectivement voir ces flacons, mais plus tard. Vous aviez la chambre à côté de la sienne, si je ne me trompe pas. Vous avez sûrement gardé un œil sur les allées et venues du couloir. Avez-vous remarqué quelque chose d'inhabituel, ou quelqu'un pénétrant dans sa chambre ? 

— J'ai monté un semblant de garde au début, il est vrai. À un moment, je ne saurais vous donner l'heure exacte, je me suis absenté pour aller chercher un repas au restaurant, que j'ai mangé dans ma cabine. Je crois que je me suis assoupi sur ma banquette dans la soirée, avant d'être réveillé par le cri de madame de Rivellont. Je suis tellement honteux, si seulement j'avais été plus alerte, j'aurais pu empêcher cela... 

— Ne vous jetez pas la pierre, il n'est pas certain que vous auriez pu y faire quoi que ce soit. Allez donc vous reposer et faire votre deuil, je vous remercie pour vos informations. 

Le vieux majordome se leva tout en s'inclinant, avant de quitter la pièce. J'interrogeais par la suite tous les autres passagers, mais j'avais entendu mes trois principaux suspects. À première vue, aucun d'entre eux n'avait d'intérêt à tuer Viscenti, même Zurich et De Rivellont qui ne le portaient pas dans leurs cœurs. Quelque chose me chiffonnait cependant. Plusieurs éléments même. Je n'avais pas été capable de mettre la main sur l'enfant qui m'avait bousculé. Je l'ignorais pour le moment, j'avais mon coupable, et le visionnage des enregistrements devrait être en mesure de le confirmer. Je ne connaissais pas encore son motif, mais ce n'était qu'une question de temps. 

Cela me peinait de condamner une aussi charmante personne, mais la justice devait être faite. À moins que... Rien ne me forçait à rendre une quelconque justice, en particulier pour ce type d'individu. Ces considérations qui ne m'effleuraient plus depuis longtemps avaient refait surface, sans doute à cause du décor ancien dans lequel j'étais immergé depuis à peine une journée. Non, tout bien réfléchi, le coupable pouvait continuer à vivre librement. Il serait probablement attrapé à notre arrivée, une fois qu'une véritable enquête aurait lieu. Il n'avait plus qu'à savourer ces dernières semaines de liberté à bord de l'Orion Express. 

 