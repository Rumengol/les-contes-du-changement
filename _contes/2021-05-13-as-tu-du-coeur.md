---
bg: "as-tu-du-coeur"
layout: post
title: As-tu du cœur ?
summary: A l'apogée de son existence, le roi des démons se remémore sa vie.
source: “legend of the cryptids” par Dong Geon Son
ref: https://www.artstation.com/artwork/eR8vG
date: 2021-05-13
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---

“Arthur, as-tu du cœur ?”

Les mots de sa mères revenaient soudain à l’esprit d’Ar’khelturahm. Enfant il riait à cette question et lui promettait d’en avoir autant qu’elle. C’était son exemple, une femme pieuse qui suivait les commandements de l’église du Paradis à la lettre, mettant le bien-être d’autrui devant sa propre vie. Même lorsqu’elle tomba malade, elle n’avait cessé de venir en aide aux plus démunis, traînant son corps frêle dans les recoins les plus sordides de la capitale pour y apporter la lumière divine.

Le petit Arthur avait souhaité suivre son modèle, vivant de sacrifices pour le bien des autres. Quand sa mère fut si faible qu’elle ne pouvait sortir du lit, il avait repris son œuvre, dépensant les maigres sous qu’il gagnait dans des médicaments au prix exorbitant. Hélas, rien ne semblait à même de guérir sa mère, tout juste son mal progressait plus lentement. Les prêtres du Paradis se détournèrent de ses supplications, lui faisant comprendre que si leur dieu a décidé de rappeler la sainte à ses côtés il fallait se réjouir pour elle.

Au comble du désespoir, ne pouvant se résoudre à abandonner son unique parent, la lumière du petit Arthur vacilla. Il s’aventura dans les souterrains les plus sombres, que même sa mère ne pouvait illuminer, à la recherche de solutions bannies par la lumière. Il échangea la santé de sa mère contre sa propre lumière, et terni son âme dans l’espoir de sauver celle que chérissait son cœur. Il entra dans des combines toujours plus sombres, aux récompenses toujours plus grandes. Il n’avait pas su réaliser alors que la flamme de vie de sa mère s’estompait en même temps que sa lueur. Ainsi, la nuit où il prit une vie pour la première fois, deux personnes périrent par sa faute.

Le cœur noir et fou de rage, Arthur embrassa les ténèbres. S’installant définitivement dans la cité souterraine, il se fit rapidement une réputation d’homme de main impitoyable, assoiffé de sang. La rencontre avec le maître d’une guilde l’initia à la voie de l’assassin, pour laquelle il se révéla incroyablement talentueux. Nul roi ni général n’était à l’abri de sa lame, il repoussa à plusieurs reprises les limites de l’art des ombres. Il ne comptait plus les guerres dont il était le déclencheur. Il se délectait de chaque vie qu’il retirait au monde.

Lorsqu’il se fit vieux, ses réflexes diminuant, il fut immanquablement tué par ses meilleures élèves, combinant leurs efforts pour l’abattre. De son dernier souffle, il maudit ses apprentis dans la langue interdite des démons. Sa dernière pensée alla à sa mère définitivement hors de portée. Une sainte comme elle pouvant s’élever au Paradis, tandis que le monstre sans cœur qu’il était serait avalé par les Enfers. Le Duc l’accueillit de sa kyrielle de bras ouverts. Mais tourmenter son âme jusqu’à éteindre toute humanité était inutile, pour un être si sombre que les démons étaient aveuglés. Il lui fit cadeau d’un nouveau nom, d’un corps démoniaque à la peau aussi noire que lui, dont le torse était continuellement dévoré par les flammes de sa haine.

La deuxième vie d’Ar’khelturahm fut aussi exceptionnelle que la première. Surpassant la cruauté de bien des démons, le Duc en fit son exécuteur. Il vainquit plusieurs seigneurs des Enfers, avant de se retourner contre son maître, dévorant son essence sans la moindre pitié. Proclamé roi des enfers, son regard se tourna vers le monde des vivants. Il transgressa l’interdit absolu : détruire l’essence d’un être de vie, un prêtre du Paradis de surcroît. Le ciel ne pouvait ignorer un tel affront, et une nouvelle guerre divine s’amorça. Pour la troisième fois, les actes du démon plongèrent le monde dans le chaos. Mais cette fois serait la dernière des vivants.

Le monde qui l’avait vu naître était transformé en un méconnaissable champs de bataille. Démons et anges se mêlaient dans la mort, leurs essences dispersées dans l’air saturait l’atmosphère d’une puissance primordiale qui renforçait chaque camp, jusqu’à menacer l’intégrité du monde. Mais ce danger n’était pas assez grand pour détourner Ar’khelturahm de sa soif de carnage. Dans un ultime recours, le Paradis envoya ses plus grands archanges le combattre directement. Le monde fut irrémédiablement détruit dans la bataille, et avec lui les deux armées. C’est lorsqu’il se tenait au sommet de l’aboutissement de son massacre, debout dans le champs de ruines qui avait été autrefois un monde, que les paroles de sa mère lui étaient revenues.

“Arthur, as-tu du cœur ?”

Sa voix d’outre ricanait.

“Non. Mais j’ai le tien.”
