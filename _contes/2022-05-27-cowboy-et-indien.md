---
bg: "cowboy-et-indiens"
layout: post
title: Cowboy et indiens
summary: Un marshal enquête sur des attaques de Sioux.
source: “Discovery” par Josh James
ref: https://www.artstation.com/artwork/b5PELG
date: 2022-05-26
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---
Encore une fois, j'arrive trop tard. Je mords ma cigarette de rage. Mauvaise idée. Le bout du mégot tombe directement dans ma gorge et me fait tousser violemment. Mais c'est trop tard, je l'ai déjà avalé. Tant pis. L'autre bout est tombé sur le sol humide et continue de se consumer en soulevant un filet de fumée. Je déglutis difficilement et avance vers la maison en bois, dans la campagne silencieuse. Elle n'a pas brûlé, elle non plus. C'est très curieux, loin de leurs manières de faire habituelles. 

Ils ont bel et bien attaqué, ce point est indéniable. Des dizaines de flèches sont fichées dans le sol et le bois du porche, sans parler des dizaines de traces de sabots qui encerclent l'habitation. Mon fusil bien en main, j'avance vers elle avec prudence. Je ne sais pas depuis combien de temps c'est arrivé, et ils pourraient être toujours là. Une fois que je suis suffisamment proche, mes craintes se confirment. La forme allongée face contre terre devant la porte d'entrée est bien un cadavre. Une femme, si j'en juge aux longs cheveux et aux courbes de son corps. Elle me tourne le dos, la main tendue vers le rocking-chair comme si elle avait essayé de se rattraper avant de mourir. 

Deux revolvers sont au sol, en compagnie d'une dizaine de douilles. S'ils étaient à la femme, elle n'avait pas ménagé ses efforts pour défendre son foyer, mais ils s'étaient avérés insuffisants. Il ne fallait plus que prier pour qu'elle ait emporté quelques-uns de ces sauvages avec elle. Je les ouvre pour vérifier leurs barillets. Presque vides, évidemment. Je n'aime pas dépouiller les morts, mais avec les rumeurs de la guerre contre les Sioux qui s'intensifient, on n'aura jamais trop d'armes. Je les range dans mon manteau et m'approche du corps. 

J'adresse une prière silencieuse au seigneur pour l'âme de cette pauvre femme et retourne son corps froid. J'étais surpris de ne voir aucune flèche transpercer sa jolie robe, mais l'explication me vient rapidement. Elle n'est pas morte touchée par un tir ennemi, mais la gorge tranchée. Un mouvement net, sans trembler, ses carotides avaient été sectionnées d'un seul geste, par un assaillant la prenant en traître. Le plus étonnant dans tout cela, c'est qu'ils l'aient simplement éliminée, sans abuser d'elle. Les sévices que les indiens font subir aux femmes blanches qui tombent vivantes entre leurs griffes font froid dans le dos. Celle-ci a eu la chance d'avoir une mort rapide. 

Je laisse la femme décédée à son repos éternel et pénètre dans la maison. La porte grince atrocement et m'arrache un rictus. L'intérieur est ravagé, comme si une dizaine d'hommes s'y étaient battus. Ils avaient dû prendre tout ce qui avait de la valeur pour eux et saccager l'endroit après leur victoire. Typique. Je monte à l'étage en restant sur mes gardes. J'ai vu quelque chose en bas qui me fait craindre le pire. Et j'ai raison, il y avait bien un enfant ici. Je colle mon oreille contre la porte peinte en bleu. Aucun bruit n'en sort. Je la pousse lentement, espérant de tout cœur devoir déloger un gamin effrayé. 

Ma désillusion ne se fait pas attendre. Il repose sur son lit, le petit garçon. Le visage ravagé par les larmes, il avait subi le même sort que sa mère, certainement arraché de sous le lit où il se cachait pour être sauvagement exécuté. Ses yeux ouverts mais sans vie fixaient le plafond. Je prie pour son âme aussi et ferme ses yeux. Repose en paix, petit. Tu es mort dans un combat qui ne te concernait même pas. 

Quelque chose ne va pas, et je mets trop de temps à le comprendre. Il y a deux lits dans cette chambre, mais un seul corps. Je fouille rapidement l'étage, mais aucune autre dépouille d'enfant. Mon soulagement est de courte durée. S'ils ne l'ont pas tué sur place, c'est certainement pour le torturer ou l'offrir en sacrifice à une quelconque divinité païenne. Je descends en quatrième vitesse au rez-de-chaussée, et débarque dans le salon. 

Le sauvage s'est montré incroyablement discret à mon arrivée. Pourtant ça a dû être dur dans son état. Une ribambelle de bandages imbibés de sang couvre son torse. Il sue à grosses gouttes, le canapé sur lequel il repose est complètement souillé. Mais son poitrail se soulève rapidement. Un tomahawk repose à côté de lui, et il fait mine de s'en emparer à mon entrée, mais je suis plus rapide. La crosse de mon fusil le cueille au menton et le repousse, avant que mon poing ne s'enfonce dans sa blessure. J'écarte son arme tandis qu'il grogne de douleur. Je retourne mon fusil et pointe son front. 

— Où est l'enfant, enflure ? Et à quoi riment vos attaques ? 

À travers sa souffrance, il parvient à esquisser un sourire en me parlant avec un anglais remarquable. Certainement un élève des établissements catholiques qui ont tenté d'inculquer la civilisation à ces sauvages. Ça n'a pas marché avec lui, de toute évidence. 

— T'es arrivé trop tard, shériff. 

Je lui donne un nouveau coup de crosse, dans l'épaule cette fois. 

— C'est marshal, l'abruti. 

— Héhéhé... T'as aucune idée de ce qui arrive. Le grand embrasement est proche. Le temps où Wakan Tanka sera invoqué signera la fin de votre règne de terreur. 

— Mais de quoi tu parles, espèce de sauvage ? On vous apporte la civilisation et c'est comme ça que vous nous remerciez ? Tu me fais vomir. Et l'enfant, alors ? 

— Regarde dans la grange, mais ça va pas te plaire. 

Il rigole, le maudit. Je lui assène un coup à la tête et enfonce la crosse dans sa blessure, avant de lui arracher ses bandages. Je n'ai pas envie de tirer un coup de feu tant que je ne sais pas ce qui se cache dans cette grange, mais dans son état il ne devrait pas prendre trop de temps avant de se vider de son sang. Une fin qui convient à un chien de son genre. Je sors de la maison et me dirige vers la grange, à plusieurs dizaines de mètres. Je l'ai ignorée à mon arrivée, habitué à les voir brûlées. En général c'est ce qu'ils ciblent en premier.  

À chaque pas je crains de voir une foule d'indiens en furie en sortir et se jeter sur moi. Mais ils ne sont pas assez stupides, ils n'auraient aucune chance contre mon fusil. S'il y en a dans la grande construction de bois, ils m'attendront à l'intérieur. La grande double porte est fermée. J'approche en silence, pour écouter à l'intérieur. Aucun son ne me parvient, même au travers des ouvertures sur le côté. Soit il n'y a personne, soit ils sont trop silencieux pour ignorer ma présence. Foutu pour foutu, je prends mon élan et ouvre la grande porte d'un violent coup de pied. Elle n'était pas verrouillée, et les battants s'écartent sans opposer la moindre résistance. 

Je ne m'avance pas tout de suite. Mon fusil est prêt à cracher la mort contre quiconque s'avancerait sur moi. Mais rien ne se passe. J'entre lentement dans la pénombre de la grange, mesurant chacun de mes pas. Mes yeux commencent à s'habituer à l'obscurité, et je peux discerner le mur au fond de la grange. Cette vision me glace le sang. Cloué contre les planches de bois dans un cercle fait de son propre sang, le corps de celui qui devait être le deuxième fils. Des symboles obscènes sont dessinés sur toute la hauteur du mur et un totem est planté à ses pieds. Les salauds. Je jure de les tuer tous, et de livrer leurs cadavres en pâture aux vautours ! 

Le bois craque derrière moi. Je me retourne brusquement, pour voir une dizaine d'indiens suspendus aux poutres. C'était un piège, évidemment ! Quel idiot j'ai été. Mais je ne partirais pas seul. À cette distance, il m'est impossible de les louper. Ils tombent comme des mouches, mais il y en a trop. Il pleut littéralement de ces raclures, et mon fusil est trop lent. Je sors l'un des pistolets que j'ai récupéré sur le porche et abats le plus proche. Le barillet se met à cliquer et tourner dans le vide. J'aurais dû vérifier les balles. Il se jettent sur moi en hurlant, et malgré ma résistance acharnée, un coup de tomahawk sur la tête me fait sombrer dans l'inconscience. 

 