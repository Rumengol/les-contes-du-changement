---
bg: "l-agent-d-anubis"
layout: post
title: L'agent d'Anubis
summary: La plus difficile des missions pour un passeur des morts.
source: “Anubis”, par Julia Balyskova
ref: https://www.artstation.com/artwork/QDODx
date: 2020-02-10
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---

Les agents d'Anubis étaient les serviteurs divins les plus craints. Même les agents de Seth, pourtant porteurs de chaos et de dévastation, ne terrifiaient pas autant. Car un agent d'Anubis était toujours annonciateur de mort. Grâce aux progrès de la science et à l'engouement que générait Thot, ils se faisaient rares et leur présence était devenue synonyme de malheur. Les vocations étaient exceptionnelles, d'autant que leur immense pouvoir attirait plus d'endeuillés prêts à tout plus que de candidats sérieux.

Chaque agent divin recevait la bénédiction de son dieu. Les agents d'Horus, par exemple, obtenaient une force et une agilité surhumaine, en plus des notions de magie enseignées à tout agent. Aux serviteurs d'Anubis était donné le pouvoir d'accéder au monde des morts ainsi qu'une hypersensibilité à la mort. Leur rôle était d'être les passeurs des âmes du monde des vivants au royaume souterrain. Essentiels, ils agissaient comme des guides permettant de trivialiser les longs et éprouvants périples que devaient effectuer leurs ancêtres. Grâce à eux, les dépouilles pouvaient être incinérées sitôt le jugement de l'âme accompli.

Il s'agissait également d'un métier dangereux. Les redoutables guerriers d'Horus ou de Sekhmet se mettaient souvent en tête de "tuer la mort" afin de sauver l'un des leurs. De nombreux agents d'Anubis périrent avant que leur divin patron accepte de leur confier une troisième bénédiction : la paix du deuil, leur permettant d'effacer tout ressentiment dans le cœur d'un être. Une bénédiction inestimable dont Sekhel faisait chaque jour usage malgré lui.

Mais pour une fois, il aurait aimé s'en servir. Sur lui-même. Fallait-il qu'Anubis soit cruel pour lui imposer une pareille épreuve. Sa fille était née faible, et tombait souvent malade. Quelque chose dans ses gènes, qui limitait à l'extrême sa production de plasmoblastes. Il n'avait jamais voulu s'inquiéter, elle était soignée dans le meilleur hôpital du Caire, et l'épigénétique progressait de jour en jour. Certes, il y avait parfois du travail, dans cet hôpital, mais c'était pour des cas déjà perdus, des cancers en phase terminale, bien plus grave que ce qui accablait Nerylet.

Il venait la voir chaque fois qu'il pouvait, abusant de son pouvoir à de nombreuses reprises. Elle était là, toujours éclatante de joie, avec sa diction approximative ponctuée d'éclats de rire infantiles. A sa dernière visite, il avait trouvé sur la table de chevet un adorable dessin de sa petite fille tenant la main à un chacal bipède. Il avait éprouvé un mélange de fierté et de scepticisme à se voir ainsi représenté dans son habit de travail.

Elle semblait si pleine de vie alors. Alors pourquoi, pourquoi son instinct morbide le guidait à présent vers la chambre 206 ? Pourquoi, alors qu'il savait que c'était déjà trop tard, pourquoi refusait-il d'y croire ? Lorsqu'il entra dans la pièce, sa femme eut un air mêlé de soulagement et de désespoir. Sa fille le reconnaissait sous son masque animal, mais elle n'avait connu que lui. En revanche, il n'avait jamais compris comment Meelya faisait pour ne jamais douter de son identité. Tarok, son frère, était à côté d'elle, il la tenait dans ses bras. Sekhel avait la gorge nouée. Elle lui adressa une question muette à laquelle il ne put répondre.

Nerylet semblait simplement dormir paisiblement. Il avait l'impression que tout son être s'asséchait. Il s'approcha, un pas après l'autre, les jambes tremblantes, du lit. Il trébucha plusieurs fois. Lorsqu'il arriva enfin au niveau de sa fille, il lui semblait avoir parcouru plusieurs kilomètres. Son cœur battait fort. Son instinct lui criait son devoir, comme un bourdonnement assourdissant qui ne voulait pas s'arrêter. Alors il commit l'interdit. Il ôta son masque, révélant son visage en plein jour pour la première fois depuis qu'il occupait le poste d'agent d'Anubis. Sa femme hoqueta et son beau-frère laissa échapper une exclamation de surprise. Pour la première fois depuis qu'il était à l'hôpital, il déposa un baiser sur le front de sa fille.

Il n'avait pas de mot. Aucune parole ne pourrait exprimer ce qu'il ressentait. En silence, les yeux embués de larmes, il regarda Meelya, puis il remit à contrecœur son masque. Il voulait rester auprès d'elle, la soutenir dans cette épreuve. Mais Nerylet avait plus besoin de lui que sa mère. Il ne pouvait la laisser affronter l'errance jusqu'au jugement seule. Avec toute la délicatesse dont un père est capable, il enlaça l'âme de sa fille, la maintenant contre son cœur. Elle rêvait, sans doute d'un jour pouvoir sortir de cet hôpital. Il ouvrit le portail vers le royaume d'Osiris. Il ferait en sorte que son jugement soit prononcé rapidement, elle n'était coupable de rien, son cœur sera bien plus léger que la plume de Mâat.

Sekhel ne se retourna pas. S'il regardait derrière lui, il n'aurait plus la force de traverser le portail, il le savait. Il sentait le regard implorant de sa femme sur son dos, un regard qui semblait peser plus lourd que la terre entière. Il força un pas, comme pour s'extraire de sables mouvants, et disparut de la surface du monde vivant.
