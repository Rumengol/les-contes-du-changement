---
bg: "l-ultime-armada"
layout: post
title: L'ultime armada
summary: Le dernier titansecte scellant l'humanité est traqué, sa fin est imminente.
source: “Something creeping in the fog” par alexiuss
ref: https://www.deviantart.com/alexiuss/art/Something-creeping-in-the-fog-702886075
date: 2020-01-20
categories: posts
tags: ["La prison des dieux"]
type: Conte
author: Rumengol
lang: fr
---

Tikatikatikatikatikatikatikatikatika....

Le bruit d'innombrables pattes métalliques frappant inlassablement la grève ne peut provenir que d'un seul être : l'Aptékorydoptrix. Le brouillard dense qui recourvait la région ces derniers jours semble s'éclaircir au fur et à mesure que nous approchons. Un frisson parcourt tout l'équipage. Je le ressens moi aussi. Ils s'agitent, et je les comprends. Nous arrivons au terme de plus de vingt années de traque. Choleptra, ma titansecte, semble maintenant réticente à avancer, comme ceux du reste de l'armada. Heureusement, je l'ai élevée depuis sa naissance, nous sommes liés et la trentaine d'écumeurs sur son dos ne l'effarouchent pas. En revanche…

Je me tourne vers la gauche, où il est possible, malgré la purée de poix, de distinguer l'immense silhouette du Gronzéphax. Voyager près de lui, surtout avec cette tension, ne me rend que plus anxieux. Il a beau être dirigé par l'élite des pilotes, si le titansecte séculaire panique la moitié des écumeurs seront écrasés, et l'Aptékorydoptrix nous échappera. Ce serait pire que tout. Il est le dernier sceau qui nous retient dans ce monde de non-vie. L'abattre nous libérera de cette prison de sable noir.

Tikatikatikatikatikatikatikatikatika....

Le brouillard est maintenant devenu suffisamment fin pour que l'aéro nous donne le ciel.

"Ciel d'obéga, capitaine, frilan !"

Ciel d'obéga, à mi-hauteur, c'est les conditions idéales. Les obégas militaires pourront décoller, et ça nous met à l'abri des plus gros prédateurs. Frilan, en revanche, est de moins bonne augure. Cette teinte cache souvent une tempête. Enfin, ce n'est pas comme si nous aivons le choix.

"Et tu vois quelque chose ?"

"Rien et... droit devant ! Ô Dieux !"

Tikatikatikatikatikatikatikatikatika....

Au même moment, des froissements d'ailes assourdissants se font entendre de partout, couvrant les exclamations des écumeurs. Une corne sonne au loin, étouffée par le brouillard. Ca doit venir d'en haut. Le Gronz s'arrête, puis dissipe brutalement le brouillard. Son souffle manque de me faire passer par dessus bord, mais j'ai eu le temps de m'accrocher. La brume opaque à présent effacée, je constate que tous n'ont pas eu ma chance, et plusieurs équipages s'activent à faire remonter les membres tombés. Tous les titansectes se sont arrêtés au son de la corne, comme une machine bien huilée.

Tikatikatikatikatikatikatikatikatika....

Il sort de la brume, poursuivant son chemin, indifférent au souffle du Gronz et à l'effervescence qu'il génère à quelques centaines de mètres à peine. Je suis tétanisé. Les sceaux précédents étaient terrifiants, destructeurs et impitoyables, mais tous étaient mortels. Lui est... simplement monstrueux. Jusqu'à aujourd'hui, j'étais persuadé que le Gronzéphax était le plus gros titansecte de ce monde. En réalité, il ne tient même pas la comparaison. Ô Dieux, sauvez nous, jamais nous ne pourrons vaincre cet ultime obstacle.

Tikatikatikatikatikatikatikatikatika....

Pourtant, la trompe de charge retentit, galvanisant tous les titansectes, et nous sort de notre torpeur. Nous ne pouvons plus reculer. Quitte à mourir, autant livrer la plus grande des batailles, pour la plus noble des causes, celle de la libération de toute l'humanité ! Je hurle sans m'en rendre compte, accompagné par les autres. Plus que deux cent mètres, et je me demande comment un monstre à ce point démesuré peut être aussi silencieux. Un signal, tous doivent armer. Une minute plus tard, l'intégralité des balistes de l'ultime armada font feu, visant le corps bien trop haut du titan. Plusieurs ratent, certaines ricochent sur ses pattes de métal, mais la plupart touchent. Poison, explosif, acide, feu, c'est un spectacle lumineux qu'offrent les alchimistes et artificiers. Chaque trait est unique ou presque, portant avec lui sa charge de haine et de souffrance. Le titansecte s'arrête et pousse un cri si rauque qu'il semble provenir des profondeurs de la terre elle-même. Il résonne en nous, je sens mon coeur peiner à équilibrer cette vibration incroyablement pusisante. Je crois bien qu'il s'est arrêté, à un moment, avant de repartir. Beaucoup d'entre nous viennent de tomber. Notre charge est brisée, tout comme la marche de l'Aptékorydoptrix.

Tikatikatikatikatikatikatikatikatika....

Il me faut du temps pour réaliser que le son qui nous parvient émane du reste du corps du titansecte, toujours plongé dans la brume. Des secondes qui me paraissent être des minutes passent. Peu à peu, je reprends possession de mon corps chancelant. Choleptra se remet en marche la première, puis le reste suit. Le Gronz se relève du choc en dernier. Aujourd'hui est le dernier jour. Aujourd'hui, nous brisons le dernier sceau !

Tikatikatikatikati.
