---
bg: "le-coeur-du-monde-la-cite-de-lumiere"
layout: post
title: Le Coeur du monde - La cité de lumière
summary: Shahid et Méaris infiltrent la cité de lumière pour dérober le Coeur du monde, détenu par un peuple abusant de ses pouvoirs.
source: “Recreance”, par Rob Joseph
ref: https://www.rob-joseph.com/personal-work?pgid=jn0qg42n-c188ad73-66a7-448a-bdb5-e9c1801a760e
date: 2020-10-06
categories: posts
tags: ["Le coeur du monde"]
type: Conte
author: Rumengol
lang: fr
---

« Tu es sûre que tu veux m'accompagner ?

&mdash; Écoute, c'est moi qui ai découvert la première cette cité, et depuis j'en ai étudié les moindres recoins. Tu as besoin de moi, que ça te plaise ou non.»

Shahid soupira d'amusement. Méaris avait raison, même s'il essayait, il serait incapable d'empêcher son amie de l'accompagner. Il en avait honte, mais il se sentait soulagé. Seul, il doutait d'avoir le courage d'aller jusqu'au bout. Mais tant que la guetteuse serait à ses côtés, tout serait possible, même le vol du Cœur du monde. Lorsque la cité de lumière était apparue en une nuit, cinq ans auparavant, c'était elle qui avait signalé sa présence. Puis, quand la délégation diplomatique qu'il dirigeait s'était faite attaquer par les effriti gardiens de la cité, c'est encore elle qui lui avait sauvé la vie.

Par la suite, Al-Sariff s'était préparée à subir un siège par ces envahisseurs, mais ils n'étaient jamais sortis de leur forteresse, chargeant les effriti d'éliminer toute personne s'aventurant trop près de leurs murs. Cinq ans durant, ils avaient observé de loin cette ville à l'architecture exotique, qui brillait de milles feux la nuit. L'ordre des lecteurs avait conclu que le cœur de l'incroyable construction était aussi celui du monde. Nulle autre magie n'était capable de faire apparaître des cités de nulle part et de plier les effriti.

Mais ce pouvoir reposait entre les mains d'hommes mauvais, qui ne s'en servaient que pour leur prospérité, refusant de partager leur trésor avec les autres peuples. Le calife avait chargé Shahid de le dérober et de ramener cette puissance incroyable, dont il saurait faire un usage plus juste. On lui avait proposé toute une troupe, mais il considérait qu'un homme seul avait plus de chance de tromper la vigilance des sentinelles mortelles. Méaris avait insisté pour être de l'aventure, et il n'avait pas su s'y opposer.

Ils utiliseraient des voiles pour passer par dessus le premier mur. Leur utilisation était encore rare et peu sûre, mais c'était là le seul moyen. Un vol d'oiseau devrait masquer leur présence suffisamment longtemps pour qu'ils franchissent l'enceinte. Après cela, ils seraient livrés à eux mêmes. Méaris se passa une main dans les cheveux. Elle avait baissé son hijab pour apprécier la sensation du vent sur son visage. C'était peut être la dernière fois. À regret, elle défit sa cuirasse de métal dont le poids l'alourdirait terriblement en l'air, pour ne conserver qu'une armure de cuir, moins protectrice mais bien plus légère.

Les deux compagnons prirent un instant pour revoir leur plan, admirer la cité des lumières, majestueuse malgré la menace de mort qu'elle représentait. L'un comme l'autre ignoraient s'ils seraient capables d'en ressortir en vie, mais ils accomplissaient cette mission pour leur peuple et leur calife, et avaient juré de mettre leur vie en jeu s'il le fallait. Ils équipèrent leurs voiles, puis firent signe aux trois soldats qui les accompagnaient qu'ils pouvaient libérer les oiseaux. Shahid fit craquer les os de son coup, inspira profondément, et courut vers le vide.

Les premières secondes furent les plus terrifiantes. La voile prenait mal le vent. Avec horreur, il pouvait déjà s'imaginer écrasé au sol, réduit à un tas de chair sanguinolent. Priant plus intensément que jamais, il lutta pour ne pas fermer complètement les yeux. Ces quelques secondes lui semblèrent une éternité. Il peinait à se maintenir à l'horizontale, conscient que le moindre faux mouvement lui serait fatal. Le guerrier se stabilisa enfin, bien plus bas que prévu mais en vie. Risquant un coup d'œil à droite, il aperçut Maéris qui se débrouillait bien mieux, glissant avec aise sur l'air, comme si elle y était née.

Le subterfuge des oiseaux avait fonctionné, ou bien était-ce la distraction causée par les prisonniers barbares relâchés à proximité de la cité pour en attirer les effriti. Après d'angoissantes minutes à batailler avec les courants du vent, ils se posèrent à l'orée d'un bloc de forêt. Ils auraient sûrement été capables de rallier la couronne intérieure par les airs, mais la discrétion était la clef de leur mission. Une fois les voiles dissimulées sous un tas de feuilles et recouvertes de terre, ils s'enfoncèrent dans les bois.

Ces arbres étaient inconnus de Shahid. Il interrogea Méaris du regard, qui secoua la tête. À leur soulagement, ils ne croisèrent personne durant leur escapade forestière. Cependant, à peine sortis du couvert des arbres, ils tombèrent sur un vieil homme. Il tenta de lever la main, mais ne put achever son geste, la lame de Shahid se chargeant de sa vie. Ils tirèrent le corps encore chaud à l'ombre de la forêt pour l'étudier.

«Au moins, ce sont des hommes, commença Shahid. Ils meurent aussi facilement.

&mdash; Mais leur peau est si pâle. Elle ressemble à celle des hommes du Nord. Et ces vêtements, je n'avais jamais rien vu de pareil.
&mdash; Je n'aime pas où tu veux en venir, Méaris.
&mdash; Nous devrions nous vêtir comme eux, leur ressembler pour les tromper. Pas besoin de le déshabiller, il y a une maison isolée près d'ici.»

L'honorable guerrier qu'il se considérait être détestait cette idée, mais il faisait confiance à Méaris, et devait reconnaître que leur peau et leurs armures de cuir ne les feraient pas passer inaperçus dans une ville d'hommes pâles vêtus de tissu fin. La maison semblait vide. Celle du vieil homme ? Il ne préférait pas y penser. Il s'apprêtait à briser une paroi de glace quand la guetteuse l'interrompit : des vêtements séchaient derrière la maison. Il n'y avait pas de tenue de femme, et le gabarit de leur propriétaire était plus petit que celui de Shahid. Elle flottait dans ses habits là où il pouvait les entendre craquer, surtout ce pantalon taillé dans une matière extrêmement inconfortable. Avisant un sac, il le récupéra et entreprit d'y ranger ce qu'ils ne pouvaient laisser derrière ni cacher. Il y entreposa son cimeterre, et se tourna vers Méaris pour lui demander son arc et ses dagues.

C'est quand il se redressa qu'il remarqua l'enfant immobile et silencieux qui le regardait. Il ne devait pas avoir plus de huit ans, et semblait hésiter entre la peur et la curiosité. Lui-même ne savait quoi faire ou dire. Il sourit timidement à l'enfant, sans résultat. Il vit la flèche dépasser de la poitrine du garçon avant d'entendre le claquement de l'arc. Effaré, il était incapable de détacher son regard du flot de sang qui s'échappait de son cœur encore battant.

« Viens !»
Méaris le prit par l'épaule et le força à se relever, récupérant au passage sa flèche du corps pas tout à fait mort.

«Mais tu es folle ! Shahid faisait tout son possible pour ne pas hurler.

&mdash; Comment ça ? Il nous avait vu.
&mdash; C'était un enfant !
&mdash; C'était un obstacle à notre mission. Tu sais que j'ai eu raison de le faire, et je le referais si j'y étais à nouveau obligée. Tu devrais t'y préparer aussi.»

Shahid ne répondit rien, son visage se ferma. Elle n'avait pas tort, cet enfant représentait un péril, mais de là à le tuer froidement ? Il en était incapable, et l'absence de regret sur le visage de son amie l'avait perturbé plus que tout. Il se rattrapait de toutes ses forces à son sens du devoir, tentant d'occulter celui de l'honneur. Ils traversèrent les champs au pas de course, mais durent ralentir leur progression à leur arrivée dans la région habitée.

Si la plupart des habitants étaient pâles, certains avaient un teint aussi mat que celui des imposteurs. Du moment que personne ne s'intéressait de près à eux, ils pouvaient passer inaperçus en pleine lumière. Malgré leur tension permanente, ils étaient comme invisibles. Quelques passants se retournèrent à leur rencontre, intrigués par le large sac que portait le costaud, ou bien visiblement indisposés par leur odeur. A chaque fois, Méaris refermait ses doigts sur la dague qu'elle dissimulait dans sa manche, mais les curieux les oubliaient bien vite.

Le passage du mur intérieur fut autrement plus difficile. Des hommes à l'air sévère surveillaient ceux qui cherchaient à traverser le mur, et inspectaient leurs affaires. Lorsque l'un d'entre eux pointa le sac de Shahid, il l'ouvrit maladroitement et présenta la couverture et les vêtements qu'ils avaient ajouté pour masquer leurs armes. Méaris était prête à bondir au cas où le subterfuge ne fonctionnait pas. Finalement, d'un air las le garde leur fit signe de poursuivre leur chemin. Avant qu'ils ne s'éloignent, l'homme lança quelque chose d'un ton badin dans une langue qu'ils ne comprenaient pas. Essayant de paraître naturel, Shahid lui répondit d'un sourire.

La violence des bruits les saisit dès qu'ils firent un pas à l'extérieur du mur. C'était comme se retrouver piégé dans le plus animé des souks, lorsque tous les marchands repéraient en même temps la même proie. Shahid ne trouva pas de meilleure comparaison, pourtant il peinait à décrire son supplice. Il se retrouvait écrasé par ces sons qu'il ne reconnaissait pas, des lames s'entrechoquant à sa gauche, les hurlements abyssaux des effriti gardiens à sa droite, et le murmure de cette foule qui se muait en brouhaha.

Son esprit de soldat se fixa sur une réflexion militaire pour s'empêcher de défaillir. Quelle formidable arme pourrait faire ce genre de son, s'il était possible de le contrôler. Grâce à cette pensée, il trouva cette cacophonie plus supportable, presque agréable. Il essayait d'en saisir les subtilités, se demandait comment les ingénieurs du calife pourraient réaliser cette idée...

Méaris n'avait pas sa résilience. Habituée aux longs périples solitaires, elle s'était faite du silence un ami, et grimaçait déjà lorsqu'ils traversaient les faubourgs. Elle prit sa tête dans une main et s'appuya sur Shahid de l'autre. Les jambes coupées, elle se laissa faire quand il l'emmena dans une ruelle étroite, éloignée de l'axe principal. L'agression sonore était atténuée, ils pouvaient au moins s'entendre parler.

« Ça va ?

&mdash; Pas vraiment...Tout ce bruit... J'ai un terrible mal de crâne, j'arrive plus à y voir clair.
&mdash; Tu veux t'arrêter là ?
&mdash; Et puis quoi encore ? Aïe... J'ai... juste besoin de me reposer quelques minutes, et je serais d'attaque. Je ne vais pas être un fardeau.»

Shahid se garda de lui rétorquer qu'elle était loin d'être un fardeau. Sans elle, il ne serait certainement pas allé si loin, son imagination se limitait à la stratégie militaire. Jamais il n'aurait eu l'idée de se vêtir comme les habitants de la cité pour s'infiltrer chez eux. Laissant son amie reprendre ses forces, il retourna sur l'axe principal, cherchant à estimer la distance qui les séparait de l'imposant bâtiment central. Si la route était en ligne droite, une heure ou deux seraient amplement suffisantes.

Hélas, rien n'était aussi simple. Méaris progressait lentement, et devait parfois s'arrêter pour reprendre son souffle. Signe encourageant, ces pauses étaient de plus en plus courtes. À moins qu'elle n'était en train de se faire violence pour ne pas le ralentir. Au delà de ces arrêts fréquents, le chemin qu'ils suivaient prenait de nombreux détours, quand il ne s'agissait pas d'une énième route circulaire, et par dessus tout, la foule devenait à certains moment si compacte qu'il était impossible d'avancer, pendant plusieurs minutes.

Alors que le soleil avait dépassé son zénith, Shahid réalisa que l'intensité du capharnaüm s'amenuisait au fur et à mesure qu'ils étaient proches du centre. Il avait au début cru que c'était lui qui s'y habituait de mieux en mieux, mais la différence était indéniable. Méaris aussi s'en rendait compte, et reprenait péniblement son rythme assuré.

Le soleil commençait à décliner lorsqu'ils atteignirent enfin la place entourant l'édifice aux tours jumelles. Il était encore plus incroyable vu de près. À ses pieds, c'était comme s'il s'élevait jusqu'au ciel. D'un commun accord, ils contournèrent le bâtiment à la recherche d'une porte réservée aux domestiques. Même avec l'aide d'une magie divine, un palais de cette taille employait forcément des domestiques. Leur intuition s'avéra correcte, et ils découvrirent une petite porte à moitié camouflée dans le mur, portant une inscription dans un alphabet étrange, que Méaris reconnut comme étant celui des hommes du Nord. Elle déchiffra les lettres E, X, I et T sans les comprendre. Peut-être était-ce là le nom donné à leurs serviteurs par ces hommes ?

Haussant les épaules, Shahid se saisit de l'étrange poignée et tenta de l'abaisser en vain. Après plusieurs essais infructueux, il trouva enfin le moyen d'ouvrir la porte. Elle était étonnamment lourde, mais pas suffisamment pour lui résister. Ils pénétrèrent dans un escalier sombre, sans torche ni fenêtre. Ils sursautèrent quand des bulbes de lumière s'illuminèrent au plafond. Aucun feu ne semblait brûler à l'intérieur, mais ils éclairaient plus sûrement qu'une dizaine de torches. Le Cœur du monde possédait définitivement une magie incroyable.

Ils abandonnèrent le sac et les faux-semblants dans cet escalier. Shahid déchira son vêtement trop serré, et regretta d'avoir dû laisser son armure en arrière. Il s'empara de son cimeterre et ouvrit la voie, arme au clair. Il semblait évident que le Cœur du monde soit sous terre, aussi descendirent-ils. L'escalier était lugubre, rarement emprunté, et encore plus rarement entretenu. Plus ils s'enfonçaient dans le sol, moins les bulbes lumineux qui s'illuminaient sur leur passage étaient puissants. Pourtant, le bourdonnement qui emplissait l'espace ne laissait aucun doute : ils s'approchaient d'une force sans pareille, qui faisait trembler les murs par moment.

Ils avaient ignoré toutes les portes jusqu'à la dernière, au plus profond de la cité. Le bourdonnement était plus intense que jamais, ils pouvaient même sentir leurs cheveux se dresser sur leur tête. L'énergie était presque palpable, derrière cette porte. Heureusement pour Shahid, celle-ci possédait une poignée ordinaire. Méaris était déjà prête, une flèche encochée. Elle pourrait le couvrir s'il y avait du danger &mdash; un artefact si puissant était probablement sous la garde d'une troupe de mages, peut être même d'un efrit.

Il ouvrit la porte. La salle était plongée dans une demi-obscurité, les sphères suspendues au plafond comme mortes. Nul gardien, nul efrit, personne. Les deux voleurs s'interrogèrent du regard. Étaient-ils réellement au bon endroit ? Pourtant, pas d'erreur possible, le bourdonnement était plus fort que tout dans cette salle. Et il provenait d'un immense cylindre de métal dont s'échappait une lumière bleutée.

«C'est lui. Ils l'ont mis en cage ? Comment osent-ils ?»

L'éclat de colère du soldat résonna dans la salle bien plus grande qu'il ne l'aurait cru. De l'obscurité, une voix grondante et des pas précipités leurs parvinrent. Agissant de concert, Shahid se précipita vers la menace alors que Méaris bandait son arc. Elle décocha sa flèche dans l'œil du gros homme au moment où le cimeterre lui tranchait la gorge. Il s'effondra sans avoir eu le temps de réaliser ce qui lui arrivait.

Aussitôt que le sang eut fini de jaillir de sa plaie, des bulbes rouges tourbillonnants apparurent et une voix démoniaque se mit à hurler. Le gardien avait sonné l'alerte sans qu'ils ne s'en rendent compte. Il était sûr que le sous-sol serait envahi de mages et de soldats dans les minutes à venir, il serait impossible de les combattre.

«Il faut le détruire, hurla Shahid pour couvrir le son de l'alarme. On ne peut laisser ces hommes maltraiter le pouvoir du monde.

&mdash; Comment ? répondit Méaris sur le même ton.
&mdash; Je ne sais pas, mais il le faut.»

Ils avaient très peu de temps. Alors que l'archère fouillait la pièce à la recherche d'un artefact magique, le soldat s'acharna sur la paroi de verre, seul point faible de la prison qui retenait le Cœur du monde. Elle était si fine, et pourtant si résistante qu'un pommeau d'acier manié par un bras puissant parvenait tout juste à l'érafler. Il persévéra jusqu'à ce que le pommeau se fissure, laissant une marque dans le verre. Il pouvait déjà entendre la course de dizaines d'hommes résonner dans les escaliers. Sans autre idée, il enfonça sa lame dans la fente qu'il venait de créer et frappa, encore et encore, jusqu'à s'en faire saigner les mains.

Une porte vola en éclat. Il entendit des gens hurler et Méaris lui crier de continuer. Elle était prête à se sacrifier pour lui faire gagner du temps. Essayant de ne pas penser à quel point l'imaginer périr lui déchirait le cœur, il redoubla d'effort. Enfin, la vitre se brisa. Très peu, à peine pour qu'il puisse y faire pénétrer son poignet. Une douleur vive dans l'épaule gauche le foudroya. Une flèche. D'autres le transpercèrent. À bout de force, puisant dans ses dernières ressources, il lança son cimeterre en direction de la boule d'énergie qui pulsait dans la cage. Avant de sombrer, il discerna le regard paniqué des hommes qui venaient de l'abattre. La température augmentait dans la salle, et pourtant il avait de plus en plus froid. Le son l'abandonna en premier, puis ce furent les images. La dernière chose qu'il ressentit fut une sensation de brûlure intense sur chaque parcelle de sa peau.
