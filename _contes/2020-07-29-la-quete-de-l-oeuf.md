---
bg: "la-quete-de-l-oeuf-1"
layout: post
title: La quête de l'œuf
summary: Deux condamnées à morts envoyés dans une mission suicide au sommet d'un pic inhospitalier.
source: “Secret Pass”, par Greg Rutkowski
ref: https://www.artstation.com/artwork/A595q
date: 2020-01-27
categories: posts
tags: ["Les Divinépées"]
type: Conte
author: Rumengol
lang: fr
---

"Au nom des Dieux, on doit être devenus complètement fous..."

"C'est pas les Dieux qui nous aideront ici, mais l'acier et ta magie. Et puis, on a bien vu l'aigle s'envoler tout à l'heure, non ? Il va pas revenir de sitôt. Ah, enfin j'en vois le bout. Prépare toi."

Alkor gravit les dernières marches et dégaina son épée. Léréina, en retrait, fit de même et commença à former une boule de feu dans sa main. Elle se détestait pour ce qu'elle allait faire, mais le choix ne lui appartenait plus depuis bien trop longtemps. Elle, Alkor et deux de leurs compagnons devaient escorter un collecteur d'impôts lors de sa récolte dans des terres encore hostiles à l'empire. Deux guerriers, une magicienne et un archer kerstrel, la tâche s'annonçait plutôt simple. Jusqu'à l'embuscade. Des dizaines de draugr, serviteurs réincarnés qui guidaient en temps normal les armées squelettiques de Borgath. Même sans la surprise, il leur aurait été difficile de vaincre un seul de ces êtres maudits.

Ils n'avaient aucune chance, et pourtant ils s'étaient battus vaillamment. Célithor, Deggron, les soldats qui les accompagnaient et le dignitaire furent tués dans le feu de l'action. Alkor et elle avaient étés laissés pour morts, gravement blessés, le visage du guerrier déformé par un coup de hache de guerre. Au choc de l'attaque s'ajouta l'étonnement de découvrir, en retournant à Brumefort, qu'ils étaient considérés comme des traîtres ayant abandonné leurs compagnons et la mission. Ils furent à peine soignés et emprisonnés vingt jours durant, en attendant leur exécution.

Elle était demeurée en état de choc presque toute leur captivité, s'alimentant peu, ne dormant que lorsque son corps s'effondrait. Alkor était déjà mercenaire bien avant de les rencontrer, il était parvenu à rester fort, à lui faire conserver une étincelle d'espoir. Selon lui, cette déclaration de traîtrise était bien trop brusque, trop rapide, même pour l'empire. Il supposait qu'on les ferait mariner un long moment, puis, lorsqu'ils seraient épuisés et affamés, à quelques jours de leur exécution présumée, un mystérieux intendant encapuchonné viendrait leur faire une proposition qu'ils ne pourraient pas refuser, probablement une mission tellement suicidaire qu'aucun individu sain d'esprit ne s'y risquerait.

Il s'était avéré que le guerrier était dans le vrai, exception faite de l'identité de leur commanditaire : le régent de la province en personne leur avait rendu visite en découvrant son visage. Sa proposition était simple, la liberté et suffisamment d'or pour refaire leur vie jusqu'en dehors de l'empire s'ils le souhaitaient, en échange d'un objet unique : l'œuf d'un aigle géant, des êtres mythiques rarissimes et dont le plumage résistait à la plus acérée des lames. Leur équipement leur fut rendu et ils furent escortés jusqu'au pied du Pic de l'Aigle. Le capitaine qui les y déposa avait lancé d'un ton sans équivoque que la fuite n'était pas acceptable.

Une après-midi à éprouver la liberté retrouvée, plusieurs jours pour étudier le pic escarpé et une demi douzaine de tentatives d'escalades infructueuses avaient été nécessaires pour découvrir l'escalier creusé à même la roche, brillamment dissimulé dans la façade du pic. Une autre demi journée fut passée à gravir les milliers de marches irrégulières qui couraient sur le flanc et parfois l'intérieur de la formation géologique. Quand ils avaient vu l'aigle s'envoler des hauteurs, ils s'étaient tout deux figés instantanément, avant de tenter de s'effacer dans les recoins de la corniche.

Ils touchaient enfin au but de leur ascension, et il se présentait sous la forme d'une petite plateforme et d'une porte en bois qu'Alkor s'apprêtait à ouvrir.

"Attends, l'arrêta la magicienne en dissipant le feu qui naissait dans sa paume. Regarde le signe. Rovik Navarr, Maître des runes nain. C'est probablement un ermite, on ne va tout de même pas faire irruption chez lui armés ?"

"T'es trop naïve, Léréina. Faut jamais faire confiance à un nain. On le crame peut-être pas à vue, mais j'entre pas chez un de ces court-sur-patte sans avoir de quoi le raccourcir encore plus."

Son ton n'admettait aucune réplique, aussi la jeune femme se contenta d'abaisser son épée. Il ouvrit la porte avec fracas, sans y entrer d'abord, pour déclencher un quelconque piège dissimulé, puis en se précipitant à l'intérieur. Le nain n'eut que le temps de se retourner avant de sentir le métal de l'arme d'Alkor sur son cou.

"Vous n'êtes pas un client, vous, n'est-ce pas ? fit-il sans se laisser décontenancer. Un bandit alors ? Désolé, mais j'ai peur que vous soyez déçu de votre grimpette ici, ma seule richesse est mon savoir."

"Alkor ! protesta Léréina. Arrête, on est pas venus pour ça !"

"Tout juste, vociféra le guerrier, on est pas là pour tes runes merdiques, on veut l'œuf de l'aigle."

Le nain ne répondit pas. Son corps devint flasque et Alkor laissa tomber le cadavre égorgé. Léréina laissa échapper un cri de surprise.

"Alkor ! Pourquoi tu as fait ça ?"

"Je te l'ai dit Léréina. T'es trop naïve."

L'homme s'écarta et pointa la main de sa victime, qui tenait fermement une dague inscrite de runes sombres. En fait d'un maître de runes, ils étaient dans la demeure d'un maître assassin. S'éloignant de la dépouille, Alkor fouilla la pièce et découvrit un étroit passage qui semblait monter encore plus haut, peut-être au sommet du pic. Ils avancèrent courbés pendant de longues minutes qui s'étirèrent autant que le conduit semblait s'étrécir. Enfin ils débouchèrent sur un promontoire rocheux.

{% include image.html url="la-quete-de-l-oeuf-2.png" description="“Secret Pass - Eagle Nest”, par Greg Rutkowski" ref="https://www.artstation.com/artwork/o1bqz" %}

Là où ils s'attendaient à trouver, savaient-ils un nid gigantesque fait de troncs d'arbres avec, reposant en son centre, un œuf brillant aussi grand qu'eux, se retrouvèrent sur une corniche nue, battue par les vents. Ils s'avancèrent, luttant contre le souffle qui tentait de les arracher du sol. Le panorama qui s'étendait devant eux était à couper le souffle. Ils étaient passés au dessus de la couche nuageuse, à une hauteur où neige et nuage se mêlaient dans une substance laiteuse.

Un battement d'ailes les arracha à leur torpeur. Le ciel s'assombrit comme l'aigle géant semblait surgir du néant, serres dehors, prêtes à les déchiqueter.

"Merde, protège-nous ! tonna Alkor"

Mais la magicienne n'avait pas attendu son ordre et déjà une barrière magique se formait, englobant les deux compagnons. L'oiseau frappa de plein fouet le mur invisible et une onde de choc s'y propagea, repoussant violemment en arrière les intrus. La barrière tint bon, mais Léréina suait déjà à grosses gouttes, et un filet de sang commençait à s'écouler de son nez. Alkor la tira brutalement en arrière, à l'abri du conduit. A l'extérieur, l'aigle glatit sauvagement, les invectivant dans une langue qui leur était inaccessible.

"L'œuf... on doit..."

"Oublie l'œuf ! Il y avait rien là bas, le nain était le gardien d'un foutu trésor qui a disparu ! Ca m'étonnerait que ce soit un coup du régent alors quelqu'un l'a doublé. Et il va pas aimer qu'on revienne les mains vides, la potence, tu te souviens ? Je te le dis, faut qu'on calte, qu'on se planque quelque part loin de l'empire, on a plus rien à faire ici."

"On va devenir des criminels recherchés partout."

"Oui. Mais on va pas errer sans but. Dis moi que toi aussi tu veux savoir pourquoi on s'est fait embusquer par ces foutus draugr. Tu penses pouvoir nous emmener loin d'ici ?"

"Je suis trop faible pour invoquer un portail. Un portail de sang oui mais... c'est interdit."

"On peut plus se permettre de se préoccuper des interdits. Le corps du nain fera l'affaire."
