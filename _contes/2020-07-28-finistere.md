---
bg: "finisterre"
layout: post
title: Finistère
summary: L'éternel gardien du phare du bout du monde assiste au grand exode.
source: “End of the world”, par Denis Loebner
ref: https://www.artstation.com/denisthemenace
date: 2020-01-23
categories: posts
tags: ["La prison des dieux"]
type: Conte
author: Rumengol
lang: fr
---

Le phare du Finistère n'avait jamais aussi bien porté son nom. Chaque jour, la terre reculait un peu plus. Le gardien était un vieil homme solitaire, qui ne s'était jamais intéressé aux affaires de l'archiduché. Son rôle était de veiller sur la mer éternelle, de s'assurer que rien n'en venait, et d'avertir les navires égarés qui vogueraient à leur perte. Privilège des gardiens, il avait obtenu un corps qui ne vieillissait qu'au rythme de son esprit. Il avait donc pleuré successivement sa femme, son fils puis sa fille, et vivait désormais seul dans un hameau qui lui paraissait chaque jour plus grand. Faute d'entretien, les bâtiments tombaient en ruine, seul le phare trônait, fier, droit, solitaire.

Son nom était depuis longtemps oublié de tous, lui comprit. Il ne connaissait d'homme que Randier, qui lui apportait une fois par mois de quoi se nourrir, et de temps en temps de nouveaux vêtements. Il ne s'appelait probablement plus Randier depuis plusieurs générations, mais les identités s'entremêlaient, éphémères et superficielles. Le jeune homme lui avait apporté des vivres la semaine passée, aussi lui sembla-t-il étrange d'apercevoir une voile qui pointait aussi directement vers lui.

Lorsqu'elle fut à portée de vue, il constata que la caravelle qui s'approchait avec vélocité n'était en rien semblable au petit voilier de Randier. Son premier réflexe fut de se précipiter aussi vite que son corps fatigué le lui permettait, avant de se rappeler que sa lumière, en plein jour sous un ciel sans nuage, n'aurait que peu d'effet. Il ne pouvait les arrêter. Des fous qui avançaient sciemment vers les hauts fonds du Finistère, qui allaient s'échouer ici et mourir à petit feu, comme d'autres avant eux.

Puis il remarqua la voile. Dessus, des armoiries qu'il n'avait pas contemplé depuis peut-être des siècles. Il se rappelait vaguement de la forme des deux lions dévorant le soleil comme étant le symbole de l'archiduché. Etait-il tombé en désuétude ? Peut-être le navire avait-il été détourné ? Enfin, il n'était plus concerné depuis bien longtemps. Il se contenta d'attendre.

Le navire jeta l'ancre au large du Finistère. Une embarcation fut mise à l'eau et accosta quelques minutes plus tard. Un homme se présentant comme un haut-commissaire annonça pompeusement la réquisition de toutes les informations que le gardien possédait sur la mer éternelle au nom de l'archiduc. Celui-ci ne répondit qu'avec un haussement de sourcil. Aucun des explorateurs qui avaient dirigé leur voile vers l'Ouest n'était revenu, et rien n'y n'était jamais apparu. Il avait beau être la frontière de l'inconnu, il ne savait rien. L'officiel paru déçu. Un second haussement de sourcil, interrogatif cette fois, l'enjoint à expliquer les raisons de sa venue.

Un désastre avait frappé la capitale. La terre s'était effondrée, formant un puits qui ne semblait avoir de fond. Les premiers jours, sont expansion éclair avait été mise sur le compte d'une activité géologique. Mais il s'était tant étendu qu'il fut rapidement un gouffre avalant l'illustre cité en une semaine. Personne désormais ne doutait du caractère divin du gouffre : quelque dieu avait décidé de les punir pour un acte... obscur et incertain, et désirait à présent les voir disparaître. Devant cette implacable fin, l'archiduc avait décidé de monter une flotte et de partir en exil dans la mer éternelle, espérant trouver une terre d'accueil pour son peuple.

Les mois suivants, il vit beaucoup de réfugiés fuyant la terre pour s'aventurer dans la mer. Randier s'enfuit aussi, avec sa famille. On pilla sa réserve de nourriture. Certains tentèrent de discuter avec lui. Tous partirent. Le gouffre commençait à entamer son île. Tout le monde était au fond du puits, ou au loin dans la mer. Peut-être aurait-il dû partir. Mais sa vie était liée à son phare, et si son existence devait s'achever, mieux valait que ce soit sur ce sol dont il connaissait chaque brin d'herbe. Le vide s'avançait et la terre reculait, chaque jour un peu plus. Il monta dans son phare, attendant. Le phare du Finistère, peut-être aussi de l'unique terre.
