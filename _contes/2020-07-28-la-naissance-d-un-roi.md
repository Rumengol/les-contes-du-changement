---
bg: "la-naissance-d-un-roi"
layout: post
title: La naissance d'un roi
summary: Lorsqu'un démon ravage sa ville, un petit garçon éveille l'antique pouvoir qui sommeille en lui.
source: “With Grandfather’s help”, par Pavel Kolomeyets
ref: https://www.artstation.com/artwork/JQRgA
date: 2020-01-24
categories: posts
tags: ["Les Divinépées"]
type: Conte
author: Rumengol
lang: fr
---

"Tu es le descendant d'une grande lignée, Arthur. Les esprits de tes ancêtres vivent en toi, ils sont avec toi à chaque instant." Les mots de sa mère tournaient dans sa tête inlassablement. Tout le monde avait fui la ville quand le titan du volcan s'était éveillé. Sa mère était trop malade pour sortir de son lit, il ne pouvait se résoudre à l'abandonner. Alors il s'était équipé de son épée et de son écu de bois, et était sorti affronter le démon. Le quartier semblait métamorphosé sans toutes ces femmes à moitié déshabillées qui se couvraient maladroitement à son passage et les hommes vendant toutes sortes de remèdes douteux.

Il tourna vers la place du grand-comte Funéral. Là-bas, il aurait une vue plus dégagée pour combattre le volcan. Il agissait d'instinct, pour protéger sa mère, faisant fi du danger. Déjà l'air était devenu lourd, et il pouvait distinguer les panaches de fumées venant de plusieurs quartiers. Des rochers gigantesques jaillirent dans le ciel au-dessus du petit garçon, qui ralentit un temps pour les regarder, avant de continuer son chemin. Il ne devait pas faillir, se montrer courageux, pour sa mère.

Il était le descendant des rois légendaires de jadis, et leur sang coulait dans ses veines. Il était persuadé que son grand-père n'aurait fait qu'une bouchée de ce titan, quel qu'il soit. La rue s'élargissait, et il vit pour la première fois la catastrophe. Il était si grand, son corps semblait être fait d'un panache de cendres volcaniques. L'air était maintenant étouffant. Il se rendit compte qu'il n'atteindrait jamais la place du grand-comte. Sa volonté vacilla. L'image de sa mère alitée suffit à raviver la flamme de sa détermination. Il cria de défi. Qu'importe si lui mourait, tant que la femme qui le dorlotait chaque soir pour l'endormir pouvait vivre !
L'atmosphère se rafraîchit soudainement. Le petit garçon rouvrit les yeux. Il était enveloppé d'une aura bleutée. Ses yeux ne piquaient plus, il n'inhalait plus de fumée. Sans trop savoir comment, il comprit intérieurement que son grand père était là, le protégeant. Plus que ça, son esprit était apparu, faisant plusieurs mètres de haut, vêtu d'une armure intégrale, d'un magnifique écu et d'une épée reluisante.

Arthur manqua de trébucher de surprise. Une voix lui parla en son for intérieur.

"Allons, fils. Je suis avec toi maintenant. J'ai fondé cette ville et je ne laisserais personne la détruire !"

Galvanisé, le garçon reprit sa course effrénée, sans plus se soucier de la chaleur ou des émanations toxiques. Un hurlement guerrier jaillit à la fois de sa gorge et de la forme éthérée. Le titan se tourna vers eux, le regard brûlant d'une haine infernale. Il semblait se demander qui osait s'opposer à lui. Il n'eut pas le temps de comprendre que déjà l'écu fantomatique le percutait comme s'il s'agissait de métal, et l'envoyait voler à travers la ville. L'être titanesque s'effondra, écrasant plusieurs quartiers sous le poids de son corps monstrueux.

Il se releva et projeta un torrent de flammes. Le fantôme leva son écu, encaissant le coup. Mais Arthur, qui reproduisait chacun des gestes de son aïeul, en ressentait aussi les effets, et ne pouvait s'empêcher de reculer sous la pression du jet enflammé. Malgré lui, le chevalier perdait du terrain, lié qu'il était à l'enfant. Il l'exhortait d'avancer, qu'il ne fallait pas céder, mais chaque pas le déséquilibrait un peu plus.

La chute de l'enfant n'était plus qu'une question d'instants. La forme éthérée vacillait, l'écu de bois noircissait, comme s'il était en feu. Sa détermination seule ne suffisait plus face à une puissance pure surgie des entrailles de la terre. Le titan savourait déjà sa victoire, avançant au delà du raisonnable. L'enfer sur terre se déchaînait autour de lui, et cet impudent ancêtre protecteur n'était qu'une nuisance.

Tout à sa satisfaction, le démon ne vit rien venir. Ni le sursaut d'énergie du chevalier, ni son bond en avant. Pas plus que l'épée décrivant un arc de cercle vertical pour s'abattre sur sa nuque, tranchant net son cou. Le flot infernal s'interrompit. Le gigantesque corps sembla se consumer, puis s'éteignit rapidement. Seule la tête vivait encore, vaincue mais pas éliminée. L'essence du feu l'alimentait encore, faisant fondre les pavés. Comment était-ce possible ? Il était vainqueur l'instant d'avant, le chevalier ne feintait pas, il était en train de disparaître !
Avant que l'épée métaphorique ne l'achève, il distingua, au fond de la rue, un enfant mimant les gestes du chevalier, droit et fier, et à ses côtés, la silhouette fantomatique d'une femme l'enlaçant.
