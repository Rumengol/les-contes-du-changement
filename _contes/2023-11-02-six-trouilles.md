---
bg: "six-trouilles"
layout: post
title: Six Trouilles
summary: Des enfants transgressent un interdit séculaire et ont la peur de leur vie.
source: “-80-” par Veronika Kozlova
ref: https://www.artstation.com/artwork/aoZee9
date: 2023-11-02
categories: posts
tags: ["Le monde d'Alkénor"]
type: Conte
author: Rumengol
---
Une effervescence frénétique semblait s'être emparée du village de Haewon. Deux évènements se superposaient et se mélangeaient dans la clameur du bourg. La récolte des citrouilles de cette année venait de s'achever, et elle était particulièrement fructueuse. Cette nouvelle signalait la fin d'une saison de récolte éprouvante et marquait le début du festival d'automne. Le jour même, le fils aîné du chef du village était de retour, après trois années d'études à la grande ville. C'était une grande nouvelle, car il était la première personne du village à passer le concours d'admission à l'académie depuis des générations. Il revenait pour trois semaines de congés avant de compléter son cycle d'apprentissage. Ensuite, il était attendu de lui qu'il revienne pour prendre la tête du commerce familial ou qu'il ne s'élève jusqu'aux plus hautes sphères de la société.

Au milieu des rires et des cris, les enfants couraient et jouaient, bosuculaient les travailleurs et renversaient les caisses. Malgré les remontrances, rien ne semblaient être en mesure de calmer l'excitation des plus petits. En désespoir de cause, les villageois firent appel à la doyenne Ihwa, qui aimait conter des histoires et captiver l'attention des petits diables. Elle les attira loin du coeur de l'agitation avant de s'installer sous le grand arbre qui perdait ses dernières feuilles.

"Approchez, les enfants, et écoutez un peu les histoires d'une vieille femme. Vous savez pourquoi notre village cultive les citrouilles, au point d'en être les plus importants producteurs de la région, peut-être même du royaume ?"

Sa question fut accueillie par un mur de hochements de tête négatifs. Elle s'y attendait, les enfants en âge d'avoir déjà entendu son histoire étaient en train d'aider leurs parents, elle avait devant elle un public vierge.

"Eh bien, nos collines ne sont pas propices aux cultures, même à celle des citrouilles. Lorsque notre village y fut fondé, rien n'y poussait, et la famine menaçait les premiers colons. Ces gens étaient poussés par le désespoir, ayant fuit des terres gouvernées sans merci par un seigneur de misère qui tourmentait ses sujets. Tout avait été tenté, mais rien n'y faisait. Les communautés des alentours avaient déjà abandonné et s'en étaient retournées d'où elles venaient, acceptant de vivre sous la coupe du cruel seigneur. Mais les fondateurs de notre village étaient déterminés, et même lorsque leur nombre diminuait par la maladie ou les abandons, ils ne cessèrent d'espérer.

Un beau jour, alors que nous n'étions réduits qu'à un hameau grelottant, un messie est descendu du temple de la plus haute colline. C'était un ancien dieu dont la dernière maison tombait en ruine et la foi était oubliée. Touchée par la persévérance dont ont faisaient preuve nos ancestres, la divinité leur fit dont de quinze graines dans lesquelles elle avait infusée de son essence divine. Ces graines prendront racines, leur a promis l'être divin, et leurs fruits seront les meilleures citrouilles que les villageois verront de leur existence. La première récolte serait suffisamment fructueuse pour leur permettre de survivre à l'hiver, et les graines de celle-ci seraient plus nombreuses encore. En échange, le dieu ne demandait rien d'eux au début. Puis, lorsque l'ombre de la famine ne les menaçerait plus, il ne réclamait que les trois meilleurs fruits de chaque récolte, portés à son temple par une jeune fille pure, un homme fort et une grand-mère à la descendance vigoureuse. Telle était la façon de le vénérer, car il n'acceptait nulle prière et refusait que quiconque ne monte jamais sa colline en dehors des porteurs d'offrandes.

Et c'est depuis ce jour que nous honorons ce gracieux don et que, chaque saison, nous sélectionnons les plus grosses et les plus juteuses de nos citrouilles pour être portées jusqu'au temple, comme le veut la tradition. Ce soir, moi aussi je porterais sur mon dos le symbole de notre prospérité et je l'offrirais au dieu dans le temple. Tout comme la petite Elise qui a été choisie cette année. Quant aux hommes, ils se livreront à des épreuves pour savoir lequel d'entre eux est le plus fort du village. Mais vous ne devriez pas regarder ça, ces jeux sont horriblement violents pour vos petits yeux innocents.

Alors maintenant, laissez les adultes travailler et n'allez pas déranger leur préparation pour ce soir."

La foule d'enfants qui écoutait la vieille Ihwa avec attention se disperse soudainement, partant dans un nouveau concert de cris et de bousculades. Néanmoins, son conte semblait avoir eu un effet car ils évitaient désormais de déranger les préparatifs, préférant chahuter dans les champs vides qui avaient revêtus l'apparence d'un gigantesque terrain de jeu à leurs yeux. Un groupe seulement se tenait à l'écart, dissimulé dans la pénombre d'une cabane. Sitôt l'histoire de leur aïeule entendue, ils s'étaient réunis dans leur repaire sous ordre de leur chef, le plus jeune fils du chef du village.

Tiwal n'était pas le plus âgé, mais il s'était imposé comme chef de sa petite bande autrement que grâce à son père. Grand et musclé pour son âge, il était capable de battre n'importe quel autre enfant de son âge et même la plupart des plus vieux. Il s'était rapidement imposé comme un petit chef et se faisait obéir des autres enfants, sous peine d'un passage à tabac. Indécrottable fauteur de trouble, il se faisait régulièrement remonter les bretelles par son père, et ses cheveux roux repoussaient à peine sur son crâne chauve depuis que son père l'avait tondu en punition d'une farce qui avait tourné au désastre la semaine passée, causant l'incendie d'une grange heureusement vide. Il avait réuni ses lieutenants, ses amis les plus proches et pour la plupart des brutes qui aimaient tourmenter les plus jeunes qu'eux.

"Tout ça, c'est des conneries, qu'à dit mon frère. Il a étudié à la grande ville, et les dieux d'avant ils sont tous morts, vu qu'ils ont plus de prêtres. En fait, ces histoires c'est juste de la tradition pour qu'on se pose pas trop de question. Mon frère, il dit qu'en fait dans les collines on peut faire pousser pas mal de céréales, mais qu'on fait que des citrouilles parce que c'est meilleur économie-ment. Et en fait, le temple il est vide et c'est des animaux sauvages qui mangent nos plus belles citrouilles.

— Et qu'est-ce que ça peut nous faire ? rétorqua Bwu-il, un garçon à peine plus âgé que Tiwal et bâti comme un lutteur.

— Eh ben mon frère dit qu'on perd de l'argent, vu qu'on jette à chaque fois nos meilleures trouilles. Mais lui il est obligé de rester au festival ce soir. Alors nous, on va récupérer les trouilles !

— Tu veux dire... commença Seolhwa, qui le regardait avec des gros yeux.

— Ouais ! On va aller dans le temple et récupérer les trouilles avant qu'elles pourissent ou qu'on nous les vole !"

***

Le festival battait son plein malgré l'heure tardive. Les enfants sages étaient déjà parti se coucher, épuisés par une longue journée et leurs jeux. Seul un petit groupe luttait tant bien que mal contre la fatigue qui les gagnait, attendant impatiamment que vienne leur heure. Tiwal et ses sous-fifres étaient en marge des festivités de façon à pouvoir s'esquiver facilement. Ils s'étaient amusés avec les autres tout à l'heure, puis avait fait mine de rentrer en même temps que le reste des gamins. En réalité, ils guettaient la vieille Ihwa et Elise qui était plus âgée qu'eux de plusieurs années mais qu'ils n'aimaient pas. Sa famille ne s'était installée dans les collines que depuis deux générations, et ils avaient toujours du mal à se faire accepter par certains des villageois les plus rétifs. Les deux femmes discutaient avec les principaux cultivateurs tout en observant le tournoi de lutte final qui déciderait de l'homme qui les acompagnerait. Sans que ce la ne surprenne qui que ce soit, ce fut Yuhwan qui remporta haut la main cette compétition. Il s'agissait de l'un des gardes du village, le père de Bwu-il qui tenait plus de l'ours que de l'homme. S'il était parfois la risée du village à cause de son intellect limité, il n'y avait personne à même de lui offrir un véritable défi sur ces terres.

Le trio quitta le village sous les acclamations, chacun portant une lourde charge sur les épaules. Les vieux os de la doyenne étant incapables de supporter un tel poids, Yuhwan portait en réalité deux citrouilles à lui seul, une dans chaque main. Ainsi chargés, leur progression était lente mais constante. Assez vite, les villageois se désintéressèrent de la procession qui peinait à progresser sur le sentier mal entretenu. Ce fut ce moment que choisit la bande de Tiwal pour se lancer sur leurs traces. Ils les suivirent à une bonne distance, veillant à ne pas se faire repérer. A la moindre erreur, le garde se retournerait et les ramènerait au village en leur tirant les oreilles. Cette fois, ils ne s'en sortiraient pas avec un crâne rasé, et même Tiwal redoutait la punition que son père lui réserverait.

Une fois à l'orée du sous-bois qui entourait la colline, les enfants se tapirent dans les fourrés. D'aussi loin, ils perdirent vite les citrouilles de vue quand la procession passa derrière un rocher. Ils rongèrent leur frein pendant de très longues minutes en attendant que les adultes redescendent. Au bout d'un certain temps, Seolhwa et Gi-bu décidèrent qu'il se faisait trop tard et que leurs parents risquaient de s'inquiéter. Ils se séparèrent du groupe sous les invectives de Tiwal qui les traitait de lâches et de faux amis. Le ton haussa tant qu'ils se firent presque prendre tandis que les autres rejoignaient la fête après avoir terminé leur devoir. Les deux lâcheurs profitèrent du moment de silence pour s'éclipser.

Maudissant ces deux là et se jurant de leur faire regretter cette trahison, Tiwal guida les autres jusqu'au temple abandonné. L'édifice était dans un encore pire état que ce à quoi il s'était attendu. Les pierres étaient craquelées et éboulées. Tout ce qui avait pu témoigner un jour de la fonction de ces ruines n'avait pas survécu à l'épreuve du temps. Des colonnades, seule une restait encore debout, amputée au quart de sa hauteur. Sur le côté du bâtiment, une tour s'était effondrée et avait ouvert le mur en deux, laissant s'engouffrer un vent froid et sifflant. Ce qu'il restait du toit menaçait de s'effondrer à chaque instant, le moindre toucher pouvait suffir à détruire le bâtiment entier.

Les citrouilles n'étaient nulle part en vue. La vieille Ihwa avait dit qu'ils déposaient les citrouilles sur le parvis du temple, mais il semblait qu'ils entraient bel et bien dans l'enceinte du temple.

"Sur l'autel, peut-être ? hasarda Hyeji."

Tiwal opina du chef et leur fit signe de le suivre. Même des loups n'auraient pas été assez rapide pour emporter les grosses citrouilles avant leur arrivée. Le groupe pénétra l'intérieur de l'inquiétante construction, surveillant les murs lézardés avec méfiance. Plus ils avançaient, plus l'air devenait froid et le ciel sombre. Malgré la petite taille de l'édifice, ils cheminèrent pendant plusieurs minutes sans même arriver dans la pièce principale. Les murs devinrent poisseux, comme recouvert de mélasse, et des symboles carmins apparurent. L'un d'entre eux se répétait de nombreuses fois, comme un emblème. Un fin croissant de lune orienté vers le ciel comme deux cornes acérées. Tiwal frissonait en contemplant ce signe, qui lui rappelait quelque chose. Il était certain de l'avoir déjà rencontré, mais où ? Sûrement lors de ses leçons, auxquelles il prêtait rarement attention.

Il lui fallu plusieurs minutes de progression à la lumière de la lune pour enfin se souvenir. L'illumination le frappa lorsqu'il leva les yeux vers le ciel et que les arches de pierre déformées imitaient grossièrement le même symbole. Aussitôt, il sentit toute sa bravoure fondre comme neige au soleil. Il murmura :

"On devrait peut-être faire demi-tour. C'est l'emblème de la lune de sang-

— Rouge. La lune rouge."

Les enfants se retournèrent brusquement. La voix qui avait tranché l'air froid du soir venait de derrière eux. Elle était aïgue, et pourtant avait la tessiture d'un homme. Ou plutôt, elle était tellement éraillée qu'il était impossible de deviner à qui elle appartenait. La lune qui brillait au travers de l'ouverture créée par le mur effondré les ébloui pendant un moment, après tant de temps passé dans l'obscurité. Les enfants clignèrent des yeux et tentèrent d'appréhender la scène absurde qui se déroulait devant leurs yeux.

Des dizaines, peut-être même des centaines de citrouilles s'entassaient dans le grand hall du temple. Elles étaient toutes gravées avec une attention perverse qui les avait doté d'expression malveillantes. Leurs dents acérées révélaient une lueur à l'intérieur, mais nulle bougie ne s'agitait dans un légume creusé. La pulpe pulsait, rayonnante comme de la lave, remuant sans pourtant déborder. Certaines des cucurbitacées étaient posées sur des piliers détruits, des pans de murs fissurés ou à même le sol. Mais certaines se tenaient grossièrement à la place du chef de statues de pierre partiellement décomposées. Les figures des anges et prophètes de cette ancienne religion avaient été remplacées par des masses légumineuses qui semblaient en bien meilleur état. Plus perturbant encore, ces cruels visages oranges semblaient s'être tournés vers l'entrée de la salle, fixant les enfants de leurs regards vides.

Au centre de la pièce en ruine, assise sur une pile de pulpe de citrouille comme sur un trône de viscères, une silhouette pâle les fixait de ses yeux sombres. A l'évidence, elle n'avait rien d'humaine. Sa peau était cendreuse, ses oreilles pointues et de longues cornes paraient sa longue chevelure poisseuse tachée de citrouille. Ses yeux sombres fixaient les petits intrus alors qu'un rictus agacé déformait sa bouche mainte fois écorchée. L'être posa par terre la citrouille qu'il était en train de graver et retira un long couteau dégoulinant de pulpe. Alors qu'il se tournait pour faire face aux enfants, la lune éclaira son visage pâle et dessina les contours du symbole sur son front, identique en tout point à ceux qui parsemaient les murs. Recroquevillée sur son tas de détritus, la forme était déjà terrifiante, mais lorsqu'elle se redressa de toute sa hauteur et projeta son ombre sur les murs, les enfants plongés dans l'obscurité s'enfuirent en courant.

Tous sauf un. Tiwal avait les jambes coupées. La peur le paralysait, et l'empêchait d'esquisser le moindre mouvement. Il entendait ses amis s'éloigner toujours plus loin en hurlant, jusqu'à ce que même leurs cris s'évanouissent. Il souhaitait par dessus tout les suivre, fuir loin de cet endroit, quitte à se faire gronder par son père. Il voulu s'excuser, supplier pour sa vie, pleurer, n'importe quoi. Au lieu de cela, il resta là, immobile tandis que la créature s'avançait pas à pas vers lui.

"Ils ne devraient pas crier comme ça, tes amis. Les citrouilles ont horreur du bruit."

Cette voix n'était définitivement pas humaine. On aurait dit l'accumulation de quatre personnes parlant en même temps, mais dont les gorges seraient percées de multiples barres de fer. Le son se répercutait plusieurs fois d'une façon qui était loin d'être naturelle avant de franchir ses lèvres. Ses paroles elles-mêmes avaient un goût de sang et de rouille qui asséchait la bouche d'un Tiwal peinant à déglutir.

"Je me demande si tu es le plus brave ou le plus lâche de la bande, continua l'engence avant de remarquer la flaque qui s'étendait sous le gamin. Seconde option, donc. Cela faisait longtemps que je n'ai pas pu savourer la moindre compagnie. Les têtes de citrouilles sont loyales, mais manquent de conversation. Trop de pulpe.

Je dois dire, la patience de ces villageois impose le respect, même de ma part. Je pensais que définir des conditions absurdes au contrat me permettrait de sortir rapidement, mais ils ont suivi mes demandes à la lettre pendant plusieurs siècles. Peux-tu imaginer, petit homme, ce que ça fait d'être prisonnier d'un temple décati pendant des siècles ? A graver des citrouilles et développer une armée de sbires avides de vengeances ? En fait, c'était la seule partie amusante de mon calvaire.

Au bout de deux siècles, j'ai fini par abandonner l'idée qu'un intrus ne pénètre dans le temple. J'ignore quel avertissement ils ont pu ériger, mais force est de reconnaître qu'il a été efficace, jusqu'à maintenant. Je pensais que la descendance vigoureuse serait le critère le plus difficile à remplir, mais c'était trop vague. Une importante leçon. Mais non, c'est une bande de gamins inconscients qui finit par transgresser l'interdit et me libérer de ma geôle."

Le démon de la lune rouge se retourna, sa queue fourchue fouettant l'air derrière lui. Il se saisit de la citrouille qu'il était en train de creuser avant d'être interrompu et jaugea son regard borgne et édenté.

"Ca fera l'affaire. Pour te remercier de ta bonne action, je vais te faire une faveur, mon petit. Tu vas pouvoir suivre ma glorieuse ascension dans la première loge."

Il plongea la citrouille inachevée dans la tête de Tiwal, lequel cria enfin. Son crâne traversa la carcasse molle de la citrouille qui lui recouvrit le visage comme un casque. La pulpe était bouillante au contact de sa peau qu'il sentit fondre et se mêler à sa chair. Son cri s'étouffa dans la mélasse qui pénétra sa gorge. Il s'était déjà tu lorsque son corps s'effondra sur le sol.

"Et à présent, murmura d'excitation le démon, il est temps de faire un festin sans citrouille."
