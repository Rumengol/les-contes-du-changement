---
bg: "pleine-loune"
layout: post
title: Pleine Loune
summary: Un mineur remonte de la Vallée pour découvrir un astre inconnu dans le ciel.
source: Concept Art par Alex Shatohin
ref: https://www.artstation.com/manusmortis
date: 2020-02-14
categories: posts
tags: []
type: Conte
author: Rumengol
lang: fr
---

« Remonte ! Remonte ! »

Ulkvar hurlait comme si sa vie en dépendait. Ce qui était le cas. Il entendait les poulies geindre sous l'effort. La cabine se balançait et craquait elle aussi, signe qu'elle n'était pas faite pour subir de telles contraintes. Le cœur du pauvre homme battait si fort qu'il lui semblait en entendre l'écho dans la Vallée. Il s'efforçait de bouger le moins possible malgré sa peur panique. D'autres avaient déjà survécu dans des circonstances similaires, alors pourquoi pas lui ?

« Geooooooorges !

– Je fais c'que j'peux ! lui répondit une voix étouffée par l'effort. »

Son salut était entre les mains d'un ivrogne aux bras d'acier. Il y a pire, probablement. La flamme de sa lanterne tenait bon, à l'abri de sa cage de verre. Elle au moins restait sereine. Il se pencha par dessus le bastingage par curiosité. Il était mort. Le loup qui le poursuivait venait de s'élancer dans les airs, et dans quelques secondes il aurait broyé la cabine. De surprise, Ulkvar lâcha sa lanterne. Sans essayer de la rattraper, il fit ce que son instinct de survie lui dictait : sauter le plus haut possible, s'éloigner autant qu'il pouvait des terribles crocs.

Au risque de ruiner le fragile équilibre de l'ascension, il se projeta vers le haut, se plaquant contre le plafond de la cabine. Une mesure désespérée qui lui sauva la vie. Les mâchoires du monstre se refermèrent à quelques centimètres de son dos, réduisant la moitié inférieure de l'habitacle à néant. Les chaînes furent rudement secouées, mais tinrent bon. Le loup disparut dans la brume de l'abîme.

« Y s'passe quoi ? tonna le nain.
– Re...monte moi... d'abord. »

Cette fois c'était sûr : son cœur faisait un vacarme suffisant pour réveiller tous les loups de la Vallée. L'ascension linéaire reprit. Tout semblait si calme d'un coup, alors qu'il venait d'échapper à une mort atroce. Quelques instants plus tard, Georges l'aidait à se hisser sur le seuil où il s'écroula, essoufflé. Toute sa vie il avait connu le risque de la perdre. C'était le lot de ceux nés humains à Nidavellir.

Enfant déjà, il avait été cueilleur sur les flancs des Hautes Terres. Une occupation à risque, réservés aux esclaves les plus agiles. Maintenu par une corde, il remplissait de lourds sacs de mousse d'erh, qui servait de nourriture fade mais nourrissante à ses semblables. Lorsqu'un gisement d'ertelweiss était découvert, il y était aussitôt dépêché, et qu'importe les rochers tranchants sur lesquels il s'écorchait tant qu'il ramenait les précieuses fleurs de vie.

Étant parmi les meilleurs, il put rester plus longtemps que la moyenne cueilleur. Mais passés ses vingt ans, on l'arracha des flancs qu'il connaissait si bien et on le condamna à un sort pire que la mort. Les nains régulaient astucieusement la population d'humains dans leurs étroites cavernes, en faisant les adultes mineurs.

Au sol de la Vallée affleuraient des gisements grandement recherchés : des cristaux de nuit, principal catalyseur de la magie runique. Malgré leur abondance, ils étaient quasiment impossibles à obtenir, à cause de leurs gardiens. Des loups gigantesques, gros comme une petite île, chassant impitoyablement tout intrus sur leurs terres. Rares étaient les mineurs qui remontaient une première fois, infimes était la proportion de ceux qui remontaient une deuxième fois. Les cabines de bois étaient descendues par des chaînes jusqu'au plancher de la Vallée. Il fallait ensuite récupérer des cristaux, autant que possible, puis remonter avant d'être repéré par un loup. Car c'était alors la fin. Lorsque la tension se libérait, ou qu'une cabine remontait vide, on sortait un nouvel esclave de la réserve.

Ulkvar en était ce soir à sa treizième descente. Deux autres hommes vivants pouvaient se vanter du même exploit. Sa longévité était à la fois due à sa discrétion et à son instinct, qui là encore l'avait sauvé. Il entamait le deuxième quart de la descente quand il ressentit quelque chose. Une tension inhabituelle dans l'air. Il avait demandé à Georges d'arrêter la cabine, puis allumé sa lanterne pour tenter de percer le brouillard. Il avait réagi juste à temps quand il avait compris qu'un loup était en train d'escalader les hautes falaises des Hautes Terres.

« Qu'est c'que c'tait qu'ça ? maugréait le nain. »

S'il râlait, c'était signe que tout allait bien. Mais tout de même. Il n'avait jamais vu -ou entendu parler- de loups capables d'escalader les parois abruptes des Hautes Terres. Enfin. Il pourrait y penser plus tard. Il se releva en gémissant. Il avait hâte de se coucher à présent, terrifié mais ravi d'avoir échappé à une nouvelle virée en enfer. La nacelle ne serait pas réparée avant quelques jours, il avait donc du répit. Il accepta avec gratitude la pomme que lui tendait le nain et s'approcha de la fenêtre en la savourant au maximum. Georges s'éloigna pour examiner l'état des chaînes en grommelant que la lumière était trop forte pour ses pauvres yeux.

Ulkvar écarquilla les yeux et recracha son morceau de pomme. Dans le ciel, perçant à travers les nuages, un immense rocher nitescent leur fondait dessus. Gigantesque, une sphère presque parfaite avait surgi dans le ciel. Il pouvait jurer que le ciel était pourtant aussi sombre qu'à l'accoutumée lorsqu'il avait fait ses traditionnels adieux aux hauteurs.

« Georges ! Rentre vite ! Viens voir !»

Il était comme possédé. Le nain s'approcha de lui, interrogatif, avant de se figer dans une expression de stupeur qui lui était pourtant étrangère. Sa bouche s'ouvrait et se fermait sans émettre le moindre son. Il paraissait chercher ses mots, paniquer peut-être, mais il en était incapable. Les hurlements des loups résonnèrent. Il n'était pas rare d'en entendre quelques uns étouffés, repris par une meute. Il l'était plus qu'un de ces hurlements soit si proche qu'il fasse trembler les murs de bois. Les deux amis se regardèrent, désemparés.

Ils n'eurent jamais de réponse à la question muette qu'ils s'étaient mutuellement posés. La mâchoire qui broya l'habitat ne leur en laissa pas l'occasion.
