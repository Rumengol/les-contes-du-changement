---
bg: "orian-l-aventurier"
layout: post
title: Orian l'aventurier
summary: Une petit garçon naufragé stellaire décide d'explorer la planète.
source: “Meteor Vista - Robinson The Journey” par Richard Wright
ref: https://www.artstation.com/artwork/8lvnBm
date: 2021-05-27
categories: posts
tags: ["Les 5 ciels","Les aventures d'Orian"]
type: Conte
author: Rumengol
lang: fr
---
Orian se disait que ça aurait pu être pire. Il s’était écrasé sur une planète inconnue, dans la cime d’un arbre qui craquait dangereusement, mais au moins, il était en vie. C’était toujours ça de plus que les centaines de membres d’équipage de l’*Arpeggo*, dont la carcasse éventrée gisait de l’autre côté d’un gouffre sans fond. Et puis, il pourrait pleuvoir.

En prononçant ces mots, il s’était attendu à ce qu’une quelconque intervention maligne l’exauce. Ce qu’il n’avait pas prévu, c’était le type de pluie qui s’abattit sur la région. Le pont principal avait touché le sol en premier, mais le vaisseau s’était disloqué en entrant dans l’atmosphère, et tous les petits morceaux suivaient maintenant la course funeste de l’*Arpeggo*.

S’il devait se faire percuter par une plaque de métal chutant à plusieurs centaines de kilomètres-heures, Orian préférait que ce soit à l’intérieur de sa capsule de sauvetage. Il referma consciencieusement la porte et se recroquevilla dans le noir, attendant la fin de l’averse aux airs d’apocalypse. 

Le petit garçon ne pouvait s’empêcher de sursauter à chaque impact qui semblait secouer la planète entière. Il ne pouvait s’empêcher de se sentir un peu coupable vis-à-vis du paysage, défiguré pour des siècles au moins. Il jura de devenir un défenseur de la nature en signe de repentance, si jamais il s’en sortait.

Une série d’impact, beaucoup plus proche que les précédents, secoua l’arbre dans lequel il avait atterrit. Les bruissements et les craquements du bois tendaient tous ses muscles, le faisant craindre le pire. Il était certain d’entendre distinctement plusieurs arbres être projetés au bas d’une pente si longue que le son des troncs percutant le sol se perdit avant de toucher le fond.

Puis la paroi de sa capsule fut perforée par une plaque d’acier. Tranchée net, il s’en était fallu de quelques centimètres pour que la tête d’Orian partage le sort de sa capsule. Cette fois il ne put retenir ses larmes, et se replia encore plus sur lui même, espérant se réveiller de ce cauchemar.

Il ouvrit les yeux quelques heures plus tard pour constater que rien n’avait changé. Il était toujours dans cet accoutrement ridicule d’astronaute, en boule dans une capsule de survie éventrée. Il les referma, pour voir, mais en vain. 

Rassemblant ses forces, il sortit d’une boîte de métal des biscuits secs. Ils étaient apparemment très nourrissant. À condition d’avoir des dents de métal certainement. Dépité, il se rabattit sur les jus concentrés. Ils étaient supposés être consommés dans l’espace, comme dernier recours au cas où le générateur de pesanteur tombait en panne. Et comme tout repas de dernier secours, ils étaient ignobles.

Écœuré mais revigoré, Orian tâcha de s’extraire de la capsule, pour constater que la porte était à moitié défoncée. Elle ne laissait entrevoir qu’un faible rai de lumière, insuffisant pour laisser passer même un enfant, et refusait de bouger. Il restait la trappe. L’ouverture automatique était désactivée comme le reste de l’électronique, alors il s’en remit aux vieilles méthodes.

Il manquait incontestablement de force pour user des vieilles méthodes. Peut-être, les plus anciennes encore ? Il avait remarqué tout à l’heure un fusil à plasma dans l’une des armoires. La seule technologie un tant soit peu moderne ici. Il n’en avait jamais employé, mais ça ne devait pas être sorcier. Appuyer sur le bouton qui changeait la diode de rouge à vert, puis maintenir fermement l’arme et presser la gâchette.

Le recul fut imprévu. En crachant un jet de plasma brûlant, le fusil avait bondi des mains d’Orian pour s’élancer vers son torse. Pour la première fois, le garçon fut ravi de porter l’inconfortable combinaison, qui venait de lui sauver la vie. Elle était salement enfoncée au niveau du poitrail, mais lui n’avait rien. Et comble du bonheur, le plasma avait fondu une ouverture suffisamment large pour le laisser passer.

Le rescapé se hissa péniblement sur le toit de la capsule et observa plus attentivement la région. Au loin, les derniers morceaux du vaisseau terminaient leur course dans un large fleuve qui semblait traverser une immense vallée. Il se jetait ensuite dans une succession de cascade avant d’enfin s’abandonner dans le gouffre qu’il avait remarqué mais dont il était décidément incapable d’estimer la profondeur. Mieux valait ne pas tomber, c’était certain. 

Les montagnes bordant la vallée étaient toutes abruptes et fortement boisées. Selon toute logique, il était perché sur le flanc de l’une d’entre elles. Si seulement il avait pu capturer ce paysage, même avec l’épave de l’*Arpeggo* il restait envoûtant. Mais il devait laisser son enchantement de côté et penser pratique. 

En dehors de son vaisseau, il ne repérait aucune trace de civilisation. Et il n’avait pas la moindre idée de la planète sur laquelle il s’était échoué. Encore une chose à laquelle il aurait dû prêter plus d’attention. Comme la fois où l’équipage avait répété la procédure à suivre dans les situations comme celle qu’il vivait. Il était tellement persuadé que c’était impossible, à l’époque. s’il pouvait, Orian bafferait son ancien lui.

En se retournant, il avisa la plaque qui l’avait terrifié tout à l’heure. Il  croyait reconnaître une partie de la coque du vaisseau, qui avait sectionné l’arbre avant de s’encastrer dans la roche. Magiquement, la partie supérieur du tronc ne s’était pas affaissée, mais était restée en place, donnant presque l’impression que l’arbre avait poussé à travers l’acier.

Orian retourna dans la capsule, fouillant les rangements à la recherche de quelque chose ressemblant à une balise de détresse, de quoi envoyer un message qui pourrait être capté par une équipe de secours. Entre deux boîtes de biscuits immangeables &mdash; c’était une capsule de survie ou un entrepôt d’invendus ? &mdash; il dénicha un antique modèle. L’engin était supposé envoyer en permanence un S.O.S. en direction de la dernière antenne relai reconnue, pendant des décennies.

Au vu de l’âge de l’appareil, il ne devait pas avoir la carte stellaire à jour. En effet, les coordonnées qui apparurent sur l’écran ne dirent rien à Orian. D’un autre côté, il ne connaissait pas par cœur les emplacements de la myriade d’antennes qui parsemaient le ciel. Il l’activa et le posa sur une surface à peu près plane du toit. Maintenant, il n’avait qu’à attendre.

Le treizième jour, les concentrés épuisés, le garçon découvrit par accident que les biscuits ramollissaient jusqu’à être comestibles s’il versait de l’eau dessus. 

Le trente-troisième jour, le recycleur d’eau cracha de l’eau boueuse. Les pluies diluviennes de la veille avaient malmené la capsule et Orian avait failli se noyer. Heureusement, il avait eu le temps de protéger le matériel le plus sensible et la nourriture. La boue avait pris toute la nuit à s’évacuer par la porte entrouverte.

Le lendemain, le jeune homme faisait ses adieux à la capsule qui l’avait abrité pendant le mois qui venait de s’écouler. Il avait réuni la nourriture restante dans un sac avec l’émetteur du signal de détresse et harnaché le fusil à plasma dans son dos à l’aide d’une corde de fortune. L’arme était encore trop puissante pour lui, mais il ne pouvait s’aventurer dans l’inconnu désarmé.

Un mois, et les secours n’étaient toujours pas arrivés. Il ne pouvait plus se contenter d’attendre et de mourir lentement de faim et de soif. Il devait prendre les devants tant qu’il avait encore des provisions et survivre jusqu’à ce qu’on vienne le chercher. 

Son premier objectif était la carcasse à moitié noyée de l’*Arpeggo*. Avec un peu de chance, il y trouverait du matériel plus avancé. Une deuxième balise, peut-être. Elle était beaucoup plus éloignée qu’il ne l’avait cru au départ, mais il était résolu.

Orian l’explorateur venait de débuter son aventure, et la planète n’avait qu’à bien se tenir.