---
bg: "le-chevalier-creux"
layout: post
title: Le Chevalier Creux
summary: Le périple d'un chevalier à travers un monde déchu pour en libérer la lumière.
source: “Heaven’s Dead”, par John Sommo
ref: https://www.deviantart.com/tacosauceninja/art/Heaven-s-Dead-833300267
date: 2020-02-13
categories: posts
tags: ["Fanfictions"]
type: Conte
author: Rumengol
lang: fr
---

J'ai entendu d'innombrables récits sur le Gardien. Un être créé par un dieu mauvais afin de priver le monde de la lumière. Une coquille vide, dissimulée au cœur du monde, qui a jadis avalé toute la lumière du monde. Le dieu sombre a pu asseoir sa tyrannie sur notre terre devenue ténèbres. Il a rempli nos yeux, autrefois billes incandescentes, de néant. Les anciens se rappellent encore d'histoires contées par leurs aïeux décrivant des couleurs. Un mot qui n'a plus de sens dans notre royaume sacré.

Dans les premiers temps, paraît-il, le peuple avait acclamé le dieu. Ils l'avaient traité comme un roi, et lui avaient même érigé un palais de nacre. Un geste que je ne comprenais pas. La folie du changement, peut-être ? Ou bien la contrainte ? Heureusement, la lumière n'était pas vaincue. Petit à petit, à travers sa prison de chair, elle est parvenue à nous parler, à instiguer en nous un libre arbitre que nous croyions perdu. La puissance du roi pâlissait, il sentait sa démise approcher.

Alors il disparut avec son palais et ses quelques fidèles, fuyant le lieu de ses exactions. La lumière irradie chaque jour un peu plus. Les plus clairvoyants nous promettent des couleurs dans les temps à venir. Et au travers du néant percent dans les yeux de certains des bribes de monde. Je suis le premier à voir. Distinguer plutôt, des formes. Je me déplace avec plus d'aisance que les autres. Je suis prêt à accomplir la mission qui m'a été confiée.

Les créatures inférieures qui se terrent dans les tréfonds du Nid sont plus sensibles aux suggestions du dieu sombre. Mais je suis le détenteur de la lance céleste, la torche qui ranimera la lumière dans les couloirs de ce monde en stase ! Lorsque le heaume recouvre mon visage, je me sens enfin un. Les armes de la lumière qui me gardent sont pour le moment éteintes, mais elles me guident vers la source de tout. Les ténèbres tentent de m'engloutir à de nombreuses reprises mais je ne faillis pas.

Je m'aventure plus profondément qu'aucun être avant moi. L'ancienne carte ne m'est bientôt d'aucune utilité. Je tâtonne, me perds dans les endroits les plus terrifiants, peuplés de monstres et d'horreurs indicibles. J'aurais pu échouer de nombreuses fois si la lumière n'irradiait pas en moi. Mes yeux sont ouverts, éclatants de pureté. Ils éclairent ma voie. Ma peau se perce, par endroits, de petites excroissances de lumière. Elle est en moi, je suis son champion.

Les créatures m'attaquent sporadiquement. Je remarque que les orbites de certaines tentent de s'illuminer. Ma quête est déjà auréolée de succès si même les êtres les plus primitifs sont à nouveau capables de ressentir l'attrait de la lumière. Les cavernes résonnent à présent. Deux rythmes se chevauchent, la pulsation calme et apaisante de la lumière, et le souffle rauque du Gardien. Les galeries sont vides. Chaque pas me rapproche plus que jamais de l'accomplissement de ma quête.

Le temple. Enfin. L'entrée des prophéties. Une lueur éclatante tente de s'en échapper, couverte par une ombre massive. A l'intérieur est la bête, le Gardien. Mes yeux pourtant déjà baignés de clarté brûlent devant cette source qui irradie la caverne. C'est donc à ça que ressemblait le monde avant l'arrivée du dieu sombre ? C'est merveilleux.

Le Gardien porte bien son nom. Les bras en croix, la tête bestiale couronnée de bois majestueux, il est enchaîné par d'innombrables chaînes au mur de la grotte. Figé dans sa tâche éternelle. Il est entièrement sombre dans l'océan de lumière, au point de perturber ses contours. Si grand. J'entends directement les paroles de la lumière à présent. Tuer... Je dois abattre ce titan pour la libérer.Je m'avance bravement.

Il me remarque, ses yeux s'ouvrent. Des yeux incandescents. Je me fends d'un rire hystérique. Même telle abomination ne peut résister à l'attrait de la lumière. Ses forces sont sapées, le néant en lui peine à contenir la puissance d'un soleil. Je n'ai qu'à détruire ce vaisseau imparfait. La lance céleste s'embrase soudain, renouant avec sa nature profonde d'arme lumineuse.

Le Gardien émet un grognement de défi. Il n'a pas encore renoncé à sa mission. Moi non plus. Mû par une force inconnue il y a peu, je projette la lance de toutes mes forces. Elle vole dans l'espace de la caverne avant de se perdre dans le torse gargantuesque. L'éclat disparaît, absorbé par des bras ténébreux. La bête souffle. De mépris ou de dépit ? Je maudis ma sottise et m'empare de mon glaive. Je ne l'ai jamais sorti, il fait peine à voir. Mais qu'importe, la foi est mon arme. Un halo lumineux entoure la lame et régénère mes forces.

Je m'apprête à m'élancer lorsque le monstre pousse un rugissement qui me plaque au sol. Les chaînes tremblent, tout comme les murs de la caverne. D'immenses blocs de pierre se détachent du plafond par endroits, dispersant des nuages de poussière. Ouvrant péniblement les yeux, je comprends. Une étincelle perce le corps d'obscurité du Gardien. La lance a atteint son but.

Le titan se contorsionne, tente de se libérer des chaînes pour retirer l'arme de la plaie béante qui grandit chaque instant. La lumière chasse peu à peu les ténèbres, qui luttent, désespérément affaiblis. Chaque spasme du colosse fragilise un peu plus la roche. Certains crochets se décrochent déjà de la paroi. L'infection lumineuse le gangrène, gagnant du terrain inexorablement, jusqu'à l'abandon des ténèbres. Le Gardien pousse un ultime hurlement. De rage, de souffrance, de soulagement. Puis il s'effondre. Sa carcasse sans vie est maintenue, branlante, par les maillons qui ont tenu.

La lumière éclate, irradie à présent. Libérée de tout obstacle, elle explose littéralement. Je me retrouve une nouvelle fois soufflé au sol. Soudain, toute forme disparaît. Je me retrouve baigné dans une mer jaune pâle. La chaleur est réconfortante. Comme dans le ventre d'une mère. Je m'y sens bien. J'aimerais y rester à jamais. Tout est uniforme à perte de vue, si ce n'est ces ondulations immatérielles que je semble saisir. Au loin, au delà de ce que mes sens peuvent percevoir, je distingue une silhouette. Ce monde émane d'elle. Une figure divine, maternelle. Elle est la lumière. Elle me chérit. Je m'abandonne à elle pleinement.
