---
bg: "il-vous-reste-de-l-energie"
layout: post
title: Il vous reste de l'énergie ?
summary: Une équipe d'archéologues Newak s'aventure dans un temple ancien jamais exploré.
source: “Mechanical Horror” par Diana Franco
ref: https://www.artstation.com/artwork/03rwVK
date: 2022-05-20
categories: posts
tags: ["Et factum est machina"]
type: Conte
author: Rumengol
lang: fr
---
— C'est bon, poussez encore un peu... Attention aux chutes de pierres ! Un peu plus de nerf sur la gauche ! Reculez ! Le mécanisme est enclenché, c'est bon. 

La porte du temple coulissa dans un grincement rocailleux. Elles étaient dures à enclencher, mais une fois que l'ouverture était suffisamment importante, un mécanisme prenait le relai et finissait le travail. C'était le cas pour toutes les anciennes portes. Sauf celles dont le mécanisme était grippé, à jamais scellées. Alira était surexcitée. Elle étudiait l'architecture des grands anciens depuis des années, mais la plupart des bâtiments d'intérêt avaient déjà été explorés ou étaient bien trop dangereux. 

La découverte de ce temple dans la forêt de fer était une aubaine. Il était immense, au point que sa tribu l'avait longtemps confondu avec une colline. Le fait qu'il soit en partie enfoncé dans la terre et recouvert d'arbres n'avait pas aidé à sa découverte. C'était par hasard qu'un chasseur avait mis au jour sa véritable nature, en traquant un renard jusque dans son terrier. Il s'était empressé de rapporter sa trouvaille aux shamans, qui ont confié à Alira la tâche d'en révéler les secrets. 

C'était l'opportunité que la jeune Newak avait attendue toute sa vie. Elle avait avec elle une vingtaine d'hommes forts pour ouvrir l'entrée du temple et porter les trésors qui se cachaient à l'intérieur, ainsi qu'un couple de guerriers, au cas où le bâtiment recèlerait du danger. C'était très improbable, le territoire Newak étant largement épargné par les démons, mais cela ne coûtait rien d'être prudent. Dana, son amie de toujours, avait souhaité l'accompagner, ou plutôt la dissuader. Elle aussi fascinée par la civilisation des grands anciens, elle était le penchant froussard d'Alira. Là où cette dernière était toujours en quête de nouvelles sensations, Dana était terrifiée par sa connaissance de l'antique culture. 

Au grand désespoir de son amie, la jeune exploratrice savait se montrer bornée quand elle le voulait. Elle avait dirigé à la baguette les gaillards qui composaient sa troupe, et maintenant que la porte était ouverte, elle fut la première à pénétrer le couloir sombre, une torche à la main. L'intérieur était en tout point identique à celui des autres vestiges qu'elle avait visités par le passé. Le sol était fait d'un matériau blanc dont la composition échappait au plus éminent shaman, et les murs étaient encore plus étranges, composés d'un mélange de matériau friable et de fines tiges de métal, il était improbable que de telles structures puissent supporter un bâtiment, encore moins face à l'épreuve du temps. Et pourtant elle était dans une telle construction, qui était restée debout des centaines, peut-être des milliers d'années. 

Des pierres lumineuses étaient placées dans les angles à intervalles réguliers, et balisaient un chemin qui s'enfonçait hors du rayon de sa torche. Il n'y avait aucune trace des grands halls caractéristiques des entrées principales, alors Alira estima qu'ils devaient être entrés par une issue réservée aux serviteurs. C'était d'autant plus excitant que ces accès étaient en général reliés aux chambres les plus privées. Dana tenta de lui faire changer d'avis une dernière fois, en vain. Résolue, elle suivit son amie, précédée par les guerriers qui avaient pris lance et bouclier en main. Nat et Syl étaient parmi les meilleurs de la tribu, et elle devait avouer qu'elle se sentait plus en sécurité sous leur protection. 

— Faites attention aux pièges, et ne vous éloignez pas des chemins tracés par les pierres vertes. Si vous en voyez des brillant en rouge, prévenez-moi mais ne vous aventurez pas seuls. 

Alira donna ses consignes avant de répartir les hommes par groupes de deux ou trois. La plupart des portes de ce couloir s'ouvraient sur des officines de petite taille. Le matériel qui y était entreposé était bien souvent hors d'état, mais ils pouvaient mettre la main sur un trésor avec un peu de chance. Une fois que chacun fut occupé à sa tâche, elle s'enfonça dans le couloir, les explorateurs restants à sa suite. Arrivée à un embranchement, elle envoya deux hommes suivre et cartographier le chemin vert, tandis qu'elle se dirigea vers le bleu. Elle n'avait jamais vu de couloir de pierres bleues jusque-là, mais c'étaient toujours ceux qui menaient aux plus grandes découvertes. 

— Alira, fit Dana en s'approchant de son amie, les pierres de lumière au plafond ne sont pas supposées s'allumer lorsqu'on est suffisamment enfoncés dans les ruines ? 

— Si, tu as raison. J'imagine que dans un édifice aussi ancien, la puissance qui servait à les alimenter a pu s'épuiser. 

— Non, c'est peu probable. Tu as vu comme moi les inscriptions sur les murs et le style des autels, ce temple est l'un des plus récents jamais découverts. Fin de l'âge du métal, milieu au plus tôt. Ce ne serait même pas impossible qu'il ait été érigé quelques années à peine avant leur disparition ! Quelque chose cloche, partons d'ici avant de déclencher un piège ou d'attirer la colère des anciens. 

— Tu peux t'en aller si ça te chante. C'est l'occasion de ma vie, et je ne vais pas la gâcher pour une simple superstitieuse ! 

Elle s'arrêta, réalisant le ton blessant qu'elle venait d'employer. 

— Pardon, Dana, je ne voulais pas dire ça. 

— C'est pas grave. Tu as raison, c'est juste une bête superstition. Je veux rester avec toi. 

Nat, parti en éclaireur un peu plus tôt, revint sans encombre. Il était tombé sur une porte invisible au bout du couloir. Ces portes étaient toutes identiques, larges et sans la moindre couleur. Il était facile d'en percuter une sans jamais la voir, et c'était la cause principale des nez cassés chez les explorateurs. Elles venaient toutes avec un boîtier, situé soit sur le côté soit au-dessus, qui devait commander son ouverture. Mais la forme de la clef les satisfaisant ou la phrase qui activait ses boîtes n'avaient jamais été découvertes. Heureusement, ces portes étaient très fragiles et un simple coup de lance suffisait à les faire voler en éclats, projetant des fragments tranchants et invisibles tout autour. Plus d'un inconscient s'était lacéré les pieds en tentant de passer à travers une porte détruite. Ces fragments tranchaient la peau aussi sûrement que des lames, mais se brisaient immédiatement après, au grand dam des maîtres de chasse. 

Sur les ordres d'Alira, Nat brisa la porte qu'ils traversèrent avec prudence. De l'autre côté, une cage d'escalier. Ils pouvaient soit monter soit descendre à partir de là, et Alira hésita. Elle décida finalement de descendre. Ils étaient entrés par un étage supérieur, et le rez-de-chaussée contenait la plupart du temps nombre d'informations essentielles sur la nature du temple. Les marches s'achevèrent un étage plus bas, après ce qui semblait être une éternité. La petite équipée était descendue bien plus profondément qu'elle ne l'avait prévu, mais les torches pouvaient encore brûler longtemps avant que le besoin de remonter ne se fasse sentir. 

Pleine d'appréhension, Alira pénétra dans le grand espace qui avait dû être le hall d'entrée. Il était réellement immense, le plafond se trouvait hors du rayon de la torche. Là où les couloirs et une multitude de bureaux donnaient l'impression que l'étage par lequel ils étaient entrés était petit, la salle aux dimensions inconcevables prouvait le contraire. Elle devait s'étendre sur toute la surface du bâtiment, comme un terrain de jeu gigantesque. Ici, le sol était fait de plaques de métal, confirmant l'intuition de Dana et l'opulence de l'endroit. 

Incapables de se retenir, Alira s'élança à la découverte de ce hall gigantesque. Elle s'arrêta net en arrivant devant une cage faite du même matériel que les portes, dans laquelle était enfermé un démon. Il semblait être endormi et recroquevillé sur lui-même, mais sa vision suffit à faire pousser un cri d'horreur à l'exploratrice. Syl s'approcha d'elle et toucha la barrière invisible de la pointe de sa lance, puis posa sa main dessus. 

— Aucun risque, ne t'inquiète pas. On dirait le même matériau que celui des portes, mais celui-ci est bien plus résistant. Je m'y suis déjà confrontée, et même une dizaine de guerriers n'a réussi qu'à l’égratigner malgré nos coups répétés. Je doute que même les démons puissent sortir de cette cage. Et puis, celui-ci à l'air mort. 

— Ali... 

La voix tremblotante de Dana fit se retourner ses trois compagnons. Elle s'était avancée un peu plus loin et sa torche illuminait le sol autour d'elle, jonché de squelettes humains. Les ossements, blancs comme la neige, semblaient très anciens, et il y en avait beaucoup trop pour que ce soient de simples pilleurs de tombe. Nat traversa la pièce en courant, révélant que le sol entier était recouvert de carcasses identiques. 

— Ce n'est pas un temple. C'est une tombe. 

Ces mots restèrent suspendus dans l'air pendant quelques instants, le temps que chacun prenne pleinement conscience du lieu où ils se trouvaient. 

— On annule l'expédition, décréta Alira. On embarque ce que les autres ont trouvé, et on scelle le bâtiment à nouveau. 

Soudainement pressés de quitter l'endroit, ils se hâtèrent de retourner dans les escaliers. Mais comme ils posaient les pieds sur la première volée de marches, un grincement strident suivi du bruit sourd d'un objet lourd tombant au sol résonna dans le hall et les escaliers, les figeant sur place. Ils échangèrent un regard, incertains de la marche à suivre. Les guerriers raffermirent la prise sur leurs lances tout en jetant des regards angoissés à l'ouverture plongée dans les ténèbres. L'écho du choc continuait de se réverbérer, mais en dehors de ça le silence régnait, pesant. C'est alors que la figure cauchemardesque pénétra dans la cage d'escalier. 

— **Il vous reste de l'énergie ??** 

La voix grave et éraillée était à peine audible, mais suffisante pour réveiller les instincts de conservation les plus primaires des quatre Newaks. Ils se mirent à escalader les marches quatre à quatre, priant pour que la créature démoniaque ne les rattrape pas. Mais la silhouette difforme se déplaçait à moitié sur les murs, usant de ses trop nombreux membres, et elle attrapa Nat qui disparut dans les ténèbres en hurlant. Syl se retourna pour aider son amour et se fit aussitôt empaler par une lame qui surgit de l'obscurité. 

Les deux exploratrices survivantes profitèrent du temps que le sacrifice des guerriers leur avait offert, et elles arrivèrent à l'étage les joues noyées de larmes de terreur. Aussitôt dans le couloir, elles reprirent le tracé des pierres bleues en sens inverse, le démon toujours sur leurs talons. Il semblait avoir ralenti, le couloir s'avérant trop étroit pour sa carrure imposante. Alira criait tout en courant. 

— Tout le monde, partez ! Il y a un démon ici, retournez tous au village ! 

Aucune réponse ne lui parvenait, mais elle continua de s'époumoner jusqu'à revenir au croisement. Les pierres qui brillaient d'une lueur émeraude avaient tourné au rouge. La cheffe de l'expédition eut un pressentiment avant même d'arriver là où elle avait laissé ses hommes. Les éclaboussures de sang jaillissaient de chaque officine. La bête avait été méthodique, éliminant un à un les hommes sans leur laisser le temps de réagir. Certains toutefois avaient tenté de résister, voire de s'enfuir, tous en vain. 

Elle entendit Dana réprimer un haut-le-cœur, et la prit par la main. Elles devaient s'échapper de ce tombeau et rallier leur tribu. Les guerriers sauront vaincre le démon, il fallait simplement qu'ils soient prévenus. Derrière, il commençait déjà à les rattraper, et elles purent entendre un hurlement rauque alors qu'elles s'extirpaient de l'entrée et s'enfonçaient dans la forêt. 

&nbsp; 

Depuis combien de temps était-elle en train de courir ? Elle avait perdu Dana de vue. Elle espérait que son amie soit en sécurité. Mais elle ne pouvait pas perdre de temps. Elle serait bientôt descendue de la montagne. Mais elle était poursuivie, elle entendait le démon gagner du terrain. Les arbres le ralentissaient encore, mais ce n'était plus qu'une question de temps. Elle sentait déjà son souffle rance empester et ses griffes métalliques enfoncer le sol juste derrière elle.  

Comment était-elle tombée ? Elle sentait l'herbe froide lui caresser la joue. Sa tête la lançait, elle avait dû se heurter contre un arbre. Le temps qu'elle réalise ce qui venait de lui arriver, le démon était sur elle. Sa carcasse de métal s'arrêta à sa hauteur, toute à sa difformité. L'apparence du démon n'avait rien d'humain ou d'animal, c'était un pur être de malveillance donc l'unique œil rouge brillait de haine. Une vapeur d'eau s'échappait à l'endroit où aurait pu se trouver la bouche. Et à nouveau, cette voix qui fit vibrer le corps de l'exploratrice au moment où la créature se penchait sur elle. 

— **Il vous reste de l'énergie ??** 

 