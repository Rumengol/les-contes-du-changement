---
bg: "la-livraison"
layout: post
title: La livraison
summary: La petite Naoya se voit confier la livraison d'un étrange sachet dans les niveaux inférieurs du Silo.
source: “Mine” par David Arno Schwaiger
ref: https://www.artstation.com/artwork/zPqx3w
date: 2023-11-25
categories: posts
tags: ["Post-Terre"]
type: Conte
author: Rumengol
---
Naoya se tenait sur le bord du Silo et contemplait rêveusement la vue qui s'offrait à elle au travers de la visière de son masque à gaz. Si elle ignorait les gigantesques tuyaux de métal qui s'enfonçaient jusqu'au profondeurs les plus insondables, tout était merveilleux. Partout à la surface des murs de l'immense puits, des carrés de lumière formaient une guirlande qu'elle ne se lassait pas d'admirer. Malgré la pénombre permanente, la ville d'en-bas ne connaissait pas la nuit tandis qu'ici seules les voyants des usines servaient d'étoiles dans le noir  opaque.

On lui avait raconté, bien sûr, à quoi ressemblait la ville qui se cachait derrière cette façade lumineuse. Des rues souterraines, éclairées par des néons de toutes les couleurs, des publicités, des vendeurs et une décadence bienheureuse. Les gens qui vivaient dans le Silo hermétiquement clos ne connaissaient qu'une vie de plaisir, alimentés par ceux d'en haut. Et plus on descendait bas, dans cette tour creuse organisée en des centaines d'étages, plus l'architecture était fastueuse, diverse et riche.

Et mieux que tout, aucun d'entre eux ne portait de masque à gaz, et pourtant ils pouvaient respirer à pleins poumons l'air recyclé et traité du souterrain. Ne serait-ce que pour ça, la jeune fille rêvait de pouvoir un jour habiter dans le Silo, symbole de réussite sociale à la surface. Mais les places étaient chères, et le souterrain avait besoin des ouvriers qui risquaient leur vie quotidiennement pour fonctionner. Malgré les excavations continues et les projets de forages qui se multipliaient, la place manquait cruellement en raison de la mer avoisinante et de la composition de la roche, difficile à travailler. Toutes les industries avaient été déplacées à la surface, avec les milliers de personnes nécessaires pour leur bon fonctionnement.

Un sifflet lui vrilla les tympans. La pause était terminée, elle devait se remettre au travail. C'était un travail épuisant, bien trop pour son frêle corps adolescent, mais chacun devait faire sa part. Elle se releva et retourna dans l'ombre des gigantesques ruines des temps jadis. Elle retira son filtre un instant afin d'estimer combien de temps il lui restait. Pas suffisamment pour finir la journée, et elle avait dépassé l'heure de la pause. Almadeus allait lui tirer les oreilles à son retour.

Elle se mit à courir à travers les ruelles du complexe industrielle, cheminant avec confiance dans ce dédale de bâtiments, d’échafaudages et de véhicules. Ils dataient pour la plupart de bien avant la construction du Silo, quand il s'agissait encore d'un plan d'extraction de métaux précieux. La rouille dévorait lentement ces reliques d'une époque où les préoccupations étaient bien différentes. De temps en temps, les plus anciens contemplaient ces structures s'élevant à des centaines de mètres de hauteur avec un air mélancolique. Naoya avait toujours échoué à saisir l'attrait de construire des choses aussi hautes, dont le sommet était noyé dans la chape de nuages toxiques qui planait au dessus d'eau.

Et cette fois, elle pouvaient encore moins se permettre d'admirer le décor dans lequel elle avait grandit et donc elle connaissait le moindre recoin. Elle atteint l'échoppe du vieux Tirésias en un temps record, toute essoufflée. Apparemment, il ne s'appelait pas réellement Tirésias, c'était un nom qu'il s'était donné après qu'une fuite d'azote en plein visage l'ait aveuglé. Une référence que les plus anciens reconnaissaient parfois, mais qu'ils ne prenaient jamais la peine d'expliquer. Une histoire sans intérêts, ils disaient.

Depuis son accident, Tirésias était devenu l'intendant du district 16. Il avait perdu le sens de la vue, mais il l'avait remplacé par une étonnante faculté à connaître son magasin par cœur. Il n'en sortait jamais, car lorsqu'il s'y trouvait c'était comme s'il pouvait encore voir, et plus encore. Demandez-lui où il avait rangé quelques chose des mois auparavant et il ressortait l'objet de la requête en moins de deux minutes. Il était donc chargé de tenir l'inventaire des possessions du district, ainsi que de cataloguer les besoins et pénuries pour les transmettre au chef de section Alaster.

« Salut Tirésias, lança la jeune fille guillerette. Mon filtre gauche est mort, je peux en avoir un autre ?

— Bonjour Naoya. Je suppose que tu comptes me payer celui-là aussi ?

— Envoie la facture à Almadeus, à plus !»

Elle s'empara du filtre qu'il venait de sortir de sa caisse et fuit la scène devant un vieil homme fatigué de la fougue de la jeunesse. Tirésias était gentil, mais trop procédurier, et souhaitait prendre son temps avant d'accéder à la moindre demande. Elle n'avait pas le temps de lui faire la conversation pendant des heures, pas tout de suite. Sans ralentir elle dévissa son filtre avant de le jeter et de le remplacer par le neuf. D'ici un peu plus de deux heures, lorsque le droit serait trop encrassé pour lui transmettre de l'air purifié, ce nouveau prendrait le relai.

À l'approche de l'usine, le terrain se complexifia. Là où pour rejoindre l'intendance elle pouvait se permettre de courir en ligne droite en contournant l'occasionnel obstacle, son lieu de travail se situait à l'intérieur d'un gigantesque complexe industriel à moitié démoli. Elle devait sauter par dessus des tuyaux de cuivre, certains rouillés et glacés et d'autres dont le simple toucher pouvait faire fondre sa peau; escalader des tas de gravats parfois plus hauts qu'elle; glisser au travers de trappes pensées pour être traversées par des chariots de mine bien plus petits.

Quand elle arriva enfin à l'usine de traitement des filtres usagés. Rien à voir avec ceux qui l'empêchaient d'étouffer dans l'atmosphère toxique, ceux-là étaient d'immenses mécanismes destinés au traitement de l'eau. L'océan qui recouvrait une grande partie de la planète atteignait les prémices de la ville industrielle, et de gigantesques turbines étaient utilisés pour purifier l'eau à destination des niveaux inférieurs, ainsi que de la séparation du sel et des autres minéraux utiles qu'elle contenait, comme l'or ou le plomb.

Esteban fut le premier à remarquer son arrivée tardive. Alors qu'elle tentait de rejoindre sa ligne de travail comme si de rien n'était, il lui fit signe de s'approcher. Le garçon avait presque le même âge qu'elle et habitait l'appartement voisin. Ils étaient meilleurs amis depuis l'enfance, lorsque bébés ils se servaient d'un trou creusé dans le mur séparant les deux habitations pour se rejoindre quand la surveillance de leurs parents se relâchait. 

Il connaissait tout de ses rêves de rejoindre l'intérieur du Silo. Il faut dire que ce n'était pas un secret, tant de monde aspirait à ça à la surface. Cependant, lui était moins enthousiaste à l'idée. Sa mère était malade et son père était encore plus épuisé par son travail  que le fils ne l'était. Il rechignait à l'idée de les abandonner pour vivre sa vie égoïstement. Naoya comprenait, bien qu'elle n'ait pas ce genre d'états d'âmes. Ses deux parents étaient morts dans un accident dramatique quatre ans auparavant, et elle vivait seule depuis, avec comme seul soutien Esteban.

« Almadeus te cherche partout ! Je sais pas ce que t'as fait, mais il avait l'air furax, tu devrais aller le voir fissa. »

L'adolescente déglutit difficilement. Almadeus était le genre de chef à superviser son équipe de loin, sans s'impliquer s'il n'y avait pas besoin de lui. Et il ne convoquait que ses lieutenants, ou les ouvriers qui avaient gravement fauté. Ceux-là ressortaient de son bureau la visière embuée par leurs larmes et se retrouvaient expulsés de l'usine, ainsi que de la plupart des autres industries. On retrouvait leurs corps quelques jours plus tard, suicidés plus ou moins volontairement. Il lui arrivait de simplement discuter avec l'un de ses employés, mais c'était uniquement lorsqu'il était bien luné. Et au vu de l'avertissement d'Esteban, ce n'était pas un de ces jours.

« S'il te balance au fond du Silo je récupère ta chambre ! cria celui-ci dans son dos. »

Elle lui répondit avec un doigt d'honneur. Prenant son courage à deux mains, elle frappa trois coups timides à la porte du bureau de l'usine dont les rideaux étaient comme toujours baissés. Elle s'ouvrit aussitôt, et elle se retrouva nez à nez avec un Almadeus furibond. C'était un homme fort, marqué par les années. Sa fameuse moustache n'était pas visible sous son masque, mais son crâne dégarni et les quelques cheveux grisonnants qu'il conservait témoignaient de son âge. Malgré cela, il mesurait près de deux mètres de muscles, et portait tout le temps un marcel et un pantalon de camouflage. Il exhibait ses bras nus comme pour appuyer son autorité et montrer que l'air nauséabond n'avait pas d'impact sur sa musculature de compétition. Peu d'hommes dans le district 16 étaient capables de rivaliser avec lui en terme de physique, et même ceux qui le pouvaient évitaient de le mettre en colère.

Naoya ne donnait pas cher de sa peau face à ce visage pourpre sur le point d'exploser.

« TOI ! hurla-t-il. OÙ ÉTAIS-TU PASSÉE ?

— Désolé, répondit-elle toute penaude, et encore haletante. J'étais chez Tirésias pour recharger mon filtre, mais il ne trouvait plus les filtres et...

— Tu me prends pour un idiot en plus ? Est-ce que j'ai l'air assez stupide pour croire que Tirésias peine à trouver quelque chose ? Un filtre par dessus le marché ? Hein ?!

— N-non Almadeus.

— Alors réponds moi honnêtement Naoya, t'étais en train de rêvasser sur le bord du Silo ?»

Elle hocha la tête, résistant à l'envie de pleurer. Des larmes perlaient sur le bord de ses yeux mais elle se força à les retenir. L'expression de son chef se radoucit un peu.

« Il parait que ton rêve c'est d'aller tout en bas, avec les autres richoux dans leurs palais d'ivoire où on mange de la vraie viande à chaque repas ?

— O-oui, Almadeus.

— Eh bien ma petite, ça aurait pu être ton jour de chance. J'ai besoin d'envoyer quelqu'un en bas avec la prochaine plateforme, et tous mes courriers m'ont fait faux bond. J'ai pensé à toi, mais j'ai besoin de quelqu'un de confiance...

— Pitié Almadeus ! à ces mots, la jeune fille avait sursauté et s'était jetée à ses pieds. Je suis fiable, je te jure ! Je ferais tout ce que tu voudras !

— Tout, vraiment ?

— Tout !

— Alors je n'ai besoin que d'une chose, que tu livres ce paquet à la bonne adresse. »

Il sortit de sa poche un sachet opaque de la taille d'une enveloppe, qu'il déposa dans la main de Naoya. Le contenu était granuleux, comme si il venait de lui confier un sachet de gravier. Elle observa la poche pendant un moment en haussant les sourcils.

« Qu'est-ce que c-

— Plusieurs règles, si tu veux que ça se reproduise : tu ne demandes pas ce qu'il y a dans le sachet, tu ne l'ouvres pas, et tu n'essaies même pas de deviner. Quand tu seras en bas, tu ne parles à personne, tu ne touches à rien, et tu te fais aussi invisible que possible. Tu vois qui est Sid ? »

Elle hocha la tête. Sid faisait partie des rares personnes à pouvoir pénétrer librement dans le Silo et en ressortir. Il se rendait fréquemment aux district 16 et y restait quelques jours avant d'effectuer ce qu'il appelait une «tournée des districts». En réalité, il était un inspecteur des niveaux inférieurs qui venait évaluer la production et identifier toute défaillance dans le travail des ouvriers de la surface. Mais contrairement aux autres qui se montraient froids et distants, il était affable et apportait même parfois des friandises aux enfants. Sa venue était une attraction incontournable, et il se repérait de loin avec son fameux tricorne et son long manteau de cuir. Naoya avait même pu discuter avec lui pendant longtemps de la vie dans le Silo, elle voyait parfaitement à quoi ressemblait son visage.

« Bien. C'est lui le destinataire, à l'étage 18. Quand tu sors, continues tout droit sans dévier et tu finiras par lui tomber dessus. Tu ne parles qu'à lui, tu ne montres le paquet qu'à lui, tu prends ce qu'il te donne en retour et tu remontes avec la prochaine plateforme. C'est capiche ? Si tu fais correctement ton travail, je te donne un petit bonus et il se pourrait même que je t'appelle les prochaines fois que j'ai une course du genre. »

Naoya hocha vivement la tête, rayonnante. Elle répéta mot pour mot ses instructions tout en rangeant le sachet dans la poche intérieure de sa veste, où il disparut dans les épaisseurs de tissu. Almadeus parut satisfait et la congédia.

« Vas-y maintenant, la plateforme descend dans une heure, ça te laisse tout juste le temps d'y aller. Prends ça, ça t'autorise à descendre et remonter à titre exceptionnel, ne la perd surtout pas. Et essaie de pas trop sourire en sortant, j'ai quand même une réputation à préserver. »

Il retourna s'asseoir à son bureau couvert de feuilles de comptes noircies, avant de faire mine de se rappeler quelque chose.

« Ah, et au cas où t'envisagerais de ne pas revenir. Ne joue pas à la plus maligne, surtout avec moi.  Tu reviens avec la prochaine plateforme. »

Sa menace eut l'effet escompté, et c'est la mine sombre que Naoya ressortit du bureau. Elle ne pouvait pas nier que l'idée ne lui avait pas traversé l'esprit, mais elle s'était quand même sentie blessée par le manque de confiance d'Almadeus. Et surtout, elle savait qu'il avait les capacités de mettre ses menaces à exécution. Elle quitta en vitesse l'usine, sans même passer voir Esteban. Le garçon avait les moyens d'extirper tous ses secrets, mais elle avait la sensation que l'instruction d'Almadeus s'appliquait aussi bien aux gens des niveaux intérieurs que ceux de la surface.

Les opérateurs de la plateforme entassaient les dernières caisses quand elle se présenta à eux. En reconnaissant le laisser-passer d'Almadeus, ils la laissèrent monter à bord. La plateforme descendait lentement, s'arrêtant à chaque étage. Ils étaient bien moins hauts que ce à quoi la jeune fille s'attendait, faisant à peine trois mètres de haut. C'était bien moins que ce à quoi elle s'attendait, et même l'étage 18 était assez proche de la surface, au final. Elle pouvait encore distinguer le pourtour de l'anneau et les nuages sombres qui se déplaçaient dans le ciel.

A chaque étage, une lourde porte en métal lui bloquait la vue vers l'intérieur. Des ouvriers avec le même teint blafard qu'elle sortaient d'un sas pour récupérer ce qui leur était destiné, puis retournaient à l'intérieur lourdement chargés. Ils portaient des masques à gaz similaires au sien, si ec n'est qu'ils étaient plus propres, presque polis. La porte se refermait ensuite, et le sas se remplissait d'une fumée blanche. Elle ne voyait jamais la suite, car c'est à ce moment-là que la plateforme reprenait sa descente.

Enfin, son étage arriva. Les travailleurs se mirent en branle pour embarquer les caisses dans le sas, et cette fois elle les accompagna. Personne ne prêtait attention à elle, sa seule difficulté étant de prendre garde à ne pas se faire écraser par les boîtes de métal posées à la hâte. Ce déchargement lui paru durer bien plus longtemps que les précédents, mais les quatre homme de la femme rentrèrent finalement dans le sas. Naoya se retrouva assaillie par les jets de fumée qui emplirent la pièce. A peine la fumée était dissipée que ses voisins retirèrent leur masque. Un peu intimidée par cette perspective, elle finit néanmoins par les imiter et respira, d'abord par petite bouffées, puis par grandes inspirations l'air pur qui lui parvenait.

Elle n'avait jamais connu un air aussi bon ! Il était léger, comme s'il ne pesait rien, et clair. Pour la première fois, elle voyait le monde net par ses propres yeux et non au travers d'une visière parsemée de particules de crasse et de suie. Elle fut prise d'une violente quinte de toux. Ses poumons, habitués à respirer un air toxique pauvrement filtré avaient du mal à réduire leur activité et sécrétaient plus de mucus que nécessaire. La porte se rouvrit alors que Naoya tentait de reprendre son souffle.

La jeune fille se retrouva en plein milieu d'une rue souterraine éclairée de chaque côté par des néons violets, roses, jaunes et bleus. Les habitations comme les magasins étaient creusées à même la roche, soutenue par de solides piliers de métal. C'était exactement comme elle l'imaginait. La lumière artificielle baignait le couloir de lueurs arc-en-ciel qui se reflétait dans une brume à l'odeur sucrée. Une musique aux échos électronique résonnait dans les couloirs, alternant les compositions classiques avec d'autres titres plus excentriques. Pour la jeune fille, tous étaient d'un délicieux exotisme.

Les passants n'avaient rien à voir avec les figures ternes qu'elle côtoyait au dessus. Ils était resplendissant, avec des cheveux aux coupes variées et aux teintures chatoyantes. Leurs vêtements laissaient apparaître bien plus de peau qu'il n'était sain de le faire pour elle. Elle se sentit soudain ridicule, engoncée dans ses couches protectrices. Ici, personne n'avait besoin de se protéger des toxines omniprésentes dans l'atmosphère, ou du froid mordant qui ne cessait jamais.

Il faisait même chaud. Des radiateurs devaient être dissimulés quelque part où elle ne pouvait pas les voir. Elle avait entendu parler de tuyaux géothermiques qui connectaient tout le souterrain au centre de la Terre, mais elle n'avait aucune idée de comment cela marchait en réalité. Une autre chose, tout le monde semblait bien nourri, en bonne santé et confiants. Une chose ne changeait cependant pas, personne ne lui prêtait la moindre attention, malgré son teint maladif et ses habits qui indiquaient sans équivoque son origine. Elle s'était attendue à devoir brandir son passe à tous les coins de rue pour qu'on la laisse circuler, personne ne lui accordait plus d'un regard.

Bien qu'elle ait eu l'interdiction de toucher quoi que ce soit, elle dévorait des yeux son environnement. Un magasin tenu par une femme à l'allure peu commode vendait directement aux passants des brochettes de boulettes que Naoya ne reconnaissait pas. Peut-être que c'était de la viande ? Elle saliva à l'idée. Plus loin, trois jeunes garçons un peu plus vieux qu'elle, bardés de chaînes, piercings et lunettes de soleil, étaient assis devant une porte où il était inscrit «tatouage». Elle ignorait ce que c'était ni même si elle avait lu le mot correctement, mais ça ne semblait pas avoir un rapport avec la nourriture. Peut-être un loisir réservé aux habitants du Silo ? Elle avait tellement à découvrir !

Malheureusement, elle n'avait pas le temps de languir. Cette plateforme ne descendait que jusqu'au 45ème étage, ce qui lui laissait moins de trois heures, et elle ne savait pas combien de temps cela lui prendrait de retrouver Sid. Elle aurait toujours le temps de papillonner une fois sa livraison accomplie. Elle marchait donc sans ralentir, mais sans pour autant rater une miette du spectacle dont elle avait tant rêvé.

Un bruit de chute attira son attention, dans une ruelle perpendiculaire. Poussée par la curiosité, elle se rapprocha du mur et jeta un œil. Dans l'ombre des bâtiments, une dizaine de silhouettes se battaient. Naoya avait déjà assisté à des tournois de lutte, notamment quand Almadeus était la star du spectacle, mais ça ne ressemblait pas à ça. C'était toujours des duels où les adversaires se jaugeaient avant de s'échanger quelques coups bien calculés. Ce dont elle était témoin était une mêlée désordonnée. Elle ne savait même pas qui était allié tant le combat était brouillon.

Soudain, elle distingua un reflet métallique sortir de la manche de l'un des belligérants. Il planta sa lame dans le cœur d'un autre qui hoqueta de surprise avant de s'effondrer. Les règles ayant changé, d'autres armes sortirent de leur fourreau. Une deuxième mêlée s'engagea, bien plus meurtrière, jusqu'à ce que la tension escalade à nouveau quand une autre ombre révéla une arme encore plus mortelle et tira une balle dans la tête du premier meurtrier.

C'en était trop pour Naoya. Elle quitta la scène en courant, des larmes aux yeux. Elle espérait que personne ne l'avait vu. Elle peinait à enregistrer ce qu'elle avait vu. Pourquoi des gens se battaient-ils dans un lieu aussi paradisiaque ? Même à la surface, où les conditions étaient bien plus dures, l'entraide était le maître mot et les disputes étaient toujours réglées par les poings. Elle n'avait jamais vu un homme en tuer un autre de sa vie, et ne s'était certainement pas attendue à ce que ce soit dans les niveaux inférieurs, de tous les endroits, où elle vivrait cette expérience pour la première fois.

Le voile merveilleux semblait s'être levé sur le Silo. Elle décelait maintenant les armes à peine dissimulée dans tous les pantalons. Couteaux et armes à feu faisaient partie de la panoplie de l'habitant du Silo, au même titre que les cheveux colorés et les piercings fantaisistes. Elle captait les regards, ce qu'elle prenait pour du dédain ou du désintérêt était en réalité de la méfiance, chacun jaugeant les autres passants, la main jamais loin de leur arme. Les graffitis qu'elle ne prenait auparavant pas le temps de lire n'étaient pas de simples dessins mais des mots d'insulte, de menace ou des représentations obscènes.

Son rêve était en train de s'effriter sous ses yeux. Elle se raccrochait cependant à un espoir : peut-être que le niveau 18 était une exception dans tout le Silo. Après tout, le district 31 avait la rumeur d'être très mal famé et le repaire de plusieurs groupes hors-la-loi. Elle se doutait que les affaires entre Almadeus et Sid n'étaient pas des plus légales, c'était donc logique de se retrouver dans un endroit sans foi ni loi. Oui, c'était sans doute ça.

Ragaillardie, elle redressa la tête et le reconnu. Adossé contre un mur, il avait troqué son tricorne avec un chapeau fedora, mais elle pouvait reconnaître ce long manteau de cuir entre mille. Elle l'avait enfin trouvé. En plus d'accomplir sa mission, elle pourrait lui raconter ce qu'elle avait vu et lui demander si c'était quelque chose de courant dans le Silo. Il s'excuserait probablement de l'avoir fait venir ici, dans le pire étage de tout le puits.

« Hé, Sid ! cria-t-elle joyeusement depuis l'autre bout de l'avenue. »

En entendant son nom, le concerné tourna la tête vers elle et devint aussitôt pâle comme un linge. Une expression de terreur pure traversa son visage comme un silence de mort plana sur la rue. Même la musique s'interrompit le temps d'un ange ne passe. Puis une rumeur monta, d'abord des murmures puis le bruit de dizaines d'armes dont on retirait le cran de sécurité. Déstabilisée par la tournure des évènements, Naoya s'arrêta et baissa lentement la main qu'elle avait levée pour saluer son ami, qui décampa.

Il lui tourna le dos et commença à courir comme si sa vie en dépendait. C'était le cas, mais il ne fut pas assez rapide. Le tonnerre retentit dans le couloirs trop étroit pour contenir son éclat et le corps de Sid s'effondra, foudroyé par une nuée de balle provenant d'une trentaine d'armes différentes. Naoya émit un cri de stupeur qui fut noyé par celui de la décharge. Le corps de son ami n'était même pas tombé au sol qu'une deuxième salve le balaya, réduisant son manteau en charpie.

Le corps de Naoya réagit plus vite que son esprit tétanisé. Elle se jeta dans une ruelle, derrière la couverture d'une benne à ordure. Là, elle se recroquevilla et étouffa ses pleurs pour faire le moins de bruit possible. Dans la rue, l'écho des deux déflagrations s'éloignait au loin. Une grosse voix s'éleva pour couvrir ce qu'il restait du boucan.

« Quelqu'un a vu qui l'a appelé ? 

— Moi, répondit une voix aiguë à peine audible, c'était une gamine de la surface. Elle s'est sûrement barrée vers la plateforme !

— Faut qu'on la choppe les gars, venez ! »

Une clameur triomphante résonna après son annonce, et des dizaines de bottes battirent le pavé noir en direction de son seul moyen de sortie. La jeune fille ramena encore plus ses genoux au niveau de son menton, jusqu'à ce que les deux se touchent.

Son rêve était définitivement brisé. Le Silo n'avait rien à voir avec ce qu'elle s'était imaginée, elle en avait maintenant la preuve. Des dizaines de personnes la traquaient au travers d'une ville labyrinthique dont elle était étrangère, probablement pour la tuer à son tour. Elle ne pouvait pas prendre la prochaine plateforme, ni même la suivante. Almadeus ne lui pardonnerait jamais, peut-être qu'il allait envoyer des gens à sa poursuite aussi. Et pire que tout, elle avait la conviction que c'était sa faute si Sid était mort.

Une fois que le fracas de ses poursuivants s'éloigna, elle se laissa à pleurer.