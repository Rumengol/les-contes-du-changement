---
bg: "jour-de-paye"
layout: post
title: Jour de paye
summary: C'est jour de paye pour Kellic. [fanfiction Star Wars]
source: “Bounty Hunter Kellic”, par Gus Mendonca
ref: https://www.brushonfire.com/star-wars-1
date: 2020-02-03
categories: posts
tags: ["Fanfictions"]
type: Conte
author: Rumengol
lang: fr
---

Cette planète puait. Non que Kellic pouvait la sentir, cette partie de son corps étant depuis longtemps cybernétisée, mais l'odeur était presque tangible. La cité basse de Taris n'avait jamais brillé par son hygiène, mais à l'approche des bas-fonds, c'était pire que tout. Fallait-il que le snivvien soit désespéré et que Kellic ait vraiment besoin de cette prime pour que tous deux s'aventurent dans un tel endroit.

Les pouilleux d'en bas crevaient la gueule ouverte et étaient prêts à tout pour quelques crédits. Kellic devait probablement son immunité à son apparence robotique et son attirail qui, s'il valait une petite fortune, garantissait également une mort éclair à quiconque le reluquerait avec insistance. Le snivvien était une petite frappe de la cité moyenne, qui devait avoir fourré son nez où il n'aurait pas dû et était maintenant en fuite. Sa survie ici relevait déjà du miracle. Et Kellic détestait les petits chanceux.

Le fuyard se savait sûrement poursuivi, et il avait déployé des trésors d'ingéniosité pour semer d'éventuels poursuivants. Malheureusement pour lui, Kellic n'était pas de ceux que de petites ruses pouvaient rouler. Il voyait à travers les fausses pistes et les voyous engagés pour lui barrer la route firent à peine diversion. Le problème venait de la fin de la piste, qui s'enfonçait dans les bas-fonds. C'était pas idiot, quand on était pourchassé sans moyen légal ou illégal pour quitter la planète, aller dans l'endroit le plus dangereux était cohérent, la plupart des chasseurs de primes rebrousseraient chemin.

Mais pas Kellic. Ce n'était même pas une question d'honneur, simplement qui il était. Les bas-fonds n'étaient pas aussi fréquentés par la vermine qu'il s'y attendait. La peur du snivvien illuminait l'air d'une façon que seuls les capteurs du droïde pouvaient percevoir. Le chasseur de primes se rapprochait, sa proie n'avait plus que quelques heures d'avance, qui diminuait rapidement. Cependant, quelque chose n'allait pas. Plutôt que de se diriger vers les communautés de marginaux éparpillés dans ce dépotoir, ses traces le conduisaient droit dans le territoire des mutants...

Évidemment, cet imbécile n'avait pas tenu une seconde sans se faire remarquer, et à présent il se faisait dévorer par une horde d'abominations... Au moins, il était mort. Ses employeurs seraient satisfaits. Et ils auraient intérêt à le payer. Il enregistra la scène et s'extirpa des souterrains putrides. Un nouveau jour, une nouvelle mission accomplie. Quoique celle-ci lui laissait un arrière goût aigre dans la bouche. Façon de parler. Il n'avait pas de bouche.
