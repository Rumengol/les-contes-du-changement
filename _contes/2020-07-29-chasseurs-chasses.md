---
bg: "chasseurs-chasses"
layout: post
title: Chasseurs chassés
summary: Un groupe de chasseurs est engagés pour traquer une bête responsable de la destruction d'un village.
source: “Cold Forest”, par Llia Yu
ref: https://www.artstation.com/artwork/vD1KY
date: 2020-01-28
categories: posts
tags: ["Le monde d'Alkénor"]
type: Conte
author: Rumengol
lang: fr
---

"Abattez-la ! vociférait Ulrienne"

Mais c'était bien plus facile à dire qu'à faire, et le groupe de chasseurs n'avait assurément pas les capacités de vaincre une créature pareille. Lorsqu'ils avaient été engagés pour traquer la ou les bêtes qui avaient détruit Halfebourd, ils s'étaient attendus à une meute de lycan, ou un gros Ourserune qui aurait massacré les habitants. Pas à ce que le village soit littéralement détruit, au point qu'il n'en reste que les fondations. Déjà là, Rohirim et Thorfen avaient fait part de leurs inquiétudes, mais Ulrienne les avait balayées d'un revers de main. Ils avaient été engagés pour une chasse, alors ils allaient chasser.

Elle était l'aînée des chasseurs, leur cheffe, et les protestations avaient cessé. Ils suivirent la piste évidente laissée par la source de cette destruction. Les empreintes larges et les troncs déracinés rendaient le travail de Tal'Sen inutile. Il essaya de faire bonne figure en étudiant les traces de pas pour déterminer leur proie, là encore en vain.

"Des empreintes comme ça, je dirais rhinocéros ou éléphant, mais jamais de cette taille. Ce truc doit faire au moins la taille d'un oliphant, mais c'en est pas un. Jamais vu un truc pareil..."

Plus informés et moins rassurés, ils s'étaient avancés dans le bois gelé. La taille de la bête ne représentait que des avantages, du moins pour la traque : Elle était impossible à perdre, et lente. Là où Tal'Sen estimait leur retard à une vingtaine de jours, ils le comblèrent en sept. Quand ils furent suffisamment proches pour entendre les pas lourds de la créature, Ulrienne déclara une halte et envoya Dramick et Sylvienne en éclaireurs pour leur collecter des informations. En attendant, chacun fourbissait son attirail, enduisant leurs lames d'huiles diverses, lorsque le rythme régulier de la créature s'accéléra soudain, et se rapprocha d'eux.

Ils n'eurent pas l'occasion de comprendre pourquoi un torrent de langues acérées venait de surgir dans la clairière avant que l'une d'entre elles ne s'empare d'Ulrienne et la soulève. Rapidement, mais pas suffisamment, tous les chasseurs se saisirent de leurs armes. L'une d'elles, étonnamment véloce, enserra Tolvak qui tentait de la taillader. Une autre projeta Aria contre un arbre. Le craquement de ses os eut un écho sinistre dans le bois. Tal'Sen eut tout juste le temps de décocher un trait en direction de l'ouverture infernale qui scindait la corne de la créature avant de se jeter hors de portée d'une pointe qui visait sa tête. Il se releva avec quelques égratignures, mais en vie. D'autres n'avaient pas eu cette chance.

L'attaque avait été fulgurante. Thorfen gisait au sol et se desséchait déjà, transpercé par une langue qui semblait à présent aspirer son sang. Ulrienne avait cessé de se débattre, et son corps sans vie était doucement ramené vers la fente buccale de la bête. Olfren était cloué à un tronc à plusieurs mètres du sol. La charge de l'animal avait dissipé tout espoir de Tolvak de se libérer, l'empalant sur sa corne frontale. Les yeux fous, Tal'Sen se réfugia derrière un tronc d'arbre. En se tournant, il vit Rohirim qui le fixait depuis un autre abri avec ce même regard de terreur, sa hache tremblant dans ses mains.

"Mais qu'est-ce que c'est que cette bête ?"

"Une... Hypercérafos. C'est un démon, rarissime, aucun croquis de chasse n'en mentionnait ! "

"Je commence à comprendre pourquoi ! Aucun des chasseurs qui l'ont rencontré n'ont survécu !"

"Se... Selon les anciens écrits, on peut la vaincre, il devrait suffire de AHHHHH"

Deux langues avaient encerclé en un éclair l'arbre derrière lequel s'était réfugié Tal'Sen, et venaient de le tirer violemment, décapitant le tronc du même geste. Rohirim se releva prestement. Ils n'étaient plus que trois en vie. Deux. Il était seul. Il se retourna et courut autant qu'il put, tentant de faire le moins de bruit possible maintenant que les derniers râles d'agonie s'éteignaient. Il hurla en sentant la pointe s'enfoncer dans son dos. Dans son dernier souffle, il se demanda si la piste si évidente était une manœuvre démoniaque pour les mettre en confiance, les pousser au relâchement. Si depuis le début, ce n'était pas eux qui étaient chassés.
